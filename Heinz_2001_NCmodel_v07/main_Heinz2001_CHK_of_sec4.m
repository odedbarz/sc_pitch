%
% main_JND_Heinz2001_sec4.m
% --------------------------
% 
% To be use with the ifr vector of Heinz program
% 

%% Create the two input tones for the JND simulation:

L_dB = 43; %62.98;              	% [dB SPL]
L_Pa = 20e-6*10^(L_dB/20);          % [Pa]

T_ms        = 50;                   % [msec] total input time in mili-seconds
T_s         = 1e-3*T_ms;            % [msec] --> [sec] total input time in seconds
dT_s        = 20e-6;                % [sec] time resolution
N_time_res  = floor(T_s/dT_s);      % [] resolution 
t_ms_v      = linspace(0, T_s, N_time_res)';    % [sec vector] the time line for the input signal 

tone_freq       = 500;             % [Hz] tone input frequency
tone_delta_freq = 0.1e-3;          % [Hz] tone input (JND) delta frequency

min_frq_filter = 500;          % [Hz] Lowest frequency filter
max_frq_filter = 2000;         % [Hz] Lowest frequency filter

% # of Filters in Bank:
filter_banks = 5;

% define the different filenames:
base_filename       = sprintf('tone_%gHz', tone_freq);
info_filename       = [base_filename, '_info'];       % input signal file name
info_plus_filename  = [base_filename, '_plus_info'];       % input signal file name
sig_filename        = sprintf('%s_v', base_filename);
sig_plus_filename   = sprintf('%s_plus_v', base_filename);

% save the info data into the input file:
Write_Info(...
    info_filename,...
    min_frq_filter,...
    max_frq_filter,...
    filter_banks,...
    sig_filename,...
    dT_s,...
    T_ms...
);

% save the info data into the input file:
Write_Info(...
    info_plus_filename,...
    min_frq_filter,...
    max_frq_filter,...
    filter_banks,...
    sig_plus_filename,...
    dT_s,...
    T_ms...
);


%% Create the input signal vector:
tone_sig_v = Write_Sig( sig_filename, tone_freq, N_time_res, L_Pa, t_ms_v );
tone_sig_plus_v = Write_Sig( sig_plus_filename, tone_freq + tone_delta_freq, N_time_res, L_Pa, t_ms_v );


%% Simulate using Heinz (2001) C++ code:
dos( sprintf('anmodheinz00 < %s', info_filename) );

load ifr.dat
ifr_1 = ifr';

% sin(2*pi*(f+df)*t):
dos( sprintf('anmodheinz00 < %s', info_plus_filename) );

load ifr.dat
ifr_1_plus = ifr';

clc

[stimpts, Nchs] = size( ifr_1 );
stimpts = stimpts - 1;
Nchs    = Nchs - 1;

time    = ifr_1( 2:stimpts + 1, 1 );
ANcfs   = ifr_1( 1, 2:Nchs + 1 );

ifr_1       = ifr_1( 2:stimpts+1, 2:Nchs+1 );
ifr_1_plus  = ifr_1_plus( 2:stimpts+1, 2:Nchs+1 );


ch_n    = 1;    % Channel Number
fprintf('\n=> ch_n = %d\n=> ANcfs = %d [Hz]\n', ch_n, ANcfs(ch_n));
ir_dc   = 7;    % [spikes/sec] add a small DC to the IR to avoid a zero IR.
ifr_1      = ir_dc + ifr_1;
ifr_1_plus = ir_dc + ifr_1_plus;

% Frequency derivation:
d_ir_freq = Approx_Derivative( ifr_1_plus(:, ch_n), ifr_1(:, ch_n), tone_delta_freq );

%%
figure(10)

subplot(3,1,1)
plot( time, 1e3*tone_sig_v )
ylabel('Amp [mPa]')
title(sprintf('Stimuli (L = %d [dB SPL])', L_dB))

subplot(3,1,2)
plot( time, [ifr_1(:, ch_n), ifr_1_plus(:, ch_n)] )
ylabel('Rate [sp/sec]')
legend('r_i(t,f+\Deltaf)', 'r_i(t,f)')
title(sprintf('r_i(t,f+\\Deltaf) & r_i(t,f) (CF = %d [Hz])', ANcfs(ch_n)))

subplot(3,1,3)
plot( time, d_ir_freq.^2 )
ylabel('[sp/sec/Hz]^2')
title('Approx. Partial Derivative Squered')
xlabel('Time [msec]')



%% Delta_alpha_JND:

% y = D_alpha_JND( ifr_1_plus, ifr_1, deriv_delta, ANcfs );












