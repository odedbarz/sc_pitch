% 
% Eq. (3,1), from Heinz, 2001
% 

function [alpha_JND_AI, alpha_JND_RP] = D_alpha_JND( ifr_plus, ifr, d_alpha, time_st )
%
% RP: Rate-Place coding
% AI: All-Information coding
%

T_win = 1e-3*time_st.T_ms;
M_ifr = size(ifr, 1);

%
% ifr_avg = mean( ifr );
% d_ir_freq_avg = mean( Approx_Derivative( ifr_plus, ifr, d_alpha ) );
% FI_RP = sum( T_win./ifr_avg .* d_ir_freq_avg.^2 );

ifr_avg = mean( ifr );
ifr_plus_avg = mean( ifr_plus );
FI_RP = sum(T_win* 1./ifr_avg .* ( (ifr_plus_avg - ifr_avg)/d_alpha ).^2 );
alpha_JND_RP = sqrt( 1./FI_RP );


% d_ir_freq = Approx_Derivative( ifr_plus, ifr, d_alpha );
FI_AI = sum( trapz( time_st.time_sec_v, 1./ifr .* ((ifr_plus - ifr)/d_alpha).^2 ) );      
alpha_JND_AI = sqrt( 1./FI_AI );





















