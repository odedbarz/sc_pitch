%
% main_Heinz_v02.m
% --------------------------
% 
% To be use with the ifr vector of Heinz program
% 

clc

%% Create the two input tones for the JND simulation:

L_dB = 40; %62.98;              	% [dB SPL]
L_Pa = 20e-6*10^(L_dB/20);          % [Pa]

T_RF_ms     = 20;                   % [msec] rais\fall time
T_sig_ms    = 200; % + T_RF_ms;        % [msec] signal time, without zero padding & raise\fall time 
T_pad_ms    = 25;                   % [msec] 
T_ms        = 2*T_RF_ms + T_sig_ms + T_pad_ms;	% [msec] total input time in mili-seconds

T_s         = 1e-3*T_ms;            % [msec] --> [sec] total input time in seconds
smp_freq    = 5* 100e3;                % [Hz] Sampling frequency 
dT_s        = 1/smp_freq;           % [sec] time resolution
d_tone_f    = 1e-4;                 % [Hz] different in frequency for the derivative approximation.

%
N_sig_smp   = round( 1e-3*T_sig_ms/ dT_s ); % # of (full) signal samples 
N_RF_smp    = round( 1e-3*T_RF_ms/ dT_s );  % # of delay samples
N_pad_smp   = round( 1e-3*T_pad_ms/ dT_s ); % # of zero padding samples
N_tot_vec   = round( T_s/ dT_s );           % total # of samples of the input stimulus

time_sec_v  = linspace(0, T_s, N_tot_vec)';    % [sec vector] the time line for the input signal 

% add a small DC to the IR to avoid the zeros
ir_dc       = 7;   % [spikes/sec]  


% create time structure:
time_st.T_ms        = T_ms;
time_st.dT_s        = dT_s;
time_st.T_sig_ms    = T_sig_ms;
time_st.T_pad_ms    = T_pad_ms;
time_st.T_RF_ms     = T_RF_ms;
time_st.N_sig_smp   = N_sig_smp;
time_st.N_RF_smp    = N_RF_smp;
time_st.N_pad_smp   = N_pad_smp;
time_st.N_tot_vec   = N_tot_vec;
time_st.time_sec_v  = time_sec_v;

min_frq_filter = 100;	% [Hz] Lowest frequency filter
max_frq_filter = 10e3;	% [Hz] Lowest frequency filter

% # of Filters in Bank:
N_filter_banks = 60;

% The actual AN indexes to calc for the JND:
% ANind_2_calc = unique([[1:6:60],60]);    %1:5:N_filter_banks;
ANind_2_calc = [1:10:50, 55:2:60, 60];    %1:5:N_filter_banks;


% define the different filenames:
filename_st.current_dir         = sprintf('%s\\', cd);
filename_st.work_dir            = sprintf('%s%s', cd, '\work\');
filename_st.base_filename       = 'tone';
filename_st.info_filename       = [filename_st.base_filename, '_info.txt'];       % input signal file name
filename_st.info_plus_filename  = [filename_st.base_filename, '_plus_info.txt'];       % input signal file name
filename_st.sig_filename        = sprintf('%s_sig_v.dat', filename_st.base_filename);
filename_st.sig_plus_filename   = sprintf('%s_sig_plus_v.dat', filename_st.base_filename);


% Set the number of channels (according to Heinz paper):
x_low_cf = Map_cf2x( min_frq_filter );
x_high_cf = Map_cf2x( max_frq_filter );
dx_mm = ( x_high_cf - x_low_cf )/( N_filter_banks -1 );

ANcfs = Map_x2cf( x_low_cf + dx_mm*[0:N_filter_banks-1] );


dummy_str = '-';
dummy_str = dummy_str(ones(1, 100));

%% save the info data into the input file:

Write_Info(...
    filename_st,...
    min_frq_filter,...
    max_frq_filter,...
    N_filter_banks,...
    dT_s,...
    T_ms...
);

    
%% Start simulation with Heinz, 2001 C++ engine:

jnd_c = 0;

% Init the output vector:
alpha_JND_AI = nan(length(ANind_2_calc), 1);
alpha_JND_PR = nan(length(ANind_2_calc), 1);
Weber_AI     = nan(length(ANind_2_calc), 1);
Weber_PR     = nan(length(ANind_2_calc), 1);


    debug_derv = nan(length(ANind_2_calc), 1);
    

fprintf('\nStarting main <for> loop:\n');

for i = ANind_2_calc
        
    tone_freq = ANcfs(i);      % [Hz] (AVAILABLE) tone input frequency
    
    fprintf('\n\n%s\n', dummy_str)    
    fprintf('\t tone_freq = %g\n', tone_freq); 
    fprintf('%s\n', dummy_str)
    
    % Create the input signal vector:
    tone_sig_v = Write_Sig(...
                    [filename_st.work_dir, filename_st.sig_filename],...
                    tone_freq,...
                    L_Pa,...
                    time_st...
                );
    
    % Create the input signal + delta vector:
    Write_Sig(...
        [filename_st.work_dir, filename_st.sig_plus_filename],...
        tone_freq + d_tone_f,...
        L_Pa,...
        time_st...
    );

    fprintf('=> Run Heinz2001 C++ code (first tone with <f>)\n');  
    
    % Simulate using Heinz (2001) C++ code:
    Run_Heinz2001( filename_st, filename_st.info_filename );

    fprintf('\n')
    
    load( sprintf( '%sifr.dat', filename_st.work_dir ));
    ifr_i = ifr';

    fprintf('=> Run Heinz2001 C++ code (second tone with <f+df>)\n');     
    
    % sin(2*pi*(f+df)*t):
    Run_Heinz2001( filename_st, filename_st.info_plus_filename );
    
    fprintf('\n')
    
    load( sprintf( '%sifr.dat', filename_st.work_dir ));
    ifr_i_plus = ifr';

    %clc    
    
    %
    [stimpts, Nchs] = size( ifr_i );
    stimpts = stimpts - 1;
    Nchs    = Nchs - 1;

    if (Nchs ~= N_filter_banks)
        error( sprintf('\nERROR:\n\t-> Nchs ~= N_filter_banks\n\n' ) );
    end
    
    %ANcfs   = ifr_i( 1, 2:Nchs + 1 );
    if ~isequal(ANcfs, ifr_i( 1, 2:Nchs + 1 ) )
        error( sprintf('\nERROR:\n\t-> ANcfs ~= ifr_i( 1, 2:Nchs + 1 )!\n\n' ) );
    end
    
    % simulation time vector (in milisecond):
    time_msec_v = ifr_i( 2:stimpts + 1, 1 );
    if length(time_msec_v) ~= length(time_sec_v)
        error(...
            sprintf(...
                '\n\n%s\n\t%s == %d\n\t%s == %d\n\n',...
                '* length(time_msec_v) from loaded C++ simulator ~= length(time_sec_v):',...
                '- length(time_msec_v)',...
                length(time_msec_v),...
                '- length(time_sec_v)',...
                length(time_sec_v) ...
            )...
        );
    end

    ifr_i      = ifr_i( 2:stimpts+1, 2:Nchs+1 );
    ifr_i      = ir_dc + ifr_i;
    
    ifr_i_plus = ifr_i_plus( 2:stimpts+1, 2:Nchs+1 );
    ifr_i_plus = ir_dc + ifr_i_plus;
    
    
    % --------------
    if 1
        figure(10)
        % loglog( ANcfs(ANind_2_calc), d_tone_f./[Weber_AI, Weber_PR], '.--', 'markersize', 16 )
        loglog( ANcfs(ANind_2_calc), [Weber_PR./ANcfs(ANind_2_calc)', Weber_AI./ANcfs(ANind_2_calc)'], '.--', 'markersize', 16 )
        ylabel('\Deltaf/f');
        xlabel('[Hz]');
        
        drawnow;
    end
    
    
    jnd_c = jnd_c+1;
    
    % Calc JND:
%     [alpha_JND_AI(jnd_c), alpha_JND_PR(jnd_c)] =... 
%         D_alpha_JND( ifr_i_plus, ifr_i, d_tone_f, ANcfs, time_st ); 
    [alpha_JND_AI(jnd_c), alpha_JND_PR(jnd_c)] = D_alpha_JND( ifr_i_plus, ifr_i, d_tone_f, time_st ); 
    
    Weber_AI(jnd_c) = alpha_JND_AI(jnd_c)/tone_freq;
    Weber_PR(jnd_c) = alpha_JND_PR(jnd_c)/tone_freq;
%     Weber_AI(jnd_c) = alpha_JND_AI(jnd_c);
%     Weber_PR(jnd_c) = alpha_JND_PR(jnd_c);
    
    figure(10)
    loglog( ANcfs(ANind_2_calc), [Weber_PR, Weber_AI], '.--', 'markersize', 16 )
    drawnow;

        % DEBUG:
        debug_derv(jnd_c) = norm( Approx_Derivative( ifr_i_plus, ifr_i, d_tone_f ) );
    
end



    
%% Plot the results

figure(10)
% loglog( ANcfs(ANind_2_calc), d_tone_f./[Weber_AI, Weber_PR], '.--', 'markersize', 16 )
% loglog( ANcfs(ANind_2_calc), [Weber_PR./ANcfs(ANind_2_calc)', Weber_AI./ANcfs(ANind_2_calc)'], '.--', 'markersize', 16 )
loglog( ANcfs(ANind_2_calc), [Weber_PR, Weber_AI], '.--', 'markersize', 16 )

ylabel('\Deltaf/f')
xlabel('[Hz]')

% figure(11)
% loglog( time_sec_v, [alpha_JND_AI, alpha_JND_PR], '.--' )
% xlabel('[msec]')

% figure(12)
% GWood_fn = @(x) 165.4*(10.^(0.06*x) - 0.88);
% xxx = linspace(35, 0, 60)';
% loglog(xxx, GWood_fn(xxx))








