%
% main_Heinz_v02.m
% --------------------------
% 
% To be use with the ifr vector of Heinz program
% 

clc


%% Create the two input tones for the JND simulation:

L_dB_v = [20, 40 60];                 % [dB SPL]
len_L_dB = length(L_dB_v);

%
T_RF_ms     = 20;                   % [msec] rais\fall time
T_sig_ms    = 480 + T_RF_ms;        % [msec] signal time, without zero padding & raise\fall time 
T_pad_ms    = 25;                   % [msec] 
T_ms        = 2*T_RF_ms + T_sig_ms + T_pad_ms;	% [msec] total input time in mili-seconds
T_s         = 1e-3*T_ms;            % [msec] --> [sec] total input time in seconds
smp_freq    = 100e3;                % [Hz] Sampling frequency 
dT_s        = 1/smp_freq;           % [sec] time resolution
dL          = 1e-4;                 % [Hz] different in frequency for the derivative approximation.

%
N_sig_smp   = round( 1e-3*T_sig_ms/ dT_s ); % # of (full) signal samples 
N_RF_smp    = round( 1e-3*T_RF_ms/ dT_s );  % # of delay samples
N_pad_smp   = round( 1e-3*T_pad_ms/ dT_s ); % # of zero padding samples
N_tot_vec   = round( T_s/ dT_s );           % total # of samples of the input stimulus

time_sec_v  = linspace(0, T_s, N_tot_vec)';    % [sec vector] the time line for the input signal 

% add a small DC to the IR to avoid the zeros
ir_dc       = 7;   % [spikes/sec]  


% create time structure:
time_st.T_ms        = T_ms;
time_st.dT_s        = dT_s;
time_st.T_sig_ms    = T_sig_ms;
time_st.T_pad_ms    = T_pad_ms;
time_st.T_RF_ms     = T_RF_ms;
time_st.N_sig_smp   = N_sig_smp;
time_st.N_RF_smp    = N_RF_smp;
time_st.N_pad_smp   = N_pad_smp;
time_st.N_tot_vec   = N_tot_vec;
time_st.time_sec_v  = time_sec_v;

min_frq_filter = 100;	% [Hz] Lowest frequency filter
max_frq_filter = 10e3;	% [Hz] Lowest frequency filter

% # of Filters in Bank:
N_filter_banks = 60;


% define the different filenames:
filename_st.current_dir         = sprintf('%s\\', cd);
filename_st.work_dir            = sprintf('%s%s', cd, '\work\');
filename_st.base_filename       = 'tone';
filename_st.info_filename       = [filename_st.base_filename, '_info.txt'];       % input signal file name
filename_st.info_plus_filename  = [filename_st.base_filename, '_plus_info.txt'];       % input signal file name
filename_st.sig_filename        = sprintf('%s_sig_v.dat', filename_st.base_filename);
filename_st.sig_plus_filename   = sprintf('%s_sig_plus_v.dat', filename_st.base_filename);


% Set the number of channels (according to Heinz paper):
x_low_cf    = Map_cf2x( min_frq_filter );
x_high_cf   = Map_cf2x( max_frq_filter );
dx_mm       = ( x_high_cf - x_low_cf )/( N_filter_banks -1 );
ANcfs       = Map_x2cf( x_low_cf + dx_mm*[0:N_filter_banks-1] );

tone_freq   = ANcfs(25);

dummy_str = '-';
dummy_str = dummy_str( ones(1, 50) );

%% save the info data into the input file:

Write_Info(...
    filename_st,...
    min_frq_filter,...
    max_frq_filter,...
    N_filter_banks,...
    dT_s,...
    T_ms...
);


%% Start simulation with Heinz, 2001 C++ engine:

jnd_c = 0;

% Init the output vector:
df_ai = zeros( len_L_dB, 1 );
df_pr = zeros( len_L_dB, 1 );

fprintf('\nStarting main <for> loop:\n');

for i = 1:len_L_dB
        
    %
    L_Pa_i = 20e-6*10^( L_dB_v(i)/20 );          % [Pa]
        
    fprintf( '\n\n%s\n', dummy_str )    
    fprintf( '\t- tone_freq = %g [Hz]\n', tone_freq ); 
    fprintf( '\t- L_Pa_i = %g [dB SPL]\n', L_dB_v(i) ); 
    fprintf( '\t- L_Pa_i = %g [Pa]\n', L_Pa_i ); 
    fprintf( '%s\n', dummy_str )
    
    % Create the input signal vector:
    tone_sig_v = Write_Sig(...
                    [filename_st.work_dir, filename_st.sig_filename],...
                    tone_freq,...
                    L_Pa_i,...
                    time_st...
                );
    
    % Create the input signal + delta vector:
    Write_Sig(...
        [filename_st.work_dir, filename_st.sig_plus_filename],...
        tone_freq + dL,...
        L_Pa_i,...
        time_st...
    );

    fprintf('=> Run Heinz2001 C++ code (first tone with <f>)\n');  
    
    % Simulate using Heinz (2001) C++ code:
    Run_Heinz2001( filename_st, filename_st.info_filename );

    fprintf('\n')
    
    load( sprintf( '%sifr.dat', filename_st.work_dir ));
    ifr_i = ifr';

    fprintf('=> Run Heinz2001 C++ code (second tone with <f+df>)\n');     
    
    % sin(2*pi*(f+df)*t):
    Run_Heinz2001( filename_st, filename_st.info_plus_filename );
    
    fprintf('\n')
    
    load( sprintf( '%sifr.dat', filename_st.work_dir ));
    ifr_i_plus = ifr';
    
    %
    [stimpts, Nchs] = size( ifr_i );
    stimpts = stimpts -1;
    Nchs    = Nchs -1;

    if (Nchs ~= N_filter_banks)
        error( sprintf('\nERROR:\n\t-> Nchs ~= N_filter_banks\n\n' ) );
    end
    
    ANcfs   = ifr_i( 1, 2:Nchs + 1 );
    if ~isequal(ANcfs, ifr_i( 1, 2:Nchs + 1 ) )
        error( sprintf('\nERROR:\n\t-> ANcfs ~= ifr_i( 1, 2:Nchs + 1 )!\n\n' ) );
    end
    
    % simulation time vector (in milisecond):
    time_msec_v = ifr_i( 2:stimpts + 1, 1 );
    if length(time_msec_v) ~= length(time_sec_v)
        error(...
            sprintf(...
                '\n\n%s\n\t%s == %d\n\t%s == %d\n\n',...
                '* length(time_msec_v) from loaded C++ simulator ~= length(time_sec_v):',...
                '- length(time_msec_v)',...
                length(time_msec_v),...
                '- length(time_sec_v)',...
                length(time_sec_v) ...
            )...
        );
    end

    ifr_i      = ifr_i( 2:stimpts+1, 2:Nchs +1 );
    ifr_i      = ir_dc + ifr_i;
    
    ifr_i_plus = ifr_i_plus( 2:stimpts+1, 2:Nchs +1 );
    ifr_i_plus = ir_dc + ifr_i_plus;
    
    
    % --------------
    if 0
        figure(99)

        ch_n = i;    % AN channel number
        
        subplot(3,1,1)
        plot( time_msec_v, 1e3*tone_sig_v )
        ylabel('Amp [mPa]')
        title(sprintf('Stimuli (L = %d [dB SPL])', L_dB))

        subplot(3,1,2)
        plot( time_msec_v, [ifr_i(:, ch_n), ifr_i_plus(:, ch_n)] )
        ylabel('Rate [sp/sec]')
        legend('r_i(t,f+\Deltaf)', 'r_i(t,f)')
        title(sprintf('r_i(t,f+\\Deltaf) & r_i(t,f) (CF = %d [Hz])', ANcfs(ch_n)))

        
        d_ir_freq = Approx_Derivative( ifr_i_plus(:, ch_n), ifr_i(:, ch_n), dL );   % freq. derivation 
        
        subplot(3,1,3)
        plot( time_msec_v, d_ir_freq.^2 )
        ylabel('[sp/sec/Hz]^2')
        title('Approx. Partial Derivative Squered')
        xlabel('Time [msec]')
    end
    
    
    jnd_c = jnd_c +1;
    
    % Calc JND:
    [df_ai(jnd_c), df_pr(jnd_c)] =... 
        D_alpha_JND( ifr_i_plus, ifr_i, dL, time_st ); 
   
    
end



    
%% Plot the results

% heinz2001_factor = 0.04;

figure(10)
semilogy( L_dB_v, [df_ai, df_pr], '.--' )
ylabel('\Deltaf')
xlabel('[Hz]')









