% 
% Write the input signal vector into the hard-disk
% 

function tone_sig_v = Write_Sig( sig_filename, tone_freq, L_Pa, time_st, debug_mode )

if ~exist('debug_mode', 'var')
    debug_mode = 0;
end

% Create the input signal (tone) vector:
tone_sig_v = L_Pa.* sin( 2*pi*tone_freq .* time_st.time_sec_v );

% Create the raise\fall windows:
win_raise_v = 0.5* cos( 2*pi/2*[0:time_st.N_RF_smp-1]'/time_st.N_RF_smp -pi ) +0.5;
win_fall_v  = 0.5* cos( 2*pi/2*[0:time_st.N_RF_smp-1]'/time_st.N_RF_smp ) +0.5;

% Create a window in time:
% w = tukeywin( floor(0.5*time_st.N_time_res), 0.16);
% w = tukeywin( 2*N_RF_smp + , 0.16);

filt_v = zeros(time_st.N_tot_vec, 1);
filt_v(1:time_st.N_RF_smp) = win_raise_v;   % raise time
filt_v(time_st.N_RF_smp + [1:time_st.N_sig_smp]) = 1;   % signal time
filt_v(time_st.N_RF_smp + time_st.N_sig_smp + [1:time_st.N_RF_smp]) = win_fall_v;   % fall time

if 1 == debug_mode
    figure(99)
    plot(1e3*time_st.time_sec_v, tone_sig_v);
    hold on
    plot(1e3*time_st.time_sec_v, tone_sig_v.*filt_v, 'r', 'linewidth', 2);  
    plot(1e3*time_st.time_sec_v, L_Pa.*filt_v, 'k--', 'linewidth', 2);      
    hold off
end

tone_sig_v = tone_sig_v .* filt_v;

% Save signal input vector to the hard-disk:
save( sig_filename, 'tone_sig_v', '-ascii');








