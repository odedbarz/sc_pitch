% 
% Write initialization data into Heinz, 2001 file (intone)
% 

function Write_Info(...
        filename_st, min_freq, max_freq, N_filter_banks, dT_s, T_ms...
    )

% cd( filename_st.work_dir );

info_filename_full = [filename_st.work_dir, filename_st.info_filename];

file_id = fopen( info_filename_full, 'w' );

count = fprintf(file_id, '%d\n%g\n%g\n%d\n%s\n%g\n%g\n',...     % '%*f\n%*f\n%*f\n%*f\n%*s\n%f\n%f'
    1,...               Filterbank parameters: Set Center-filter freq [0], or Range of freqs [1]
    min_freq,...        Lowest frequency filter(Hz)
    max_freq,...        Highest frequency filter(Hz)
    N_filter_banks,...    # of Filters in Bank
    filename_st.sig_filename,...
    dT_s,...            Time Step Size in input file [secs]
    T_ms...             Input Duration of Simulation [msec]
);

fclose(file_id);

if 0 == count
    error(sprintf('\nERROR:\n\t* Function Write_Info():\n\t\tFailed to write info (using <fprintf>)!'));
end

% cd( filename_st.current_dir );

info_filename_full = [filename_st.work_dir, filename_st.info_plus_filename];

file_id = fopen( info_filename_full, 'w' );

count = fprintf(file_id, '%d\n%g\n%g\n%d\n%s\n%g\n%g\n',...     % '%*f\n%*f\n%*f\n%*f\n%*s\n%f\n%f'
    1,...               Filterbank parameters: Set Center-filter freq [0], or Range of freqs [1]
    min_freq,...        Lowest frequency filter(Hz)
    max_freq,...        Highest frequency filter(Hz)
    N_filter_banks,...  # of Filters in Bank
    filename_st.sig_plus_filename,...
    dT_s,...            Time Step Size in input file [secs]
    T_ms...             Input Duration of Simulation [msec]
);

fclose(file_id);

if 0 == count
    error(sprintf('\nERROR:\n\t* Function Write_Info():\n\t\tFailed to write info (using <fprintf>)!'));
end
