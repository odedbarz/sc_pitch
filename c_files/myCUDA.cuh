#ifndef MYCUDA_CUH
#define MYCUDA_CUH

#include "mainprog.h"

__global__ void x2r_K(partype* d_Vall, const partype* d_x, const size_t n_neurons);
void x2r_W(partype* d_Vall, const partype* d_x, const size_t n_neurons, dim3 blocks, dim3 threads);

#endif