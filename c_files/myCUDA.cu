//#ifndef MYCUDA_CU
//#define MYCUDA_CU

//#include "mainprog.cuh"
#include "myCUDA.cuh"

// Update Vall = [ r | z | u ] 
__global__ void x2r_K(partype* d_Vall, const partype* d_x, const size_t n_neurons)
{
	int tid =  threadIdx.x + blockIdx.x * blockDim.x;
	//TODO: !!! divergence !!! 
	if (tid < n_neurons)
		d_Vall[tid] = tanh( d_x[tid] );	// <--> this is d_r

	return;
}

// Wrapper: Update Vall = [ r | z | u ] 
void x2r_W(partype* d_Vall, const partype* d_x, const size_t n_neurons, dim3 blocks, dim3 threads)
{
	x2r_K<<<blocks,threads>>>( d_Vall, d_x, n_neurons );	// d_Vall <- phi(d_x)

	return;
}




//#endif