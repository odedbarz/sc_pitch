
//
// 16/06/2014:
// * The current algorithm performs RLS on ALL the RNN.
//
//
//
#ifndef MAINPROG_H
#define MAINPROG_H

#include <iostream>
#include <fstream>
#include <sstream>
//#include <vector>
#include <random>		// use different random distributions
#include <functional>	// for the std::bind 
#include <math.h>       // tanh 
//#include <sstream>
//#include <string>
#include <direct.h>

#include <Windows.h>	// need it for CreateProcess() 


// CUDA: 
//#pragma comment(lib, "cublas") // include cublas.lib 
#include <cuda_runtime.h>	// CUDA runtime
#include <cublas_v2.h>		// cuBLAS
#include <helper_cuda.h>
#include <helper_functions.h>	// Helper functions and utilities to work with CUDA
#include <device_launch_parameters.h>	// For VS Intellisence

//#define NVIDIA_GTX_590	// using the NVIDIA GTX 590 ?

//#define MAX_GRID_DIM_X	65535	// GTX 590
//#define THREADS_NUM		32		// GTX 590
#define MAX_GRID_DIM_X	65535	// GT 430
//#define THREADS_NUM		16		// GT 430

//#define PARTYPE_FLOAT
#define PARTYPE_DOUBLE

#ifdef PARTYPE_FLOAT
	#define partype float  
	#define cublasXgemv cublasSgemv
	#define cublasXdot	cublasSdot
	#define cublasXger	cublasSger
	#define cublasXaxpy cublasSaxpy
	#define cublasXgemm cublasSgemm
#else // PARTYPE_DOUBLE ?
	#define partype double 
	#define cublasXgemv cublasDgemv
	#define cublasXdot	cublasDdot
	#define cublasXger	cublasDger
	#define cublasXaxpy cublasDaxpy
	#define cublasXgemm cublasDgemm
#endif

const bool g_ispinned = false;	// global 

	// THRUST & DEBUG
	//#include <thrust/host_vector.h>
	//#include <thrust/device_vector.h>
	//typedef thrust::device_vector< partype > state_type;


#define TRACE2(f,l) printf("I am at file: " f " and line: " #l "\n")
#define TRACE1(f,l) TRACE2(f,l)
#define TRACE() TRACE1(__FILE__, __LINE__)

const int MAX_CHARS_PER_LINE = 512;
#define STR(x) #x		// par x to char*
#define POW2(x) x*x		// y = x^2

// main database of the program
struct database_st {

	std::string db_filename;	// input database file 
	std::string par_type;
	partype Fs;			// (Hz) input sampling rate 
	partype dt;			// simulation time steps
	partype tau;		// dynamics' time constant
	partype p;			// (Oded) density of the network 
	partype g;			// g greater than 1 leads to chaotic networks
	partype alpha;		// learning rate (alpha<<N for small error)
	partype scale;		// std of Jnet
	size_t N_rls;		// for the RLS learning: the width of the sub-net <--> size(P,1)
	bool training_flag; // training_flag == [0,1]: [testing, training] the net

	// network dynamics
	size_t N;			// # of neurons
	size_t N2;			// N^2
	size_t z_d1;		// # of dimensions for the feedback channel(s) zt
	size_t Jin_d1;
	size_t Jin_d2;		// # of input channels
	size_t Jin_tot;		// Jin_d1*Jin_d2: total # of elements in Jin
	size_t Jall_d1;		// total length of the concatenation of [ Jnet | Wf | Jin ]
	size_t Jall_d2;		// total length of the concatenation of [ Jnet | Wf | Jin ]
	size_t Jall_tot;	// total size of the overall dynamic matrix Jall = [ Jnet | Wf | Jin ]
	size_t Vall_d1;		
	size_t Vall_d2;		// total size of the overall dynamic vector Vall = [ r | z | u ]
	//size_t size_Jin;	// total size of Jin 
	size_t wf_tot;		// total size of wf
	//size_t wo_d1;		// dim1 of the output vector wo
	//size_t wo_d2;		// dim2 of the output vector wo
	size_t wo_tot;		// [N_rls x dim1_z] total elements in wo
	size_t k_tot;		// total size of k (RLS, k = P*r, an auxiliary vector)
	size_t P_tot;		// total size of P (RLS, inverse correlation matrix)

	// stim data:
	partype msecs;				// [time units] (Oded) total # of seconds in the simulation
	size_t n_samples;			// total # of samples
	partype stim_level_dB;

	// cochlea:
	std::string anf_filename;	// input signal file (ft)
	//long numchs;		// # of channels
	//partype lowcf;
	//partype highcf;
	partype* anf_tn;			// [n_samples x Jin_dim2] the input vector
	
	// ft (desired output):
	std::string ft_filename;	// ft signal file (z)
	size_t ft_tot;				// []n_samples x z_d1] total # of elements of the desired output(s)
	//partype* ft;				// [n_samples x 1] desired signal vector

	// zt (readout):
	size_t learn_every;			// (iteration) learn each iteration step (tn) 
	std::string zt_filename;	// readout file (zt)

	// 
	std::string data_dir;	// result directory

	// save data on HD
	bool save_X;			// save X?
	bool save_Jall;			// save Jall?
	bool save_Vall;			// save Vall?

	// more files:
	std::string x0_filename;
	std::string X_filename;
	std::string M_filename;
	std::string Wf_filename;
	std::string Jin_filename;
	std::string Jall_filename;
	std::string Vall_filename;
	std::string z0_filename;
	std::string wo_filename;
	std::string P_filename;

	// CUDA:
	size_t threads_num;		// # of threads to use in the kernels
	size_t cudaDevNum;

	// debug:
	bool write2screen;
};


inline void checkCUDA();
inline void checkCUDA(cudaError_t cudaResult, std::string errmsg = "");
inline void checkCUDA(cublasStatus_t cublasStatus, std::string errmsg = "");
void InitCUDADevice(cublasHandle_t* cublasHandle, size_t cudaDevNum); // Initialize the CUDA device	
//__global__ void x2r_K(partype* d_Vall, const partype* d_x, const size_t n_neurons);
//void x2r_W(partype* d_Vall, const partype* d_x, const size_t n_neurons, dim3 blocks, dim3 threads);

#endif