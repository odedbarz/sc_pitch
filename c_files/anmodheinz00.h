#ifndef HEINZ_AN_MODEL_H
#define HEINZ_AN_MODEL_H

#define MAXTIMPTS 20000000L		// Maximum number of samples in time 
#define MAXCHS 60				// Maximum number of frequency channels 

#define PI   3.14159265359

#define DOFOR(i, to) for(i = 0; i < to; i++)

#define CMULTR(X,Y) ((X).x*(Y).x-(X).y*(Y).y)
#define CMULTI(X,Y) ((X).y*(Y).x+(X).x*(Y).y)
#define CTREAL(z,X,re) {(z).x=(X).x*(re);(z).y=(X).y*(re);}
#define CMULT(z,X,Y) {(z).x=CMULTR((X),(Y));(z).y=CMULTI((X),(Y));}
#define CADD(z,X,Y) {(z).x=(X).x+(Y).x;(z).y=(X).y+(Y).y;}


namespace anmodheinz00
{
	#include <stdio.h>
	#include <stdlib.h>
	#include <stdarg.h>
	#include <math.h>
	#include <time.h>


	void ANF(void);
	void error(char *fmt, ...);
	void stimulus();
	void gamma4();
	void synapse();
	double erbGM(double);
	double cmaph_x2f(double);
	double cmaph_f2x(double);

	struct complex_st { double x; double y;} ;
	struct complex_st compexp(double);
	struct complex_st compmult(double, struct complex_st);
	struct complex_st compprod(struct complex_st, struct complex_st);
	struct complex_st comp2sum(struct complex_st, struct complex_st);
	double REAL(struct complex_st);
	long myRound(double);

}
#endif