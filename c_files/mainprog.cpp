//#ifndef MAINPROG_CPP
//#define MAINPROG_CPP

//#include "anmodheinz00.h"
#include "mainprog.h"
#include "myCUDA.cuh"
#include "anmodheinz00.h"

inline void checkCUDA()
{
	cudaError_t cudaErrorStatus = cudaGetLastError();
	if(cudaErrorStatus != cudaSuccess)	{
		// print the CUDA error message and exit
		printf("!!! checkCUDA() - CUDA error:\n\t%s\n", cudaGetErrorString(cudaErrorStatus));
		exit(EXIT_FAILURE);
	
	} else
		return;

}

inline void checkCUDA(cudaError_t cudaResult, std::string errmsg)
{
	//#if defined(DEBUG) || defined(_DEBUG)
	if(cudaResult != cudaSuccess) {
		// print the CUDA error message and exit
		printf("!!! ERROR in checkCUDA():\n\t -> errmsg: %s\n\t -> cudaGetErrorString():%s\n", errmsg.c_str(), cudaGetErrorString(cudaResult));
		exit(EXIT_FAILURE);
		//assert(result == cudaSuccess);
	} 
	//#endif
	return;
}

inline void checkCUDA(cublasStatus_t cublasStatus, std::string errmsg)
{
	//#if defined(DEBUG) || defined(_DEBUG)
	switch (cublasStatus)	{
		case (CUBLAS_STATUS_SUCCESS):	
			break;	// the operation completed successfully

		case (CUBLAS_STATUS_NOT_INITIALIZED):
			printf("!!! ERROR:\n\t -> errmsg: %s\n\t -> cublasStatus: %s\n", errmsg.c_str(), "the library was not initialized");
			exit(EXIT_FAILURE);

		case (CUBLAS_STATUS_INVALID_VALUE):
			printf("!!! ERROR:\n\t -> errmsg: %s\n\t -> cublasStatus: %s\n", errmsg.c_str(), "the parameters m,n,k<0");
			exit(EXIT_FAILURE);

		case (CUBLAS_STATUS_ARCH_MISMATCH):
			printf("!!! ERROR:\n\t -> errmsg: %s\n\t -> cublasStatus: %s\n", errmsg.c_str(), "the device does not support double-precision");
			exit(EXIT_FAILURE);

		case (CUBLAS_STATUS_EXECUTION_FAILED):
			printf("!!! ERROR:\n\t -> errmsg: %s\n\t -> cublasStatus: %s\n", errmsg.c_str(), "the function failed to launch on the GPU");
			exit(EXIT_FAILURE);

	}
	//#endif

	return;
}

// Initialize the CUDA device
void InitCUDADevice(cublasHandle_t* cublasHandle, size_t cudaDevNum)	
{

	checkCUDA( cudaSetDevice(cudaDevNum),	"!!!! Error: cudaSetDevice(0) failed !!!!\n" );
	checkCUDA( cudaDeviceReset(),	"!!!! Error: device memory allocation error (allocate d_x) !!!!\n" );

	// In case of using NVIDIA GTX 590 (two devices):
	#ifdef NVIDIA_GTX_590
	checkCUDA( cudaSetDevice(1),	"!!!! Error: cudaSetDevice(1) failed !!!!\n" );
	checkCUDA( cudaDeviceReset(),	"!!!! Error: device memory allocation error (allocate d_x) !!!!\n" );
	#endif

	// Start cublas engine:
	checkCUDA( cublasCreate( &(*cublasHandle) ), "!!!! Error: CUBLAS initialization error !!!!\n" );	

}

// Allocate memory in the host & check it
void* AllocHostMemory(size_t data_N, bool ispinned = g_ispinned, size_t data_type_size = sizeof(partype))
{
	void *h_pt;

	if (ispinned)	{	
		// Pinned memory
		cudaError_t cudaResult = cudaMallocHost(&h_pt, data_N*data_type_size);	// better than the malloc (pinned memory pages) 
		if ( cudaSuccess != cudaResult )    {
			std::cout << "!!!! Error: device memory allocation error (allocate d_x)" << std::endl;
			exit(EXIT_FAILURE);
		} else if (h_pt == 0)    {
			std::cerr << "!!!! Error in AllocHostMemory(): Host memory allocation error" << std::endl;
			exit(EXIT_FAILURE);
		}
	
	} else
		// Unpinned memory
		h_pt = malloc(data_N * data_type_size);
	

	return h_pt;
}

// Read parameter from file
partype ReadParameter(char* filename, std::string parField)
{
	partype par;	// the parameter to read

	// create a file-reading object
	std::ifstream fin;
	fin.open(filename); // open a file
	if (!fin.good()) 
		return EXIT_FAILURE; // exit if file not found
  
  std::string sub1, sub2;

  // read each line of the file
  while (!fin.eof())
  {
    // read an entire line into memory
    char buf[MAX_CHARS_PER_LINE];
    fin.getline(buf, MAX_CHARS_PER_LINE);
    
	std::istringstream iss;
	iss.str(buf);
	iss >> sub1;
	if (sub1.substr(0,2)=="//")	// a comment?
		continue;
	else if (sub1==parField)	{
		iss >> sub2;	// read the "=" sign
		iss >> sub2;	// the actual value of the parameter
		std::istringstream(sub2) >> par;	// ?? ::isdigit

		//fin.close();  no need to call it explicitly, let the destractor do the closing on its own...
		return par;
	}
  }

  std::cout << "ERROR: couldn't find " << parField << " in the file <" << filename << "> \n";
  exit(EXIT_FAILURE);
}

// Read string field from file
std::string ReadStr(char* filename, std::string parField)
{
	// create a file-reading object
	std::ifstream fin;
	fin.open(filename); // open a file
	if (!fin.good()) {
		std::cout << "-> Error in ReadStr(): Can't open file " << filename << " for reading !!!\n"; 
		exit(EXIT_FAILURE); // exit if file not found
	}

	std::string sub1, sub2;

	// read each line of the file
	while (!fin.eof())
	{
		// read an entire line into memory
		char buf[MAX_CHARS_PER_LINE];
		fin.getline(buf, MAX_CHARS_PER_LINE);
    
		std::istringstream iss;
		iss.str(buf);
		iss >> sub1;

				//std::cout << "--> sub1: " << sub1 << "\n";
				//std::cout << "--> parField: " << parField << "\n\n";

		//if (sub1.substr(0,2)=="//")	// a comment?
		//	continue;
		//else 
		if (sub1==parField)	{
			iss >> sub2;	// read the "=" sign
			iss >> sub2;	// the actual value of the parameter

			//fin.close();  no need to call it explicitly, let the destractor do the closing on its own...
			return sub2;
		}
	}		// end of while loop

  std::cout << "ERROR: couldn't find " << parField << "in the file <" << filename << "> \n";
  exit(EXIT_FAILURE);
}

// Create the main database
size_t CreateDB(char* filename, database_st* db_st)
{
	db_st->par_type = ReadStr(filename, "partype");
	if (STR(partype) == db_st->par_type) {std::cout << "ERROR in CreateDB: (STR(partype) == db_st->par_type)\n"; (EXIT_FAILURE);}

	db_st->Fs = ReadParameter(filename, "Fs");			// (Hz) input sampling rate 
	db_st->dt = 1.0/db_st->Fs;							// (sec) sampling time
	db_st->tau = ReadParameter(filename, "tau");		// neuron's dynamic time
	db_st->N = ReadParameter(filename, "N");			// # of neurons in the network
	db_st->N2 = db_st->N*db_st->N;						// N^2
	db_st->p = ReadParameter(filename, "p");			// density of the network 
	db_st->g = ReadParameter(filename, "g");			// g greater than 1 leads to chaotic networks
	db_st->alpha = ReadParameter(filename, "alpha");	// learning rate as a prior in P
	db_st->scale = 1.0/sqrt(db_st->p*db_st->N);			// std of the efficacy matrix M	
	db_st->N_rls = ReadParameter(filename, "N_rls");	// for the RLS learning: the width of the sub-net <--> size(P,1)
	if (db_st->N_rls > db_st->N)  {std::cout << "ERROR in CreateDB: (db_st->N_rls > db_st->N)\n"; (EXIT_FAILURE);}
		
	db_st->training_flag = ReadParameter(filename, "training_flag");	// training_flag == [0,1]: [testing, training] the net

	// network structure:
	db_st->z_d1 = ReadParameter(filename, "z_d1");		// # of feedback channels
	db_st->Jin_d1 = db_st->N;	
	db_st->Jin_d2 = ReadParameter(filename, "Jin_d2");	// # of input channels
	db_st->Jin_tot = db_st->Jin_d1 * db_st->Jin_d2;
	db_st->Jall_d1 = db_st->N;	
	db_st->Jall_d2 = db_st->N + db_st->z_d1 + db_st->Jin_d2;  // Jall_d2 = dim(h_Jall,2) = dim(h_Vall,1)
	db_st->Jall_tot = db_st->Jall_d1 * db_st->Jall_d2;
	db_st->Vall_d1 = db_st->Jall_d2;
	db_st->Vall_d2 = 1;
	db_st->wf_tot = db_st->N * db_st->z_d1;			// [N x z_d1] feadback vector
	//db_st->wo_d1 = db_st->z_d1;		// dim1 of the output vector wo
	//db_st->wo_d2 = db_st->N_rls;		// dim2 of the output vector wo
	db_st->wo_tot = db_st->z_d1 * db_st->N_rls; //db_st->wo_d1 *db_st->wo_d2; 
	db_st->k_tot = db_st->N_rls; //db_st->z_d1*db_st->N_rls;	 // total size of k (RLS, k = P*r, an auxiliary vector)
	db_st->P_tot = db_st->z_d1 * POW2(db_st->N_rls);	// total size of P (RLS, inverse correlation matrix)
	

	// stim data:
	db_st->msecs = ReadParameter(filename, "msecs");			// [time units] length (in mili-seconds) of the stimulation
	db_st->n_samples = floor(1e-3*db_st->msecs*db_st->Fs);		// total # of samples
	if (ReadParameter(filename, "n_samples") != db_st->n_samples) {std::cout << "ERROR in CreateDB: (ReadParameter(filename, \"n_samples\") != db_st->n_samples)\n"; (EXIT_FAILURE);}
	db_st->anf_tn = (partype*)AllocHostMemory( db_st->Jin_d2 );

	// cochlea:
	db_st->anf_filename = ReadStr(filename, "anf_filename");			// input file of the auditory nerve fibers (ANF)
	
	// ft (desired output):
	db_st->ft_filename = ReadStr(filename, "ft_filename");			// desired signal file (of <ft>)
	db_st->ft_tot = db_st->n_samples * db_st->z_d1;

	// zt (readout):
	db_st->learn_every = ReadParameter(filename, "learn_every");
	db_st->zt_filename = ReadStr(filename, "zt_filename");		// readout file (zt)

	// Auxliliary
	db_st->data_dir = ReadStr(filename, "data_dir");		// result directory

	// save data on HD
	db_st->save_X		= ReadParameter(filename, "save_X");			// save X?
	db_st->save_Jall	= ReadParameter(filename, "save_Jall");		// save Jall?
	db_st->save_Vall	= ReadParameter(filename, "save_Vall");			// save P?

	// more files:
	db_st->x0_filename	= ReadStr(filename, "x0_filename");
	db_st->X_filename	= ReadStr(filename, "X_filename");
	db_st->M_filename	= ReadStr(filename, "M_filename");
	db_st->Wf_filename	= ReadStr(filename, "Wf_filename");
	db_st->Jin_filename = ReadStr(filename, "Jin_filename");
	db_st->Jall_filename= ReadStr(filename, "Jall_filename");
	db_st->Vall_filename= ReadStr(filename, "Vall_filename");
	db_st->z0_filename	= ReadStr(filename, "z0_filename");
	db_st->wo_filename	= ReadStr(filename, "wo_filename");
	db_st->P_filename	= ReadStr(filename, "P_filename");


	// CUDA:
	db_st->threads_num = ReadParameter(filename, "threads_num");
	db_st->cudaDevNum  = ReadParameter(filename, "cudaDevNum");
	

	// debug:
	db_st->write2screen = ReadParameter(filename, "write2screen");

	return EXIT_SUCCESS;	// everything is OK

}

// Write current system matrix to a binary file
size_t Write2File(const std::string filename, partype* data2write, const size_t dataSize)
{
	//std::cout << "Writing (" << filename << ") into a binary file\n";
	std::ofstream myfile;	
	myfile.open(filename, std::ios::out | std::ios::binary);
	if (!myfile.is_open())
	{
		std::cout << "Error in Write2File(): can't open " << filename << " file for reading !!!!\n";
		return EXIT_FAILURE;
	}

	myfile.write((char*)data2write, sizeof(data2write[0])*dataSize );
	myfile.close();
}

// Read vector from file with numOfElements elements
size_t ReadVector (std::string filename, partype* pt2vector, size_t numOfElements) 
{
	//void* pt2vector = NULL;
	std::ifstream fid_in;

	//std::cout << "-> Reading from " << filename.c_str() << "\n";
	fid_in.open(filename.c_str(), std::ios::in | std::ios::binary);
	if (!fid_in.is_open())	{
		std::cout << "!!!! Error in ReadVector(): can't open " << filename.c_str() << " file for reading !!!!\n";
		exit(EXIT_FAILURE);	
	}
	fid_in.read( (char*)pt2vector, numOfElements*sizeof(partype) );
	// fid_in.close();	let the destractor do this job...

	return EXIT_SUCCESS;

}

// Load all parts of Jall
size_t LoadJall(const database_st* db_st, partype* h_Jall)
{
	std::ifstream fid_M, fid_Wf, fid_Jin;	// a file handle

	// Init. <Jnet> (within <h_Jall>):
	//std::cout << "-> Reading <M> from " << db_st->M_filename.c_str() << " into <Jall>...\n";
	fid_M.open(db_st->M_filename.c_str(), std::ios::in | std::ios::binary);
	if (!fid_M.is_open())	{
		std::cout << "!!!! Error: can't open " << db_st->M_filename.c_str() << " file for reading !!!!\n";
		return EXIT_FAILURE;	
	}

	// Init. <wf> (within <h_Jall>):
	//std::cout << "-> Reading <Wf> from " << db_st->Wf_filename.c_str() << " into <Jall>...\n";
	fid_Wf.open(db_st->Wf_filename.c_str(), std::ios::in | std::ios::binary);
	if (!fid_Wf.is_open())	{
		std::cout << "!!!! Error: can't open " << db_st->Wf_filename.c_str() << " file for reading !!!!\n";
		return EXIT_FAILURE;	
	}

	// Init. <Jin> (within <h_Jall>):
	//std::cout << "-> Reading <Jin> from " << db_st->Jin_filename.c_str() << " into <Jall>...\n";
	fid_Jin.open(db_st->Jin_filename.c_str(), std::ios::in | std::ios::binary);
	if (!fid_Jin.is_open())	{
		std::cout << "!!!! Error: can't open " <<db_st->Jin_filename.c_str() << " file for reading !!!!\n";
		return EXIT_FAILURE;	
	}

	// fill <h_Jall> column by column
	fid_M.read( (char*)(h_Jall), db_st->N2*sizeof(partype) );
	fid_Wf.read( (char*)(h_Jall+db_st->N2), db_st->N * db_st->z_d1 * sizeof(partype) );
	fid_Jin.read( (char*)(h_Jall + db_st->N2 + db_st->N * db_st->z_d1), db_st->Jin_tot * sizeof(partype) );
	
	// let the destructor to this job...
	//fid_M.close();
	//fid_Wf.close();
	//fid_Jin.close();
	return EXIT_SUCCESS;
}


// -------------------------------------------------------------------------
// Main entry into the program
// -------------------------------------------------------------------------
int main(int argc, char **argv)
{
	std::cout << "-> starting int main()...\n";	// DEBUG
	
	// change working directory if available:
	if (argc >= 3)	{ 
		if (_chdir(argv[2]))	{
			switch (errno) {
			case ENOENT:
				printf( "-> ERROR: Unable to locate the directory: %s !!!\n", argv[2]);
				break;
			case EINVAL:
				printf( "-> ERROR: invalid buffer !!!\n");
				break;
			default:
				printf(" -> ERROR: Unkown error !!!");
			}
			exit(EXIT_FAILURE);
		} else {
			std::cout << "--> Current working directory:\n    >> ";
			system("cd");
			std::cout << "\n--> Changing working directory to:\n    >> ";
			system("cd");
			printf("\n");
		}
	}

	// Random number generator:
	std::default_random_engine generator;

	// Uniform distribution:
	//std::uniform_real_distribution<partype> distribution_U(0.0, 1.0);
	auto distUniform = std::bind ( std::uniform_real_distribution<partype>(0.0, 1.0), generator );
	
	// Normal distribution:
	//std::normal_distribution<partype> distribution_N(0.0, 1.0);	// mean = 0, std = 1.0
	auto distNormal = std::bind ( std::normal_distribution<partype>(0.0, 1.0), generator );

	// Parameter list:
	database_st db_st;

	if (argc < 2)	{
		std::cout << "ERROR: Missing the parameter file!!!\n";
		return EXIT_FAILURE;
	}

	cudaError_t		cudaResult;
	cublasStatus_t	cublasStatus;
	cublasHandle_t	cublasHandle;

	//anmodheinz00::ANF();

	// Initialize the CUDA device
	InitCUDADevice(&cublasHandle, db_st.cudaDevNum);

	// Create the main database:
	CreateDB(argv[1], &db_st);
	if (db_st.write2screen) std::cout << "-> db_st initialized...\n";		// DEBUG

	//std::ifstream fid_in;
	std::ofstream fid_Vall, fid_X;	// define a handle to the input file <uin>


	// Host (CPU):
	// ---------------------------------------------------- 
	const partype dt2tau	= db_st.dt/db_st.tau;
	const partype xnCoeff	= 1.0 - dt2tau;
	const partype one		= 1.0;
	const partype zero		= 0.0;

    partype *h_x;		// [Nx1]: dynamic states (host)
    partype* h_Jall;	// [NxJall_tot]: [ Jnet | Wf | Jin ] (host)
	partype* h_wo;		// [Nxdim1_z] output weights
	partype* h_z;		// [dim1_zx1] readout unit(s)
	partype* h_zt;		// [dim1_zxn_samples] readout unit(s) for all t
	partype	 c_rls;		// c = 1.0/(1.0 + rPr);
	partype  e_rls;		// instant error: e = z-ft(ti);
	partype* h_rPr = 0;
	partype* h_P;
	partype* h_Vall;	// DEBUG Vall
	partype* h_ft;		// [n_samples x dim1_z] target (desired) signal, i.e., the pitch selectivity of the readouts

	// Allocate host memory:
	h_x		= (partype*)AllocHostMemory( db_st.N );
	h_Jall	= (partype*)AllocHostMemory( db_st.Jall_tot );
	h_wo	= (partype*)AllocHostMemory( db_st.wo_tot );
	h_z		= (partype*)AllocHostMemory( db_st.z_d1 );
	h_rPr	= (partype*)AllocHostMemory( 1 );
	h_P		= (partype*)AllocHostMemory( db_st.P_tot);
	h_ft	= (partype*)AllocHostMemory( db_st.ft_tot );
	h_zt	= (partype*)AllocHostMemory( db_st.ft_tot );	// Declare & init. a vector to save al z(t)
	if(db_st.save_Vall)   h_Vall = (partype*)AllocHostMemory( db_st.Vall_d1 );


	// Create the network's matrix <Jnet>, with normal distribution:
	if (db_st.write2screen) std::cout << "Creating the model structure...\n";

	// Init. dynamic states:
	if (db_st.write2screen) std::cout << "-> Reading <x0> from " << db_st.x0_filename << " into <Jall>...\n";
	ReadVector(db_st.x0_filename, h_x, db_st.N);

	// Load <Jall> matrix:
	LoadJall(&db_st, h_Jall);
	if (db_st.write2screen) std::cout << "-> Reading <Jall> from [M, Wf, Jin]...\n";
	if (db_st.save_Jall)	Write2File(db_st.Jall_filename, h_Jall, db_st.Jall_tot);

	// h_z:
	if (db_st.write2screen) std::cout << "-> Reading <z0> from " << db_st.z0_filename << "...\n";
	ReadVector(db_st.z0_filename, h_z, db_st.z_d1);

	// wo:
	if (db_st.write2screen) std::cout << "-> Reading <wo> from " << db_st.wo_filename << "...\n";
	ReadVector(db_st.wo_filename, h_wo, db_st.wo_tot);

	// zt:
	if (db_st.write2screen) std::cout << "-> Init. <h_zt>...\n";
	for (size_t i=0; i<db_st.z_d1*db_st.n_samples; ++i) // init <h_zt>
		h_zt[i] = 0.0;

	// ft:
	ReadVector(db_st.ft_filename, h_ft, db_st.ft_tot);


	// Device (GPU):
	// ---------------------------------------------------- 
	if (db_st.write2screen) std::cout << "-> Init. GPU device...\n";

    partype* d_x	= 0;		// [Nx1]: dynamic states (device)
    partype* d_Jall = 0;		// [NxJall_tot]: [ Jnet | Wf | Jin ] (device)
    partype* d_Vall = 0;		// [NxJall_tot]: [ r | z | u ] (device)
	partype* d_wo	= 0;		// [[N_rls x dim1_z] output weights
	partype* d_P	= 0;		// [N_rls x N_rls] the inverse-correlation matrix
	partype* d_k	= 0;		// [N_rls x 1] Aux. vector for the RLS

	// Allocate device memory:
	// ----------------------------------------------------	
	checkCUDA( cudaMalloc((void **)&d_x, db_st.N * sizeof(partype)), "allocate <d_x> error" );
	checkCUDA( cudaMalloc((void **)&d_Jall, db_st.Jall_tot * sizeof(partype)), "allocate <d_Jall> error" );
	checkCUDA( cudaMalloc((void **)&d_Vall, db_st.Vall_d1 * sizeof(partype)), "allocate <d_Vall> error" );
	checkCUDA( cudaMalloc((void **)&d_wo, db_st.wo_tot * sizeof(partype)), "allocate <d_wo> error" );
	checkCUDA( cudaMalloc((void **)&d_P, db_st.P_tot * sizeof(partype)), "allocate <d_P> error" );
	checkCUDA( cudaMalloc((void **)&d_k, db_st.k_tot * sizeof(partype)), "allocate <d_k> error" );

	// Initialize the device matrices with the host matrices:
	// ------------------------------------------------------
    checkCUDA( cublasSetVector( db_st.N, sizeof(h_x[0]), h_x, 1, d_x, 1) );						// h_x --> d_x
    checkCUDA( cublasSetVector( db_st.Jall_tot, sizeof(h_Jall[0]), h_Jall, 1, d_Jall, 1) );		// h_Jall --> d_Jall
	checkCUDA( cublasSetVector( db_st.wo_tot, sizeof(h_wo[0]), h_wo, 1, d_wo, 1) );				// h_wo --> d_wo

	partype* d_z	= d_Vall + db_st.N;						// [z_d1 x 1] starting of the vector z (of length z_d1)
	partype* d_r_rls=  d_Vall + db_st.N - db_st.N_rls;		// pointer to a subset of the vector <r> (only N_rls elements), (d_z == d_Vall + db_st.N)

	// open ANF file for reading:
	std::ifstream fid_anf;	
	fid_anf.open(db_st.anf_filename, std::ios::in | std::ios::binary);
	if (!fid_anf.is_open())	{
		std::cout << "!!!! Error: can't open file: " << db_st.anf_filename << " for reading !!!!\n";
		return EXIT_FAILURE;
	}
	fid_anf.seekg(0, fid_anf.beg); // move pointer to the beginning of the binary file
	
	// <X> init. open file:
	if (db_st.save_X) {
		fid_X.open(db_st.X_filename.c_str(), std::ios::out | std::ios::binary);
		if (!fid_X.is_open())		{
			std::cout << "!!!! Error: can't open file: " << db_st.X_filename << " for reading !!!!\n";
			return EXIT_FAILURE;
		}	
	}

	// <Vall> init. open file:
	if (db_st.save_Vall) {
		fid_Vall.open(db_st.Vall_filename.c_str(), std::ios::out | std::ios::binary);
		if (!fid_Vall.is_open())		{
			std::cout << "!!!! Error: can't open file: " << db_st.Vall_filename << " for reading !!!!\n";
			return EXIT_FAILURE;
		}	
	}


	// load P, the inverse-correlation matrix:
	ReadVector( db_st.P_filename, h_P, db_st.P_tot );

	//copy from cuda:
	checkCUDA( cublasSetVector( db_st.P_tot, sizeof(h_P[0]), h_P, 1, d_P, 1 ), "cublasSetVector() failed for the matrix P" );	// h_P --> d_P 

	dim3 threads(db_st.threads_num, 1, 1);   // according to the max threads per block (in GTX 590)
	dim3 blocks( (db_st.N_rls+threads.x-1)/threads.x, (db_st.N_rls+threads.y-1)/threads.y );	
	if (MAX_GRID_DIM_X < blocks.x)	{	// check maximum # of blocks in a grid
        std::cout << "!!!! Error: (MAX_GRID_DIM_X < blocksPerGrid) !!!!" << std::endl;
        return EXIT_FAILURE;
    }

	// <Vall>: define threads & blocks size
	//threads.x = THREADS_NUM;	// cudaFuncAttributes::maxThreadsPerBlock
	//threads.y = 1;
	//threads.z = 1;
	blocks.x = ( (threads.x+db_st.N+db_st.z_d1+db_st.Jin_d2-1)/threads.x );	
	blocks.y = 1;
	if (MAX_GRID_DIM_X < blocks.x)		{	// check maximum # of blocks in a grid
        std::cout << "!!!! Error: (MAX_GRID_DIM_X < blocksPerGrid) !!!!" << std::endl;
        return EXIT_FAILURE;
    }
	
	if (1==db_st.training_flag)
		std::cout << "-> TRAINING...\n";
	else
		std::cout << "-> TESTING...\n";

	// <z_t>
	checkCUDA( cudaMemcpy( d_z, h_z, db_st.z_d1*sizeof(partype), cudaMemcpyHostToDevice) ); // d_z (d_Vall+N) <- h_z

	// ------------------------------------------------------
	// Time Simulation (training & testing):
	// ------------------------------------------------------
	//TODO: cublasSetStream(): for parallel streaming
	//TODO: cublasGetVectorAsync(): device->host % saving (in HD) in an asyncronic fasion  
	//TODO: if z is a vector (z_dim1 > 1), then create a parallel computation of each entry.
	for (size_t tn=0; tn<db_st.n_samples; ++tn)		
	{

		// save <X> on disk: 
		if (db_st.save_X)	{			
			checkCUDA( cublasGetVector(db_st.N, sizeof(h_x[0]), d_x, 1, h_x, 1), "main loop: cublasGetVector() failed fo x");	// d_x --> h_x

			fid_X.write( (char*)h_x, sizeof(h_x[0])*db_st.N );	// write <X> into file	
			if (!fid_X) 	{
				std::cout << "!!!! Error: couldn't write to <x> into file.\n";
				return EXIT_FAILURE;
			}
		}	

		// read network's input data:
		fid_anf.read( (char*)db_st.anf_tn, db_st.Jin_d2*sizeof(partype) );		
		if (!fid_anf)	{
			std::cout << "!!!!  (tn=" <<  tn << ")Error:(db_st.anf_tn) only " << fid_anf.gcount() << " could be read (out of " << db_st.Jin_d2 << ") !!!!\n";
			return EXIT_FAILURE;
		}

		//x2r_K<<<blocks,threads>>>( d_Vall, d_x, db_st.N );	// d_Vall <- phi(d_x)
		x2r_W( d_Vall, d_x, db_st.N, blocks, threads );	// d_Vall <- phi(d_x)
		checkCUDA();

		checkCUDA( cudaMemcpy((d_Vall+db_st.N+db_st.z_d1), db_st.anf_tn, db_st.Jin_d2*sizeof(partype), cudaMemcpyHostToDevice), "failed: d_Vall <- input signal at time tn");   // d_Vall <- input signal at time tn

		// save <Vall> on disk: 
		if(db_st.save_Vall)	{
			checkCUDA( cublasGetVector(db_st.Vall_d1, sizeof(h_Vall[0]), d_Vall, 1, h_Vall, 1), "error in cublasGetVector() for h_Vall" );	// d_r --> h_Vall

			if (db_st.write2screen) std::cout << "-> DEBUG: saving Vall into: " << db_st.Vall_filename << "\n";
			
			fid_Vall.write( (char*)h_Vall, sizeof(h_Vall[0])*db_st.Vall_d1 );	// write <X> into file	
			if (!fid_Vall) 	{
				std::cout << "!!!! Error: couldn't write to <x> into file.\n";
				return EXIT_FAILURE;
			}
		}


		// solve: x(n+1) = (1-dt/tau)*x(n) + (dt/tau)*Jall*Vall <--> (cublas):  y = b*y + a*A*x 
		checkCUDA( cublasXgemv(
			cublasHandle,				// cublasHandle_t handle
			CUBLAS_OP_N,				// cublasOperation_t trans,
			db_st.N, db_st.Jall_d2,		// int m, int n (dim. of A)
			&dt2tau, //&one,			// const partype *alpha (coefficient of A)
			d_Jall,						// const partype *A
			db_st.N, 					// int lda (leading dimension)
			d_Vall,						// const partype *x (as for A*x)
			1,							// int incx,
			&xnCoeff,					// const partype *beta,
			d_x,						// partype *y, 
			1							// int incy
		), "main loop: cublasXgemv failed" );

		//  Solve for z = wo^T * r <--> C = alpha*A*B + beta*C
		checkCUDA( cublasXgemm( 
			cublasHandle, // handle,
			CUBLAS_OP_T, CUBLAS_OP_N,		//transa, transb,
			db_st.z_d1, 1, db_st.N_rls,		// m: # rows of op(A) & C, n: # colss of op(B) & C, k: # cols of op(A) & rows of op(B),
			&one,							// *alpha
			d_wo, db_st.N_rls,				// *A, int lda,
			d_r_rls, db_st.N_rls,			// *B, int ldb,
			&zero,							// *beta,
			d_z, db_st.z_d1),				// *C, int ldc),
		"failed in main loop: z = wo^T * r <--> C = alpha*A*B + beta*C" );

		// move h_d into the device:
		//checkCUDA( cudaMemcpy((d_Vall+db_st.N), h_z, db_st.z_d1*sizeof(partype), cudaMemcpyHostToDevice), "failed: d_Vall <- h_z" ); // d_Vall <- h_z
		//checkCUDA( cudaMemcpy(h_z, d_z, db_st.z_d1*sizeof(partype), cudaMemcpyDeviceToHost), "failed: h_z <- d_z" ); // d_Vall <- h_z
		checkCUDA( cublasGetVector(db_st.z_d1, sizeof(partype), d_z, 1, h_z, 1), "failed: h_z <- d_z" ); // d_Vall <- h_z

		// save <h_z> into <h_zt>:
		for (size_t ii=0; ii<db_st.z_d1; ++ii)
			h_zt[tn + ii*db_st.n_samples] = h_z[ii];	// !Note: h_z may be a vector


		// ---------------------------------
		// *** Train <d_wo> - RLS:
		if ((1 == db_st.training_flag) && ( tn % db_st.learn_every == 0 ))	{

			// update the weights wo vector: 
			for (size_t ii=0; ii<db_st.z_d1; ++ii)		{
				partype* d_P_ii = d_P + ii*POW2(db_st.N_rls);			// pointer to P(:,:,ii)
				partype* d_k_ii = d_k; //d_k + ii*db_st.N_rls;			// pointer to a subset of the auxiliary vector <k>
				partype* d_wo_ii = d_wo + ii*db_st.N_rls;
				
				// k = Pr, 
				checkCUDA( cublasStatus = cublasXgemv(
						cublasHandle,							// cublasHandle_t handle
						CUBLAS_OP_N,							// cublasOperation_t trans,
						db_st.N_rls, db_st.N_rls,				// int m, int n (dim. of A)
						&one,									// const partype* alpha (coefficient of A)
						(partype*)d_P_ii,						// <-- P: const partype* A
						db_st.N_rls,							// int lda (leading dimension)
						(partype*)d_r_rls,						// <-- r: const partype* x (as for A*x)
						1,										// int incx,
						&zero,									// const partype *beta,
						(partype*)d_k_ii,							// --> k: partype *y, 
						1 )										// int incy
					,"RLS: cublasXgemv() Failed");

					// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
					//checkCUDA( cublasGetVector(db_st.N_rls, sizeof(partype), d_k_ii, 1, h_x, 1) );	

				// Solve for rPr = r'*k = r'*P*r:
				checkCUDA( cublasXdot (
					cublasHandle, db_st.N_rls,		// cublasHandle_t handle, int n,
					(partype*)d_k_ii, 1,				// const partype *x, int incx,
					(partype*)d_r_rls, 1,			// const partype *y, int incy,
					(partype*)h_rPr )				// --> rPr, partype *result
				,"RLS: cublasXdot() Failed");

				c_rls = -1.0/(1.0 + *h_rPr); 		// !!! WATCH the MINUS !!!

				// P = P + (-c)*(k*k') <--> (cublas): A = a*x*y' + A
				checkCUDA( cublasXger(
					cublasHandle, db_st.N_rls, db_st.N_rls,	// cublasHandle_t	handle, int m, int n,
					&c_rls,									// const partype	*alpha,
					(partype*)d_k_ii, 1,						// const partype	*x, int incx,
					(partype*)d_k_ii, 1,						// const partype	*y, int incy,
					(partype*)d_P_ii, db_st.N_rls )			// --> P, partype *A, int lda
				,"RLS: cublasXger() Failed");

					// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
					//checkCUDA( cublasGetVector(db_st.N_rls, sizeof(partype), d_wo_ii, 1, h_x, 1) );	

				// calc instant error:
				e_rls = c_rls*(h_z[ii] - h_ft[tn + ii*db_st.n_samples]); 	// e_rls = (-c)*(z-ft)

				// wo = wo - e*c*k <--> (cublas): y = a*x + y
				checkCUDA( cublasXaxpy(
					cublasHandle, db_st.N_rls,	// cublasHandle_t	handle, int		n,
					&e_rls,						// const partype	*alpha,
					(partype*)d_k_ii, 1,		// const partype	*x, int incx,
					(partype*)d_wo_ii, 1 )		// partype	*y, int incy
				,"RLS: cublasXaxpy() Failed");
			

					// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
					//checkCUDA( cublasGetVector(db_st.N_rls, sizeof(partype), d_wo_ii, 1, h_x, 1) );		// d_wo_ii --> h_x


			}	// End of RLS readout for loop

		}	// *** End of RLS learning 		

	}	// ### END of TRAINING

	
	// Save matrix P into file:
	checkCUDA( cublasGetVector(db_st.P_tot, sizeof(partype), d_P, 1, h_P, 1) );	// d_P --> h_P 
	Write2File(db_st.P_filename, h_P, db_st.P_tot);

	// close open files:
	fid_anf.close();	
	if (db_st.save_X)		fid_X.close();		
	if (db_st.save_Vall)	fid_Vall.close();

	// Save results to a binary file:
	checkCUDA( cublasGetVector(db_st.wo_tot, sizeof(h_wo[0]), d_wo, 1, h_wo, 1) );	// d_wo --> h_wo
	Write2File(db_st.wo_filename, h_wo, db_st.wo_tot);		// save it for the next iteration
		
	// save last neuron states:
	checkCUDA( cublasGetVector(db_st.N, sizeof(h_x[0]), d_x, 1, h_x, 1) );	// d_x --> h_x
	Write2File(db_st.x0_filename, h_x, db_st.N);

	Write2File(db_st.zt_filename, h_zt, db_st.z_d1*db_st.n_samples);	
	Write2File(db_st.z0_filename, h_z, db_st.z_d1);	


	// Memory clean-up - Host:
	// ----------------------------------------------------	
	if (g_ispinned)		{  // instead of free(h_x) for the malloc()
		checkCUDA( cudaFreeHost( h_x ) );		
		checkCUDA( cudaFreeHost( h_Jall ) );
		checkCUDA( cudaFreeHost( h_wo ) );
		checkCUDA( cudaFreeHost( h_z ) );
		checkCUDA( cudaFreeHost( h_zt ) );
		checkCUDA( cudaFreeHost( h_rPr ) );
		if (db_st.save_Vall) checkCUDA( cudaFreeHost(h_Vall) );
	
	}else {
		free( h_x );		
		free( h_Jall );
		free( h_wo );
		free( h_z );
		free( h_zt );
		free( h_rPr );
		if (db_st.save_Vall) free(h_Vall);
	}
			

	// Memory clean-up - Device:
	// ----------------------------------------------------
	checkCUDA( cudaFree(d_x) );
	checkCUDA( cudaFree(d_Jall) );
	checkCUDA( cudaFree(d_Vall) );
	checkCUDA( cudaFree(d_wo) );
	checkCUDA( cudaFree(d_P) );
	checkCUDA( cudaFree(d_k) );
	
	cublasDestroy(cublasHandle);

	std::cout << "End of PitchEst => main()..." << std::endl;
	//std::cout << "Press Enter to Continue";
	//std::cin.ignore();
	return 0;
}


//#endif