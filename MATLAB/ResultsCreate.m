

function results = ResultsCreate( N, training )
    
results.F1 = nan(N, 1);
results.F2 = nan(N, 1);
results.ft_value = nan(N, 1);
results.zt_mean = nan(N, 1);

if nargin >= 2 && training
    results.norm2wo = nan(N, 1);
end
    




