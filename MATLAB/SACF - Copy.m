%
% chk_SACF.m
%

function [pitch_SACF, F0_salience] = SACF( anf, dt, fig_num ) %net2, F0, harmonics, dF)

if exist('fig_num', 'var')
    plot2scn = 1;
else
    plot2scn = 0;
end

%% # of channels, 
% N_ch = 128;  	% (Meddis & Hewitt, 1991)
N_ch =  60;      % (Meddis & O'Mard, 1997)
% N_ch = size(anf,1);


%% # of autocorrelation lags,
% N_lags  = 343;	% (Meddis & Hewitt, 1991)
% N_lags  = 191;	% (Balag, Meddis et al., 2008)
N_lags  = 191;	


%% time constant

% ** (Balag, Meddis et al., 2008)
lags_start = 1/1000;    % [sec]
lags_end   = 1/30;      % [sec]
lags_v     = linspace(lags_start, lags_end, N_lags); 
tau_L      = 2*lags_v;

% % ** (Meddis & Hewitt, 1991)
% lags_v = linspace(1/20e3, 1/60, N_lags); % (Meddis & Hewitt, 1991)
% tau_L = 2.5e-3*ones(1, N_lags);

% % ** (Meddis & O'Mard, 1997)
% lags_v = linspace(1/20e3, 1/60, N_lags); % (Meddis & Hewitt, 1991)
% tau_L = 10e-3*ones(1, N_lags);


%%
    
% ACF   = zeros(net2.SACF.N_chn, net2.SACF.N_lags);    
ACF = zeros(N_ch, N_lags);

% dt = net2.cochlea.dt;

start_smp = 0;   % [samples] avoid the onset at the beginning of the cochlear ANF response

% time_win = zeros(1, ceil( 5 * 1/( dt/(2*lags_v(end)) ) ) );

%
for k = 1:N_ch %net2.SACF.N_chn
    
    for l = 1:N_lags %  net2.SACF.N_lags
        time_win_size = ceil(5*1/(dt/tau_L(l)));
        time_win_smp = start_smp + (1:time_win_size);
        time_win = exp( -(1:time_win_size)*dt/tau_L(l) );
        
        lag_smp = ceil(1/(dt/lags_v(l)));
        
%         ACF(k,l) = anf(k,time_win_smp) *...
%             (anf(k,time_win_smp + lag_smp).*time_win(1:time_win_size) )' * dt;        
%         ACF(k,l) = 1/time_win_size * anf(k,time_win_smp) *...
%             (anf(k,time_win_smp + lag_smp).*time_win(1:time_win_size) )' * dt;
        ACF(k,l) = 1/tau_L(l) * anf(k,time_win_smp) *...
            (anf(k,time_win_smp + lag_smp).*time_win )' * dt;

    end
    
end

sum_ACF = sum(ACF);

% pitch:
[pks, locs] = findpeaks(sum_ACF);
peak_value = max(pks);
pks_loc_max = pks == peak_value;
pitch_SACF = 1./lags_v(locs( pks_loc_max ));

% salience:
pks_ = pks;
pks_(pks_loc_max) = min(pks_) -1;
peak_value2 = max(pks_);
pks_loc_max2 = pks_ == peak_value2;
F0_salience = 1 - peak_value2/peak_value;


%%

if 1 == plot2scn
    figure(fig_num)
    plot( lags_v*1e3, sum_ACF, 'linewidth', 2.5 );
    set(gca, 'fontsize', 18)
    grid on;
    xlabel('lags [msecs]')
    hold on
    plot(lags_v(locs)*1e3, pks, 'ro', 'linewidth', 3, 'markersize', 10)
    plot(lags_v(locs(pks_loc_max))*1e3, peak_value, 'ko', 'linewidth', 3, 'markersize', 18)
    
    %F0_sacf2 = 1./lags_v(locs( pks_loc_max2 ));
    plot(lags_v(locs(pks_loc_max2))*1e3, peak_value2, 'k^', 'linewidth', 3, 'markersize', 18)

    hold off

    % title(sprintf('f_0_{,stim} = %0.1f Hz, f_0_{,sum_ACF} = %0.1f Hz', F0, pitch_SACF))    
    title(sprintf('f_{0,SACF} = %0.1f Hz, salience(f_0) = %.3g', pitch_SACF, F0_salience) )    
    %title(sprintf('f_{0,stim} = %0.1f Hz, f_{0,sum_ACF} = %0.1f Hz, f_{0,ML} = %0.1f Hz', F0, pitch_SACF, results.tst.pitch_ML(tn)) )    

end

end

















