%
% PlotCompisite.m
%

linewidth = 3;
fontsize = 34;
fontsize_legends = 34;
fontsize_labels = 48;

if 1 ~= net.dic.downsmp
    error('This script works ONLy for net.dic.downsmp == 1 (see h2)');
end

cochlea_freq_ = cochlea_freq/F02;   % [Hz] ==> [kHz]

% figure locations:
x0 = 0.10;
y0 = 0.15;
width_anf = 0.3;
hight_anf = 0.8;
dx = 0.01;
x1 = x0 + width_anf + dx;
y1 = y0; 
width_plot = 0.185;
x2 = x1 + width_plot + dx;
y2 = y0; 
x3 = x2 + width_plot + dx;
y3 = y0; 

min_cf = 0;     % [kHz]
max_cf = 22;     % [kHx]
%% Create the figure + axes:
if exist('fig81', 'var') && ishandle(fig81); close(fig81); end;
fig81 = figure(81);
set(gcf, 'WindowStyle', 'docked');
set(gcf, 'Units', 'normalized');

h_anf   = axes('Position', [x0, y0, width_anf, hight_anf]);
h_sumT  = axes('Position', [x1, y1, width_plot, hight_anf]);
h_LS    = axes('Position', [x2, y2, width_plot, hight_anf]);    % lambda == 0
h_SC    = axes('Position', [x3, y3, width_plot, hight_anf]);    % lambda > 0


%% ANF Image:
axes(h_anf);
ttt = linspace(net.dic.start_msec, net.dic.end_msec, net.dic.time_smp);
[X,Y] = meshgrid(ttt, cochlea_freq_);
surface(X,Y,anf,'EdgeColor','none', 'Linestyle','none'); 
view(0,90);
% colormap summer
% set(gca,'yscale','log');
% colorbar;
set(h_anf, 'fontsize', fontsize)
% set(h_anf, 'XTickLabel', []);    % disable the left axis
ylabel(sprintf('CF/(%g Hz)',F02), 'fontsize', fontsize_labels, 'interpreter', 'latex');
xlabel('msec', 'fontsize', fontsize_labels, 'interpreter', 'latex')

%% ANF energy--absolute value over time
axes(h_sumT)
sumT = sum(abs(anf),2)/size(anf,2);

plot(sumT, cochlea_freq_, 'linewidth', linewidth);

% set(gca, 'YAxisLocation', 'right');
set(gca, 'fontsize', fontsize);
set(h_sumT, 'XTickLabel', []);    % disable axis
set(h_sumT, 'YTickLabel', []);    % disable axis
legend_sumT = legend('$\langle|\bf{s}_{_{AN}}|^2 \rangle_{_t}$', 'location', 'north');
set(legend_sumT, 'interpreter', 'latex', 'fontsize', fontsize_legends);
AddHarmonics2Plot(h_sumT, F02, harmonics2, net.N_hrm, 0, 1, linewidth);
axis([0, 1.1*max(sumT), min_cf, max_cf]);

%% least-squares axes
axes(h_LS)
sumT = sum(abs(anf),2)/size(anf,2);
% masd_norm = MASD(anf)/size(anf,2);  % normalized MASD

% hLS: LASSO with ista.gamma_ratio == 0
plot(hLS, fff_dic/F02, 'linewidth', linewidth);

% set(gca, 'YAxisLocation', 'right');
set(gca, 'fontsize', fontsize);
set(h_LS, 'XTickLabel', []);    % disable axis
set(h_LS, 'YTickLabel', []);    % disable axis
xlabel('Arbitrary Units', 'fontsize', fontsize_labels, 'interpreter', 'latex');
legend_LS = legend(sprintf('LS\n($\\lambda$: %g)', 0), 'location', 'north');
set(legend_LS, 'interpreter', 'latex', 'fontsize', fontsize_legends);
AddHarmonics2Plot(h_LS, F02, harmonics2, net.N_hrm, 0, 1, linewidth);

axis([0, 1.1*max(hLS), min_cf, max_cf]);

%% sparse coding axes
axes(h_SC)

plot(hSC, fff_dic/F02, 'linewidth', linewidth);
% set(gca, 'YAxisLocation', 'right');

set(gca, 'fontsize', fontsize);
set(h_SC, 'XTickLabel', []);    % disable axis
set(h_SC, 'YTickLabel', []);    % disable axis
legend_SC = legend(sprintf('SC\n($\\lambda$: %g)', ista.gamma_ratio), 'location', 'north');
set(legend_SC, 'interpreter', 'latex', 'fontsize', fontsize_legends);
axis([0, 1.1*max(hSC), min_cf, max_cf]);


%% auxiliaries:
AddHarmonics2Plot(h_SC, F02, harmonics2, net.N_hrm, 0, 1, linewidth);

% link all axes:
linkaxes([h_anf, h_sumT, h_LS, h_SC], 'y');

% 
axis(h_anf, [10 15.1 min_cf max_cf])

% a


