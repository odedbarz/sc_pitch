

function PlotBatch( Mx, N2plot, y_bias, nomean, fig_num )
%
% function PlotBatch( Mx, N2plot, y_bias, nomean, fig_num )
%
%

if 3 >= nargin
    nomean = '';
end

if 4 >= nargin
    fig_num = 80;
end


N = size(Mx,1);
n_samples = size(Mx,2);

if 1 >= nargin || isempty(N2plot)
    N2plot = N;
end

if 2 >= nargin || isempty(y_bias)
    y_bias = max(abs(Mx(:)));
end

% N2plot = 10;
units2plot = round(linspace(1,N,N2plot));
% y_bias = 5;

figure(fig_num);
plot(0);
% cla;
set(gca,'fontsize',20) ;%,'fontweight','bold');
hold on
for ii = 1:N2plot
    y_bias_ii = (ii-1)*y_bias;
    
    yii2plot = y_bias_ii + Mx(units2plot(ii),:)';
    if strcmpi('nomean',nomean) || strcmpi('nobias',nomean)
        yii2plot = yii2plot - mean(Mx(units2plot(ii),:));
    end
    plot( yii2plot, 'b', 'linewidth', 1.5 );
    
    plot( [1, n_samples], y_bias_ii*[1 1], 'k--', 'linewidth', 1.5 )
    text(10, y_bias_ii+0.2*y_bias, num2str(units2plot(ii)),'fontsize',16,'fontweight','bold');
end
hold off

if strcmpi('nomean',nomean)
    title('data minus mean');
end

grid on;
