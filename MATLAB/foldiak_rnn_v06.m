%
%
% foldiak_rnn_v06.m
%


if 1 == debug_mode.sparse
    Y_      = nan(Ny,N_time);
    Ya_     = zeros(Ny,N_time);    
    dQ_     = nan(Nepoch,1);
    dW_     = nan(Nepoch,1);
    esterrprod = nan(Nepoch,1);   % estimation error
end


misses_  = 0;


%%
Ix_v    = 1:(net.cochlea.Nfb * slide_win.smp);
% x       = zeros(length(Ix_v),1);


for tn = 1:Nepoch    
% for tn = 402:   Nepoch
    % Create the stimulus:
    if 1 < length(F0_tst)
        [stim2, F02, harmonics2, tst_db.phase2(tn), dF2] = CreateIO(net, F0_tst(tn), harmonics2_, dF2_, phase2);     
    else
        [stim2, F02, harmonics2, tst_db.phase2(tn), dF2] = CreateIO(net, F0_tst, harmonics2_, dF2_, phase2);      
    end
    %
    fprintf('\n');   
    fprintf('-> tn = %d (out of %d)\n', tn, Nepoch);
    fprintf('--> harmonics2: ['); fprintf(' %.3g', harmonics2); fprintf(' ]\n');                        
    fprintf('--> F02:                       %.3g Hz\n', F02);
    
    
    % stimulus --> ANFs:
    anf = CreateANF( net, stim2, Hd_ear, fcoefs, centerfreq );        
    
    if length(anf(:)) ~= Nx; error('Error, check it out: << length(anf(:)) ~= Nx >> !'); end
    
 
    % Init. conditions:
    y       = abs(0.01*rand(size(y)));
    ya      = 0.0 + 0*y;   % <y>
    theta   = 0*gamma0 + 0*theta;
    theta_a = 0*gamma0 + 0*theta;
    
    ya2 = 0*y;
    theta_a2 = 0*theta_a;
      
    
    %
    if 1 == debug_mode.sparse
        Ya_     = 0*Ya_;
    end
    
    
    % Running the RNN for N_time iterations...
    for ti = 1:N_time        
        
        % input with sliding window:        
        Ix_lag = net.cochlea.Nfb*(ti-1);
        Ix_win = Ix_lag + Ix_v;
        xs = anf(Ix_win)';
        Qs = Q(:,Ix_win);
        
        if (1 == flags.anf_normalize)
            xs = fun_norm(xs);    % set the input:
            xs = xs/(1e-16+max(xs));
        end
        
        %
        %y = (1.0-dt2tau)*y + dt2tau*phi( Qs*xs + W*( y + mean(Ya_(:,1:ti),2) ) - theta ); 
        y = (1.0-dt2tau)*y + dt2tau*phi( Qs*xs + W2*( y ) + 0*ya2 - 0*theta ); 
        
        % Threshold modification:
        y_ = medfilt1(y, 150/3 );
        theta = (1-gamma0)*theta + gamma0*y_;
        

        
        %ya = (1-delta)*ya + delta*y;
        ya = (1-delta)*ya + delta*phi( y - theta_a );        

        % Threshold modification:
        y_ = medfilt1(ya, 150/3);   % 150/2
        theta_a = (1-gamma1)*theta_a + gamma1*y_;
          
        
        %ya = (1-delta)*ya + delta*y;
        ya2 = (1-delta)*ya2 + delta*phi( ya -W2*ya - theta_a2 );        

        % Threshold modification:
        y_ = medfilt1( ya2, 30 );    % 150/3
        theta_a2 = (1-gamma1)*theta_a2 + gamma1*y_;
        
        
        % ** DEBUG **
        if (1 == debug_mode.sparse)
            Y_(:,ti) = y;               % DEBUG                   
            Ya_(:,ti) = ya;               % DEBUG                   
            %dQ_(tn) = norm(dQ,2);       % DEBUG
            %dW_(tn) = norm(dW,2);       % DEBUG
        end
        
%         %{
        if (1 == debug_mode.sparse)
            figure(99);
            subplot 211
            imagesc(anf); colorbar;
            hold on
            contour( ti+(1:slide_win.smp)-1, (1:net.cochlea.Nfb)'-1, reshape_s_mt(xs), 'r');
            hold off

            subplot 212
            %plot(f_v, [Qs*xs, theta, theta_a, phi( Qs*xs + W*y - theta )], '.-');
            plot(f_v, [Qs*xs, theta, theta_a], '.-');
            hold on
            plot(f_v, [y, ya, ya2], 'linewidth', 2);            
            partials = nonzeros(harmonics2.*[1:net.N_hrm]*F02');
            plot(repmat(partials',2,1), repmat([0,1]',1,length(partials)), '--r');
            hold off
            %legend('Qs*xs', '\theta', '\theta_a', 'phi()', 'y', 'y_a'); 
            legend('Qs*xs', '\theta', '\theta_a', 'y', 'y_a', 'y_{a2}'); 
            title(sprintf('f_0: %.1f Hz', F02));
            xlabel('freq. [Hz]');
            axis([0, 1.4*max(partials), -0.2, 1.0]);
            
            
            pause(0.1);
            drawnow;
        end
        %}
        
    end
  
    
    % **************************************************************  
    % training:
    % **************************************************************
    if (1 == net.training_flag) && (net.learn_every)        
        %[ W, ~, ~, dW ] = Foldiak_Training( W, [], anf(:), ya2, alpha, 0, 0, 0, pbits, flags );
        dW = -alpha*(ya2*ya2' - pbits);
        dW( dW > 0 ) = 0;   % make it all-negative only
        dW = dW - diag(diag(dW));   % set wij == 0
        W = W + dW;     % updatethe feedforward matrix

    end
    
    
     if  (1 == net.training_flag) && ~mod(tn-1,10)
         
        figure(51)
        subplot 121
        imagesc(f_v,f_v,W); colorbar
        xlabel('Hz');
        ylabel('Hz')
        subplot 122
        imagesc(f_v,f_v,dW); colorbar
        xlabel('Hz');
        ylabel('Hz')
        subplot 122
        grid on;
         
        drawnow;
     end
        
     if  (1 == net.training_flag)               
        fprintf('--> training mode! don''t perform any estimation.\n');
        continue;
     end
    
    
    % *** DEBUG ***
    %{
    if (1 == debug_mode.sparse)
        figure(99);
        subplot 211
        imagesc(anf); colorbar;
        hold on
        contour( ti+(1:slide_win.smp)-1, (1:net.cochlea.Nfb)'-1, reshape_s_mt(xs), 'r');
        hold off

        subplot 212
        %plot(f_v, [Qs*xs, theta, theta_a, phi( Qs*xs + W*y - theta )], '.-');
        plot(f_v, [Qs*xs, theta, theta_a], '.-');
        hold on
        plot(f_v, [y, ya], 'linewidth', 2);            
        partials = nonzeros(harmonics2.*[1:net.N_hrm]*F02');
        plot(repmat(partials',2,1), repmat([0,1]',1,length(partials)), '--r');
        hold off
        %legend('Qs*xs', '\theta', '\theta_a', 'phi()', 'y', 'y_a'); 
        legend('Qs*xs', '\theta', '\theta_a', 'y', 'y_a'); 
        title(sprintf('f_0: %.1f Hz', F02));
        xlabel('freq. [Hz]');
        %axis([0, 1.4*max(partials), -0.2, 1.0]);


        pause(0.1);
        drawnow;

     end
    %}
    
    
            figure(101)
            plot(f_v, [ya2, y_pr]); grid on
            hold on
            plot(linspace(0,net.stim.Fs,Ny), abs(fft(stim2,Ny)))
            hold off
            axis([0 5e3, 0, 1.3])           
            
            drawnow;
            pause(0.1);
    
            
    %
    y_pr = max(0, ya2 - theta_a2 );
    y_pr = y_pr/max(y_pr(:));

    
    %%
    % **************************************************************  
    % Estimate pitch:
    % **************************************************************
    %[~, pitch_est, pitch_opt] = PitchEst_SC( y_pr, base_db.f_v, 0.05, 0 );
    
    
%     %{ 
    
    y_pr_itr = interp1( 1:Ny, y_pr, linspace(1,Ny,Nyp) )';
        
    yp = Qp * y_pr_itr;
    
    % locate peaks:
    %[pitch_salience, pitch_loc] = findpeaks(yp,'MinPeakProminence',0.2*max(yp), 'NPeaks', 8, 'SortStr', 'descend');
    [pitch_salience, pitch_loc] = findpeaks(yp, 'NPeaks', 4, 'SortStr', 'descend');
    
    [~,Ix_p] = max(pitch_salience);
    
    % pitch options:
    pitch_opt = fff_yp(pitch_loc);
    
    % pitch estimation:
    pitch_est = pitch_opt(Ix_p);
    
    %}
    
    
    
    
    
    %%
    
    % save the results into the database:
    tst_db.F02(tn)          = F02;
    tst_db.harmonics2(tn,:) = harmonics2;
        
    if ~isempty(dF2)
        tst_db.dF2(tn) = dF2;
    end
    tst_db.pitch_est(tn) = pitch_est;
%         tst_db.pitch_est_(tn) = pitch_est_;  % *** DEBUG ***
    tst_db.pitch_opt{tn} = pitch_opt;
    tst_db.err_est(tn)   = abs(pitch_est-F02);
    esterrprod(tn) = F02/pitch_est;

    %
    fprintf('--> pitch_est:                     %.1f Hz\n', pitch_est);
    fprintf('--> est. error:                    %.1f Hz\n', tst_db.err_est(tn));
    fprintf('--> est. prod. error:              %.1f \n', esterrprod(tn));  

    
    % plot results on-screen:
    if  (1 == debug_mode.plot) && ~mod(tn-1,10)      %~mod(tn-1,fix(Nepoch/10))        
        figure(30)
        plot(tst_db.F02, tst_db.pitch_est, '.', 'markersize', markersize);
        %hold on;
        %plot([0, net.F0_end], [0, net.F0_end], '--k', 'linewidth', 2)
        %plot([0, net.F0_end], [0, 2*net.F0_end], '--k')
        %plot([0, net.F0_end], [0, 4*net.F0_end], '--k')
        %plot([0, net.F0_end], [0, 1/2*net.F0_end], '--k')
        %plot([0, net.F0_end], [0, 1/3*net.F0_end], ':k')
        %plot([0, net.F0_end], [0, 1/4*net.F0_end], '--k')
        %hold off;
        grid on;
        xlabel('f_L [Hz]');
        ylabel('Estimations [Hz]');
        title('Pitch Estimations');
        %axes([min(tst_db.F02), max(tst_db.F02), net.F0_start, 500]);    
        
        drawnow;

        %}
                
        %fprintf('--> pitch_salience:           1e3* [ ');fprintf('%.1f ', 1e3*pitch_salience); fprintf('] \n');    
        fprintf('--> pitch_opt/F02:                 [ ');fprintf('%.1f ', pitch_opt/F02); fprintf('] \n');
        fprintf('--> pitch_opt/(F02/esterrprod):    [ ');fprintf('%.1f ', pitch_opt/(F02/esterrprod(tn))); fprintf('] \n');
        fprintf('--> misses_: %d out of tn: %d ==> ratio: %.1f \n', misses_, tn, misses_/tn);
        

    end
    
    
end

 

%% plot results:

if ~(1 == net.training_flag)               

    figure(30)
    plot(tst_db.F02, tst_db.pitch_est, '.', 'markersize', markersize);
    set(gca, 'fontsize', fontsize);
    hold on;
    plot([0, net.F0_end], [0, net.F0_end], '--k', 'linewidth', 2)
    plot([0, net.F0_end], [0, 2*net.F0_end], '--k')
    plot([0, net.F0_end], [0, 4*net.F0_end], '--k')
    plot([0, net.F0_end], [0, 1/2*net.F0_end], '--k')
    plot([0, net.F0_end], [0, 1/3*net.F0_end], ':k')
    plot([0, net.F0_end], [0, 1/4*net.F0_end], '--k')
    hold off;
    grid on;
    xlabel('Stimulus Pitch [Hz]');
    ylabel('Pitch Estimation [Hz]');
    stim_partials = nonzeros(harmonics2.*[1:net.N_hrm]);
    title( ...
        sprintf( 'Pitch Estimations of %d Epoches, ANF(%d) , Complex Harmonics (%d-%d)\n D_{num}: %d, N_{fb}: %d', ...
        Nepoch,...
        anf_method,...
        stim_partials(1), stim_partials(end),...
        net.dic.dim,...
        net.cochlea.Nfb ...
        ) ...
    )

end













