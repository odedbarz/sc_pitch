function [ Sp, pitch_est, pitch_opt ] = PitchEst( h, freq, sig, debug_mod )
%
% Input:
%   
%
% Output:
%
% Desciption:
%
%
% Notes:
% * You must have: length(h) == length(freq)
%
    
    % interpulation factor:
    interp_factor = 10;
    
    N_f = length(freq);    
    
    freq = interp1( 1:N_f, freq, 1:1/interp_factor:N_f );
    h = interp1( 1:N_f, h, 1:1/interp_factor:N_f )';
    
    N_f = length(freq);

    % plausible pitches vector:
    Sp = zeros(N_f, 1);
    
    % energy concentration:
    Es = cumsum(h.^2)/sum(h.^2);
    

    sieve = zeros(1,N_f);   % init. the sieve response (freq. domain)
    
    
    for ii = 1:N_f
        
        %sig = -5*log2( freq(ii)/freq(end) );
        %sig_ii = sig*freq(ii);
        %sig_ii = sig*freq(1);
        sig_ii = min(10*sig*freq(1), sig*freq(ii));
        
        sieve = 0*sieve;   % init. the sieve response (freq. domain)
        
        nn = 0;	% init. the harmonic counter
        
        % create the sieve:
        for jj = ii:N_f
            
            if freq(end) <= nn*freq(ii)  % stop if: the next first gaussian is bigger than the possible frequency
                break;
            elseif Es(jj) > 0.95   % stop if: most of the signal was analyzed
                break;
            else 
                nn = nn+1;
            end
            
            sieve = sieve + gaussmf( freq, [sig_ii, nn*freq(ii)]);
            
            %sieve = sieve + interp1(1:N_f,gaussmf( freq, [sig_ii, nn*freq(ii)]),1:1/interp_factor:(N_f+1-1/interp_factor));
            
            %freq_ = interp1(1:N_f,freq,1:0.1:N_f);
            %win = gaussmf( freq_, [sig_ii, nn*freq_(10*ii)]);
            %sieve = sieve + win(1:N_f);
            
        end
        
%                 plot(freq, [h, sieve']);
%                 freq(ii)

                
        Sp(ii) = sieve*h;
        
    end
    
    
    %Sp = Sp/max(Sp);
    Sp = max(0,Sp);
    Sp = Sp/sum(Sp);
    
    
    [pitch_salience, pitch_loc] = findpeaks(Sp,'MinPeakProminence',0.2*max(Sp), 'NPeaks', 8, 'SortStr', 'descend');
    
    [~,Ix_p] = max(pitch_salience);
    
    % pitch options:
    pitch_opt = freq(pitch_loc);
    
    % pitch estimation:
    pitch_est = pitch_opt(Ix_p);
    
    
    
    % debug mode:
    if exist('debug_mod', 'var') && (debug_mod)
        figure(98);
        subplot 211
        plot(freq, h);
        title('The Sparse Code')
        xlabel('Hz');
        ylabel('SC Coef.');
        grid on;
        
        subplot 212
        plot(freq, Sp);
        hold on;
        plot(freq(pitch_loc), pitch_salience, 'xr', 'markersize', 3, 'linewidth', 14);
        hold off;
        grid on;
        axis([freq(1), max(500,1.2*max(pitch_opt)), min(0,1.2*min(Sp)), 1.2*max(Sp)]);
        title(sprintf('Pr(pitch), max at f_{est} = %.1f Hz', pitch_est));
        xlabel('Hz');
        ylabel('S_p');
    end
    
end

