%
% main_createDict
%
% Creates the dictionaries & save them
%

clc


for ii = 1:5
    
    % Init. the database:
    net = CreateDB;
    
    % Set the ANFs' lower threshold:
    net.anf.thr = (ii-1)*0.2;
        
    fprintf('-> net.anf.thr: %g\n', net.anf.thr);
    
    
    % create the bases' database:
    Build_Base;

    %save('..\data\ws_anfmth(10)_thr(08).mat');
    save( sprintf('..\\data\\ws_anfmth(10)_thr(0_%d)_dB(%d).mat', fix(10*net.anf.thr), net.stim.amp_dBSPL) );

    
end