% 
% CreateTarget.m
% 

% [pitch_SACF, sal_SACF] = SACF(net, F0, harmonics, 0);
pitch_SACF = 0;
sal_SACF = 0;

        
%% ft
ft = zeros(net.n_samples, net.z_d1);
ft_value = zeros(net.z_d1, 1);

%         ft_win = hamming(net.cochlea.n_samples);

for ii = 1:net.z_d1
    
    if length(net.neu_std) == net.z_d1
        neu_std = net.neu_std(ii);
    else
        neu_std = net.neu_std;
    end
    
    ft_value(ii) = NeuSel( F0, net.ft_cf(ii), neu_std, net.ft_amp );  
    
    ft(:,ii) = ft_value(ii);

    
end


%     % AMPLITUDE!
%     ft_win = tukeywin(net.n_samples, 0.1);
%     ft = ft .* ft_win(:,ones(1,4));


% network's target (ft):
ft_fid = fopen( net.ft_filename, 'w');
count = fwrite(ft_fid, ft, net.partype);
if length(ft(:)) ~= count
    error('!!! Error: Failed to write input vector !!!\n');
end
fclose(ft_fid);





