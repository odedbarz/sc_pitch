%
% ML_accuracy.m
%
% Claculates the error of the ML decoding.
%
% Input:
%   N_curves:   [1x1] # of selectivity curves.
function [err1, err2, ft_cf] = ML_accuracy( N_curves, ft_cf_ )

    global fff ft_cf N_curves_ selectivity     % it's quick & dirty

    if nargin < 3
        f_start = 100;      % [Hz]
        f_end   = 1000;     % [Hz]
    end

    L_fff = 100;

    % the interval:
    f_start     = 100; %100;      % [Hz]
    f_end       = 1000;     % [Hz]
    
    if nargin <2
        ft_cf       = [200, 800]';
        %ft_cf       = linspace(f_start, f_end, N_curves)'; 
    else
        ft_cf = ft_cf_;
    end
    ft_cf = ft_cf(:);   % column vector
    
    neu_std      = 300;          % [Hz] assumung same std for all selectivity curves
    ft_amp      = 1.2;          % max gain of the selectivity curves    
    N_curves_   = N_curves;
    fff         = linspace(f_start, f_end, L_fff)';
    err1         = zeros(L_fff,1);
    err2         = zeros(L_fff,1);

    if length(neu_std) == 1
        neu_std = neu_std*ones(1, N_curves_);
    end
    
    rates = zeros(N_curves_,1);
    
    for ii = 1:L_fff
        
        for jj = 1:N_curves
            % N_curves selectivity curves taken in fff(ii) Hz: 
            rates(jj) = NeuSel(fff(ii), ft_cf(jj), neu_std(jj), ft_amp);
        end

        % The estimated frequency (pitch):
        [p_est1, p_est2] = ChkEst( rates, ft_cf, neu_std, ft_amp );

        % note that the error should be zero for e perfect decoding,
        % becuase there is no additive noise:
        err1(ii) = abs( p_est1 - fff(ii) )/fff(ii);
        err2(ii) = abs( p_est2 - fff(ii) )/fff(ii);
    end

    % just to plot the selectivity curves:
    selectivity = zeros(L_fff,length(ft_cf));
    for ii = 1:length(ft_cf)
        selectivity(:,ii) = NeuSel(fff, ft_cf(ii), neu_std(ii), ft_amp);
    end
    
    fig_num = 10;
    PlotErr(err1, err2, fig_num);

    fprintf('-> p_est1 max. error: %g\n', max(err1));
    fprintf('-> p_est2 max. error: %g\n', max(err2));
    
end

%{
% 
% Creates neural selectivity curves according to a given policy 
% (I chose a Gaussian curve).
%
function s = NeuSel(f, ft_cf, alpha, ft_amp)

    if 4 > nargin 
        ft_amp = 1.0;
    end

    % option 0:
    % theta = alpha* (2*pi.*(f-ft_cf));
    % s = cos(theta);

    % option 1:
    % theta = alpha* (2*pi.*(f-ft_cf));
    % s = max( 0.0, cos(theta) );

    % option 2:
    % s = hamming(length(f-ft_cf))'.*(f-ft_cf);

    % option 3:
    % s = sign( cos(theta) );

    % option 4, A*exp(...):
    s = ft_amp * exp( -0.5*( (f-ft_cf)./alpha ).^2 );

end
%}

% 
% function p = ChkEst( rates, ft_cf, ft_amp )
% 
% Decoding network's preferred pitch from its readouts.
% 
% ft_cf: a vector of the peaks of the selectivity curves.
% ft_amp: the maximum value of the selectivivty curves.
%
function [p1, p2] = ChkEst( rates, ft_cf, neu_std, ft_amp )
    
    global N_curves_;

    if size(rates) ~= size(ft_cf), error('ERROR in ChkEst: size(rates) ~= size(ft_cf)');  end
    if nargin <= 3, ft_amp = 1.0; end    
    if length(rates) ~= N_curves_, error('ERROR in ChkEst(): length(rates) ~= N_curves_, CHANGE it'); end
        
    % normalize rates:
    %rates = rates/ft_amp;   % normalize the weights

    % ML:
    % * assuming poisson noise with LOTS of gaussian selectivity curves
    p1 = rates' * ft_cf ./ sum(rates);

    % ML:
    % * assuming poisson noise with a FEW gaussian selectivity curves
    % * assuming SAME STD for all curves
    
%{    
    syms f0;
    %fa = @(x,m,s) ft_amp * exp( -0.5*( (x-m)/s )^2 );    
    fa = @(x,m,s) NeuSel(x, m, s, ft_amp);
    %ml_i = @(x,n) (ft_cf(n)-x)*( rates(n) - fa(x,ft_cf(n),neu_std(1)) );
    ml_i = @(x,n) ((ft_cf(n)-x)/neu_std(n)^2)*( rates(n) - fa(x,ft_cf(n),neu_std(n)) );

    ml_fun = @(x) ...
        ml_i(x,1) + ...
        ml_i(x,2) + ...
        ml_i(x,3); % + ...
%         ml_i(x,4) + ...
%         ml_i(x,5) + ...
%         ml_i(x,6); % + ...
%         ml_i(x,7) + ...
%         ml_i(x,8) + ...
%         ml_i(x,9) + ...
%         ml_i(x,10) + ...
%         ml_i(x,11) + ...
%         ml_i(x,12) + ...
%         ml_i(x,13) + ...
%         ml_i(x,14) + ...
%         ml_i(x,15) + ...
%         ml_i(x,16) + ...
%         ml_i(x,17) + ...
%         ml_i(x,18) + ...
%         ml_i(x,19) + ...
%         ml_i(x,20);
%} 

    ml_fun = aux_ml(rates, ft_cf, neu_std, ft_amp);

    % solve for the estimated pitch frequency f0 (ML)
    % * init. freq. = 500 was set arbitrarily
    p2 = fzero( ml_fun, 500 );   
    
    
end


function ml_fun = aux_ml(rates, ft_cf, neu_std, ft_amp)
    %syms f0;
    %fa = @(x,m,s) ft_amp * exp( -0.5*( (x-m)/s )^2 );  
    
    fa = @(x,m,s) NeuSel(x, m, s, ft_amp);
    
    ml_i = @(x,n) ((ft_cf(n)-x)/neu_std(n)^2)*( rates(n) - fa(x,ft_cf(n),neu_std(n)) );

    ml_fun = @(x) 0;
    for ii = 1:length(ft_cf)
        ml_fun = @(x) ml_fun(x) + ml_i(x,ii);
    end
    


end



%
% function PlotErr(err, fig_num)
%
% plot the results on screen:
%
function PlotErr(err1, err2, fig_num)

    global fff ft_cf N_curves_ selectivity;

    figure(fig_num);
    set(gca, 'fontsize', 20);
    set(gca, 'linewidth', 2);

    h1 = plot(fff, 100*[err1 err2], '.--', 'linewidth', 4, 'markersize', 20);
    
    hold on
    h2 = plot(ft_cf, 10 ,'vk', 'markerfacecolor', 'k', 'markersize', 12);
    h3 = plot(fff, 100*selectivity/max(selectivity(:)), ':', 'linewidth', 3 );
    h4 = plot(fff, 100*sum(selectivity,2)/max(sum(selectivity,2)), '--', 'linewidth', 3 );
    hold off

    axis([fff(1), fff(end), -0.1, 100]);

    xlabel('pitch [Hz]', 'interpreter', 'latex')
    ylabel('$100*|\frac{f_{est}-f}{f}|$', 'interpreter', 'latex')
    title(sprintf('ML error estimation, N_{curves}=%d', N_curves_));
    grid on;
    drawnow;

    lgnd_hnd = legend(...
        [h1', h2(1), h3(1), h4],...
        '$|f_0-\hat{f}_0|_{[\%]}$ ($\Sigma{f(s)}\approx$Const)',...
        '$|f_0-\hat{f}_0|_{[\%]}$ (full est.)',...
        'Gaussian means',...
        'selevtivity curves',...
        'sum of selevtivity curves'...
    );
    set(lgnd_hnd, 'interpreter', 'latex', 'fontsize', 18);
    
end





