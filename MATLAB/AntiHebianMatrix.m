function  W = AntiHebianMatrix( fff_dic, D_dim2 )

    W = zeros(D_dim2,D_dim2);     % feedback matrix connections
    f_erb = ERB(1e-3*fff_dic);  

    
    for ii = 1:(D_dim2-1)
        Ix_w = logical( (fff_dic > fff_dic(ii)) .* (fff_dic < fff_dic(ii) + f_erb(ii)) );

        w0 = find(Ix_w,1,'first');
        w1 = min( D_dim2, find(Ix_w,1,'last') );
        if isempty(w0) || isempty(w1) || (w0==w1)
            continue;
        end
        w_v = w0:w1;
        W(ii,w_v) = (+1) * (1 - (w_v-w0)/(w1-w0));

    end

    W = (W+W')/2;    


end

