%
% Concatenated_D.m
%



% dictionary_files = {'dic_anf(4)_hrm(1,1)_atoms(500)_Nfb(200)_dicFs(25kHz)_ms(15)',...
%     'dic_anf(4)_hrm(1,4)_atoms(500)_Nfb(200)_dicFs(25kHz)_ms(15)'};
dictionary_files = {...
    'D1_anf(10)_tri_hrm(1-4)_SPL(30)_atoms(500)_Nfb(200)_dicFs(25kHz)_ms(15)',...
    'D2_anf(10)_hrm(1-1)_SPL(30)_atoms(500)_Nfb(200)_dicFs(25kHz)_ms(15)',...
    'D3_anf(10)_tri_hrm(3-6)_atoms(500)_Nfb(200)_dicFs(25kHz)_ms(15)'...
    ... 'D4_anf(10)_tri_dichrm(1-6)_atoms(500)_Nfb(200)_dicFs(25kHz)_ms(15)',...
    ... 'D5_anf(10)_ones_dichrm(1-3)_atoms(500)_Nfb(200)_dicFs(25kHz)_ms(15)',...
    ... 'D6_anf(10)_hrm(1-1)_atoms(1000)_Nfb(200)_dicFs(25kHz)_ms(15)',...
    ... 'D7_anf(10)_dic(10%)_dichrm(1-1)_atoms(500)_Nfb(200)_dicFs(25kHz)_ms(15)',...
    ... 'D8_anf(10)_dic(5%)_dichrm(1-1)_atoms(500)_Nfb(200)_dicFs(25kHz)_ms(15)'...
    };


% mysparse = @(x) sparse(x); 
mysparse = @(x) x;      fprintf('-> mysparse is inactivated !\n');

D_dim1 = 0;
D_dim2 = 0;
D = [];
base_db.fff_dic = [];

for ii = 1:length(dictionary_files)
    dummy = load(['..\\data\\', dictionary_files{ii}]);
    
    if 1 == ii
        net             = dummy.net;
        base_db         = dummy.base_db;
        cochlea_freq    = dummy.cochlea_freq;
    end
    
    D = [D, dummy.base_db.D];
    D_dim1 = D_dim1 + dummy.base_db.D_dim1;
    D_dim2 = D_dim2 + dummy.base_db.D_dim2;
    amp_sBSPL(ii) = dummy.net.stim.amp_dBSPL;        % [dB SPL] stimulus level
    stim_noise_level(ii) = dummy.net.stim.noise_level;     % [0,1] Set the amount of added white noise in the stimulus, noise_level*[SNR]
    D_thr(ii)     =  dummy.net.anf.thr;
    base_db.fff_dic(ii,:) = dummy.fff_dic;
end

% fff_dic         = dummy.fff_dic;


    net.dic.dim2        = D_dim2;   % '*** dim2 = 3*500 ***'     % '# of "atoms" in the dictionary
    net.dic.dim1        = net.cochlea.Nfb * net.dic.time_smp;     % # of "word" in the dictionary    

  
base_db.D_dim1          = D_dim1;  % # of dictionaries (bases)
base_db.D_dim2          = D_dim2;  % # of dictionaries (bases)
% base_db.fff_dic         = fff_dic;
base_db.D               = mysparse(D);
base_db.thr             =  D_thr;
base_db.amp_sBSPL       = amp_sBSPL;        % [dB SPL] stimulus level
base_db.stim_noise_level= stim_noise_level;      % [0,1] Set the amount of added white noise in the stimulus, noise_level*[SNR]

net.hs.G1 = [];

clear dummy

% save( [net.data_dir, sprintf('Concatenated_D')] );

