

clc

net_sacf = net;
net_sacf.secs               = 0.100; % [ms] Meddis & O'Mard
net_sacf.stim.n_samples     = floor( net_sacf.secs * net_sacf.stim.Fs);
net_sacf.cochlea.n_samples  = net_sacf.stim.n_samples;
% net_sacf.dic.Fs = net_sacf.stim.Fs;
net_sacf.dic.start_smp      = 1;
net_sacf.dic.stop_smp       = net_sacf.secs*net_sacf.dic.Fs; 

sacf_pitch_v    = zeros( N_runs, 4 );
sacf_salience_v = zeros( N_runs, 4 );

N_runs = length(F0_tst);

% to do:
% * missing raise time (sine)
% * cut the time to be a complete periodic length of the stimulus

for tn = 1:N_runs
    fprintf('\n-> [run_SACF.m]: tn = %d / %d\n', tn, N_runs);
    
    if length( F0_tst ) > 1     % for a batch of test frequencies:
        F0_n = F0_tst(tn);
    else
        F0_n = F0_tst;
    end

    ttt = linspace(0, net_sacf.secs, net_sacf.stim.n_samples)'; 
    
    harmonics2 = harmonics2_;
    harmonics2( F0_n*[1:net_sacf.N_hrm].*harmonics2_> net_sacf.cochlea.highfreq ) = 0;
    
    stim2 = harmonics2 * sin( 2*pi* F0_n*(1:net_sacf.N_hrm)' *ttt' );     
    stim2 = stim2(:);
    
    anf = CreateANF( net_sacf, stim2, Hd_ear, fcoefs, cochlea_freq );        

%     sacf_correlogram = CorrelogramFrame(anf, size(anf,2), 1);
%     [sacf_pitch_v(tn), sacf_salience_v(tn)] = CorrelogramPitch_v02(sacf_correlogram(:), size(anf,2), net_sacf.dic.Fs );
    [sacf_pitch_v(tn,:), sacf_salience_v(tn,:)] = SACF( anf, net.dic.Fs );

    fprintf('-> [run_SACF.m]: F0 = %g\n', F0_n);
    fprintf('-> [run_SACF.m]: F0 sacf = %g\n', sacf_pitch_v(tn,1));
    fprintf('-> [run_SACF.m]: salience = %g\n', sacf_salience_v(tn,1));
    
    if ~mod(tn-1,10)
        plot(F0_tst(1:tn), sacf_pitch_v(1:tn), '.', 'markersize', 24);
        hold on;
        plot([125 5e3], 2*[125, 5e3], '--k', 'linewidth', 2);
        plot([125 5e3], [125, 5e3], '--k', 'linewidth', 2);
        plot([125 5e3], 0.5*[125, 5e3], '--k', 'linewidth', 2);
        hold off
        grid on    
    end
    
end



%%
figure(11);
plot(F0_tst, sacf_pitch_v, '.', 'markersize', 24);
hold on;
plot([125 5e3], 2*[125, 5e3], '--k', 'linewidth', 2);
plot([125 5e3], [125, 5e3], '--k', 'linewidth', 2);
plot([125 5e3], 0.5*[125, 5e3], '--k', 'linewidth', 2);
hold off
grid on
xlabel('f_L [Hz]')
ylabel('f_{est}')
title('SACF Pitch Estimation')

figure(12);
plot(F0_tst, sacf_salience_v, '.', 'markersize', 24);
xlabel('f_L [Hz]')
grid on
title('SACF Pitch Salience')



