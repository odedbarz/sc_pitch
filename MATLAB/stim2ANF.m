%
% stim2ANF.m
%



% Init. the stimulus matrix:
stim = 0*stim;

% Onset:
stim(stim_win.onset,1) = 1;
% stim(stim_win.onset,1) = win_fun(length(stim_win.onset));
% stim(stim_win.onset(1):stim_win.win2(end),1) = 1;

% Stimulus
switch net.stim_type

    case 1
        % Creating F1:
        [stim1, F1, harmonics1, phase1, dF1] = CreateIO(net, [], harmonics, [], 0); %, F0, harmonics, dF); %, 0);       

        % Creating F2:
        [stim2, F2, harmonics2, phse2, dF2] = CreateIO(net, [], harmonics, [], 0); %, F0, harmonics, dF); %, 0);       

        
        % Constant stimuli:
        stim(stim_win.win1,2) = (F1)/(net.F0_end) .* win_fun(stim_win.win_length); 

        stim(stim_win.win2,2) = (F2)/(net.F0_end) .* win_fun(stim_win.win_length); 

        
    case 2
        
        if net.training_flag
            F1 = 0;
            F2 = inf;     % inf\0
            while (abs(F1-F2) >= net.stim_margin)    % && (abs(F1-F2) <= 0.1))

                % Creating F1:
                [stim1, F1, harmonics1, phase1, dF1] = CreateIO(net, [], harmonics, [], 0); %, F0, harmonics, dF); %, 0);       

                % Creating F2:
                [stim2, F2, harmonics2, phase2, dF2] = CreateIO(net, [], harmonics, [], 0); %, F0, harmonics, dF); %, 0);       

            end
            
        else
            
            % Creating F1:
            [stim1, F1, harmonics1, phase1, dF1] = CreateIO(net, [], harmonics, [], 0); %, F0, harmonics, dF); %, 0);       

            % Creating F2:
            [stim2, F2, harmonics2, phase2, dF2] = CreateIO(net, [], harmonics, [], 0); %, F0, harmonics, dF); %, 0);       
            
        end
        
        
        % 1 sine stimuli:
        stim(stim_win.win1,2) = stim1(1:stim_win.win_length) .* win_fun(stim_win.win_length);  
        
        % stim(stim_win.win2) = (F2)/(net.F0_end);
        stim(stim_win.win2,2) = stim2(1:stim_win.win_length) .* win_fun(stim_win.win_length);  

        % * input 2\onset
        %stim(stim_win.win2,1) = win_fun(stim_win.win_length);  
        
        
        % Set the overall gain:
        stim = net.stim_amp * stim;
 
        % check stuff:
        if 2 ~= size(Jin,2)
            error('--> Error: net.stim_type == 2 requires: size(Jin,2) == 2 !');
        end
        
        % <stim> + white NOISE
        stim = stim + net.stim_noise_level * randn( size(stim) );
        
        
        % stimulus --> ANFs:
        anf = resample(stim, 1, net.cochlea.D_sample);
        anf = net.anf_max_amp * anf;    % stimulus gain
        anf = anf';     % set the anf's dimensions for the FORCE algo.
        
        
        
    case 3
        % Creating F1:
        [stim1, F1, harmonics1, phase1, dF1] = CreateIO(net, [], harmonics, [], 0); %, F0, harmonics, dF); %, 0);       

        % Creating F2:
        [stim2, F2, harmonics2, phse2, dF2] = CreateIO(net, [], harmonics, [], 0); %, F0, harmonics, dF); %, 0);       

        
        % first sine stimuli:
        stim(stim_win.win1,2) = stim1(1:stim_win.win_length) .* win_fun(stim_win.win_length);  

        % second sine stimuli:
        stim(stim_win.win2,2) = stim2(1:stim_win.win_length) .* win_fun(stim_win.win_length);  

        % Set the overall gain:
        stim = net.stim_amp * stim;

        % half-rectify:
        stim = max(0, stim);
            
        
        % stimulus --> ANFs:
        anf = resample(stim, 1, net.cochlea.D_sample);
        anf = net.anf_max_amp * anf;    % stimulus gain
        anf = anf';     % set the anf's dimensions for the FORCE algo.
        
        
    case 4
        % stim #1:
        F1 = round( (net.F0_end - net.F0_start)*rand(1) + net.F0_start );        
        stim1 = sin( linspace(0, pi*F1, stim_win.win_length) );
        stim(stim_win.win1,2) = stim1;  

        % stim #2:
        F2 = round( (net.F0_end - net.F0_start)*rand(1) + net.F0_start );        
        stim2 = sin( linspace(0, pi*F2, stim_win.win_length) );
        stim(stim_win.win2,2) = stim2;  
        
        
        % stimulus --> ANFs:
        anf = resample(stim, 1, net.cochlea.D_sample);
        anf = net.anf_max_amp * anf;    % stimulus gain
        anf = anf';     % set the anf's dimensions for the FORCE algo.
        
        
    case 5
          
        %{
        if net.training_flag
            F1 = 0;
            F2 = inf;     % inf\0
            while (abs(F1-F2) >= net.stim_margin)    % && (abs(F1-F2) <= 0.1))

                % Creating F1:
                [stim1, F1, harmonics1, phase1, dF1] = CreateIO(net, [], harmonics1_, [], 0); %, F0, harmonics, dF); %, 0);       

                % Creating F2:
                [stim2, F2, harmonics2, phase2, dF2] = CreateIO(net, [], harmonics2_, [], 0); %, F0, harmonics, dF); %, 0);       

            end
            
        else
        %}
        
            % Creating F1:
%             F1 = 300 + 10*randn(1);
            [stim1, F1, harmonics1, phase1, dF1] = CreateIO(net, [], harmonics1_, dF1, 0); %, F0, harmonics, dF); %, 0);       

            % Creating F2:
%                     F0_end          = net.F0_end;
%                     F0_start        = net.F0_start;
%                     net.F0_end      = 220;
%                     net.F0_start    = 180;
            [stim2, F2, harmonics2, phase2, dF2] = CreateIO(net, [], harmonics2_, [], 0); %, F0, harmonics, dF); %, 0);       
%                     net.F0_end      = F0_end;
%                     net.F0_start    = F0_start;
            
%         end
        
        
        % Add stimulus #1:
        stim(stim_win.win1,2) = stim1(1:stim_win.win_length) .* win_fun(stim_win.win_length);        
        
        % Add stimulus #2:
        stim(stim_win.win2,2) = stim2(1:stim_win.win_length) .* win_fun(stim_win.win_length);  
        
        
        % Set the overall gain:
        stim = net.stim_amp * stim;
        
        % <stim> + white NOISE
        stim = stim + net.stim_noise_level * randn( size(stim) );
        
        
        % stimulus --> ANFs:
        anf = CreateANF( net, stim(:,2), Hd_ear, fcoefs );        
        
        % Replace the first (highest freq.) ANF with the onset:
        anf(1,:) = resample(stim(:,1), 1, net.cochlea.D_sample);
        anf(1,:)  = net.anf_max_amp * anf(1,:);    % stimulus gain
        
        
        % check stuff:
        if size(anf,1) ~= size(Jin,2)
            error('--> Error: net.stim_type == 5 requires: size(anf,1) ~= size(Jin,2) !');
        end
        
        
end




if (net.USE_CUDA)
    % Save signal input vector to the hard-disk:
    save( net.stim_filename, 'stim', '-ascii');
end





