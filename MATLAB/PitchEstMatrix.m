function [ Qp, yp_freq, est_win ] = PitchEstMatrix( freq, Ny, sieve_sigma, gaussLPF, interp1factor, upper_pitch_limit, sparse_flag )
%
% Input:
%   * h: the sparse representation vector   
%   * freq: the appropriate frequencies of the sparse vector
%   * sieve_sigma: the sieves' sigma
%   * gaussLPF: [mean, sig], the two moments of the Gaussian LPF
%   * debug_mod: debug mode flag
%
% Output:
%   * Qp: the matrix for the sieve (the Hebbian section).
%
%
% Desciption:
%   Calculates the possible pitches for a given sparse representation vector
%   h.
%
% Notes:
% * You must have: length(h) == length(freq)
%

   
    % interpulation factor:
    %interp1factor = 10;
    
    %dummy = length(freq);    
    
    %freq = interp1( 1:dummy, freq, 1:1/interp_factor:dummy );
    yp_freq = interp1( 1:Ny, freq, linspace(1,Ny,interp1factor*Ny) );
    
    % 
    if ~isempty(upper_pitch_limit)
        Ix_max = find(yp_freq > upper_pitch_limit, 1);
        if ~isempty(Ix_max)
            Nyi = Ix_max;
        else
            Nyi = length(yp_freq);     % Ny after interpulation
        end    
    else
        Nyi = length(yp_freq);     % Ny after interpulation
    end
    yp_freq = yp_freq(1:Nyi);
    
    
    % declare the matrix for the sieve (the Hebbian section):
    %Qp = sparse(zeros(Nyi,N_itr));
    Qp = zeros(Nyi,Nyi);
    
    % Give bigger priority for smaller pitches (LPF):
    if ~isnan(gaussLPF)
        est_win = gaussmf( yp_freq, gaussLPF )';     % estimation window
        %est_win = tukeywin(5e3,0.01);   '**** est_win: using tukeywin instead of gaussmf ****'
    else
        est_win = ones(length(yp_freq),1);
    end
    
%     sieve = zeros(1,Nyi);   % init. the sieve response (freq. domain)
    
    sigma_ = min( 10*sieve_sigma*yp_freq(1), sieve_sigma*yp_freq );
    
%     sigma_ = sieve_sigma .* ones(1,Nyi);   
%     warning('@ PitchEstMatrix(): I have changed it to be const. for all sigma !')
    
    parfor ii = 1:Nyi
        
        %sigma_ii = min(10*sieve_sigma*freq(1), sieve_sigma*freq(ii));
        
        sieve = zeros(1,Nyi);   % init. the sieve response (freq. domain)
        %sieve = 0*sieve;   % init. the sieve response (freq. domain)
        
        %nn = 0;
        
        % create the sieve:
        for jj = 1:(Nyi-ii+1)%ii:Nyi
            %nn = nn +1;
            sieve = sieve + gaussmf( yp_freq, [sigma_(ii), jj*yp_freq(ii)]);   
            
        end
                
        Qp(ii,:) = sieve; % .* est_win;
        %Qp(ii,:) = Qp(ii,:)./sum(Qp(ii,:));
        
    end
    
    
    %Qp = max(0,Qp);                         % remove non-zeros from the estimation
    %Qp = Qp.*repmat(est_win,size(Qp,1),1); 	% priority for low pitches
    %Qp = diag(1./sum(Qp,2))*Qp;             % row-wize normalization
    
    if 1 == sparse_flag
        Qp = sparse(Qp);
    end
    
end


























