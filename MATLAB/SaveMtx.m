%
% function SaveMtx(filename, partype)
% Saves a matrix or a vector as a binary file.
%
function SaveMtx(v, filename, partype)

if 2 >= nargin
    partype = 'double';     % default par type
end

% filename = [filename, '.dat'];

fid = fopen(filename, 'w');

if (-1 == fid)
    error(sprintf('-> ERROR: unable to open file <%s>!!!\n', filename));
end;

count = fwrite(fid, v, partype);

fclose(fid);

if count ~= size(v,1)*size(v,2)
    error('-> SaveMtx(): count ~= size(v). couldn''t save all data to the file !!!');
end
