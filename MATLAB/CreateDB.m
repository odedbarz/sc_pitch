%
% CreateDB_anf4.m
% -----------------
%
% Creates a database function. 
% This function is to be used both by MATLAB & CUDA compilers.
%
%

function net = CreateDB(anf_method)
    
    %net.mysparse = @(X) sparse(X);

    net.N_epoch  = nan;      % # of training epoches
    net.N_epoch2 = nan;       % # of training epoches

    %
    net.training_flag = nan;    % choose between [1]=[training] & [0]=[testing]

      
    % -------------------------------------------------------------------------    
    % system time & sample rate:
    % -------------------------------------------------------------------------       
    % [ms] total stimulus time:
    net.msecs       = 5 + 10.0;      %5.0 + 10.0;    
    net.secs        = 1e-3*net.msecs;
    
    
    % -------------------------------------------------------------------------    
    % stimulus:
    % -------------------------------------------------------------------------
    net.stim.Fs         = 100e3;      % [Hz]
    net.stim.msecs      = net.msecs;          
    net.stim.dt         = 1/net.stim.Fs;
    net.stim.secs       = 1e-3*net.stim.msecs;
    net.stim.amp_dBSPL  = nan;          % [dB SPL] stimulus level
    net.stim.noise_level= nan;          % [0,1] Set the amount of added white noise in the stimulus, noise_level*[SNR]
    net.stim.n_samples 	= floor( net.stim.secs * net.stim.Fs);
    net.stim.L_win     	= net.stim.n_samples;   
%     net.stim.win        = StimulusWindow( net.stim );
    net.stim.win        = StimulusWindow( net.stim, 2 );
%     net.stim.win        = ones(net.stim.L_win,1);   '****  net.stim.win *****'

    
    % Spacial stimulus types:
    % -----------------------
    net.stim.mu         = [0.4, 3.0];      % [0.4, 2.5];     %[0.5, 3];
    net.stim.sigma      = 0.6*[1.0, 1.0];  % 0.6*[1.0, 1.0];   %[0.5, 1];
    net.stim.margin     = 5* 25;    
    
    % * stim.IRN:
    net.stim.IRN.rep     = 1;    % # of repetitions for the IRN. 
    net.stim.IRN.sign    = +1;   % cos +\- 
    net.stim.IRN.Hd_irn  = irn_LPF(net.stim.Fs);
    
    % * stim.Musical COrd (C-major):
    net.stim.musical_cord.F_list = [392, 523.2, 659.2];      % Hz   

    % * stim.SAM:
    net.stim.SAM.m_idx = 1;      % modulation index
    %net.stim.SAM.fc    = nan;    % [Hz] the carrier is just broad-band noise
    net.stim.SAM.A     = 1;      % the carrier (white noise) amplitude
    net.stim.SAM.M     = net.stim.SAM.m_idx * net.stim.SAM.A;    % the modulated amplitude
    %net.stim.SAM.fm_v  = []     % the modulated sine frequency interval

    % * stim.QFM:
    net.stim.QFM.n      = 12;
    net.stim.QFM.m      = 2.55; %2.55;
    net.stim.QFM.F0     = 2000/net.stim.QFM.n;

    % * stim.APh:
    net.stim.APh.phi = 40/180*pi;
            
    % * Oxenham 2004 (TT):
    net.stim.TT.Fc = [4000, 6350, 10080];   % [Hz] carrier freq.            
    net.stim.TT.hrm = 1;  % 1 or 3:5 (see Oxenham's paper)
    
    % Shackelton & Carlyon, The role of resolved and unresolved harmonics in pitch, 94:
    net.stim.Shackelton94.region = 'LOW';   % options: {'LOW', 'MID', 'HIGH'}    
    net.stim.Shackelton94.type = 'SINE';    % options: {'SINE', 'ALT'}
    
    %
    net.N_hrm = 25;
    
    % Training & Testing Policies:
    net.trn_freq_slc    = 0;    % pitch (F0 frequency) selection policy  
    net.trn_hrm_slc     = 13;    % pitch-coloring policy
    net.tst_freq_slc    = 0;    % pitch (F0 frequency) selection policy  
    net.tst_hrm_slc     = 13;    % pitch-coloring policy   
          
    
    % -------------------------------------------------------------------------    
    % choose ANF method:
    % -------------------------------------------------------------------------
    % Options for the <anf_method> field:
    % 1. Heinz model;
    % 2. Roy Patterson's Model (Malcom Slaney's toolbox, version 2, 1998);
    % 3. Lyon Passive Ear model (Malcom Slaney's toolbox, version 2, 1998);
    % 4. Ray Meddis� hair cell model (Malcom Slaney's toolbox, version 2, 1998);
    % 5. Modification (#1) of Ray Meddis� hair cell model (Malcom Slaney's toolbox, version 2, 1998);
    % 10. The model of Zilany et. al. (2014);
    % 20. Miriam, Oded et. al., (2012) MATLAB & Time-Domain;
    % 21. Miriam, Oded et. al., (2012) C++ & Time-Domain;
    if ~exist('anf_method', 'var')
        anf_method = 10;     % set the default method
    end;
    net.anf.method      = anf_method;
    net.anf.max_amp     = 1.0;  % ANF maximum amplitude to the FORCE algorithm
    %net.anf.normalize   = nan;
    net.anf.nomean      = 0;  % remove mean flag
    net.anf.zscore      = 1;    1,            '********** >>>>>   zscore = 1'
    net.anf.thr         = 0.2;  0.2,          '*** net.anf.thr --> 0.2 ***'  % relative ANF threshold
    net.anf.norm_flag   = 1;    % normalization flag: anf <-- anf/max(anf)
    %net.anf.dim1        = net.cochlea.Nfb;
    %if (net.cochlea.n_samples/net.dic.downsmp ~= floor(net.cochlea.n_samples/net.dic.downsmp)), error('-> Error @ [CreateANF.m]: (net.cochlea.n_samples/net.dic.downsmp ~= floor(net.cochlea.n_samples/net.dic.downsmp))\n'); end;
    %net.anf.dim2        = net.cochlea.n_samples/net.dic.downsmp;
    
    % temporary function to set the minimum frequency in the system:
    if (net.anf.method == 10)
         NET_MIN_FREQ = 125;    % Hz
    else
         NET_MIN_FREQ = 50;    ''   % Hz
    end
    
    % Zilany et. al., 2014 Model:
    % ---------------------------
    net.anf.zilany.cohc         = 1.0;      % normal ohc function
    net.anf.zilany.cihc         = 1.0;     	% normal ihc function
    net.anf.zilany.species      = 2;        % 1 for cat (2 for human with Shera et al. tuning; 3 for human with Glasberg & Moore tuning)
    net.anf.zilany.noiseType    = 0;        % 1 for variable fGn (0 for fixed fGn)
    net.anf.zilany.fiberType    = 3;      	% spontaneous rate (in spikes/s) of the fiber BEFORE refractory effects; "1" = Low; "2" = Medium; "3" = High
    net.anf.zilany.implnt       = 0;      	% "0" for approximate or "1" for actual implementation of the power-law functions in the Synapse
    net.anf.zilany.nrep         = 1;        % The number of stimulus repetitions, e.g., 50 (always 1 by default in this implementation). 
    %net.anf.zilany.psthbinwidth = 0.5e-3;   % binwidth in seconds;


    % -------------------------------------------------------------------------    
    % Cochlea:
    % -------------------------------------------------------------------------
    % # of cochlear filters:
    net.cochlea.Nfb         = 300;    '*** Nfb == 300 ***' % [200] # of Filters in Bank    
    net.cochlea.lowfreq     = NET_MIN_FREQ; %50; % [Hz]    
    net.cochlea.highfreq    = 20e3;      % [Hz] into the ERB space; to use to calc. anf 
    net.cochlea.Fs          = nan;                 % [Hz] (==2*Nyquist) kHz when using ERB filters    
    net.cochlea.n_samples   = nan;   % total # of samples            
    net.cochlea.resmp       = nan;
    
    % zilany:
    net.cochlea.zilany.Fs       = 100e3; 
    net.cochlea.zilany.resmp    = [floor( net.cochlea.zilany.Fs/net.stim.Fs ), 1];   % downsample ratio for the ANFs
    net.cochlea.zilany.n_samples= floor( net.stim.secs * net.cochlea.zilany.Fs ); % total # of samples
    
    % slaney:
    net.cochlea.slaney.Fs       = net.stim.Fs;  % [Hz] (==2*Nyquist) kHz when using ERB filters    
    net.cochlea.slaney.resmp    = [1, floor( net.cochlea.slaney.Fs/net.stim.Fs )];   % downsample ratio for the ANFs
    net.cochlea.slaney.n_samples= floor( net.stim.secs * net.cochlea.slaney.Fs );  % total # of samples            
    
    % update the following fields:
    % --> net.cochlea.Fs;             % the actual freq.
    % --> net.cochlea.n_samples       % total # of samples
    % --> net.cochlea.resmp
    net = UpdateCochlea( net );
    
    
    % -------------------------------------------------------------------------    
    % SC & dictionary:
    % -------------------------------------------------------------------------
    net.stim.F0         = NET_MIN_FREQ;          % [Hz]  
    net.stim.F1         = 500;          % [Hz]
    
    net.dic.atom_amplitude_range_dB = 45*[1 1]; %[30,70];
    net.dic.Fs          = 25e3;       %net.cochlea.Fs; %100e3;     % [Hz]    
    net.dic.downsmp     = floor(net.cochlea.Fs/net.dic.Fs);
    net.dic.start_msec  = 10;               % [msecs] starting time to cut from the overall stimulus
    net.dic.end_msec    = net.msecs;        % [msecs] end of the overall stimulus
    net.dic.start_smp   = max(1,floor( 1e-3*net.dic.start_msec * net.dic.Fs )); % [ms]-->[smp] starting index at the ANF matrix  
    net.dic.stop_smp    = floor( 1e-3*net.dic.end_msec * net.dic.Fs );   % [ms]-->[smp]
    net.dic.time_smp    = net.dic.stop_smp - net.dic.start_smp +1;  
    net.dic.msec        = 1e3*net.dic.time_smp*1/net.dic.Fs;    % total number of mili-seconds in the dictoinary 
    net.dic.F0          = NET_MIN_FREQ; %125;  %50;  % [Hz]  
    net.dic.N_triang    = 6; % # of consecutive harmonics
%     net.dic.N_ones      = 0;
%     net.dic.hrm_start   = 1;    % each atom start from the hrm_start harmonics  (out of N_traing) 
%     net.dic.sucfreq     = [1.0]; % [1.04, 1.08, 0.96]
%     net.dic.weights     = [1.0]; %[0.6, 0.4, 0.4]';
%     net.dic.nnzhrm      = 0;    % if set to be nonzero (i.e., m), the algorithm randomely selects m harmonics out of N_traing for each atom
    net.dic.F1          = min(20e3/net.dic.N_triang, 20e3);        % [Hz]    
    net.dic.dim2        = 1500;     % '# of "atoms" in the dictionary
    net.dic.dim1        = net.cochlea.Nfb * net.dic.time_smp;     % # of "word" in the dictionary    

    
    
    % -------------------------------------------------------------------------    
    % Harmonic Sieve Matrix:
    % -------------------------------------------------------------------------
    net.hs.interp_factor = 10;
    net.hs.G             = [];
    net.hs.freq_hs       = []; 
    net.hs.sigma_type    = [];
    
    
    % debug mode:
    % -----------
    net.anf.oded.debug_mode.enable.all                = 0;              
    net.anf.oded.debug_mode.enable.txt_2_cmd          = 0;  
    net.anf.oded.debug_mode.enable.plot_real_time     = 0;  
    net.anf.oded.debug_mode.enable.enable_release_txt = 0;

    
    % -------------------------------------------------------------------------
    % Directories:
    % -------------------------------------------------------------------------
    if isunix
        dd = '/';
    else
        dd = '\';
    end
    
    net.current_dir     = cd;
    net.data_dir        = ['..', dd, 'data', dd];              % result directory
    net.AN_heinz_dir    = ['..', dd, 'AN_heinz2001', dd];      % directory of the exe file of Heinz et al. model
    net.AN_zilany_dir   = ['..', dd, 'AN_Zilany2014', dd];     % directory of the mex file of Zilany et al. model
    net.AN_slaney_dir   = ['..', dd, 'Malcolm_Slaney', dd];    % directory of malcom slaney's MATLAB's toolbox
    net.anf.filename    = [net.data_dir, 'anf.dat']; % the ANF activity (output of the cochlear model)
    net.zt_filename     = [net.data_dir, 'zt.dat'];		% readout file (zt)
    net.ft_filename     = [net.data_dir, 'ft.dat'];     % desired signal file (of <ft>)
    net.train_filename  = [net.data_dir, 'train_info.txt'   ];
    net.test_filename   = [net.data_dir, 'test_info.txt'    ];
    net.x0_filename     = [net.data_dir, 'x0.dat'           ];
    net.X_filename      = [net.data_dir, 'X.dat'            ];
    net.M_filename      = [net.data_dir, 'M.dat'            ];
    net.Wf_filename     = [net.data_dir, 'Wf.dat'           ];
    net.Jin_filename    = [net.data_dir, 'Jin.dat'          ];
    net.Jall_filename   = [net.data_dir, 'Jall.dat'         ];
    net.Vall_filename   = [net.data_dir, 'Vall.dat'         ];
    net.z0_filename     = [net.data_dir, 'z0.dat'           ];
    net.wo_filename     = [net.data_dir, 'wo.dat'           ];
    net.P_filename      = [net.data_dir, 'P.dat'            ];

    
end







