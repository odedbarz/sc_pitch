function [ W, Q, theta, dW ] = Foldiak_Training( W, Q, x, y, alpha, beta, gamma, theta, pbits, flags )

    if 1 == flags.y_round
        y = round(y);
    end

    %  --- if sum(abs(y)>1); error('-> !!! sum(abs(y)>1)\n'); end;

    % anti-Hebbian learning:
    if (0 ~= alpha)
        dW = -alpha*(y*y'-pbits);
        dW( dW > 0 ) = 0;   % make it all-negative only
        dW = dW - diag(diag(dW));   % set wij == 0
        W = W + dW;     % updatethe feedforward matrix
    else
        dW = [];
    end
    
    % Hebbian learning:
    if (0 ~= beta)
        dQ = beta*(y*x' - (y   *ones(1,length(x))) .* Q );
        Q = Q + dQ;
    end
    
    
    % Threshold modification:
    if (0 ~= gamma)
        d_theta = gamma*(y - pbits);
        theta = theta + d_theta;
    end

end

