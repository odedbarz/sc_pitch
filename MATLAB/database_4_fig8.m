%
% Temporary script to create the database for the "resolved vs. unresolved
% harmonics".
% 


%% Initiate everything:

N = 5;

%{
fig8_db.info = 'Temporary script to create the database for the resolved vs. unresolved harmonics';


fig8_db.fff_dic = fff_dic;   % dictionary's frequencies

    % interpulation:
    interp_factor = 10;
    dummy = length(fff_dic);    
    fff_dic_10 = interp1( 1:dummy, fff_dic, linspace(1, dummy, dummy*interp_factor) );
fig8_db.fff_dic_10 = fff_dic_10;


fig8_db.F02 = F02;

fig8_db.harmonics = zeros(N,net.N_hrm);

fig8_db.H = zeros(length(h),N);  % SC coefficients h

fig8_db.pitch_est = zeros(1,5);

fig8_db.Pr_pitch = zeros(length(Pr_pitch),5);

Sp.M_salience = zeros(8,N);



%}

%% get input:
%{
% ------------------------------
nn = 4;
% ------------------------------


[ pitch_est, pitch_opt, pitch_salience, Pr_pitch ] = PitchEst( h,fff_dic, sieve_sig, gauss_LPF, debug_est );

fig8_db.harmonics(nn,:) = harmonics2;
fig8_db.H(:,nn) = h;
fig8_db.pitch_est(nn) = pitch_est;
fig8_db.Pr_pitch(:,nn) = Pr_pitch;
fig8_db.pitch_salience(:,nn) = pitch_salience;
%}


%% plot everything

disp(fig8_db)

fontsize = 40;

% plot #1:
figure(11);
fff = fig8_db.fff_dic/fig8_db.F02;
plot_1 = plot(fff, fig8_db.H, 'linewidth', 6);
hold on
plot(repmat([0:(30-1)],2,1), repmat([0, 1]',1,30), 'k--', 'linewidth', 1.5);
hold off
axis([0 26 0 0.85]);
set(gca, 'fontsize', fontsize);
xlabel(sprintf('f/%gHz', fig8_db.F02));
legend('h_1', 'h_2', 'h_3', 'h_4', 'h_5')

% plot #2:
fff10 = fig8_db.fff_dic_10/fig8_db.F02;

%{
for kk = 1:N
    figure(11 + kk);
    set(gcf, 'Position', [57 629 1831 466]);
    ykk = fig8_db.Pr_pitch(:,kk);
    plot(fff10, ykk, 'linewidth', 6, 'Color', plot_1(kk).Color);
    set(gca, 'fontsize', fontsize);
    axis([0, 4, 0, 0.016]);  %axis([0, 4, 0, 1.1*max(ykk)]);
    xlabel(sprintf('f/%gHz', fig8_db.F02));
    legend(sprintf('S_{p,%d}(f_d)', kk));
    grid on;
end
%}


if ishandle('fig12'); close(fig12); end;
    
fig12 = figure(12);

set(gcf, 'units', 'normalized');

% [left bottom width height]:
left    = 0.1;
bottom  = 0.1;
width   = 1-bottom-0.05;
delta4hight = 0.1/N;
height  = (1-bottom)/N;

axes_array = zeros(N);

for kk = 1:N
    axes_array(kk) = axes('Position', [left, bottom+(kk-1)*height, width, height-delta4hight]);
    ykk = fig8_db.Pr_pitch(:,kk);
    plot(fff10, ykk, 'linewidth', 6, 'Color', plot_1(kk).Color);
    set(axes_array(kk), 'fontsize', fontsize);
    axis([0, 4, 0, 0.016]);  %axis([0, 4, 0, 1.1*max(ykk)]);
    if (1 ~= kk); set(axes_array(kk), 'XTickLabel', []);  end;  % disable axis
    legend(sprintf('S_{p,%d}(f_d)', kk));
    grid on;
end

xlabel(axes_array(1), sprintf('f/%gHz', fig8_db.F02));

%% save

% save('..\\data\\fig8_db', 'fig8_db');












