% 
% anf = CreateANF(net, stim, Hd_ear, fcoefs)
% 

function anf = CreateANF(net, stim, Hd_ear, fcoefs, cochlea_freq, debug_mod)

    % from stimulus sampling to cochlear sampling:
    %stim_ = resample(stim, net.cochlea.resmp(1), net.cochlea.resmp(2));

    % check for debug_mode flag:
    if (nargin > 5); debug_mod = 1; else debug_mod = 0; end;
        
    % avoid the jittering in the upsampling process:
    stim_ = 1/net.cochlea.resmp(1)*cumsum( resample( diff([0; stim]), net.cochlea.resmp(1), net.cochlea.resmp(2)  ));
    
    % apply the middle ear filter:
    %stim_ = Hd_ear.filter(stim_);
    
    switch net.anf.method
        case 4  % Ray Meddis� hair cell model    
            % apply the middle ear filter:
            %stim_ = ifft( Hd_ear.*fft(stim_), 'symmetric' );
            stim_ = Hd_ear.filter(stim_);

            %
            coch = ERBFilterBank( stim_, fcoefs );
            anf = MeddisHairCell( 80* coch/max(max(coch)), net.cochlea.Fs ); %, 1 );
            
            % from cochlear sampling to dictionary sampling:
            anf = resample(anf', 1, net.dic.downsmp)';

            
        case 10  % zilany(2014)Code_and_paper
           
            downsmp = net.dic.downsmp;
            
            if (net.cochlea.n_samples/downsmp ~= floor(net.cochlea.n_samples/downsmp))
                error('-> Error @ [CreateANF.m]: (net.cochlea.n_samples/downsmp ~= floor(net.cochlea.n_samples/downsmp))\n');
            end
            anf = zeros(net.cochlea.Nfb, net.cochlea.n_samples/downsmp);
            
            secs = net.secs;
            
            % model fiber parameters
            cohc        = net.anf.zilany.cohc;      % normal ohc function
            cihc        = net.anf.zilany.cihc;      % normal ihc function
            species     = net.anf.zilany.species;   % 1 for cat (2 for human with Shera et al. tuning; 3 for human with Glasberg & Moore tuning)
            noiseType   = net.anf.zilany.noiseType; % 1 for variable fGn (0 for fixed fGn)
            fiberType   = net.anf.zilany.fiberType; % spontaneous rate (in spikes/s) of the fiber BEFORE refractory effects; "1" = Low; "2" = Medium; "3" = High
            implnt      = net.anf.zilany.implnt;    % "0" for approximate or "1" for actual implementation of the power-law functions in the Synapse

            % stimulus parameters:
            Ts = 1/net.cochlea.Fs;
            
            % PSTH parameters
            nrep = net.anf.zilany.nrep;               % number of stimulus repetitions (e.g., 50);
            %psthbinwidth = net.anf.zilany.psthbinwidth; % binwidth in seconds;
            
            parfor ch = 1:net.cochlea.Nfb
                CF = cochlea_freq(ch);
                
                % calc. the IHCs voltage at each CF:
                % * (secs+Ts): avoid the numerical issue of (reptime < pxbins*tdres) over (reptime <= pxbins*tdres).
                % * TODO: change it in the c file (from < to <=) and compile.
                dummy = model_IHC( stim_', CF, nrep, Ts, secs+Ts, cohc, cihc, species ); 
                vihc = dummy(1:end-1);   % remove the last added sample. TODO
                
                % from IHCs voltage to mean rate:
                [meanrate,~,~] = model_Synapse( vihc, CF, nrep, Ts, fiberType, noiseType, implnt ); 
                
                % downsample & save the results in to the anf array
                anf(ch,:) = resample(meanrate, 1, downsmp);
                
                %{
                [highsr,~,~] = model_Synapse( vihc, CF, nrep, Ts, 3, noiseType, implnt );   % fiberType == 3
                [mediumsr,~,~] = model_Synapse( vihc, CF, nrep, Ts, 2, noiseType, implnt ); % fiberType == 2
                [lowsr,~,~] = model_Synapse( vihc, CF, nrep, Ts, 1, noiseType, implnt );    % fiberType == 1
                meanrate = resample(0.6*highsr + 0.25*mediumsr + 0.15*lowsr, 1, downsmp);
                anf(ch,:) = meanrate;
                %}

                
            end
            
            anf = anf(end:-1:1,:);
                        
    end

    % resize the anf (cut the initial samples to save space):
    anf = anf(:,net.dic.start_smp:net.dic.stop_smp); 
    
    % remove DC:
    if (net.anf.nomean)
        anf = anf - ones(size(anf,1),1) * mean(anf,1);
    end
    
    % * Threshold the ANFs:
    if (net.anf.zscore)
        anf = zscore(anf);
    end
    
    % * Normalize the bases response:
    if (1 == net.anf.norm_flag)
        anf =  anf .* 1./max(anf(:));       % normalize by max. value
    end
    
    % * Threshold the ANFs:
    I_thr = ( anf > net.anf.thr );
    anf = anf .* I_thr;   

    
    if ( 0 == nnz(anf(:)) )
        error('-> CreateANF: 0 == nnz(anf(:) !!!');
    end
    
   
    %% plot the ANF using log scale for the frequencies:
    if 1 == debug_mod
        xxx = linspace(net.dic.start_msec, net.msecs, net.dic.time_smp);
        [X,Y] = meshgrid(xxx, 1e-3 * cochlea_freq(end:-1:1));
        figure(49);
        hold off; cla
        surface(X,Y,anf,'EdgeColor','none');     
        % axis([10, 15, 1e-3* 125, 1e-3* 5e3]);
        colorbar;
        set(gca, 'fontsize', 36);

        xlabel('Time [ms]');
        ylabel('CF [kHz]');
        title('AN Population Response')
    end


    
end















