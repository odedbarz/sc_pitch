

function [f_crm, f_nrm] = freq2chroma(fff, freq)

f_nrm = freq./fff;

% normalize into chroma:
semitone = 2^(1/12);
halfoctave = semitone^6;

f_crm = mod( log2(f_nrm)+0.5, 1) -0.5;
% f_crm = mod( log2(f_nrm)-semitone, 1) + semitone;
% f_crm = mod( log2(f_nrm)-halfoctave, 1);


