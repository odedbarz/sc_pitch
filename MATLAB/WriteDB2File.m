
function WriteDB2File( filename, db_st, prev_field, fid )
% Description: 
%   write's the database into a text file. the procedure writes all fields 
%   in the db_st structure.
%
% Input:
%   filename: file name to save. 
%   db_st: the database structure to save
%

db_fields = fieldnames(db_st);

if ~exist('prev_field', 'var')
    prev_field = '';
else
    prev_field = [prev_field, '.'];
end

if ~exist('fid', 'var')
    [fid, errMsg] = fopen(filename, 'w');
    if (-1 == fid)
        error(['Error in WriteDB2File: ', errMsg]);
    end
    close_this_file = true;
else
    close_this_file = false;
end

for i = 1:length(db_fields)
    % a string?
    if ischar( db_st.(db_fields{i}) )    % a text field?
        count = fprintf(fid,'%s%s = %s\n', prev_field, db_fields{i}, db_st.(db_fields{i}));
    
    % a structure?
    elseif isstruct( db_st.(db_fields{i}) )
%         prev_field = db_fields{i};
        WriteDB2File( filename, db_st.(db_fields{i}), db_fields{i}, fid );
%         prev_field = '';
        
    % a numeric vector?
    elseif ( 1 <length(db_st.(db_fields{i})) )  % isnumeric(...) == 1
        fprintf(fid,'%s%s = [', prev_field, db_fields{i});
        for jj = 1:length( db_st.(db_fields{i}) )
            fprintf( fid, ' %g ', db_st.(db_fields{i})(jj) );   
        end
        fprintf( fid, ']\n' );
        
    % a numeric value...
    elseif (floor(db_st.(db_fields{i})) == db_st.(db_fields{i}))   % is integer?
        count = fprintf(fid,'%s%s = %d\n', prev_field, db_fields{i}, db_st.(db_fields{i}));
    
    else % it's a double\float
        count = fprintf(fid,'%s%s = %f\n', prev_field, db_fields{i}, db_st.(db_fields{i}));
    
    end
        
    
    if (0 == count)
        error(['Error in WriteDB2File: fprintf() failed to write into file']);
    end
end

if (close_this_file)
    fclose(fid);
end

end



