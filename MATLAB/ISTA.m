function [h, err] = ISTA( D, h0, x, ista )
    % 
    % h = ISTA( D, h0, x )
    % 
    % x ~ D*h
    % x: [Nx1] the database
    % D: [NxM] Dictionary
    % h: [Mx1] the sparse weights
    % h0: initial values for the sparse weights
    % ista: data structure
    %
    % Description:
    %   ISTA: Iterative Shrinkage & Thresholding Algorithm. The algorithm provides a sparse 
    %   weights for the vector h, in the expresion x ~ D*h
    %


    DD = D'*D;

    % step size - make it small for convergence
    alpha = ista.alpha_ratio * 1/abs(eigs(DD,1)); 	
    gamma = ista.gamma_ratio;                       % the "shrinkage" factor
    %gamma = ista.gamma_ratio * std(h0/norm(h0,2));
    %gamma = ista.gamma_ratio * 1/norm(h0,2);
    % gamma = ista.gamma_ratio * std(h0/max(h0));
    % gamma = ista.gamma_ratio * var(h0/max(h0));
    % gamma = (1-0.1)*ista.gamma_ratio - 0.1*ista.gamma_ratio* std(h0/max(h0));
    %gamma = ista.gamma_ratio * (nnz(h0)/length(h0));

    %hEng = @(xx,hh) 0.5*norm(xx-D*hh,2).^2 + gamma*norm(hh,1);

    alphaDD = alpha*(DD);
    alphaDx = alpha*(D'*x);

    err = Inf;

    h = h0;

    counter = 0;

    if (1 == ista.h_positive)
        fprintf('--> ISTA: ista.h_positive = 1 !\n');
    end

    
    while err > ista.max_err
        % calc. next weihgts using gradient descent:
        %h = h - alpha*( DD*h - Dx );
        h = h - (alphaDD*h - alphaDx);

        % shrink h:
        h = sign(h).*max(0, abs(h)-alpha*gamma);


        if (1 == ista.h_positive)
            h = max(0,h);
        end


        % update the error:
        if 0 == nnz(h)
            %error('-> error in ISTA(): h is all ZEROS !!!');
            fprintf('-> ISTA(): h == 0\n');        
            break;
        else
            %err = ista.err_fun(h,h0);
            %norm(h-h0,2)/norm(h0,2);
            err = norm(x-D*h,2); %/norm(x,2);
            %err = hEng(x,h);    % the functional
        end

        h0 = h;

        counter = counter + 1;
        if (counter > ista.max_loop_thr)

            if ista.debug_mode
                fprintf('-> ISTA(): breaking the WHILE loop, err = %.2e\n', err);
            end

            break;

        end
    end


end

