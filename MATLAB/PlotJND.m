%
% PlotJND.m
%
% plots the JND on screen
%

fontsize = 35;
linewidth = 3;
markersize = 48;

P       = tst_db.P;
limen_f = tst_db.limen_f;
fff_hs  = tst_db.fff_hs;
F0_tst  = tst_db.F0_tst;

jnd = CalcJND( P, fff_hs, limen_f );
% jnd = CalcJND( tst_db.H, fff_dic, limen_f );

% the JND's frequencies:
freq_jnd = F0_tst(1:2:end);

figure(51);
% plot( 1e-3*freq_jnd, jnd./freq_jnd, '.--', 'linewidth', linewidth, 'markersize', markersize);
% plot( 1e-3*freq_jnd, log10((freq_jnd+jnd)./freq_jnd), '.--', 'linewidth', linewidth, 'markersize', markersize);
loglog( 1e-3*freq_jnd, jnd./freq_jnd, '.--', 'linewidth', linewidth, 'markersize', markersize);
set(gca,'fontsize', fontsize);
grid on;
xlabel('$f [kHz]$', 'interpreter', 'latex');
ylabel('Weber Fraction $\frac{\Delta f}{f}$', 'interpreter', 'latex');
title('JND', 'interpreter', 'latex');




