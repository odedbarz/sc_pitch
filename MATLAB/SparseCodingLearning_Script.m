% 
% SparseCodingLearning_Script.m
%
% function [ D, H ] = SparseCodingLearning( X, sc )
%



% sparse code database:
% sc.dictionary_ratio = 2;
sc.convergance      = 1e-2;
sc.max_loop_thr     = 100;
sc.debug_mode       = 1;


    % ISTA parameters:
    ista.alpha_ratio = 1; %0.0025; %0.05; %0.001;
    ista.gamma_ratio = 0.01; %0.5e-4;  % \ 1.0% 0.01* std(base_db.D(:)); %1.0 *1e-4; % 0.5e-3;
    ista.max_err     = 1e-5;
    ista.seed_level  = 1e-6; %*std(base_db.D(:)); % 1e-6;
    ista.max_loop_thr= 5e3;
    ista.debug_mode  = 0;
    ista.h_positive  = 0;   % removes negative values from h
    ista.err_fun     = @(h,h0) norm(h-h0,2)/norm(h0,2);
    ista.method      = 1;   % [1] for "my" ISTA(); [2] for MATLAB's lasso()


% ### after loading... ###
% X = full( base_db.D );


% '$$$$$$$$$$$$$$$$$$$$$$$$$'
%     X = X(:,1:50);

if 1 == sc.debug_mode
    fprintf('-> !! SparseCodingLearning(): sc.debug_mode is ON !!\n');
    
    fontsize = 20;
    markersize = 30;
    
end
    

dictionary_size = size(X,2); %base_db.D_num;


% Init. a dictionary:
D = InitDictionary( size(X,1), dictionary_size );


%

convergance = Inf;
counter = 0;
eps1 = nan;     % 1 / 1e-24;  % eps1  > 0 (default == 1e-3)
eps2 = 0;       % 1e-24;  % eps2  > 0 (default == 1e-6)
%thr1 = 1e-3;


fprintf('-> starting SparseCodingLearning()...\n');

while convergance > sc.convergance     
    
    fprintf('\n-> while loop counter: %d (/%d)\n', counter, sc.max_loop_thr);
    
    H = InitSP( size(D,2), size(X,2), ista.seed_level);

    B = zeros(size(D,1),size(D,2));
    A = zeros(size(D,2),size(D,2));
    
    % ISTA:
    fprintf('--> starting ISTA...\n');    
    parfor ii = 1:size(X,2)
        if ~mod(ii-1,100)
            fprintf('--> SparseCodingLearning() => ISTA #%d /%d \n', ii, size(X,2));
        end
        
        %
        [H(:,ii), ista_err] = ISTA( D, H(:,ii), X(:,ii), ista );   % infer the code h(x(ii))          
        
        B = B + X(:,ii)*H(:,ii)';
        A = A + H(:,ii)*H(:,ii)';
    end
    fprintf('--> end of ISTA\n');    
    
    
    % Check for errors:
    assert( 0 == nnz(isnan(H)), '\n\n --> Error in SparseCodingLearning(): \n\t0 ~= nnz(isnan(H))\n\n' );    
    assert( 0 == nnz(isnan(A)), '\n\n --> Error in SparseCodingLearning(): \n\t0 ~= nnz(isnan(A))\n\n' );

    % need it for the convergence:
    D_prev = D;
    %D_prev_norm = norm(D(:),2);
    
    
    eps1 = 1e-6*max(diag(A));
    %fprintf('\n\t---> max(diag(A)): %g\n\t---> eps1: %g\n\n',  max(diag(A)), eps1);
    
            
    % D = ( B - D*A + D*diag(diag(A)) ) * diag(1./max(eps1,diag(A))) ;
    D = ( B - D*A ) * diag(1./max(eps1,diag(A))) + D;
        
    % %{ Init. the column D(:,jj)
    parfor jj = 1:size(D,2)
        if nnz(D(:,jj)) == 0
            %D(:,jj) = 0.01*D_prev(:,jj);
            D(:,jj) = InitDictionary( size(X,1), 1 );
            fprintf('--- !!! -> D(:,%d) was initialized !!!', jj);
        end
    end
    %}
    
    % Check for errors:
    assert( 0 == nnz(isnan(D)), '\n\n --> Error in SparseCodingLearning(): \n\t0 ~= nnz(isnan(A))\n\n' );
        
    
%             eps2 = 0.01*max(sqrt(sum(D.*D)));

    
    % normalaize the variance of the dictionary
    D = D*diag( 1./max(eps2,sqrt(sum(D.*D))) );
           
    
    % Check for errors:
    assert( 0 == nnz(isnan(D)), '\n\n --> Error in SparseCodingLearning(): \n\t0 ~= nnz(isnan(A))\n\n' );
    
    % check for convergance:
    convergance = (norm(D(:) - D_prev(:),2))/norm(D_prev(:),2);
    %convergance = abs(norm(D(:),2) - D_prev_norm)/D_prev_norm;
    
    % residual error:
    if 1 % ~mod(counter,5)
        err = norm(X - D*H,2)/norm(X,2);
        fprintf('-> estimation error: %g [%%] \n', 100*err );
        fprintf('-> SparseCodingLearning() convergance: %g \n', convergance );
        %fprintf('-> ISTA().err: %.2e\n', ista_err);
    end
    
    
    % DEBUG:
    if sc.debug_mode        
        figure(99);
        set(gca, 'fontsize', fontsize)        
        subplot 131
        imagesc(abs(D))
        set(gca, 'YDir', 'normal'); colorbar;
        title('|D|')
        
        subplot 132
        imagesc(abs(D-D_prev)); colorbar;
        title('|D-D_{prev.}|')

        subplot 133
        nn = ceil(rand*size(X,2));
        plot(X(:,nn), 'o:');
        hold on
        plot(D*H(:,nn), '.-');
        hold off
        grid on
        legend('x', 'D*h')
        title(sprintf('counter = %d', counter))
        
        figure(101);
        plot(H(:,nn), '.-')
        title(sprintf('h_{%d}, sparsness: %.f %%', nn, 100*(1-nnz(H(:,nn))/length(H(:,nn))) ));
        grid on
        
        drawnow;
        pause(1e-6);
    end
    
    %
    counter = counter + 1;
    
    if (counter > sc.max_loop_thr)
        fprintf('-> SparseCodingLearning(): breaking the WHILE loop\n');
        fprintf('--> counter > sc.max_loop_thr==%d\n', sc.max_loop_thr);
        break;
    end 
        
    
end   
  
fprintf('-> end of SparseCodingLearning()...\n');



















