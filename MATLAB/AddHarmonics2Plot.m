

function AddHarmonics2Plot(axes_hnd, F0, harmonics2, N_hrm, minY, maxY, linewidth)

axes(axes_hnd)
hold on
stim_hrm = nonzeros(F0.*[1:N_hrm].*harmonics2)';    % current stimulus' harmonics
% stim_hrm = 1e-3*stim_hrm;    % [Hz] ==> [kHz]
stim_hrm = stim_hrm/F0;    % [Hz] ==> [kHz]
plot(repmat([minY maxY]', 1, 4), repmat(stim_hrm,2,1), '--k', 'linewidth', linewidth);
