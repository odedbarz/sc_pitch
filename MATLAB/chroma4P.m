
function [pc, xx] = chroma4P(fff, p, f0, m, fignum)

pc = zeros(m, 1);
f_mod = mod( log2(fff/f0), 1);
Ixf = fix(m*f_mod)+1;
xx = linspace(-0.5, 0.5, m);

for ii = 1:length(p)
    %pc(Ixf(ii)) = pc(Ixf(ii)) + p(ii);   
    pc(Ixf(ii)) = max(pc(Ixf(ii)), p(ii));   
    
end

% move the peak (the octave) to the center:
pc = fftshift(pc);


if exist('fignum', 'var') && isnumeric(fignum)
    figure(fignum);
    subplot 211
    plot(xx, pc, 'linewidth', 3);
    grid on
    title(sprintf('Pitch Probability Chroma, (f_0 = %g Hz)', f0));
    legend('p_{chroma}')

    subplot 212
    plot(fff/f0, p, 'linewidth', 3);
    grid on
    legend('p');
    
end