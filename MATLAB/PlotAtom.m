

function PlotAtom(net, D, cochlea_freq, fff_dic, atomnum, fignum)
%
% function PlotAtom(net, D, cochlea_freq, fff_dic, atomnum, fignum)
%
% Inputs:
% * cochlea_freq: the frequencies used in the cochlear model;
% * fff_dic: the frequencies if the different dictionaries;
%

if ~exist('fignum', 'var')
    fignum = 99;    % default figure
end

figure(fignum);

cla; 

cochlea_freq_ = 1e-3*cochlea_freq; %(end:-1:1);   % [Hz] ==> [kHz]

ttt = linspace(net.dic.start_msec, net.dic.end_msec, net.dic.time_smp);

Dfun = @(atomnum) reshape(D(:,atomnum),net.cochlea.Nfb,net.dic.time_smp);

% linear axis:
[X,Y] = meshgrid(ttt, cochlea_freq_);
surface(X,Y,Dfun(atomnum),'EdgeColor','none', 'Linestyle','none'); 
view(0,90);
% set(gca,'yscale','log');

min_freq = net.dic.F0;
max_freq = 5e3;
min_freq = 1e-3*min_freq;   % [Hz] ==> [kHz]
max_freq = 1e-3*max_freq;   % [Hz] ==> [kHz]

% axis([net.dic.start_msec, net.dic.end_msec, min_freq, max_freq]); %net.dic.F1]);

set(gca, 'fontsize', 45);

xlabel('Time [ms]');
ylabel('CF [kHz]');
title(sprintf('Atom #%d of Freq: %g Hz', atomnum, fff_dic(atomnum)))

% axis square
colorbar;


