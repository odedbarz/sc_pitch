%
% plot_overall_pitch_shift_v2.m.m
%
% Note: You need to load the data in <tst_db>, and plot the pitch-shift
% results.
%

fontsize    = 40;
markersize  = 36;
linewidth   = 5;

dF2 = tst_db.dF2(1);

figure(10);
plot(tst_db.F02/dF2, tst_db.pitch_est, '.', 'markersize', markersize);
title('Pitch Shift, All Results');
set(gca, 'fontsize', fontsize);
xlabel(sprintf('Normalized Frequency (by \\Deltaf=%g Hz)', dF2));
ylabel('Pitch Est. [Hz]');


If = tst_db.F02/dF2;
Ip = tst_db.pitch_est;

% remove outliers:
y0 = 180;
y1 = 240;
If = If(logical((Ip>=y0).*(Ip<=y1)));
Ip = Ip(logical((Ip>=y0).*(Ip<=y1)));

% figure(10);
% plot(If, Ip, '.', 'markersize', markersize);

N_lines = 10;

% Save all the lines:
AB = zeros(2, N_lines);
  
df4std = 3.5;    % Hz

for ii = 1:N_lines    
    f0 = (100 + (200+2)*(ii-1))/dF2;   % hz
    f1 = (100 + (200-1)*ii)/dF2;   % hz
    
    % Set the index without outliers:
    Ix = logical( (If >= f0).*(If <= f1) ); 
        
    % Prepare for least square model y = H*[a,b]':
    x_ii = If(Ix);
    H = [x_ii, ones(nnz(Ix),1)];
    ab = (H'*H)\H'*Ip(Ix);
    y_ii = ab(1)*H(:,1)+ab(2);
    
    % remove outliers:
    for jj = 1:2
        std_ii = abs(Ip(Ix) - y_ii);    
    %     Ix(nonzeros( find(Ix).* (std_ii>df4std) )) = 0;
        Ix(nonzeros( find(Ix).* (std_ii > df4std*std(std_ii)) )) = 0;

        % recalc. the LS without the outliers:
        x_ii = If(Ix);
        H = [x_ii, ones(nnz(Ix),1)];
        ab = (H'*H)\H'*Ip(Ix);
        y_ii = ab(1)*H(:,1)+ab(2);

    end
    
    
    figure(10);    
    hold on;
%     plot(H(:,1), y_ii, 'k--', 'linewidth', linewidth);
    x2plot_ii = [0.9*H(1,1), 1.1*H(end,1)]';
    y2plot_ii = ab(1)*x2plot_ii+ab(2);
    plot(x2plot_ii, y2plot_ii, 'k--', 'linewidth', linewidth);
    
    % ** DEBUG **
    % -> plot the dots for each LS session:
%     plot(If(Ix), Ip(Ix), '.', 'markersize', 0.4*markersize);
    
    % shows the data points to participate in each LS:    
    test4line = sprintf('%.2f', ab(1)/dF2);
    %text(1.15*H(floor(end/2),1), 1.1*(ab(1)*H(end,1)+ab(2)), test4line, 'fontsize', fontsize);
    text(0.95*x2plot_ii(end), 1.03*y2plot_ii(end), test4line, 'fontsize', fontsize);
    
    % clear previous points for further analysis:
    If(Ix) = -1;
    
    %{
    % Show first effect lines dp = 1/n*df:
    y_dummy = 1/ii*x_ii([1,end]);
    Ix_dummy = y_dummy(2) <= y_ii(end);
    dummy_v = ii*[-0.1, 0.1]';
    plot( ii+dummy_v, 1.0/ii*dF2*dummy_v+dF2, 'k--', 'linewidth', 2);
    %}
    
    
    hold off;
    
    % save the results:
    AB(:,ii) = ab;
end

%%
% figure(11);
set(gca, 'fontsize', fontsize);
xlabel(sprintf('Normalized Frequency (by \\Deltaf=%g Hz)', dF2));
ylabel('Pitch Est. [Hz]');
axis([100/dF2, 2400/dF2, 177, 330]);
grid on;

   























