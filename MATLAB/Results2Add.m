



function results = Results2Add( results, tn, F1, F2, ft_value, zt_mean, norm2wo )
    
results.F1(tn) = F1;
results.F2(tn) = F2;
results.ft_value(tn) = ft_value;
results.zt_mean(tn) = zt_mean;

if nargin >= 7
    results.norm2wo(tn) = norm2wo;
end





