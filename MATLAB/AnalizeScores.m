%
% AnalizeScores.m
%

% clc

% fig_num = 11;

%% plot: Net's F0 Estimation (results) using ML

N_epoch     = size(results.trn.score,1);
% N_epoch = net.N_epoch;

% N_epoch2    = size(results.tst.score,1);
N_epoch2 = tn-1
% N_epoch2 = net.N_epoch2;

if ~isfield(results.tst, 'pitch_ML')
    pitch_ML = readouts2pitch(results.tst.score(1:N_epoch2,:), net.ft_cf, net.neu_std, net.ft_amp);
else
    pitch_ML = results.tst.pitch_ML(1:N_epoch2);
end


%%
if (1)    
    figure( 10 + fig_num );
    
    plot(results.tst.F0(1:N_epoch2), pitch_ML, '.', 'markersize', 16); 
    %plot(results.tst.pitch_SACF(1:N_epoch2), pitch_ML, '.'); 
    
    set(gca, 'fontSize', 20);
    grid on
    %axis square
    xlabel('$f_0$ [Hz]', 'interpreter', 'latex', 'fontSize', 30)
    ylabel('$f_0^{est}$ [Hz]', 'interpreter', 'latex', 'fontSize', 30)
    title('Predictions')
    hold on
    plot([0, net.F0_end], [0, net.F0_end], '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]/2, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]/3, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]/4, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*2, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*3, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*4, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*5, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*6, '--k', 'linewidth', 1.5);
    hold off
    %axis([net.F0_start, net.F0_end net.F0_start, net.F0_end])
    axis([0, net.F0_end, 0, net.F0_end])
end

%% plot: Total Number of Harmonics in the Training & Testing Epoches


% compatibility issue...
if isfield(results.trn, 'harmonics')
    trn_harmonics   = results.trn.harmonics;
else
    trn_harmonics   = results.trn.hrm_f1;    
end

% compatibility issue...
if isfield(results.tst, 'harmonics')
    tst_harmonics       = results.tst.harmonics;
else
    tst_harmonics       = results.tst.hrm_f1;
end


% count how much tst_harmonics are in each stimulus:
hrm_num     = sum( tst_harmonics(1:N_epoch2,:)~=0, 2 );
hrm_trn_num = sum( trn_harmonics(1:N_epoch,:)~=0, 2 );

if (0)    
    figure( 11 +fig_num )
    
    tst_hrm_hist = hist(hrm_num,1:net.N_hrm);
    tst_hrm_hist = tst_hrm_hist/N_epoch2;
    
    trn_hrm_hist = hist(hrm_trn_num,1:net.N_hrm);
    trn_hrm_hist = trn_hrm_hist/N_epoch;
    
    % plot(1:net.N_hrm, trn_hrm_hist, 'rx', 'markersize', 10, 'linewidth', 20)
    bar([1:net.N_hrm], [trn_hrm_hist', tst_hrm_hist'])

    set(gca, 'fontSize', 20);
    grid on
    legend('trn: # tst_harmonics', 'tst: # tst_harmonics')
    title('Total Number of Harmonics')
    xlabel('# Harmonics')
end

%% plot: Predictions - Classified by Harmonics

if (0)    

    y_hrm = zeros(N_epoch2, net.N_hrm);
    is_hrm = nan(1, net.N_hrm);
    lgnd_ = [];

    for ii = 1:net.N_hrm
        y_hrm(:,ii) = pitch_ML .* (hrm_num == ii);
        is_hrm(ii) = nnz(hrm_num == ii) ~= 0;   % check that the stimulus has ii tst_harmonics

        if 0 ~= is_hrm(ii)
            lgnd_{end+1} = sprintf('%d', ii);
        end
    end


    y_hrm = y_hrm( :, logical(is_hrm) );
    if (1)    
        figure( 12 + fig_num)
        plot(results.tst.F0(1:N_epoch2), y_hrm, '.', 'markersize', 18); 
        set(gca, 'fontSize', 20);
    end

    % chack for missing fundamental
    no_f0 = tst_harmonics(1:N_epoch2,1) == 0; 

    hold on
    plot(results.tst.F0(no_f0), pitch_ML(no_f0), 'sk', 'markersize', 8, 'linewidth', 2); 
    hold off

    hold on
    plot([0, net.F0_end], [0, net.F0_end], '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]/2, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]/3, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]/4, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*2, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*3, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*4, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*5, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*6, '--k', 'linewidth', 1.5);
    hold off

    %axis([net.F0_start, net.F0_end net.F0_start, net.F0_end])
    axis([0, net.F0_end, 5, net.F0_end])

    % axis square
    grid on;
    legend(lgnd_, 'MF')     % MF: missing fundamental
    xlabel('$f_0$ [Hz]', 'interpreter', 'latex', 'fontSize', 30)
    ylabel('$f_0^{est}$ [Hz]', 'interpreter', 'latex', 'fontSize', 30)
    title('Predictions - Classified by # of Harmonics')

end


%% plot: Predictions - Classified by first harmonic number

if (0)    
    figure( 13 + fig_num)
    
    lgnd = [];
    hnd = {};
    hrm_f0_loc = zeros(1,N_epoch2);
    for ii = 1:N_epoch2
        hrm_f0_loc(ii) = find( tst_harmonics(ii,:) ~= 0, 1, 'first' );
    end
    
    hold on    
    for ii = 1:net.N_hrm
        if 0 ~= nnz(hrm_f0_loc == ii)
            hnd{end+1} = plot(results.tst.F0((hrm_f0_loc == ii)), pitch_ML(hrm_f0_loc == ii), '.', 'markersize', 18); 
            lgnd{end+1} = sprintf('%d', ii);    
        end
    end
    
    set(gca, 'fontSize', 20);
    
    plot([0, net.F0_end], [0, net.F0_end], '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]/2, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]/3, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]/4, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*2, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*3, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*4, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*5, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*6, '--k', 'linewidth', 1.5);
    hold off

    %axis([net.F0_start, net.F0_end net.F0_start, net.F0_end])
    axis([0, net.F0_end, 5, net.F0_end])

    % axis square
    grid on;
    xlabel('$f_0$ [Hz]', 'interpreter', 'latex', 'fontSize', 30)
    ylabel('$f_0^{est}$ [Hz]', 'interpreter', 'latex', 'fontSize', 30)
    title('Predictions - Classified by first harmonic number')
    legend(lgnd);
end


%%
if (0)    
    figure( 14 + fig_num );
    
    %
    if ~isfield(results.tst, 'salience1')
        Efun = @(S,X) sqrt( sum( (S - X).^2, 2) )/(net.ft_amp^2);
        salience1 = zeros(N_epoch2,1);
        for nn = 1:N_epoch2
            desired_nn = NeuSel(pitch_ML(nn), net.ft_cf, neu_std, net.ft_amp);
            salience1(nn) = 1./Efun(results.tst.score(nn,:), desired_nn);

        end
    else
        salience1 = results.tst.salience1(1:N_epoch2);
    end
    
    
    %plot(results.tst.F0(1:N_epoch2), pitch_ML, '.', 'markersize', 16); 
    %plot(results.tst.pitch_SACF(1:N_epoch2), pitch_ML, '.'); 
    
%     markersize = salience1;
%     markersize = min(100, salience1);

    %scatter_color = [salience1(1:N_epoch2), zeros(N_epoch2,1), 1-salience1(1:N_epoch2)];
    scatter_color = salience1/max(salience1);
    markersize = 100*scatter_color;
    scatter(results.tst.F0(1:N_epoch2), pitch_ML, markersize, scatter_color, 'fill');
    
    set(gca, 'fontSize', 20);
    grid on
    %axis square
    xlabel('$f_0$ [Hz]', 'interpreter', 'latex', 'fontSize', 30)
    ylabel('$f_0^{est}$ [Hz]', 'interpreter', 'latex', 'fontSize', 30)
    title('Predictions')
    hold on
    plot([0, net.F0_end], [0, net.F0_end], '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]/2, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]/3, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]/4, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*2, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*3, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*4, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*5, '--k', 'linewidth', 1.5);
    plot([0, net.F0_end], [0, net.F0_end]*6, '--k', 'linewidth', 1.5);
    hold off
    %axis([net.F0_start, net.F0_end net.F0_start, net.F0_end])
    axis([0, net.F0_end, 0, net.F0_end])
    
    colorbar;
end

















