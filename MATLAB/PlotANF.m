

function PlotANF(net, anf, cochlea_freq, F02, fignum)
%
% PlotANF(net, anf, cochlea_freq, fff_dic, fignum)
%
% Inputs:
% * cochlea_freq: the frequencies used in the cochlear model;
%

if ~exist('fignum', 'var')
    fignum = 89;    % default figure
end

figure(fignum);
cla;
fontsize = 24;

cochlea_freq_ = cochlea_freq/F02; %(end:-1:1);   % [Hz] ==> [kHz]

ttt = linspace(net.dic.start_msec, net.dic.end_msec, net.dic.time_smp);

% linear axis:
[X,Y] = meshgrid(ttt, cochlea_freq_);
surface(X,Y,anf,'EdgeColor','none', 'Linestyle','none'); 
view(0,90);
colorbar;

% set(gca,'yscale','log');

min_freq = net.dic.F0;
max_freq = 2e3;
% min_freq = 1e-3*min_freq;   % [Hz] ==> [kHz]
% max_freq = 1e-3*max_freq;   % [Hz] ==> [kHz]
min_freq = min_freq/F02;   % [Hz] ==> [kHz]
max_freq = max_freq/F02;   % [Hz] ==> [kHz]
axis([net.dic.start_msec, net.dic.end_msec, min_freq, max_freq]);

set(gca, 'fontsize', fontsize);

xlabel('Time [ms]');
ylabel(sprintf('CF [kHz] (Normalized by %g)', F02));
title('ANF')




