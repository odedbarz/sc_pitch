function [ pitch_est, pitch_opt, pitch_salience, P, freq_hs ] =...
    PitchEst( h, freq_hs, G, range_Hz, debug_mod )
%
% Input:
%   * h: the sparse representation vector   
%   * freq: the appropriate frequencies of the sparse vector
%   * sieve_sig: the sieves' sigma
%   * gaussLPF: [mean, sig], the two moments of the Gaussian LPF
%   * debug_mod: debug mode flag
%
% Output:
%   * pitch_est: The selected pitch, the one with the maximum salience in
%                Pr_pitch.
%   * pitch_opt: The different options of possible pitches
%   * pitch_salience: The salience of each of the possible pitches.
%   * Pr_pitch: The resulting vector of the multiplications between the sparse
%         representation h vector and the Gaussian seives.
%
%
% Desciption:
%   Calculates the possible pitches for a given sparse representation vector
%   h.
%
% Notes:
% * You must have: length(h) == length(freq)
%
    
    % interpulation factor:
%     dummy = length(freq);    
%     freq_hs = interp1( 1:dummy, freq, linspace(1, dummy, dummy*interp_factor) );
%     h = interp1( 1:dummy, h, linspace(1, dummy, dummy*interp_factor) )';


    % # of peaks for alternatives:
    NPeaks = 4;
    pitch_opt = nan(1, NPeaks);
    pitch_salience = nan(1, NPeaks);
    h = interp1( 1:length(h), h, linspace(1, length(h), length(freq_hs)) )';
      
    P = G*h;
    
    % Consider only certain range for the pitch (usually about +/- one octave):
    if exist('range_Hz', 'var') && ~isempty(range_Hz) && ~isnan(range_Hz(1)) 
        P(freq_hs<=range_Hz(1)) = 0;
        P(freq_hs>=range_Hz(2)) = 0;
    end    
    
    % pitch probability:
    P = P/sum(P);    % normalize everything
    
    % locate peaks:
    [salience, pitch_loc] = findpeaks(P,'MinPeakProminence',0.01*max(P), 'NPeaks', NPeaks, 'SortStr', 'descend');
    pitch_salience(1:length(salience)) = salience;
    [~,Ix_p] = max(pitch_salience);
    
    % pitch options:
    pitch_opt(1:length(pitch_loc)) = freq_hs(pitch_loc);
    
    % pitch estimation:
    pitch_est = pitch_opt(Ix_p);
        
    if isempty(pitch_est); 
        error('--> [PitchEst] ERROR: pitch_est is empty !!!\n'); 
    end;
    
    % debug mode:
    if exist('debug_mod', 'var') && (debug_mod)
        figure(98);
        subplot 211
        plot(freq_hs, h, 'linewidth', 3);
        title('The Sparse Code', 'interpreter', 'latex')
        xlabel('Hz', 'interpreter', 'latex');
        ylabel('SC Coef.', 'interpreter', 'latex');
        grid on;
        set(gca, 'fontsize', 36);
        
        subplot 212
        plot(freq_hs, P, 'linewidth', 3);
        hold on;
        plot(freq_hs(pitch_loc), pitch_salience, 'xr', 'markersize', 3, 'linewidth', 14);
        hold off;
        grid on;
        %axis([freq_hs(1), max(500,1.2*max(pitch_opt)), min(0,1.2*min(Pr_pitch)), 1.2*max(Pr_pitch)]);
        title(sprintf('Harmonic Sieve, $f_{est} = %.2f$ Hz', pitch_est), 'interpreter', 'latex');
        xlabel('Hz', 'interpreter', 'latex');
        ylabel('$S_p$', 'interpreter', 'latex');
        set(gca, 'fontsize', 36);
        drawnow;
    end
    
end

