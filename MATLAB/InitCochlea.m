%
% InitCochlea.m
%
% Init. the cochlear model


% update the cochlea's parameters according to the ANF method:
net = UpdateCochlea( net );

%
switch net.anf.method
    case 4  % Ray Meddis� hair cell model
        fcoefs = MakeERBFilters(...
            net.cochlea.Fs,...
            net.cochlea.Nfb,...
            net.cochlea.lowfreq ...
        );

        cochlea_freq = ERBSpace(net.cochlea.lowfreq, net.cochlea.Fs/2, net.cochlea.Nfb);
        
        fprintf('\n --> Using Ray Meddis� hair cell model (from Malcolm Slaney auditory toolbox)\n');
        
        % Pre-emphasis filter - middle ear filter (taken from Meddis & O'Mard, 1997):
        Hd_ear = OuterEarFilt(net.stim.Fs);  
                

    case 10  % Zilany(2014)
        fprintf('\n-> Using Zilany et al. Code, (2014)\n');
        addpath( net.AN_zilany_dir );

        cochlea_freq = ERBSpace(net.cochlea.lowfreq, net.cochlea.highfreq, net.cochlea.Nfb);
        
        if ~nnz([100e3, 200e3, 500e3] == net.cochlea.Fs)   % sampling rate in Hz (must be 100, 200 or 500 kHz)
            error('-> ERROR in InitCochlea(): Sampling rate in Hz (must be 100, 200 or 500 kHz) !!!');
        end
        
        % don't use the following parameters in this option:
        Hd_ear = nan;
        fcoefs = nan;
        
        
end











