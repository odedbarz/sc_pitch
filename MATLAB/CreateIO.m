% 
% CreateIO.m
% 
% function [stim, F0, harmonics, stim_phase] = CreateIO(net, F0, harmonics, dF, stim_phase)
%
% 
function [stim, F0, harmonics, stim_phase, dF] = CreateIO(net, F0, harmonics, dF, stim_phase)

% if (4 > nargin) || ( isexist('dF', 'var') && isempty(dF))
if (4 > nargin) || isempty(dF) || isnan(dF)
    inharmonic_flag = 0;
    dF = [];
else 
    inharmonic_flag = 1;
    fprintf('-> CreateIO(): inharmonic_flag is ON!\n');
end

complex_hrm_flag = 1;   % the complex harmonic flag (the "usual" harmonic structure)
% IRN_flag    = 0;
% Cmajor_flag = 0;
% SAM_flag    = 0;   
% WM_flag     = 0;    % Wightman's argument agaist the peak-pickers
% APh_flag    = 0;


% choose the right policy
if 1 == net.training_flag
    freq_slc = net.trn_freq_slc;
    hrm_slc  = net.trn_hrm_slc;
else
    freq_slc = net.tst_freq_slc;
    hrm_slc  = net.tst_hrm_slc;
end


% ut:
% ---        
% stim = zeros(net.stim.n_samples,1);
if (nargin <= 1) || isempty(F0)
    switch freq_slc
        case 0  % Uniform sampling (F0)
            F0 = (net.stim.F1-net.stim.F0)*rand(1) + net.stim.F0; % base frequency of channel 1        

        case 1  % Normal sampling (F0)
            F0 = neu_std*randn(1) + ft_cf; % base frequency of channel 1        

        case 2
            %pd = makedist('Gamma','a',2,'b',2);
            %F0 = random(pd,[1,1]);
            
            F0 = 2*net.stim.F1;
            F_start = 100; % net.stim.F0 % [Hz]
            while F0 > net.stim.F1
                %F0 = 100*random('gamma',2,2,[1,1]) + net.stim.F0;
                F0 = 140*random('gamma', 1.1, 2.0, [1,1]) + F_start;
                %F0 = 80*random('gamma', 1.1, 2.0, [1,1]) + F_start;
            end
            
        case 3
            F0 = (net.stim.F1-net.stim.F0)*rand(1) + net.stim.F0; % base frequency of channel 1        
            inharmonic_flag = 1;    '!!!!!!!!'
            dF = 5.0*rand(1) - 2.5;
            
            
        otherwise
            error('=> freq_slc is undefine!')

    end
    
else
    % fprintf('-> CreateIO(): using <F0>=%g from input...\n', F0);
    
end


% ----------------------------------------------------------------------------
% Set the weights for each of harmonic:
if (nargin <= 2) || isempty(harmonics)
    switch hrm_slc
        case 0 % Option 0: flat 
            harmonics = ones(1,net.N_hrm);

        case 1 % Option 2: Using log-normal distribution
            stim_mu_1 = (net.stim.mu(2)-net.stim.mu(1))*rand(1) + net.stim.mu(1);
            stim_sigma_1 = (net.stim.sigma(2)-net.stim.sigma(1))*rand(1) + net.stim.sigma(1);
            harmonics = lognpdf( 1* [1:net.N_hrm], stim_mu_1, stim_sigma_1); % using log-normal function to create correlation


        case 2
            %           [ 1 2 3 4 0 0 ... 0]  
            harmonics = zeros(1, net.N_hrm);
            harmonics(randperm(7,5)) = 1;
            %harmonics(1:3) = 1;
            
        case 3
            %           [ 1 2 3 4 5 6 7 8 9 10]  
            harmonics = zeros(1, net.N_hrm);
            harmonics(0+(1:4)) = 1;

        case 4 % log-normal
            
            if ~exist('harmonics', 'var')
                harmonics = zeros(1,net.N_hrm);
            end
            
            % make sure that there are sufficient harmonics in the
            % stimulus:
            while 2 >= nnz(harmonics)
                stim_mu_1 = (net.stim.mu(2)-net.stim.mu(1))*rand(1) + net.stim.mu(1);
                stim_sigma_1 = (net.stim.sigma(2)-net.stim.sigma(1))*rand(1) + net.stim.sigma(1);

                harmonics = lognpdf( 1* [1:net.N_hrm], stim_mu_1, stim_sigma_1); % using log-normal function to create correlation

                harmonics = harmonics/norm(harmonics);

                harmonics_threshold = 0.3*rand(1);

                harmonics( harmonics < harmonics_threshold ) = 0; 
            end

        case 5
            harmonics = ones(1, net.N_hrm);
            num_of_zeros = min(10,ceil(net.N_hrm*rand));
            nz_hrm = randi(net.N_hrm, [num_of_zeros,1]);
            harmonics(nz_hrm) = 0;
            
            
        case 6  % its a 5+ case
            harmonics = ones(1,net.N_hrm);
            harmonics(randi(net.N_hrm,[1,floor(net.N_hrm/3)-1])) = 0;

                
        case 7  % 
            harmonics = zeros(1,net.N_hrm);
            harmonics(randi(net.N_hrm,[1,floor(net.N_hrm/2)+2])) = 1;
            
        case 8
            if ~exist('harmonics', 'var')
                harmonics = zeros(1,net.N_hrm);
            end
            
            % make sure that there are sufficient harmonics in the
            % stimulus:
            while 4 > nnz(harmonics)
                stim_mu_1 = (net.stim.mu(2)-net.stim.mu(1))*rand(1) + net.stim.mu(1);
                stim_sigma_1 = (net.stim.sigma(2)-net.stim.sigma(1))*rand(1) + net.stim.sigma(1);

                harmonics = lognpdf( 1* [1:net.N_hrm], stim_mu_1, stim_sigma_1); % using log-normal function to create correlation

                harmonics = harmonics/norm(harmonics);

                harmonics_threshold = 0.2; 

                harmonics( harmonics < harmonics_threshold ) = 0; 
            end

        case 9
            harmonics = zeros(1,net.N_hrm);
            harmonics(1:min(net.N_hrm,6)) = 1;
            %harmonics(1) = 1;
            
            
        case 10
            harmonics = zeros(1,net.N_hrm);
            harmonics(8:20) = 1;
            
        case 11 % IRN (iterated rippled noise)
            if 0 < net.stim.IRN.sign; cos_sign_str = '+'; else cos_sign_str = '-'; end;
            fprintf('-> ### Performing IRN (cos %s) ###\n', cos_sign_str);
            
            % The IRN doesn't have an explicit harmonic structure:
            harmonics = zeros(1,net.N_hrm);     % no need for explicit harmonics in this case
            
            IRN_flag = 1;   % set the IRN flag to ON
            complex_hrm_flag = ~IRN_flag;
            
            if net.stim.IRN.rep > 0
                delay_smp = floor((1/F0)/(1/net.cochlea.Fs));   % the iterated delay, in samples
            end
            
            stim = randn(net.stim.n_samples, 1);
            for rep = 1:net.stim.IRN.rep
                stim = stim + net.stim.IRN.sign * circshift( stim, delay_smp );
            end
            
            % LPF at 4kHz (see: Yust, Pitch of itterated ripple noise, 96)
            stim = net.stim.IRN.Hd_irn.filter(stim);
            

        case 12
            harmonics = ones(1,net.N_hrm);
            harmonics(randi(net.N_hrm, [1,2])) = 0;
            
            
        case 13
            if ~exist('harmonics', 'var')
                harmonics = zeros(1,net.N_hrm);
            end
            
            % make sure that there are sufficient harmonics in the
            % stimulus:
            while 4 > nnz(harmonics)
                stim_mu_1 = (net.stim.mu(2)-net.stim.mu(1))*rand(1) + net.stim.mu(1);
                stim_sigma_1 = (net.stim.sigma(2)-net.stim.sigma(1))*rand(1) + net.stim.sigma(1);

                harmonics = lognpdf( 1* [1:net.N_hrm], stim_mu_1, stim_sigma_1); % using log-normal function to create correlation

                harmonics = harmonics/norm(harmonics);

                harmonics_threshold = 0.2; 

                harmonics( harmonics < harmonics_threshold ) = 0; 
            end
            
            harmonics( harmonics ~=0 ) = 1;
           
        case 14
            if ~exist('harmonics', 'var')
                harmonics = zeros(1,net.N_hrm);
            end
            
            % make sure that there are sufficient harmonics in the
            % stimulus:
            while 4 > nnz(harmonics)
                stim_mu_1 = (net.stim.mu(2)-net.stim.mu(1))*rand(1) + net.stim.mu(1);
                stim_sigma_1 = (net.stim.sigma(2)-net.stim.sigma(1))*rand(1) + net.stim.sigma(1);

                harmonics = lognpdf( 1* [1:net.N_hrm], stim_mu_1, stim_sigma_1); % using log-normal function to create correlation

                harmonics = harmonics/norm(harmonics);

                harmonics_threshold = 0.2; 

                harmonics( harmonics < harmonics_threshold ) = 0; 
                
                dummy = find(harmonics~=0,1,'last');
                harmonics( (dummy+1):end ) = 1; 
                harmonics(1:dummy) = 0;
            end
            
        case 15
            harmonics = zeros(1,net.N_hrm);
            
            while 8 >= nnz(harmonics(8:end))
                harmonics(randi(net.N_hrm, [10,1])) = 1;
                harmonics(1:7) = 0;
            end
            
            
        case 21     % Approx. to Musical Note of C-major, Terhardt 1979
            fprintf('-> !!! Performing the Musical Cord stimulus test:\n');
            fprintf('\t The *** C-major thriad ***\n');
            
            % activatre the complex harmonic flag:
            Cmajor_flag = 1;
            complex_hrm_flag = ~Cmajor_flag;

            ttt = linspace(0, net.secs, net.stim.n_samples);
            
            net.stim.musical_cord.F_list
            
            % (Hewitt & Meddis, 1991)
            F0_G4 = net.stim.musical_cord.F_list(1);     % Hz
            F0_C5 = net.stim.musical_cord.F_list(2);     % Hz
            F0_E5 = net.stim.musical_cord.F_list(3);     % Hz
            
            F0 = 130;   % [Hz] output the equivalance fundamental
            
            %{
            % (Hewitt & Meddis, 1991)
            F0_G4 = 392;    % Hz
            F0_C5 = 523.2;  % Hz
            F0_E5 = 659.2;  % Hz

            % % 
            %F0_G4 = 3*F0;  % Hz
            %F0_C5 = 4*F0;  % Hz
            %F0_E5 = 5*F0;  % Hz
            %}
            
            % Creatin the 
            harmonics_G4 = zeros(1,net.N_hrm); harmonics_G4(1:4) = 1; % [0.6981 0.6221 0.3133 0.1460];
            harmonics_C5 = zeros(1,net.N_hrm); harmonics_C5(1:3) = 1; %[0.6981 0.6221 0.3133];
            harmonics_E5 = zeros(1,net.N_hrm); harmonics_E5(1:3) = 1; %[0.6981 0.6221 0.3133];
            
            %
            stim_G4 = harmonics_G4 * sin( 2*pi* F0_G4*(1:net.N_hrm)' *ttt + stim_phase * ones(net.N_hrm, net.stim.n_samples) );     
            stim_C5 = harmonics_C5 * sin( 2*pi* F0_C5*(1:net.N_hrm)' *ttt + stim_phase * ones(net.N_hrm, net.stim.n_samples) );     
            stim_E5 = harmonics_E5 * sin( 2*pi* F0_E5*(1:net.N_hrm)' *ttt + stim_phase * ones(net.N_hrm, net.stim.n_samples) );     
            
            stim = (stim_G4 + stim_C5 + stim_E5)';

            % This is a matter of convension - don't write the explicit
            % harmonics in this particular case:
            harmonics = zeros(1,net.N_hrm);     % no need for explicit harmonics in this case
            
            
            
        case 22     % SAM: Sinusoidally Amplitude Modulation noise 
            fprintf('-> ### Performing SAM ###\n');
            
            SAM_flag = 1;
            complex_hrm_flag = ~SAM_flag;
            
            ttt = linspace(0, net.secs, net.stim.n_samples)';

            ct = net.stim.SAM.A * randn( net.stim.n_samples, 1 );  % carrier vector (the white noise)
            mt = net.stim.SAM.M * cos( 2*pi*F0*ttt );
            
            stim = (1 + mt).*ct;
            %stim = stim/max(stim(:));   % normalaize by the maximum peak
            
            harmonics = zeros(1,net.N_hrm);     % no need for explicit harmonics in this case
            
            
        case 23 % Wightman's counter example against the peak-pickers
            fprintf('-> ### Wightman''s counter example - against the peak-pickers ###\n');
            
            WM_flag = 1;
            complex_hrm_flag = ~WM_flag;
            
            ttt = linspace(0, net.secs, net.stim.n_samples)';

            nn = 5;     % # of main harmonic 
            m = 1;      % modulation index
            
            stim = cos( 2*pi*nn*F0 *ttt + stim_phase);
            stim = stim + 0.5*m*cos( 2*pi*(nn-1)*F0 *ttt + stim_phase);
            stim = stim + 0.5*m*cos( 2*pi*(nn+1)*F0 *ttt + stim_phase);
            
            harmonics = zeros(1,net.N_hrm);     % no need for explicit harmonics in this case
          
            
         case 24 % QFM: Quasifrequency modulation
            fprintf('-> ### QFM: Quasifrequency modulation ###\n');
            
            QDM_flag = 1;
            complex_hrm_flag = ~QDM_flag;
            
            ttt = linspace(0, net.secs, net.stim.n_samples)';

            F0 = net.stim.QFM.F0;
            m = net.stim.QFM.m;
            n = net.stim.QFM.n;
            
            stim = sin( 2*pi*n*F0 *ttt + pi/2 );
            stim = stim + 0.5*m*sin( 2*pi*(n-1)*F0 *ttt );
            stim = stim + 0.5*m*sin( 2*pi*(n+1)*F0 *ttt );
            
            harmonics = zeros(1,net.N_hrm);     % no need for explicit harmonics in this case
           
            
        case 25     % Aph vs. CPh
            fprintf('-> ### APh vs. CPh (Alternating Phase) ###\n');
            
            APh_flag = 1;
            complex_hrm_flag = ~APh_flag;
            
            ttt = linspace(0, net.secs, net.stim.n_samples)';

            harmonics = zeros(1,net.N_hrm);
            harmonics(4:24) = 1;
            
            % phase delayes:
            phi_v = cumsum(ones(net.N_hrm,1));
            phi_v = net.stim.APh.phi*mod(phi_v,2);
            
            stim = harmonics * sin( 2*pi* F0*(1:net.N_hrm)' *ttt' + phi_v*ones(size(ttt')) );
            
            stim = stim(:);     % make a row vector
            %harmonics = zeros(1,net.N_hrm);     % no need for explicit harmonics in this case

            
        case 26     % Transposed stimulus (Oxenham, 2004)
            fprintf('-> ### Transposed stimulus (Oxenham, 2004) ###\n');
            
            TS_flag = 1;
            complex_hrm_flag = ~TS_flag;
            
            % a vector of carrier frequencies:
            Fc = net.stim.TT.Fc;      % [Hz] carrier freq.            
            
            % Create a sine wave:
            ttt = linspace(0, net.secs, net.stim.n_samples)';
            
            harmonics = zeros(1,net.N_hrm);
            harmonics(net.stim.TT.hrm) = 1;                        

            
            hrm4sine = net.stim.TT.hrm(1):net.stim.TT.hrm(end);
            
            signals = sin( 2*pi* F0*ttt*hrm4sine );

            % half-wave rectify:
            signals = max( 0, signals );            

            % Butterworth LP filter at 0.2*Fc:
            Hlpf_4k  = LPF_Butterworth4( net.stim.Fs, 0.2*Fc(1), 3*net.stim.n_samples );
            Hlpf_6k  = LPF_Butterworth4( net.stim.Fs, 0.2*Fc(2), 3*net.stim.n_samples );
            Hlpf_10k = LPF_Butterworth4( net.stim.Fs, 0.2*Fc(3), 3*net.stim.n_samples );

            % a vector of carrier frequencies:
            %Fc = net.stim.TT.Fc;      % [Hz] carrier freq.            

            % Build the carrier:
            sc = @(nn) sin( 2*pi* Fc(nn) * ttt(:) );

            % refer to the nn-th signal:
            %sx = @(nn) signals(:,net.stim.TT.hrm(nn));
            sx = @(nn) signals(:,nn);
            
            
            % Amplitude Modulation:
            if 1 == harmonics(1)    % Pure tone

                % Apply the Butterworth LPF:                    
                s1 = ifft( fft(sx(1), 3*net.stim.n_samples) .* Hlpf_4k(:), 'symmetric');                  
                s2 = ifft( fft(sx(1), 3*net.stim.n_samples) .* Hlpf_6k(:), 'symmetric');
                s3 = ifft( fft(sx(1), 3*net.stim.n_samples) .* Hlpf_10k(:), 'symmetric');

            else    % Upper harmonics
                
                % Apply the Butterworth LPF:                    
                s1 = ifft( fft(sx(1), 3*net.stim.n_samples) .* Hlpf_4k(:), 'symmetric');                  
                s2 = ifft( fft(sx(2), 3*net.stim.n_samples) .* Hlpf_6k(:), 'symmetric');
                s3 = ifft( fft(sx(3), 3*net.stim.n_samples) .* Hlpf_10k(:), 'symmetric');

            end

            s1 = s1(1:net.stim.n_samples);
            s2 = s2(1:net.stim.n_samples);
            s3 = s3(1:net.stim.n_samples);
            
            
            stim = s1.*sc(1) + s2.*sc(2) + s3.*sc(3);
            
            
        case 27     % ALT-phase (sine-cosine harmonics)
            ALT_phase = 1;
            complex_hrm_flag = ~ALT_phase;
            
            ttt = linspace(0, net.secs, net.stim.n_samples)'; 
  
            harmonics = zeros(1, net.N_hrm);
            hrm_list = F0*(1:net.N_hrm);
                
            switch net.stim.Shackelton94.region
                case 'LOW'
                    harmonics( (hrm_list>125)&(hrm_list<=625) ) = 1; 
                    
                case 'MID'
                    harmonics( (hrm_list>1375)&(hrm_list<=1875) ) = 1; 
                    
                case 'HIGH'
                    harmonics( (hrm_list>3900)&(hrm_list<=5400) ) = 1; 
                    
            end
            
            % The even harmonics:
            odd_hrm     = harmonics .* mod(1:net.N_hrm,2);   % odd harmonics
            even_hrm    = harmonics .* double(~odd_hrm);   % odd harmonics
            
            stim_odd    = odd_hrm * sin( 2*pi*F0*(1:net.N_hrm)' *ttt' ); 
            if strcmpi('ALT', net.stim.Shackelton94.type)
                stim_even   = even_hrm * cos( 2*pi*F0*(1:net.N_hrm)' *ttt' );   
            elseif strcmpi('SINE', net.stim.Shackelton94.type)
                stim_even   = even_hrm * sin( 2*pi*F0*(1:net.N_hrm)' *ttt' );   
            end
            stim = stim_even + stim_odd;
            stim = stim(:);     % a column vector
            
        otherwise
            error('=> hrm_slc is undefine!')

    end
    
else
    % fprintf('-> CreateIO(), using <harmonics> from input...\n');
    %disp(harmonics);
    
end

%% phase:
if(5 > nargin) || isempty(stim_phase)
    stim_phase = 2*pi * rand(1);
end


%% Create the harmonic complex:
% %{
if complex_hrm_flag %((~IRN_flag) && (~Cmajor_flag))
    
    ttt = linspace(0, net.secs, net.stim.n_samples)'; 

    if 0 == inharmonic_flag
        stim = harmonics * sin( 2*pi* F0*(1:net.N_hrm)' *ttt' );     

    else
        if dF < 0 % || dF > 1
            error('-> error in CreateIO.m: dF should be given as ratio in the range [0,1] !');
        end
        
        %F0_dF = F0 + dF*(0:(net.N_hrm-1))';
        F0_dF = F0 + dF*(1:net.N_hrm)';

        
        stim = harmonics * sin( 2*pi* F0_dF *ttt' );     

        % return the lowest frequency of the complex:
        F0 = F0_dF(1);
        
        % return the shift:
        %dF = F0;
        
    end
    
    
    % delay (phase):
    if 0 ~= stim_phase
        stim_delay = floor( ( stim_phase/(2*pi)*1/F0 )/net.stim.dt );
        stim = stim';
        stim((stim_delay+1):end) = stim(1:(end-stim_delay));
        stim(1:stim_delay) = 0;
    else
        stim = stim';
    end
    
    
end



%%
% stimulus' amplitude in Pascal:
stim_amp_Pa = dBSPL2Pa( net.stim.amp_dBSPL );  % [dB SPL] --> [Pa]

% Add white noise: 
if ~isnan( net.stim.noise_level ) && (0 < net.stim.noise_level)
    wnoise = randn(size(stim));     % white random noise ~N(0,1)
    %noise_level_Pa = 20e-6*10^( net.stim.noise_level/20 );	% [dB SPL] --> [Pa]
    noise_level_Pa = dBSPL2Pa( net.stim.noise_level );  % [dB SPL] --> [Pa]
    %stim = stim_amp_Pa*stim/max(stim(:)) + noise_level_Pa/max(wnoise(:)) * wnoise;
    stim = stim_amp_Pa*stim/max(stim(:)) + noise_level_Pa * wnoise;     % E{wnoise} == 0
    
else
    stim = stim_amp_Pa*stim/max(stim(:)); 
    
end

% windowing the stimulus - avoid the onset effect:
stim = stim .* net.stim.win;


    
























