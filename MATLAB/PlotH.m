%
% PlotH.m
%

linewidth = 5;
markersize = 24;
fontsize = 40;

figure(11);
plot(fff_dic/F02, h, '.-', 'linewidth', linewidth, 'markersize', markersize)
set(gca, 'fontsize', fontsize)

stim_hrm = nonzeros(F02.*[1:net.N_hrm].*harmonics2)';    % current stimulus' harmonics
stim_hrm = stim_hrm/F02;    % [Hz] ==> [kHz]

hold on
plot(repmat(stim_hrm,2,1), repmat([0 2]', 1, 4), '--k', 'linewidth', 2);
hold off

title(sprintf('h (%s)', mat2str(stim_hrm)));
xlabel(sprintf('Normalized Frequency (by %g Hz)', F02))

axis([130/F02, 16, 0, 1.0])
