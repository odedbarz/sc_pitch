%
% InitSC.m
%
%
% 12/08/2015:
%   * Get rid of the small nonzeros coeff in DDI.
%


% check the dictionary dimensions:
if base_db.D_dim1 ~= D_dim1
    error('--> [InitSC.m]: @ ERROR: check for <base_db.D_dim1 ~= D_dim1>!!!\n');
elseif base_db.D_dim2 ~= D_dim2
    error('--> [InitSC.m] @ ERROR: check for <base_db.D_dim2 ~= D_dim2> !!!\n');
end

        
% fprintf('--> temporal window size: %.2g [msec]\n', ista.win.msec);
fprintf('\n-> [InitSC.m]: ista.method = %d <==> ', ista.method);

switch ista.method

    % (my) ISTA:
    case 1
        fprintf('SC is based on my implementation of ISTA\n');
        
        % ISTA parameters:
        ista.alpha_ratio = 0.10; % 0.1;     
        ista.gamma_ratio = 0.1; % 0.01;    
        ista.max_err     = 1e-10;
        ista.seed_level  = 0.01;    %*std(D(:)); % 1e-6;
        ista.max_loop_thr= 20e3;
        ista.debug_mode  = 0;
        ista.h_positive  = 0;       % removes negative values from h
        ista.D = base_db.D;

    % (MATLAB's) lasso:
    case 2
        fprintf('SC is based on MATLAB''s lasso command\n');
        
        % ISTA parameters:
        ista.alpha_ratio = 0.10; % 0.1;     
        ista.gamma_ratio = 1e-2; % 0.01;    
        ista.max_err     = 1e-10;
        ista.seed_level  = 0.01;    %*std(D(:)); % 1e-6;
        ista.max_loop_thr= 20e3;
        ista.debug_mode  = 0;
        ista.h_positive  = 0;       % removes negative values from h
        ista.D           = base_db.D;
        ista.UseParallel = 'UseParallel';
        ista.RelTol      = 1e-4;

        
    % (Foldiak's based) RNN:
    case 3
        fprintf('SC is based on Foldiak''s 2009 paper\n');
                
        ista.lambda = 0.01;
        ista.tau    = 1e-2;
        ista.tau2   = 1e-2;     % [sec] time constant of the averaging layer 
        ista.alpha  = 0.01;     % average constant for the LPF of h
        ista.gamma  = 0.02;     % threshold factor 
        ista.dt     = 1/net.dic.Fs; %1e-2*ista.tau;     % numerical time step
        ista.dt2tau = ista.dt/ista.tau;
        ista.I_win  = 1:ista.win.winlen;   % using a rectangular window
       
        % thresold function:
        ista.phi    = @(u) (u > 0).*sqrt(u) + (u <= 0).*(0);  % the RNN's non-linear function
        
        % transpose + normalization:
        ista.Dt = ( D ./ (ones(D_dim1,1) * sqrt(sum( D.^2, 1 ))) )';    % Dt <- D'/|D|^2
        ista.Dt = ista.Dt - mean(ista.Dt,2)*ones(1,D_dim1);

        % clac. the anti-hebian matrix:
        ista.W = AntiHebianMatrix( fff_dic, D_dim2 );
       
        
        
        
    % (Rozell's based) RNN:
    case 4    
        fprintf('SC is based on Rozell''s 2008 paper\n');
        
        ista.win.msec   = 2.0;                                      % [msec] temporal size of the sliding window
        ista.win.t_smp  = round(1e-3*ista.win.msec*net.dic.Fs);      % [smp] temporal size of the sliding window, in samples
        ista.win.winlen = net.cochlea.Nfb * ista.win.t_smp;          % [smp] total length of the sliding window, in samples
        ista.err_fun    = @(h,h0) norm(h-h0,2)/norm(h0,2);
        ista.debug      = 0;
        fprintf('--> [InitSC.m]: RNN''s temporal sliding window size: %.2g [msec]\n', ista.win.msec);
        
        ista.lambda     = 0.01; % 0.01
        ista.tau        = 1e-2;        
        ista.alpha      = 0.01; %0.01;     % average constant for the LPF of h       
        ista.dt         = 1/net.dic.Fs; %1e-2*ista.tau;     % numerical time step
        ista.dt2tau     = ista.dt/ista.tau;
        ista.I_win      = 1:ista.win.winlen;   % using a rectangular window
        
        % thresold function:
        ista.theta = @(x) x.*(abs(x) >= ista.lambda);
        
        % transpose + normalization:
		fprintf('--> InitSC.m: starting Dt calc...\n');
        Dt = zeros(D_dim2, D_dim1);
		for ii = 1:D_dim2
			Dt(ii,:) = D(:,ii)./norm( D(:,ii) );  %sqrt(sum( D(:,ii).^2, 1 ));
			Dt(ii,:) = Dt(ii,:) - mean(Dt(ii,:));
            Dt(ii,:) = max(0,Dt(ii,:));     % 12/08/2015
		end
		
		fprintf('--> InitSC.m: starting DDI calc...\n');
        DDI = Dt*(Dt');
        DDI = DDI - diag(diag(DDI));
        
        ista.Dt     = Dt;
        ista.DDI    = DDI;
        clear Dt DDI
        
        
    otherwise
        error('!!! Error in <InitSC.m>: wrong <ista.method> option !!!\n');

end


fprintf('\n');









