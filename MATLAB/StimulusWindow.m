function [ stim_win ] = StimulusWindow( stim_db, option )
%
% Calc. the stimulus window shape.
%

stim_win = ones( stim_db.n_samples, 1 );

if nargin<2
    option = 0; % default
end

switch option
    case 0
        [];   % do nothing
        
    case 1
        stim_win(1:stim_db.L_win) = tukeywin( stim_db.L_win, 0.3 );
    
    case 1
        dummy = tukeywin( stim_db.L_win, 0.3 );
        stim_win(1:floor(stim_db.L_win/2)) = dummy(1:floor(end/2));
        
    case 2
        dummy = hanning(stim_db.L_win);
        stim_win(1:floor(stim_db.L_win/2)) = dummy(1:floor(end/2));
        
end

end

