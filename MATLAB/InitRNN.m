%
% InitRNN.m
%

%% Create x0
x0 = 0.5*randn(net.N,1); %0.5


%% Create M - Efficacy matrix
M = sprandn(net.N,net.N,net.p)*net.g*net.scale;     % p is the density of the matrix (non-zero values). 
M = full(M);

%% Create Wf - feedback control vector\matrix
% Wf = 2.0*(rand(net.N, net.z_d1) - 0.5); 
Wf = 2.0*(rand(net.N, net.z_d1) - 0.5); 


%     '!!!!!!!!! Wf !!!!!!!!!!!!'
%     Wf = 0*Wf;
%     '!!!!!!!!! Wf !!!!!!!!!!!!'

    
%% Jin - Create the input transfer matrix
Jin = zeros(net.N,net.Jin_d2); 

Jin_opt = 1

if 1 == Jin_opt
    Jin(1:net.Jin_d2*floor(net.N/net.Jin_d2),1:net.Jin_d2) =...
        repmat( eye(net.Jin_d2,net.Jin_d2), floor(net.N/net.Jin_d2), 1 );
elseif 2 == Jin_opt
    Jrnd = randn(1, net.cochlea.N_filter_banks);
else
    error('-> Jin_opt is wrong');

%% P - Create the inverse correlation matrix
P = zeros(net.N_rls, net.N_rls, net.z_d1);
for i = 1:net.z_d1
    P(:,:,i) = 1/net.alpha * eye(net.N_rls); 
end


%% z0 - feedback control vector\matrix
z0 = 0.5*rand(net.z_d1,1);


%% create the output weights
wo = zeros(net.N_rls, net.z_d1); 


%% Save to binary files:
SaveMtx(x0, net.x0_filename, net.partype);
SaveMtx(M(:), net.M_filename, net.partype);
SaveMtx(Wf, net.Wf_filename, net.partype);
SaveMtx(Jin(:), net.Jin_filename, net.partype);
SaveMtx(P(:), net.P_filename, net.partype);
SaveMtx(z0, net.z0_filename, net.partype);
SaveMtx(wo, net.wo_filename, net.partype);

% '!!! clear x0 M Wf Jin P z0 wo !!!'
% clear x0 M Wf Jin P z0 wo

%% Distance matric for f(zt-ft):
Dxy = @(x) mean( x(ceil(end/2):end,:), 1 );
% Dxy = @(x,y) mean( max(0,1./max(1e-2,ft_win).*x) );
% Dxy = @(x,y,v) v*abs( 1-norm(x-y) );


%% Training...
%clear net

%net = CreateDB; 
%results.trn = ResultsCreate( net.N_epoch, net.z_d1, net.N_hrm, net.n_samples );

% anf_win = 2001:(net.n_samples-100);

%
InitCochlea;
   
%
if (~net.USE_CUDA)
    x = x0;
    r = tanh(x);
    z = z0;
    zt = zeros(net.n_samples, net.z_d1);
    k = zeros(net.N, 1);    
    wo = zeros(net.N, net.z_d1);   
%     k = zeros(net.N, 1);
%     dw = zeros(net.N, 1);
    
    dt2tau = net.dt/net.tau;
end


%% Testing...
%clear net

% net.N_epoch2 = 400;
% net = CreateDB(net);

%results.tst = ResultsCreate( net.N_epoch2, net.z_d1, net.N_hrm );

%
InitCochlea;

%
ttt_stim = linspace(0, net.secs, net.cochlea.n_samples)'; 
fff = linspace(0, net.Fs, net.n_samples);
ttt = [0:net.dt:net.secs-net.dt]';
freq = linspace(net.F0_start, net.F0_end, net.n_samples);
time = linspace(0, net.msecs, net.n_samples);     % [msec]

%
if (~net.USE_CUDA)
    x = x0;
    r = tanh(x);
    z = z0;
    zt = zeros(net.n_samples, net.z_d1);
    k = zeros(net.N, 1);    
    wo = LoadMtx('wo', net.N_rls, net.z_d1);
%     k = zeros(net.N, 1);
%     dw = zeros(net.N, 1);
    
    dt2tau = net.dt/net.tau;
end


% anf_ch = 55 %54;
% 
% ft_delay_Hz     = floor( xxx_cf(anf_ch) );  % Hz
% ft_delay_smp    = 2*50; %round( (1/ft_delay_Hz)/net.dt );
% 
% fprintf( '-> anf(%d,:) = %.3g Hz\n', anf_ch, xxx_cf(anf_ch));
% fprintf( '-> ft_delay_Hz = %.3g Hz\n', ft_delay_Hz);
% fprintf( '-> ft_delay_smp = %.3g samples\n', ft_delay_smp);


%% for loop: Training...
%
%
net.training_flag = 1;   
WriteDB2File( net.train_filename, net );

fprintf('-----------------------------------------------------------\n');
fprintf(' Training...\n');
fprintf('-----------------------------------------------------------\n');

clock_trn = tic;

for tn = 1:net.N_epoch   % epoches
% for tn = 101:net.N_epoch   % epoches
    fprintf('-> tn = %d \t(N_epoch = %d)\n', tn, net.N_epoch)

    if ~mod(tn-1, 10)
        rng('shuffle');
    end
    
    [stim, F0, harmonics, stim_phase] = CreateIO(net, [], [], [], 0); %, F0, harmonics, dF); %, 0);       
    
    % create the ANF input to the RNN:
    CreateANF_2;
        
    % create the desired signal:
    CreateTarget_4;
    
    % run the model using CUDA:
    ForceIt;
    
    %results.trn = Results2Add( results.trn, net, tn, F0, ft_value, Dxy(zt), harmonics, stim_phase, pitch_SACF  );

    % IC:
    if (1 == net.IC_flag)
        SetIC( net, x0, z0 );        
    end
    
    if (net.USE_CUDA) % && (net.debug_mode)
        wo = LoadMtx('wo', net.N_rls, net.z_d1);
    end
    
    results.trn.norm2wo(tn) = norm(wo(:),2);   % sqrt(wo'*wo);
        
    
    % plot the stimulus & ANF:
    if ~mod(tn-1, 5) 
        fig_num = 10;
        %PlotStatus;
        %PlotStim;
        %AnalizeScores
        %chk_SACF;
        
        figure(fig_num);
        plot( [ft, zt], 'linewidth', 3 )
        hold on
        plot(abs(ft-zt), 'r', 'linewidth', 1 );
        hold off
        legend('f_t', 'z_t', 'abs(ft-zt)');
        grid on
        title(sprintf('f_0 = %.3g Hz',F0));
        %norm(ft-zt)
        
        
        if net.USE_CUDA
            wo = LoadMtx('wo', net.z_d1, net.N_rls);
        end
        %plot(wo, '.--');
        %legend('w_o')
        
        figure(fig_num+5)
        plot( results.trn.norm2wo(1:tn), '.--', 'markersize', 20 );
        grid on;
        
        fprintf('-> f_0 = %.3g Hz\n',F0);
        fprintf('-> harmonics: [ '); fprintf('%.2g ', harmonics); fprintf(']\n');
        
        drawnow;
 
    end    
end

fprintf('-----------------------\n');
fprintf('*** End of Training ***\n');
fprintf('-----------------------\n');

time_trn = toc(clock_trn);
clear clock_trn 
fprintf('-> training time: %g [min]\n', time_trn/60);



%% for loop: Testing...

net.training_flag = 0;    % change to testing 
WriteDB2File( net.test_filename, net );

fprintf('\n-----------------------------------------------------------\n');
fprintf(' Testing...\n');
fprintf('-----------------------------------------------------------\n');


clock_tst = tic;
