function X = ZCA( X )
% 
% ZCA whitening
%
% X = ZCA( X )
%

    %{
    % remove DC
    X = X - mean(X,2) * ones(1,size(X,2));

    X = X./(std(X,[],2) * ones(1,size(X,2)));
    %}

    % remove DC:
    X = X - mean(X,2) * ones(1,size(X,2));

    [U, S] = svd(X);

    diag_S = diag(S);

    diag_S = max(min( 0.001*nonzeros(diag_S)), diag_S );   % avoid devision by zeros
    
    %Nd = length(diag_S);
    
    %X = X * (V(:,1:Nd)*diag(sqrt(1./diag_S))*V(:,1:Nd)');
    X = (U * diag(sqrt(1./diag_S)) * U') * X;

end

