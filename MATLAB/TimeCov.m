function [ Ct ] = TimeCov( X )
%
%   [ Ct ] = TimeCov( X )
%
% Description:
%   "Time" covariance
%
%   * assuming that the second dimension of X is time
%   * auto-removal of the mean 
%

    [N, T] = size(X);

    Ct = zeros(N,N);

    for ii = 1:T
        xii = X(:,ii) - mean(X(:,ii));  % remove the mean
        
        Ct = Ct + xii*xii';

    end
    
    Ct = 1/(T-1)*Ct;
    
end

