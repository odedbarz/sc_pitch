%
% plot_pitch_shift_figures.m
%
%   plot the Pitch Shift figures
%

clc

for ii = 1:5
    figure(ii);
    plot(fff_dic/F0_tst, eval(['h',num2str(ii)]), 'linewidth', 5);
    title(sprintf('Complex Harmonics, F_0=200 Hz, \\DeltaF=%g Hz', (ii-1)*50));
    xlabel('Frequency [Hz]');
    set(gca, 'fontsize', 40);
    axis([3, 9, 0, 0.8])
end