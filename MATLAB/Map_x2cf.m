% 
% Cochlea mapping of distance (in [mm]) to CF along the BM, using the Greenwood relation.
% 

function cf_v = Map_x2cf( x_v )

cf_v = 165.4*( 10.^(0.06*x_v) - 0.88 );
cf_v = round( cf_v );




