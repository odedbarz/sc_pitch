function y = NetAnsF1( DB, ft_min, tn )
% Pred_Succ: Success of prediction.
%

if 3 <= nargin
    y = ft_min == DB.R(tn,1);
else
    y = ft_min == DB.R(:,1);
end

end

