
function [pitch_est_v, pitch_opt_v] = ReEstimatePitch( net, tst_db, fignum)
%
%
%
% To update the harmonic sieve run:
%   1. gauss_LPF = nan;         % (no LPF)
%   2. net.hs.sigma_type;       % make sure to choose the correct case
%   3. re-calc. the new harmonic sieve:
%       [net.hs.G, net.hs.freq_hs] = ...
%           CreateHarmonicSieve( fff_dic, gauss_LPF, net.hs.interp_factor, net.hs.sigma_type );
%   4. then run:
%       [pitch_est_v, pitch_opt_v] = ReEstimatePitch( net, tst_db, fignum );
%
%       fignum: number of the figure. set to zero if you don't want a
%       figure, or just don't include it in the input vars.
%


    if isfield(net.hs, 'sigma_type')
        fprintf( '[ReEstimatePitch.m]: net.hs.sigma_type == %d\n', net.hs.sigma_type );
    else
        fprintf('[ReEstimatePitch.m]: net.hs.sigma_type is UNDEFINED!\n' );
        fprintf('                     ==> assuming net.hs.sigma_type == 2 BY DEFAULT\n');
        net.hs.sigma_type = 2;
    end

    N_epoch2 = size(tst_db.H, 2);
    pitch_est_v = zeros(size(tst_db.pitch_est));
    pitch_opt_v = zeros(size(tst_db.pitch_opt));

    debug_est = 0;

    for kk = 1:N_epoch2
        
        Fd = tst_db.F02(kk);
        
        %[pitch_est_v(jj), pitch_opt_v(jj,:), pitch_salience, Pr_pitch, fff_hs ] = ...
        %   PitchEst( h, net.hs.freq_hs, net.hs.G, debug_est);
        [pitch_est_v(kk), pitch_opt_v(kk,:) ] = ...
           PitchEst_private( tst_db.H(:,kk), net.hs.freq_hs, net.hs.G, Fd, debug_est);


    end


    if (nargin >= 3) && (0 ~= fignum)
        figure(fignum)
        markersize = 30;
        fontsize   = 36;

        plot(tst_db.F02, pitch_opt_v, '.', 'markersize', markersize);
        grid on;
        set(gca, 'fontsize', fontsize);
        xlabel('$f_L$', 'interpreter', 'latex');
        ylabel('$\hat{f}_0$', 'interpreter', 'latex');
    end


end






%% 
function [ pitch_est, pitch_opt, pitch_salience, Pr_pitch, freq_hs ] =...
    PitchEst_private( h, freq_hs, G, Fd, debug_mod )
%
% Input:
%   * h: the sparse representation vector   
%   * freq: the appropriate frequencies of the sparse vector
%   * sieve_sig: the sieves' sigma
%   * gaussLPF: [mean, sig], the two moments of the Gaussian LPF
%   * debug_mod: debug mode flag
%
% Output:
%   * pitch_est: The selected pitch, the one with the maximum salience in
%                Pr_pitch.
%   * pitch_opt: The different options of possible pitches
%   * pitch_salience: The salience of each of the possible pitches.
%   * Pr_pitch: The resulting vector of the multiplications between the sparse
%         representation h vector and the Gaussian seives.
%
%
% Desciption:
%   Calculates the possible pitches for a given sparse representation vector
%   h.
%
% Notes:
% * You must have: length(h) == length(freq)
%
    
    % interpulation factor:
%     dummy = length(freq);    
%     freq_hs = interp1( 1:dummy, freq, linspace(1, dummy, dummy*interp_factor) );
%     h = interp1( 1:dummy, h, linspace(1, dummy, dummy*interp_factor) )';


    % # of peaks for alternatives:
    NPeaks = 4;
    
    pitch_opt = nan(1, NPeaks);
        
    h = interp1( 1:length(h), h, linspace(1, length(h), length(freq_hs)) )';
  
    Pr_pitch = G*h;
    Pr_pitch = Pr_pitch/sum(Pr_pitch);    % normalize everything
    
    
    % Get the area of one octave around Fd:
    Pr_pitch( ~((Fd > (0.5.^0.85)*freq_hs) & (Fd < (2.^0.85)*freq_hs)) ) = 0;
    
    
    % locate peaks:
    [pitch_salience, pitch_loc] = findpeaks(Pr_pitch,'MinPeakProminence',0.2*max(Pr_pitch), 'NPeaks', NPeaks, 'SortStr', 'descend');
    
    [~,Ix_p] = max(pitch_salience);
    
    % pitch options:
    pitch_opt(1:length(pitch_loc)) = freq_hs(pitch_loc);
    
    % pitch estimation:
    pitch_est = pitch_opt(Ix_p);
        
    if isempty(pitch_est); fprintf('-> [PitchEst]: pitch_est is empty !!!\n'); return; end;
    
    % debug mode:
    if exist('debug_mod', 'var') && (debug_mod)
        figure(98);
        subplot 211
        plot(freq_hs, h, 'linewidth', 3);
        title('The Sparse Code', 'interpreter', 'latex')
        xlabel('Hz', 'interpreter', 'latex');
        ylabel('SC Coef.', 'interpreter', 'latex');
        grid on;
        set(gca, 'fontsize', 36);
        
        subplot 212
        plot(freq_hs, Pr_pitch, 'linewidth', 3);
        hold on;
        plot(freq_hs(pitch_loc), pitch_salience, 'xr', 'markersize', 3, 'linewidth', 14);
        hold off;
        grid on;
        %axis([freq_hs(1), max(500,1.2*max(pitch_opt)), min(0,1.2*min(Pr_pitch)), 1.2*max(Pr_pitch)]);
        title(sprintf('Harmonic Sieve, $f_{est} = %.2f$ Hz', pitch_est), 'interpreter', 'latex');
        xlabel('Hz', 'interpreter', 'latex');
        ylabel('$S_p$', 'interpreter', 'latex');
        set(gca, 'fontsize', 36);
        drawnow;
    end
    
end
