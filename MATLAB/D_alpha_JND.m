% 
% Eq. (3,1), from Heinz, 2001
% 

function [alpha_JND_AI, alpha_JND_RP] = D_alpha_JND( ifr_plus, ifr, d_alpha, ttt )
%
% RP: Rate-Place coding
% AI: All-Information coding
%

% Time window:
T_win = ttt(end)-ttt(1);

% Change the orientation for the JND calculation:
%   [CF,time] --> [time,CF]
ifr         = ifr';
ifr_plus    = ifr_plus';

%
% ifr_avg = mean( ifr );
% d_ir_freq_avg = mean( Approx_Derivative( ifr_plus, ifr, d_alpha ) );
% FI_RP = sum( T_win./ifr_avg .* d_ir_freq_avg.^2 );

ifr_avg = mean( ifr );
ifr_plus_avg = mean( ifr_plus );
FI_RP = sum(T_win* 1./ifr_avg .* ( (ifr_plus_avg - ifr_avg)/d_alpha ).^2 );
alpha_JND_RP = sqrt( 1./FI_RP );


% d_ir_freq = Approx_Derivative( ifr_plus, ifr, d_alpha );
FI_AI = sum( trapz( ttt, 1./ifr .* ((ifr_plus - ifr)/d_alpha).^2 ) );      
alpha_JND_AI = sqrt( 1./FI_AI );





















