%
% main_SC_v04_irn.m
%
% Notes:
%   * 07/04/2015: I have added the sparse coding with 'sliding windows'
%                 option.
%
%   * 12/08/2015: Perform_SC.m, case #4 (the Rozell's based RNN): Back to
%                 the previous version of PitchEst_v13_04.
%

clc
    
% % mysparse = @(x) sparse(x); 
% mysparse = @(x) x;      fprintf('-> mysparse is inactivated !');

reshapeM = @(M) reshape(M,size(anf,1),size(anf,2));
reshapeD = @(nn) reshape(base_db.D(:,nn),size(anf,1),size(anf,2));


fontsize   = 36;
markersize = 30;

%% create the bases' database:

% tic
% net.stim.amp_dBSPL  = 25;        % [dB SPL] stimulus level
% net.stim.noise_level= 0.0;     % [0,1] Set the amount of added white noise in the stimulus, noise_level*[SNR]

if ~exist('base_db','var')
    % Options for the <anf_method> field:
    % 1. Heinz model;
    % 2. Roy Patterson's Model (Malcom Slaney's toolbox, version 2, 1998);
    % 3. Lyon Passive Ear model (Malcom Slaney's toolbox, version 2, 1998);
    % 4. Ray Meddis� hair cell model (Malcom Slaney's toolbox, version 2, 1998);
    % 5. Modification (#1) of Ray Meddis� hair cell model (Malcom Slaney's toolbox, version 2, 1998);
    % 10. The model of Zilany et. al. (2014);
    % 20. Miriam, Oded et. al., (2012) MATLAB & Time-Domain;
    % 21. Miriam, Oded et. al., (2012) C++ & Time-Domain;
    anf_method = 10;

    
    %Build_Base;
    Build_Base_v02;    
else
    % assuming that there is already a loaded <net> structure in the workspace
	addpath(net.AN_slaney_dir);
    InitCochlea;
    
end

fprintf('-> [main.m]: net.cochlea.Nfb:  %g\n', net.cochlea.Nfb);
fprintf('-> [main.m]: net.dic.dim2:     %g\n', net.dic.dim2);
fprintf('-> [main.m]: ANF threshold:    %g\n', net.anf.thr);


%% Init. Sparse Coding data:
% [1] (my) ISTA(); 
% [2] (MATLAB's) lasso();
% [3] (Foldiak's based) RNN;
% [4] (Rozell's based) RNN;
ista.method = 2;       

% Initialize the SC engine:
InitSC;             

% set the debug mode for the ISTA precoedure (like setting verbose to ON):
ista.debug  = 0;


% For Biological plausible simulations:
if ista.method~=4 && (6 < net.dic.msec)
    warning('-> [main.m]: for this SC method, please change net.msecs to 5 ms!');
end

%% Testing the sparse coding

% testing phase. Currently it's just for legacy...
net.training_flag   = 0;

% pre-defined desired harmonics:
net.tst_hrm_slc = nan;
% net.tst_hrm_slc = 11;   '*** IRN *** '  
%     net.stim.IRN.sign   = 1;
%     net.stim.IRN.rep    = 1; 
%     %ista.gamma_ratio = 0.02;    '*** Raise the gamma for the IRN case ***'
% % net.tst_hrm_slc = 26;  'TT (Oxenham,2004)'


% ==> 'harmonics2_:'
if isnan(net.tst_hrm_slc) || isempty(net.tst_hrm_slc)
    harmonics2_ = zeros(1,net.N_hrm);   
    harmonics2_( 3:8 ) = 1; 
else
    harmonics2_ = [];
end


fprintf('-> [main.m]: Override the CreateIO.m options\n');
fprintf('-> [main.m]: harmonics2_: ['); fprintf(' %g', harmonics2_); fprintf(']\n');

% # of epoches:
net.N_epoch2 = 200;

% stim. volume:
% net.stim.amp_dBSPL = 2* 45;        '*** setting the amplitude [dB SPL] ***'
net.stim.noise_level = 0;     '*** Add Gaussian noise to the input ***'
fprintf('\n  STIMULUS FEATURES:\n');
fprintf('-> net.stim.amp_dBSPL: %.1f [dB SPL]\n', net.stim.amp_dBSPL);
if 0 ~= net.stim.noise_level, fprintf('-> net.stim_noise_level: %g\n', net.stim.noise_level); end;
fprintf('\n');

% inharmonic frequency delay:
dF2_ = [];          % [Hz]
% dF2_ = 200       % [Hz]


if (~isempty(dF2_) && ~isnan(dF2_)); fprintf('-> !! Note: using inharmonic setup, dF2_ = %g Hz\n ', dF2_); end;


% stimulus inharmonics:
% F0_tst = [];
% F0_tst = 240;   % [Hz]
F0_tst = linspace(130, 2500, net.N_epoch2);
% F0_tst = [400, 765, 840, 960, 1050, 1200, 1400, 1600, 1800, 2000]; % [Hz], taken from Oxengam, 2011
% net.N_epoch2 = length(F0_tst);

% stimulus phase:    
phase2 = 0;

%% Testing database:
tst_db.F02          = nan(net.N_epoch2, 1);
tst_db.harmonics2   = nan(net.N_epoch2, net.N_hrm);
tst_db.dF2          = nan(net.N_epoch2, 1);
tst_db.phase2       = nan(net.N_epoch2, 1);
%tst_db.fff_dic      = fff_dic;
tst_db.H            = zeros(net.dic.dim2, net.N_epoch2);
tst_db.pitch_est    = nan(net.N_epoch2, 1);
tst_db.pitch_opt    = nan(net.N_epoch2, 4);
%tst_db.masker_flag  = 0;
tst_db.sieve_sig    = []; %0.05;              
tst_db.gauss_LPF    = []; %[1e3, 3e3];  '*** gauss_LPF is ON ***'   % [sigma, mu]

debug_mode.plot     = 1;

% total time in this simulation:
fprintf('-> total stimulus time: %g [msec]\n', net.stim.msecs);
fprintf('-> ACTUAL stimulus time: %g [msec]\n',  net.stim.msecs- net.dic.start_msec);

% preparing for the parfor:
sieve_sig   = tst_db.sieve_sig;
N_epoch2    = net.N_epoch2;
gauss_LPF   = tst_db.gauss_LPF;
pitch_est_v = zeros(N_epoch2, 1);
pitch_opt_v = zeros(N_epoch2, 4);
pitch_salience_v= zeros(N_epoch2, 4);
F02_v       = zeros(N_epoch2, 1);
harmonics2_v= zeros(N_epoch2, net.N_hrm);
dF2_v       = zeros(N_epoch2, 1);


%% Create Harmonic Sieve Matrix:
if isempty(net.hs.G)
    net.hs.sigma_type = 1;
    [net.hs.G, net.hs.freq_hs] = CreateHarmonicSieve( fff_dic, gauss_LPF, net.hs.interp_factor, net.hs.sigma_type );
end

%%

% Save the probabili;ty functions:
P = zeros( size(net.hs.G,1), N_epoch2 );

% sacf_pitch_v    = zeros( N_epoch2, 1 );
% sacf_salience_v = zeros( N_epoch2, 1 );


%% running the main epoches: 
plot_every = 9;

for tn = 1:N_epoch2
    fprintf('\n -> tn = %d / %d\n', tn, N_epoch2);

    % Create the stimulus:
    % --------------------
    if length( F0_tst ) > 1     % for a batch of test frequencies:
        F0_n = F0_tst(tn);
    else
        F0_n = F0_tst;
    end
    
    % remove harmonics above 20 k Hz:
    if isnan(net.tst_hrm_slc) || isempty(net.tst_hrm_slc)
        harmonics2 = harmonics2_;
        harmonics2( F0_n*[1:net.N_hrm].*harmonics2_> net.cochlea.highfreq ) = 0;
    else 
        harmonics2 = [];
    end
    [stim2, F02, harmonics2, phase2_, dF2] = CreateIO(net, F0_n, harmonics2, dF2_, phase2);  %, F0, harmonics, dF, phase);       

    
    % stimulus --> ANF:
    % -------------------    
    % stimulus --> ANFs:
    anf = CreateANF( net, stim2, Hd_ear, fcoefs, cochlea_freq );        
    
    % ANF --> SC:
    % -------------------
    % perform the SC - extract the sparse coefficient vector h
    [h, ~] = Perform_SC(net, ista, anf); %, fff_dic, harmonics2, F02);   % >>> for debugging
    if 0==nnz(h); fprintf('-> h is ALL-ZEROS --> continue the for loop\n'); continue; end;    

    
    % SC --> pitch estimation:
    % ------------------------ 
    % Estimate pitch:
    pitch_range_Hz = F0_n * 2.^[-6/12, 6/12];      %pitch_range_Hz = F0_n * 2.^[-18/12, 18/12];    
    %pitch_range_Hz = [];
    debug_est = 0;  

    [pitch_est_v(tn), pitch_opt_v(tn,:), pitch_salience_v(tn,:), P(:,tn), fff_hs ] = ...
       PitchEst( h, net.hs.freq_hs, net.hs.G, pitch_range_Hz, debug_est );
        
    %if isempty(pitch_est); error('-> !!! ERROR: pitch_est is empty !!!'); end;
    if isempty(pitch_est_v(tn)); fprintf('-> pitch_est_v(tn) is ALL-ZEROS --> continue the for loop\n'); continue; end;
        
    
    % Save results:
    % -------------------        
    % save current results:
    tst_db.phase2(tn)   = phase2_;
    tst_db.H(:,tn)      = h;
    %tst_db.pitch_masd(tn) = MASD( anf, cochlea_freq );  % pitch estimation using Cedolin & Delgutte's (2010) method
    
    fprintf('--> F02: %.3f Hz\n', F02);
    fprintf('--> Fest: ** %.3f Hz **\n', pitch_est_v(tn));
    fprintf('--> harmonics2: ['); fprintf(' %.3g', harmonics2); fprintf(' ]\n');    
    
    % save current results:
    F02_v(tn) = F02;
    harmonics2_v(tn,:) = harmonics2;
    if ~isempty(dF2)
        dF2_v(tn) = dF2;
    end
    
    if ~mod(tn-1,plot_every)
        % {
        figure(21)
        plot(F02_v(1:tn), pitch_opt_v(1:tn,1), '.', 'markersize', markersize);
        %plot(F02_v(1:tn), pitch_opt_v(1:tn,:), '.', 'markersize', markersize);
        grid on;
        xlabel('f [Hz]', 'interpreter', 'latex', 'fontsize', fontsize);
        ylabel('\^{f} [Hz]', 'interpreter', 'latex', 'fontsize', fontsize);
        title('Pitch Estimations');
        set(gca, 'fontsize', fontsize);
        %}
        
        drawnow;
    end
    
end


% gathering all the data from the parfor procedure:
tst_db.pitch_est    = pitch_est_v;
tst_db.pitch_opt    = pitch_opt_v;
tst_db.pitch_salience= pitch_salience_v; 
tst_db.F02          = F02_v;
tst_db.harmonics2   = harmonics2_v;
tst_db.dF2          = dF2_v;
tst_db.P            = P;
tst_db.F0_tst       = F0_tst;
% tst_db.limen_f      = limen_f;
tst_db.fff_hs       = fff_hs;



%%
if (~isunix)
    fontsize   = 36;
    markersize = 24;
    linewidth = 3;
    
    % {
        figure(22);
        plot(1e-3*tst_db.F02, tst_db.pitch_opt, '.', 'markersize', markersize);
        hold off;
        grid on;
        xlabel('$f_{_L}$ [kHz]', 'interpreter', 'latex', 'fontsize', fontsize);
        ylabel('$\hat{f}_0$ [kHz]', 'interpreter', 'latex', 'fontsize', fontsize);
        title('Pitch Estimations');
        set(gca, 'fontsize', fontsize);
        set(gca, 'fontsize', fontsize);
        legend('1^{st} peak', '2^{nd} peak', '3^{rd} peak', '4^{th} peak');  
    %}
        
    %{
        figure(24);
        [f_crm, f_nrm] = freq2chroma( tst_db.F02, tst_db.pitch_est );
        plot(tst_db.F02, f_crm, '.', 'markersize', markersize);
        xlabel('Frequency [Hz]');
        ylabel('Pitch (Chroma)');
        set(gca, 'fontsize', fontsize);
        set(gca, 'fontsize', fontsize);
    %}        
    
    %{
        figure(25)
        hist(pitch_opt_v(1:tn), 200);
        title(sprintf('f0 = %g [Hz], sign(%d), rep: %d', F02, net.stim.IRN.sign, net.stim.IRN.rep))
        grid on;
    %}
end



% return;

%% save results:


% ista_: ista without D:
ista_ = ista;
ista_ = rmfield(ista_, 'D');

% save( [net.data_dir, sprintf('results_F0(%d)_(%ddB)_anf(%d)_ista(%d)_ms(%d)',...
%     F02, net.stim.amp_dBSPL, net.anf.method, ista.method, fix(net.msecs))], ...
%     'ista_', 'tst_db', 'net' ...
% );
% 

% % For IRN:
% save( [net.data_dir, sprintf( 'IRN_results_F0(%d)_sign(%d)_rep(%d)_(%ddB)_anf(%d)_ista(%d)_ms(%d)',...
%     F02, net.stim.IRN.sign, net.stim.IRN.rep, net.stim.amp_dBSPL, net.anf.method, ista.method, fix(net.msecs))], ...
%     'ista_', 'tst_db', 'net', 'fff_dic' ...
% );

% % For JND:
% save( [net.data_dir, sprintf('JND_results_hrm([%s])_SPL(%ddB)_anf(%d)_ista(%d)_ms(%d)',...
%     num2str(find(harmonics2)), net.stim.amp_dBSPL, net.anf.method, ista.method, fix(net.msecs))], ...
%     'ista_', 'tst_db', 'net' ...
% );


% save( [net.data_dir, sprintf('results_anf(%d)_hrm(%d,%d)_atoms(%d)_Nfb(%d)_dicFs(%gkHz)_ms(%d)',...
%     net.anf.method,  active_hrm(1), active_hrm(end), D_dim2, net.cochlea.Nfb, 1e-3*net.dic.Fs, fix(net.msecs))], ...
%     'ista_', 'tst_db', 'net' ...
% );

dummy = net.hs.G;
net.hs.G = [];

filename = sprintf('_results_SPL(%d)_anf(%d)_Ntriang(%d)_atoms(%d)_Nfb(%d)_dicFs(%gkHz)_ms(%d)',...
    net.stim.amp_dBSPL, net.anf.method,  net.dic.N_triang, D_dim2, net.cochlea.Nfb, 1e-3*net.dic.Fs, fix(net.msecs));

fprintf('--> saved filename: %s\n', filename);

save( [net.data_dir, filename], ...
    'ista_', 'tst_db', 'net' ...
);

 net.hs.G = dummy;























