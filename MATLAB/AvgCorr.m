

function [ C_avg, C_lags, C_all ] = AvgCorr( X, fs, fig_num )
%
% Average autocorrelation (Rajan, Abbott & Sompolinsky 2010)
%
% * Assuming that rows(X) represents channels
%

debug_mode = 0;

N_ch = size(X,1);
N_smp = size(X,2);
N_smp_half = floor(N_smp/2);
% C_avg       = zeros(N_smp, 1);
C_all   = zeros(N_ch, N_smp_half);

for ii = 1:N_ch
    
%     %{
    C_ii = xcov(X(ii,:), 'unbias');
    C_all(ii,:) = C_ii(N_smp:(end-N_smp_half));  % take just the positive part
%     C_all(ii,:) = C_ii(end - [1:N_smp_half] -1);    % take just the positive part
    %}
    
    %{
    x_ii = X(ii,:) - mean(X(ii,:));     % remove DC 
    C_ii = ifft(fft(x_ii).*conj(fft(x_ii)))/length(x_ii);   % circular correlation
    C_all(ii,:) = C_ii(1:N_smp_half);  % take just the positive part    
    %}
    
    if debug_mode
        figure(99);
        plot(C_ii);
        hold on
        plot( (N_smp:(2*N_smp-N_smp_half-1)), C_all(ii,:), 'ko')
        hold off
    end
    
    
end

C_avg = mean(C_all)';

C_lags = [0:(N_smp_half-1)]; %[-(N_smp-1):N_smp];

if 0 == nargout || exist('fig_num', 'var')
    figure(fig_num);
    set(gca,'fontsize',16,'fontweight','bold');
    
    if exist('fs', 'var')
        C_lags = 1e3*[0:(N_smp_half-1)]./fs; %[-(N_smp-1):N_smp];
        xlabel_str = 'Lags [msec]';        
    else
        %C_lags = [0:(N_smp_half-1)]; %[-(N_smp-1):N_smp];
        xlabel_str = 'Lags [samples]';
    end
    
    plot( C_lags, C_all' );
    hold on
    C_hnd = plot( C_lags, C_avg, 'k', 'linewidth', 3 );
    hold off
    xlabel(xlabel_str);
    ylabel('Correlation');
    legend(C_hnd, 'Avg. cov');
    grid on
    title('Autocovariance (only the positive half)');
    axis([0, 30, min(C_all(:)), max(C_all(:))]);
end
