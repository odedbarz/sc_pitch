%
% PlotStatus.m
%

%{
    % Set the number of channels (according to Heinz paper):
    x_low_cf    = Map_cf2x( net.cochlea.min_frq_filter );
    x_high_cf   = Map_cf2x( net.cochlea.max_frq_filter );
    dx_mm       = ( x_high_cf - x_low_cf )/( net.cochlea.N_filter_banks -1 );
    ANcfs       = Map_x2cf( x_low_cf + dx_mm*[0:net.cochlea.N_filter_banks-1] );
    PlotANF(uin, stim, N_chs, ANcfs, time_cochlea, time, fig_num );
%}

fontsize = 20;

%%
% stim window:
ttt_stim = linspace(0, net.secs, net.cochlea.n_samples)'; 
% fff = linspace(0, net.Fs, net.n_samples);
% ttt = [0:net.dt:net.secs-net.dt]';
freq = linspace(net.F0_start, net.F0_end, net.n_samples);
time = linspace(0, net.msecs, net.n_samples);     % [msec]


%%

figure(fig_num +1);
subplot 221
set(gca, 'fontsize', fontsize)

plot(time, ft, '-', 'linewidth', 1.5);
hold on
plot(time, zt, '-', 'linewidth', 2.0);
net_readouts = Dxy(zt);
plot(time, net_readouts(ones(net.n_samples,1),:), '--', 'linewidth', 2);
hold off
grid on;
%axis([0, length(zt), 0, 1.1*ft_amp]);
%legend('ft', 'zt', 'Dxy(zt)');
xlabel('time [msecs]');
title( sprintf('Readout(s) vs. Expected\n epoch_# = %d', tn) );
ylabel('Readouts Activity')

%%

% figure(fig_num+2);
subplot 222
set(gca, 'fontsize', fontsize)

slc_lines = zeros(net.n_samples, net.z_d1);
for ii = 1:net.z_d1
    if length(net.neu_std) == net.z_d1
        neu_std = net.neu_std(ii);
    else
        neu_std = net.neu_std;
    end
    
    slc_lines(:,ii) = NeuSel(freq, net.ft_cf(ii), neu_std, net.ft_amp);
end
plot( freq, slc_lines, '--', 'linewidth', 1.5 ); % target curve

hold on
if 1==net.training_flag   % training
    plot( results.trn.F0(1:tn), results.trn.score(1:tn,:), 'x', 'linewidth', 2, 'markersize', 12 );
    %plot( results.trn.pitch_SACF(1:tn), results.trn.score(1:tn,:)/net.ft_amp, 'x', 'linewidth', 2, 'markersize', 12 );
else    % testing
    plot( results.tst.F0(1:tn), results.tst.score(1:tn,:), '.', 'markersize', 26 );
    %plot( results.tst.pitch_SACF(1:tn), results.tst.score(1:tn,:)/net.ft_amp, '.', 'markersize', 26 );
end
hold off
grid on
xlabel('Pitch [Hz]')
if 1==net.training_flag   % training
    title('Readouts (training)')
else    % testing
    title(sprintf('Readouts (testing), f_{ML} = %g Hz', results.tst.pitch_ML(tn)));
end
ylabel('<z_t>')

%% stim (freq. domain)

%{
subplot 223
set(gca, 'fontsize', fontsize)

dummy2plot = log10(abs(fft(stim)));
plot( linspace(0,net.cochlea.Fs,net.cochlea.n_samples), dummy2plot, 'linewidth', 2);
grid on;
axis([0, net.F0_end*net.N_hrm, min(dummy2plot), max(dummy2plot)]);
title(sprintf('FFT(stim), f_0 = %g [Hz]',F0));
xlabel('freq. [Hz]')
ylabel('|FFT(stim)|')

hold on;
plot(F0*[1:net.N_hrm], 0*ones(net.N_hrm,1), 'vk', 'markersize', 8, 'MarkerFaceColor', 'k');
hold off;
%}
subplot 223
set(gca, 'fontsize', fontsize)

dummy2plot = max(anf,[],2);


%{
switch (net.anf_method)
    case 1  % Heinz model 2001
        xxx_cf = Map_x2cf( linspace(0,35,size(anf,1)) );
    
    case 2  % Patterson
        xxx_cf = ERBSpace( net.cochlea.min_frq_filter,...   % lowFreq, 
                       net.cochlea.Fs/2,...                 % fs/2
                       net.cochlea.N_filter_banks...        % numChannels
                    );
            
    case 3 % LyonPassiveEar        
        [~, CenterFreqs, ~] = DesignLyonFilters( net.cochlea.Fs, 4, 0.125 );
        % xxx_cf = CenterFreqs; % ???
        
end
%}

%{
switch net.anf_method
    case 1  % Heinz model 2001
        xxx_cf = Map_x2cf( linspace(0,35,size(anf,1)) );
    
    case 2  % Patterson
        xxx_cf = ERBSpace( net.cochlea.min_frq_filter,...   % lowFreq, 
                       net.cochlea.Fs/2,...                 % fs/2
                       net.cochlea.N_filter_banks...        % numChannels
                    );
            
    case 3 % LyonPassiveEar        
        [~, CenterFreqs, ~] = DesignLyonFilters( net.cochlea.Fs, 4, 0.125 );
        % xxx_cf = CenterFreqs; % ???
        
    case {4,5,6,7}
        xxx_cf = ERBSpace( net.cochlea.min_frq_filter,...   % lowFreq, 
                       net.cochlea.Fs/2,...                         % fs/2
                       net.cochlea.N_filter_banks...        % numChannels
                    );

    case 8  % Heinz model 2001
        xxx_cf = Map_x2cf( linspace(0,35,size(anf,1)) );
                
%         xxx_cf = ERBSpace( net.cochlea.min_frq_filter,...   % lowFreq, 
%                        net.cochlea.Fs/2,...                         % fs/2
%                        net.cochlea.N_filter_banks...        % numChannels
%                     );
              
end
%}

semilogx(xxx_cf, dummy2plot, 'linewidth', 2)
hold on;
semilogx( F0*[1:net.N_hrm].*(harmonics~=0), net.anf_max_amp* harmonics/max(harmonics), 'xr', 'linewidth', 4, 'markersize', 14);
semilogx( F0*[1:net.N_hrm].*(harmonics==0), 0, 'or', 'linewidth', 3, 'markersize', 10);
hold off;

grid on;

legend('max(ANF)', 'harmonics');
xlabel('freq. [Hz]');
title( sprintf('f_0 = %g [Hz]', F0) );


%% wo

% figure(fig_num+4);
subplot 224
set(gca, 'fontsize', fontsize)

if 1==net.training_flag   % training
    plot(results.trn.norm2wo(1:tn), '.:', 'markersize', 16); 

elseif isfield(results, 'trn') && isfield(results.trn, 'norm2wo')    % testing
    plot(1e3/net.N * results.trn.norm2wo, '.:', 'markersize', 16); 
    ylabel('$\frac{||w_o||_2}{N}$ [mili]', 'interpreter', 'latex')
    legend('||w_o||^2/N', 'Location', 'NorthWest');
    grid on;
    xlabel('# of training epoch (tn)');
    title( sprintf('#epoch = %d', tn) );
end


%%
PlotStim;

drawnow;









