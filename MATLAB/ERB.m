function [ y ] = ERB( f )
% [ y ] = ERB( f )
%
%  Moore and Glasberg, 1990.
% 
% y = ERB(f) = 24.7*(4.37*f +1 )
%
% f: in kHz, and in the range:      0.1[kHz] <= f <= 10[kHz]
% y: in Hz
%


f(f<0.1) = 0.1;

y = 24.7*(4.37*f +1 );



end

