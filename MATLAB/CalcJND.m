% 
% CalcJND.m
%
% Inputs:
% * P: We assuming that the columns of P contains the 
%
% Description:
%   Applying eq. (3,1), from Heinz, 2001
% 
%   

function jnd = CalcJND( P, f, limen_f )

N = size(P,2);

dc_r = 1e-16*max(P(:));
P = P + dc_r;   % add DC to avoid division by zero

jnd = zeros(1,N/2);

for nn = 1:2:N
    dr = (P(:,nn+1)-P(:,nn))/limen_f;   % approximated derivation
    dummy = trapz( f, 1./P(:,nn) .* dr.^2 );
    %dummy = sum( 1./P(:,nn) .* dr.^2 );
    jnd((nn+1)/2) = 1/sqrt(dummy);
end





















