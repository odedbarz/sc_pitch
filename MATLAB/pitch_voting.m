% 
% function p = pitch_voting( rates, ft_cf, ft_amp )
% 
% Decoding network's preferred pitch from its readouts.
% 
% Examples:
% * p = readouts2pitch(results.tst.score, net.ft_cf, net.neu_std, net.ft_amp);
% * plot( results.tst.F0, readouts2pitch(results.tst.score, net.ft_cf, net.neu_std, net.ft_amp), '.' )
%
function p_est = pitch_voting( rates, ft_cf, neu_std, ft_amp, f0_start, f0_end )

    if size(rates) ~= size(ft_cf)
        error('ERROR in readouts2pitch(): size(rates) ~= size(ft_cf)');
    end

    if nargin < 4
        ft_amp = 1.0;
    end

    %
    neu_std = neu_std(1);
    M = 1;
    p_est = zeros(size(rates,1), 1);
    
    Nfun = @(x,m,s) ft_amp*exp( -0.5*(x-m)^2/s^2 );
        
    rates_ = min( ft_amp, max(1e-4,rates) );
    
    syms xx
    
    %
    for ii = 1:size(rates,1)

        pitch_ii = zeros(1,ceil((f0_end-f0_start)/(neu_std)*M));
        
        ii
        
        for jj = 1:size(rates,2)
            Nfun_jj = @(x) Nfun(x,ft_cf(jj),neu_std/2) - rates_(ii,jj);
            sol = solve(Nfun_jj(xx) == 0);
            if isempty(sol)
                %p_est(ii) = nan;
                continue;
            end
            
            %z1 = fzero( Nfun_jj, ft_cf(jj)-10 ); 
            z1 = max( 0, double(sol(1)));
            %z2 = fzero( Nfun_jj, ft_cf(jj)+10 ); 
            z2 = max( 0, double(sol(2)));
            
            if f0_start < z1 && f0_end > z1
                index_jj = ceil((z1-f0_start)/(neu_std)*M);
                pitch_ii(index_jj) = 1 + pitch_ii(index_jj);
            end
            
            if f0_start < z2 && f0_end > z2
                index_jj = ceil((z2-f0_start)/(neu_std)*M);
                pitch_ii(index_jj) = 1 + pitch_ii(index_jj);
            end
            
        end
        
        pitch_index = find( pitch_ii == max(pitch_ii), 1, 'first');
        p_est(ii) = pitch_index * round((f0_end-f0_start)/length(pitch_ii));
        
    end

end     % end of readouts2pitch()






