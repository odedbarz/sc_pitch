function y = Succ_Pred( DB, tn )
% Pred_Succ: Success of prediction.
%

if 2 <= nargin
    y = DB.R(tn,2) == DB.R(tn,1);
else
    y = DB.R(:,2) == DB.R(:,1);
end

end

