%
% PitchShiftData.m
%
% Note: You need to load the data in <tst_db>, and plot the pitch-shift
% results.
%

fontsize = 40;
markersize = 20;

dF2 = tst_db.dF2(1);

figure(10);
plot(tst_db.F02, tst_db.pitch_est, '.', 'markersize', markersize);
title('Pitch Shift, All Results')

If = tst_db.F02/dF2;
Ip = tst_db.pitch_est;

% remove outliers:
y0 = 180;
y1 = 220;
If = If(logical((Ip>=y0).*(Ip<=y1)));
Ip = Ip(logical((Ip>=y0).*(Ip<=y1)));

figure(11);
plot(If, Ip, '.', 'markersize', markersize);

fv = [130, 294;...
      306, 494;...
      516, 678;...
      699, 895;...
      909, 1089;... 909, 1089;...
      1106, 1292;...
      1310, 1505;...
      1517, 1690;...
      1711, 1860;...
      1950, 2000];

% number of LS lines:
N_lines = size(fv,1);

% Save all the lines:
AB = zeros(2, N_lines);
  
df4std = 8;    % Hz

for ii = 1:N_lines    
    f0 = (100 + (200+0)*(ii-1))/dF2;   % hz
    f1 = (100 + (200+2)*ii)/dF2;   % hz
    
    % Set the index without outliers:
    Ix = logical( (If >= f0).*(If <= f1) ); %.*(Ip>=y0).*(Ip<=y1));
        
    % Prepare for least square model y = H*[a,b]':
    x_ii = If(Ix);
    H = [x_ii, ones(nnz(Ix),1)];
    ab = (H'*H)\H'*Ip(Ix);
    y_ii = ab(1)*H(:,1)+ab(2);
    
    % remove outliers:
    std_ii = abs(Ip(Ix) - y_ii);    
    Ix(nonzeros( find(Ix).* (std_ii>df4std) )) = 0;
    
    % recalc. the LS without the outliers:
    x_ii = If(Ix);
    H = [x_ii, ones(nnz(Ix),1)];
    ab = (H'*H)\H'*Ip(Ix);
    y_ii = ab(1)*H(:,1)+ab(2);

    figure(11);    
    hold on;
    plot(H(:,1), y_ii, 'k-', 'linewidth', 2);
    
    % shows the data points to participate in each LS:
    %plot(If(Ix), Ip(Ix), '.', 'markersize', 0.4*markersize);
    
    test4line = sprintf('%.2f', ab(1)/dF2);
    text(H(floor(end/2),1), 1.02*(ab(1)*H(end,1)+ab(2)), test4line, 'fontsize', fontsize);
    
    y_dummy = 1/ii*x_ii([1,end]);
    Ix_dummy = y_dummy(2) <= y_ii(end);
    plot(ii*y_ii([1,end])/dF2, y_ii([1,end]), 'k--', 'linewidth', 2);
    text(H(floor(end/2),1), 1.02*(ab(1)*H(end,1)+ab(2)), test4line, 'fontsize', fontsize);
    
    hold off;
    
    % save the results:
    AB(:,ii) = ab;
end

%%
set(gca, 'fontsize', fontsize);
figure(11);
xlabel(sprintf('Normalized Frequency (by \\Deltaf=%g Hz)', dF2));
ylabel('Pitch Est. [Hz]');
axis([100/dF2, 2100/dF2, 180, 225]);
grid on;

   

