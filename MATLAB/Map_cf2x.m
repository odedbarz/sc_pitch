% 
% Cochlea mapping of CF to distance (in [mm]) along the BM, using the Greenwood relation.
% 

function x_v = Map_cf2x( cf_v )

x_v = 1/0.06 * log10( (cf_v/165.4) + 0.88 );





