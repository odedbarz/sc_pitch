function [ D, H ] = SparseCodingLearning( X, sc )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

if 1 == sc.debug_mode
    fprintf('-> !! SparseCodingLearning(): sc.debug_mode is ON !!\n');
end
    

dictionary_size = sc.dictionary_ratio*size(X,1);

% Init. a dictionary:
D = InitDictionary( size(X,1), dictionary_size );

% H = zeros(size(D,2),size(X,2));
H = sc.ista.seed_level*randn(size(D,2),size(X,2));    % init. the sparse weights

convergance = Inf;
counter = 0;
eps1 = 1e-24;  % eps1  > 0 (default == 1e-3)
eps2 = 1e-24;  % eps2  > 0 (default == 1e-6)
%thr1 = 1e-3;

fprintf('-> starting SparseCodingLearning()...\n');

while convergance > sc.convergance     
    
    fprintf('-> while loop counter: %d (/%d)\n', counter, sc.max_loop_thr);
    
    B = zeros(size(D,1),size(D,2));
    A = zeros(size(D,2),size(D,2));
    
    % ISTA
    for ii = 1:size(X,2)
        if ~mod(ii-1,100)
            fprintf('--> SparseCodingLearning() => ISTA #%d /%d \n', ii, size(X,2));
        end
        
        %
        H(:,ii) = ISTA( D, H(:,ii), X(:,ii), sc.ista );   % infer the code h(x(ii))
        
        B = B + X(:,ii)*H(:,ii)';
        A = A + H(:,ii)*H(:,ii)';
    end
    
    % Check for errors:
    assert( 0 == nnz(isnan(H)), '\n\n --> Error in SparseCodingLearning(): \n\t0 ~= nnz(isnan(H))\n\n' );    
    assert( 0 == nnz(isnan(A)), '\n\n --> Error in SparseCodingLearning(): \n\t0 ~= nnz(isnan(A))\n\n' );

    % need it for the convergence:
    D_prev = D;
    %D_prev_norm = norm(D(:),2);

    for jj = 1:size(D,2)
        % normalize the column D(:,jj) to 1:
        D(:,jj) = 1/max(eps1,A(jj,jj)) * ( B(:,jj) - D*A(:,jj) + D(:,jj)*A(jj,jj) );
        
    end
    % Check for errors:
    assert( 0 == nnz(isnan(D)), '\n\n --> Error in SparseCodingLearning(): \n\t0 ~= nnz(isnan(A))\n\n' );
        
    % normalaize the variance of the dictionary
    D = D*diag( 1./max(eps2,sqrt(sum(D.*D))) );
    
    % Check for errors:
    assert( 0 == nnz(isnan(D)), '\n\n --> Error in SparseCodingLearning(): \n\t0 ~= nnz(isnan(A))\n\n' );
    
    % check for convergance:
    convergance = (norm(D(:) - D_prev(:),2))/norm(D_prev(:),2);
    %convergance = abs(norm(D(:),2) - D_prev_norm)/D_prev_norm;
    
    % residual error:
    if ~mod(counter,5)
        err = norm(X - D*H,2)/(norm(X,2)*norm(D*H,2));
        fprintf('\n--> estimation error: %g [%%] \n', 100*err );
        fprintf('--> convergance: %g [%%] \n', 100*convergance );
    end
    
    
    % DEBUG:
    if sc.debug_mode
        figure(99);
        subplot 121
        imagesc(abs(D)); colorbar;
        title('|D|')
        
        figure(99);
        %subplot 132
        %imagesc(abs(D-D_prev)); colorbar;
        %title('|D-D_{prev.}|')

        subplot 122
        nn = ceil(rand*size(X,2));
        plot(X(:,nn), 'o:');
        hold on
        plot(D*H(:,nn), '.-');
        hold off
        grid on
        legend('x', 'D*h')
        
        figure(101);
        plot(H(:,nn))
        title(sprintf('h_{%d}', nn));
        grid on
        
        drawnow;
        pause(1e-6);
    end
    
    %
    counter = counter + 1;
    if (counter > sc.max_loop_thr)
        fprintf('-> SparseCodingLearning(): breaking the WHILE loop\n');
        fprintf('--> counter > sc.max_loop_thr==%d\n', sc.max_loop_thr);
        break;
    end 
    
    fprintf('--> SparseCodingLearning(), convergance: %g [%%]\n\n', convergance );
    
    
end   
  
fprintf('-> end of SparseCodingLearning()...\n');

if sc.debug_mode
    fprintf('--> max. residual estimation error: %g (X - D*H)\n', err );
    fprintf('--> convergance: %g [%%] \n', convergance );
end

end


















