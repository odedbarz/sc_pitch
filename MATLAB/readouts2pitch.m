% 
% function p = readouts2pitch( rates, ft_cf, ft_amp )
% 
% Decoding network's preferred pitch from its readouts.
% 
% Examples:
% * p = readouts2pitch(results.tst.score, net.ft_cf, net.neu_std, net.ft_amp);
% * plot( results.tst.F0, readouts2pitch(results.tst.score, net.ft_cf, net.neu_std, net.ft_amp), '.' )
%
function [pitch_ML, salience1, salience2] = readouts2pitch( rates, ft_cf, neu_std, ft_amp )

    if size(rates) ~= size(ft_cf)
        error('ERROR in readouts2pitch(): size(rates) ~= size(ft_cf)');
    end

    if nargin < 4
        ft_amp = 1.0;
    end

    if length(neu_std) == 1
        neu_std = neu_std*ones(1, length(ft_cf));
    end

    N_epoches = size(rates,1);
    pitch_ML = zeros(N_epoches, 1);

    % % ML:
    % % * assuming poisson noise with LOTS of gaussian selectivity curves
    % p1 = rates' * ft_cf ./ sum(rates);

    % ML:
    % * assuming poisson noise with a FEW gaussian selectivity curves
    % * assuming SAME STD for all curves
    ml_fun_r = @(r) ML_aux_fun(r, ft_cf, neu_std, ft_amp);

    % salience option # 1:
    Efun = @(S,X) sqrt( sum( (S - X).^2, 2) )/(ft_amp^2);
    salience1 = zeros(N_epoches, 1);
    
    ml2_fun_r = @(r) ML2_aux_fun(r, ft_cf, neu_std, ft_amp);
    salience2 = zeros(N_epoches, 1);
    
    % loop over all epoches:
    for ii = 1:N_epoches

        %ml_fun = ML_aux_fun(rates(ii,:), ft_cf, neu_std, ft_amp);
        ml_fun = ml_fun_r( rates(ii,:) );

        % solve for the estimated pitch frequency f0 (ML)
        % >> init. freq. = 500 was set arbitrarily
        pitch_ML(ii) = fzero( ml_fun, 0 ); 
        
        % salience 1:
        desired_ii = NeuSel(pitch_ML(ii), ft_cf, neu_std, ft_amp);
        salience1(ii) = 1./(Efun(rates(ii,:), desired_ii) + 1e-16);
        
        ml2_fun = ML2_aux_fun(rates(ii,:), ft_cf, neu_std, ft_amp);
        salience2(ii) = ml2_fun( pitch_ML(ii) ); 
    end    
    
end     % end of readouts2pitch()
% --------------------------------------------




% Create a modified ML FULL estimator (Auxiliary function)
function ml_fun = ML_aux_fun(rates, ft_cf, neu_std, ft_amp)

    % selectivity curve:
    fa = @(x,m,s) NeuSel(x, m, s, ft_amp);
    
    % ML (without \Sigma(f(s)) approx.):
    ml_i = @(x,n) ((ft_cf(n)-x)/neu_std(n)^2)*( rates(n) - fa(x,ft_cf(n),neu_std(n)) );

    % create a ML function by iterations:
    ml_fun = @(x) 0;
    for jj = 1:length(ft_cf)
        ml_fun = @(x) ml_fun(x) + ml_i(x,jj);
    end
    
end     % end of ML_aux_fun()
% --------------------------------------------


% Create a modified second derivation for the ML FULL estimator (Auxiliary function)
function ml2_fun = ML2_aux_fun(rates, ft_cf, neu_std, ft_amp)

    % selectivity curve:
    fa = @(x,m,s) NeuSel(x, m, s, ft_amp);
    
    % ML (without \Sigma(f(s)) approx.):
    ml2_i = @(x,n) 1/neu_std(n)^2 * ( -rates(n)*ft_cf(n) + fa(x,ft_cf(n),neu_std(n))...
                        * (1 - (x - ft_cf(n))^2/neu_std(n)^2)...
                    );
    
    % create a ML function by iterations:
    ml2_fun = @(x) 0;
    for jj = 1:length(ft_cf)
        ml2_fun = @(x) ml2_fun(x) + ml2_i(x,jj);
    end
    
end     % end of ML2_aux_fun()
% --------------------------------------------




