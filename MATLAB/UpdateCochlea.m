function [ net ] = UpdateCochlea( net )
%
%
%

    switch net.anf.method 
        case 4             % SLANEY's toolbox (Meddis)
            net.cochlea.Fs          = net.cochlea.slaney.Fs;        	% [Hz] (==2*Nyquist) kHz when using ERB filters    
            net.cochlea.n_samples   = net.cochlea.slaney.n_samples;     % total # of samples
            net.cochlea.resmp       = net.cochlea.slaney.resmp;
        
        case 10
            net.cochlea.Fs          = net.cochlea.zilany.Fs;            % [Hz] (==2*Nyquist) kHz when using ERB filters    
            net.cochlea.n_samples   = net.cochlea.zilany.n_samples;     % total # of samples            
            net.cochlea.resmp       = net.cochlea.zilany.resmp;
    end

end

