% 
% Build_Base.m
% 
% Description:
%   This script build a base for the sparse coding algorithm.
%

% clc;
fprintf('\n--> [Build_Base.m]: staring...\n');

% mysparse = @(x) sparse(x); 
mysparse = @(x) x;      fprintf('-> mysparse is inactivated !');


%
fprintf('\n--> [Build_Base.m]: creating a new database...\n ');
    % Options for the <anf_method> field:
    % 1. Heinz model;
    % 2. Roy Patterson's Model (Malcom Slaney's toolbox, version 2, 1998);
    % 3. Lyon Passive Ear model (Malcom Slaney's toolbox, version 2, 1998);
    % 4. Ray Meddis� hair cell model (Malcom Slaney's toolbox, version 2, 1998);
    % 5. Modification (#1) of Ray Meddis� hair cell model (Malcom Slaney's toolbox, version 2, 1998);
    % 10. The model of Zilany et. al. (2014);
    % 20. Miriam, Oded et. al., (2012) MATLAB & Time-Domain;
    % 21. Miriam, Oded et. al., (2012) C++ & Time-Domain;
    anf_method = 10;
net = CreateDB( anf_method );

addpath(net.AN_slaney_dir);


%
net.stim.amp_dBSPL  = 30;        % [dB SPL] stimulus level
net.stim.noise_level= 0.0;     % [0,1] Set the amount of added white noise in the stimulus, noise_level*[SNR]

fprintf('--> [Build_Base.m]: stimulus dB SPL gain: %.1f [dB SPL]\n', net.stim.amp_dBSPL);
fprintf('--> [Build_Base.m]: stimulus noise_level: %g\n', net.stim.noise_level);


%
InitCochlea;

% average anf flag:
% * avg_flag == 0: take all information of the ANFs 
% * avg_flag == 1: use time average of the ANFs
%avg_flag = 0; 


% Dictionary dimensions:
% ----------------------
D_dim1 = net.dic.dim1;          % length of an atom
D_dim2 = net.dic.dim2;           % # of  atoms

D_smp = net.dic.time_smp; 


%
fff_dic = linspace( net.dic.F0, net.dic.F1, D_dim2 );
% fff_dic = ERBSpace( net.dic.F0, net.dic.F1, D_dim2);
if fff_dic(1) > fff_dic(end), fff_dic = fff_dic(end:-1:1); end  % make it an ascending vector


%%
fprintf('\n-->  [Build_Base.m]: staring the for loop...\n');

% Init. the harmonic structure:
harmonics1_ = zeros(1, net.N_hrm);
harmonics1_(1) = 1;

D = zeros( D_dim1, D_dim2 );


% * NOTE: <parfor> won't work for method #4.
for tn = 1:D_dim2     
    if ~mod( tn-1, round(0.05*D_dim2) )
        fprintf('---> tn = %d / %d\n', tn, D_dim2);
    end
    
    % stimulus:
    [stim1, F01, harmonics1, phase1, dF1] = CreateIO(net, fff_dic(tn), harmonics1_, [], 0);  %, F0, harmonics, dF); %, 0);       
    
    if ~isempty(dF1);   error('-> !!! Error at Build_Base.m: <dF1> MUST be empty !!!'); end;
    if (0~=phase1);     error('-> !!! Error at Build_Base.m: <phase1> MUST equal zero !!!'); end;
    if (F01~=fff_dic(tn));  error('-> !!! Error at Build_Base.m: something is wronge with the fundamental ( F01 ~= fff_dic(tn) ) !!!'); end;
     
    
    % stimulus --> ANFs:
    anf = CreateANF( net, stim1, Hd_ear, fcoefs, cochlea_freq );        


    % save results:
    D(:,tn) = anf(:); 
    
    
end

% base database:
base_db.D_dim1	= D_dim1;  % # of dictionaries (bases)
base_db.D_dim2  = D_dim2;  % # of dictionaries (bases)
%base_db.net     = net;    % number of total time (in samples)
base_db.fff_dic = fff_dic;
base_db.D       = mysparse(D);
base_db.thr     =  net.anf.thr;
base_db.amp_sBSPL       = net.stim.amp_dBSPL;        % [dB SPL] stimulus level
base_db.stim_noise_level= net.stim.noise_level;     % [0,1] Set the amount of added white noise in the stimulus, noise_level*[SNR]


clear D


%%
if (~isunix)
    figure(10);
    subplot 211
    nn = 20;
    imagesc( reshape(base_db.D(:,nn), net.cochlea.Nfb, D_dim1/net.cochlea.Nfb) ); colorbar;
    title(sprintf('the %d atom', nn));

    subplot 212
    imagesc( base_db.D ); colorbar;
    title('the whole dictionary');
    xlabel('# of atom.');
    ylabel('CF*samples')
end
    
 


%% save the current dictionary:

save( [net.data_dir, sprintf('dic_anf(%d)_F(%d,%dk)Hz_atoms(%d)_Nfb(%d)_dicFs(%gkHz)_ms(%d)',...
    net.anf.method, fff_dic(1), 1e-3*fff_dic(end), D_dim2, net.cochlea.Nfb, 1e-3*net.dic.Fs, fix(net.msecs))] ...
);


fprintf('--> [Build_Base.m]: end of Build_Base\n\n');





