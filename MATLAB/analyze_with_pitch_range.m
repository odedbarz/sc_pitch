
clc

fprintf('-> Starting...\n')

pitch_est_v = zeros(net.N_epoch2, 1);
pitch_opt_v = zeros(net.N_epoch2, 4);
pitch_salience_v= zeros(net.N_epoch2, 4);

for ii = 1:net.N_epoch2
    if (mod(ii,25) == 0)
        fprintf('-> ii = %d (%d)\n', ii, net.N_epoch2)
    end
    
    pitch_range_Hz = tst_db.F02(ii) * 2.^[-18/12, 18/12];    

    [pitch_est_v(ii), pitch_opt_v(ii,:), pitch_salience_v(ii,:), tst_db.P(:,ii), fff_hs ] = ...
       PitchEst( tst_db.H(:,ii), net.hs.freq_hs, net.hs.G, pitch_range_Hz, 0 );
    
end

fprintf('-> The end!\n', ii, net.N_epoch2)


%% plot the results:
% -----------------
fontsize   = 36;
markersize = 30;
Position = [0.075, 0.1356, 0.9, 0.7894];

figure(10);
ax1 = plot( 3* 1e-3* tst_db.F02, 1e-3*pitch_est_v, '.', 'markersize', markersize);
hold off;
grid on;
xlabel('Frequency of the First Harmonic [kHz]', 'interpreter', 'latex', 'fontsize', fontsize);
ylabel('$\hat{f}_0$ [kHz]', 'interpreter', 'latex', 'fontsize', fontsize);
title('Pitch Estimations', 'interpreter', 'latex');
set(gca, 'fontsize', fontsize);
set(gca, 'fontsize', fontsize);
% legend('1^{st} peak', '2^{nd} peak', '3^{rd} peak', '4^{th} peak');  
grid on;
hold on
plot(3* 1e-3*[125 5e3], 1e-3*[125, 5e3], '--k', 'linewidth', 2);
plot(3* 1e-3*[125 5e3], 1e-3*2*[125, 5e3], '--k', 'linewidth', 2);
plot(3* 1e-3*[125 5e3], 1e-3*0.5*[125, 5e3], '--k', 'linewidth', 2);
hold off
set(gca, 'Position', Position);


%%
figure(11);
[f_crm, f_nrm] = freq2chroma( tst_db.F02, tst_db.pitch_est );
plot(tst_db.F02, f_crm, '.', 'markersize', markersize);
xlabel('Frequency [Hz]');
ylabel('Semitones');
title(sprintf('Pitch (Chroma), Amplitude: %d dB SPL', net.dic.atom_amplitude_range_dB(1)));
set(gca, 'fontsize', fontsize);
set(gca, 'fontsize', fontsize);
grid on;


% figure(12)
% plot(tst_db.F02, abs(pitch_est_v-tst_db.F02)./tst_db.F02, '.', 'markersize', markersize);










