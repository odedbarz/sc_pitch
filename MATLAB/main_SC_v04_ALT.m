%
% main_SC_v04_irn.m
%
% Notes:
%   * 07/04/2015: I have added the sparse coding with 'sliding windows'
%                 option.
%
%   * 12/08/2015: Perform_SC.m, case #4 (the Rozell's based RNN): Back to
%                 the previous version of PitchEst_v13_04.
%

clc
    
% % mysparse = @(x) sparse(x); 
% mysparse = @(x) x;      fprintf('-> mysparse is inactivated !');

fontsize   = 36;
markersize = 30;

%% create the bases' database:

% tic
% net.stim.amp_dBSPL  = 25;        % [dB SPL] stimulus level
% net.stim.noise_level= 0.0;     % [0,1] Set the amount of added white noise in the stimulus, noise_level*[SNR]

if ~exist('base_db','var')    
    % Options for the <anf_method> field:
    % 1. Heinz model;
    % 2. Roy Patterson's Model (Malcom Slaney's toolbox, version 2, 1998);
    % 3. Lyon Passive Ear model (Malcom Slaney's toolbox, version 2, 1998);
    % 4. Ray Meddis� hair cell model (Malcom Slaney's toolbox, version 2, 1998);
    % 5. Modification (#1) of Ray Meddis� hair cell model (Malcom Slaney's toolbox, version 2, 1998);
    % 10. The model of Zilany et. al. (2014);
    % 20. Miriam, Oded et. al., (2012) MATLAB & Time-Domain;
    % 21. Miriam, Oded et. al., (2012) C++ & Time-Domain;
    anf_method = 4;

    Build_Base_ALT;   
else
    % assuming that there is already a loaded <net> structure in the workspace
	addpath(net.AN_slaney_dir);
    InitCochlea;
    
end

fprintf('-> [main.m]: net.cochlea.Nfb:  %g\n', net.cochlea.Nfb);
fprintf('-> [main.m]: net.dic.dim2:     %g\n', net.dic.dim2);
fprintf('-> [main.m]: ANF threshold:    %g\n', net.anf.thr);


%% Init. Sparse Coding data:
% [1] (my) ISTA(); 
% [2] (MATLAB's) lasso();
% [3] (Foldiak's based) RNN;
% [4] (Rozell's based) RNN;
ista.method = 2;       

% Initialize the SC engine:
InitSC;             

% set the debug mode for the ISTA precoedure (like setting verbose to ON):
ista.debug  = 0;


% For Biological plausible simulations:
if ista.method~=4 && (6 < net.dic.msec)
    warning('-> [main.m]: for this SC method, please change net.msecs to 5 ms!');
end

%% Testing the sparse coding

% # of epoches:
net.N_epoch2 = 1;

% testing phase. Currently it's just for legacy...
net.training_flag   = 0;

% pre-defined desired harmonics:
net.tst_hrm_slc = nan;
% net.tst_hrm_slc = 27;
% net.N_hrm = 100;
% % net.stim.Shackelton94.region    = 'LOW';        % options: {'LOW', 'MID', 'HIGH'}    
% net.stim.Shackelton94.region    = 'MID';        % options: {'LOW', 'MID', 'HIGH'}    
% % net.stim.Shackelton94.region    = 'HIGH';        % options: {'LOW', 'MID', 'HIGH'}    
% net.stim.Shackelton94.type      = 'SINE';       % options: {'SINE', 'ALT'}
% % net.stim.Shackelton94.type      = 'ALT';       % options: {'SINE', 'ALT'}

% ==> 'harmonics2_:'
if isnan(net.tst_hrm_slc) || isempty(net.tst_hrm_slc)
    harmonics2_ = zeros(1,net.N_hrm);   
    harmonics2_( 1+(0:3) ) = 1; 
else
    harmonics2_ = [];
end

    harmonics_ref = zeros(1,net.N_hrm); 
    harmonics_ref = harmonics2_;     
    dF = 1.01;    % difference between the two consecutive frequencies
    amplitude_A = 40;
    amplitude_B = 42;

% stimulus inharmonics:
% F0_tst = [];
% F0_tst = 62.5;   % [Hz]
% F0_tst = 125;   % [Hz]
% F0_tst = 88.4;   % [Hz]
% F0_tst = 250;   % [Hz]
F0_tst = 433;   % [Hz]

% stim. volume:
net.stim.amp_dBSPL = 0;        % setting the amplitude [dB SPL]
net.stim.noise_level = 0;       
pink_noise_amplitude = 0;                '<== Add pink noise to the input ***'



fprintf('\n  STIMULUS FEATURES:\n');
fprintf('-> net.stim.amp_dBSPL: %.1f [dB SPL]\n', net.stim.amp_dBSPL);
if 0 ~= net.stim.noise_level, fprintf('-> net.stim_noise_level: %g\n', net.stim.noise_level); end;
fprintf('\n');


% inharmonic frequency delay:
dF2_ = [];          % [Hz]
if (~isempty(dF2_) && ~isnan(dF2_)); fprintf('-> !! Note: using inharmonic setup, dF2_ = %g Hz\n ', dF2_); end;  
phase2 = 0; % stimulus phase

%% Testing database:
tst_db.F02          = nan(net.N_epoch2, 1);
tst_db.harmonics2   = nan(net.N_epoch2, net.N_hrm);
tst_db.dF2          = nan(net.N_epoch2, 1);
tst_db.phase2       = nan(net.N_epoch2, 1);
%tst_db.fff_dic      = fff_dic;
tst_db.H            = zeros(net.dic.dim2, net.N_epoch2);
tst_db.pitch_est    = nan(net.N_epoch2, 1);
tst_db.pitch_opt    = nan(net.N_epoch2, 4);
%tst_db.masker_flag  = 0;
tst_db.sieve_sig    = []; %0.05;              
tst_db.gauss_LPF    = []; %[1e3, 3e3];  '*** gauss_LPF is ON ***'   % [sigma, mu]

debug_mode.plot     = 1;

% total time in this simulation:
fprintf('-> total stimulus time: %g [msec]\n', net.stim.msecs);
fprintf('-> ACTUAL stimulus time: %g [msec]\n',  net.stim.msecs- net.dic.start_msec);

% preparing for the parfor:
sieve_sig   = tst_db.sieve_sig;
N_epoch2    = net.N_epoch2;
gauss_LPF   = tst_db.gauss_LPF;
pitch_est_v = zeros(N_epoch2, 1);
pitch_opt_v = zeros(N_epoch2, 4);
pitch_salience_v= zeros(N_epoch2, 4);
F02_v       = zeros(N_epoch2, 1);
harmonics2_v= zeros(N_epoch2, net.N_hrm);
dF2_v       = zeros(N_epoch2, 1);


%% Create Harmonic Sieve Matrix:
if isempty(net.hs.G)
    net.hs.sigma_type = 1;
    %starting_freq_G = net.dic.F0;
    starting_freq_G = 50;   % Hz
    fff_G = linspace( starting_freq_G, net.dic.F1, D_dim2 );
    [net.hs.G, net.hs.freq_hs] = CreateHarmonicSieve( fff_G, gauss_LPF, net.hs.interp_factor, net.hs.sigma_type );
end

%%

% Save the probability functions:
P = zeros( size(net.hs.G,1), N_epoch2 );
DLdF = zeros( 1, N_epoch2 );
% sacf_pitch_v    = zeros( N_epoch2, 1 );
% sacf_salience_v = zeros( N_epoch2, 1 );


%% running the main epoches: 
plot_every = 9;

%     ista.gamma_ratio = 0.05;
%     ista.gamma_ratio = 0.03;
%     ista.gamma_ratio = 0.1;

    %Hd_ear = OuterEarFilt(net.stim.Fs);

    f_fir2 = (1e3*[0, 0.5, 1, 2, 4, 10, net.dic.Fs])/net.dic.Fs;
    f_fir2(end) = 1;
    
    m_fir2 = zeros(1,7);
    m_fir2(1) = dBSPL2Pa( 22.8 );
    m_fir2(2) = dBSPL2Pa( 22.8 - 1.5 );
    m_fir2(3) = dBSPL2Pa( 20.2 - 5);
    m_fir2(4) = dBSPL2Pa( 17.2 - 3 );
    m_fir2(5) = dBSPL2Pa( 13.8 - 3.5 );
    m_fir2(6) = 0;
    m_fir2(7) = 0;
    m_fir2 = m_fir2/m_fir2(1);
    N_fir = 1024;
    b_pink = fir2(N_fir, f_fir2, m_fir2);
    
%     fff = linspace(0,net.stim.Fs,net.stim.n_samples);
%     H = abs(fft(filter( b_pink, 1, (1:net.stim.n_samples==1) )));
%     plot(fff, 20*log10(dBSPL2Pa( 22.8 )*H/20e-6))
%     grid on
    
%%
    fprintf('-> [main_SC.m]: ista.gamma_ratio = %g\n', ista.gamma_ratio)
    
for tn = 1:N_epoch2
    %net.stim.Shackelton94.region = region_options{1};        % options: {'LOW', 'MID', 'HIGH'}    
    fprintf('\n -> tn = %d / %d\n', tn, N_epoch2);
    fprintf('\n--> [main_SC_v04_ALT.m]: frequency region <%s>\n', net.stim.Shackelton94.region);

    % Create the stimulus:
    % --------------------
    if length( F0_tst ) > 1     % for a batch of test frequencies:
        F0_n = F0_tst(tn);
    else
        F0_n = F0_tst;
    end
    
    
    % remove harmonics above 20 k Hz:
    if isnan(net.tst_hrm_slc) || isempty(net.tst_hrm_slc)
        harmonics2 = harmonics2_;
        harmonics2( F0_n*(1:net.N_hrm).*harmonics2_> net.cochlea.highfreq ) = 0;
    else 
        harmonics2 = [];
    end
    
%     % '@@@@@@@@@@@@@@@@@'
%             wnoise = randn(net.stim.n_samples+N_fir,1);     % white random noise ~N(0,1)
%             noise_level_Pa = dBSPL2Pa( pink_noise_amplitude );  % [dB SPL] --> [Pa]
%             pink_noise = noise_level_Pa*filter(b_pink, 1, wnoise);
%             pink_noise = pink_noise((N_fir+1):end);
%             % '@@@@@@@@@@@@@@@@@'
    
    [stimA, F0A, harmonics2] = CreateIO(net, F0_n, harmonics2, dF2_, phase2);  %, F0, harmonics, dF, phase);       
%     stimA = stimA + pink_noise;     % E{wnoise} == 0
%     anfA = CreateANF( net, stimA, Hd_ear, fcoefs, cochlea_freq );        
%     [hA, ~] = Perform_SC(net, ista, anfA); %, fff_dic, harmonics2, F02);   % >>> for debugging
%     pitch_range_Hz = F0_n * 2.^[-6/12, 6/12];   
%     [pitch1, ~, ~, P1, fff_hs ] = ...
%        PitchEst( hA, net.hs.freq_hs, net.hs.G, pitch_range_Hz, 0 );
    
   
%             % '@@@@@@@@@@@@@@@@@'
%             wnoise = randn(net.stim.n_samples+N_fir,1);     % white random noise ~N(0,1)
%             noise_level_Pa = dBSPL2Pa( pink_noise_amplitude );  % [dB SPL] --> [Pa]
%             pink_noise = noise_level_Pa*filter(b_pink, 1, wnoise);
%             pink_noise = pink_noise((N_fir+1):end);
%             % '@@@@@@@@@@@@@@@@@'
   
    [stimB, F0B, harmonics3] = CreateIO(net, dF*F0_n, harmonics_ref, dF2_, phase2);  %, F0, harmonics, dF, phase);       
%     stimB = stimB + pink_noise;     % E{wnoise} == 0    
%     anfB = CreateANF( net, stimB, Hd_ear, fcoefs, cochlea_freq );        
%     [hB, ~] = Perform_SC(net, ista, anfB); %, fff_dic, harmonics2, F02);   % >>> for debugging
%     %pitch_range_Hz = F0_n * 2.^[-6/12, 6/12];   
%     [pitch2, ~, ~, P2 ] = ...
%        PitchEst( hB, net.hs.freq_hs, net.hs.G, pitch_range_Hz, 0 );
    
    stim2 = dBSPL2Pa( amplitude_A )*stimA + dBSPL2Pa( amplitude_B )*stimB;
    anf2 = CreateANF( net, stim2, Hd_ear, fcoefs, cochlea_freq );        
    [h2, ~] = Perform_SC(net, ista, anf2); %, fff_dic, harmonics2, F02);   % >>> for debugging
    pitch_range_Hz = F0_n * 2.^[-6/12, 6/12];   
    [pitch1, ~, ~, P1, fff_hs ] = ...
       PitchEst( h2, net.hs.freq_hs, net.hs.G, pitch_range_Hz, 0 );

    %{
            '@@@@@@@@@@@@@@@@@'
            wnoise = randn(length(stim2)+N_fir,1);     % white random noise ~N(0,1)
            noise_level_Pa = dBSPL2Pa( pink_noise_amplitude );  % [dB SPL] --> [Pa]
            pink_noise = noise_level_Pa*filter(b_pink, 1, wnoise);
            pink_noise = pink_noise((N_fir+1):end);
            
            stim2 = stim2 + pink_noise;     % E{wnoise} == 0
            
            '@@@@@@@@@@@@@@@@@'
    
       
    % stimulus --> ANF:
    % -------------------    
    % stimulus --> ANFs:
    anf = CreateANF( net, stim2, Hd_ear, fcoefs, cochlea_freq );        
    
    % ANF --> SC:
    % -------------------
    % perform the SC - extract the sparse coefficient vector h
%     [h, ista] = Perform_SC(net, ista, anf); %, fff_dic, harmonics2, F02);   % >>> for debugging
    [h, ~] = Perform_SC(net, ista, anf); %, fff_dic, harmonics2, F02);   % >>> for debugging
    if 0==nnz(h); fprintf('-> h is ALL-ZEROS --> continue the for loop\n'); continue; end;    

    
    % SC --> pitch estimation:
    % ------------------------ 
    % Estimate pitch:
    %pitch_range_Hz = F0_n * 2.^[-18/12, 18/12];   
    pitch_range_Hz = F0_n * 2.^[-6/12, 6/12];   
    debug_est = 0;  
    
    [pitch_est_v(tn), pitch_opt_v(tn,:), pitch_salience_v(tn,:), P(:,tn), fff_hs ] = ...
       PitchEst( h, net.hs.freq_hs, net.hs.G, pitch_range_Hz, debug_est );
    
    %if isempty(pitch_est); error('-> !!! ERROR: pitch_est is empty !!!'); end;
    if isempty(pitch_est_v(tn)); fprintf('-> pitch_est_v(tn) is ALL-ZEROS --> continue the for loop\n'); continue; end;
    
    % Save results:
    % -------------------        
    % save current results:
    tst_db.phase2(tn)   = phase2_;
    tst_db.H(:,tn)      = h;
    %tst_db.pitch_masd(tn) = MASD( anf, cochlea_freq );  % pitch estimation using Cedolin & Delgutte's (2010) method
    
    fprintf('--> F02: %.3f Hz\n', F02);
    fprintf('--> Fest: ** %.3f Hz **\n', pitch_est_v(tn));
%     fprintf('--> SACF: sacf_pitch = %.3f Hz,    sacf_salience = %.3f\n', sacf_pitch_v(tn), sacf_salience_v(tn));
    fprintf('--> harmonics2: ['); fprintf(' %.3g', harmonics2); fprintf(' ]\n');    
    
    %fprintf('--> pitch est. by MASD: %.3g Hz\n', tst_db.pitch_masd(tn));
    
    % save current results:
    F02_v(tn) = F02;
    harmonics2_v(tn,:) = harmonics2;
    if ~isempty(dF2)
        dF2_v(tn) = dF2;
    end
    
    fprintf('--> (tn=%d) geomean:   %g\n', tn, geomean(abs(pitch_est_v(1:tn)-F02)./F02));
    fprintf('--> (tn=%d) df/f:      %.3g %%\n', tn, 100*abs(pitch_est_v(tn)-F02)./F02);
    
    
    if 0 %~mod(tn-1,plot_every)
%         % {
        figure(21)
        plot(F02_v(1:tn), pitch_opt_v(1:tn,1), '.', 'markersize', markersize);
        %plot(F02_v(1:tn), pitch_opt_v(1:tn,:), '.', 'markersize', markersize);
        grid on;
        xlabel('f [Hz]', 'interpreter', 'latex', 'fontsize', fontsize);
        ylabel('\^{f} [Hz]', 'interpreter', 'latex', 'fontsize', fontsize);
        title('Pitch Estimations');
        set(gca, 'fontsize', fontsize);
        % }
        
        fignum = [];
        chroma_resolution = 100;    % # of bins
        chroma4P(fff_hs, P(:,tn), F02, chroma_resolution, fignum);
        
        % {
        figure(25)
        hist(pitch_opt_v(1:tn), 100);
        % }
        
        drawnow;
    end
          
    %}

    fprintf('\n *** Results: ***\n');
    % fprintf('--> geomean:   %g\n', geomean(abs(pitch_est_v(1:tn)-F02)./F02));
    % fprintf('--> df/f:      %.3g %%\n', 100*abs(pitch_est_v(1:tn)-F02)./F02);
    fprintf('--> pitch1: %g\n', pitch1);
    fprintf('--> pitch1/F0_n: %g [%%]\n', 100*abs(F0_n-pitch1)/F0_n);
%     fprintf('--> pitch2: %g\n', pitch2);
%     DLdF(tn) = abs(pitch1-pitch2)/pitch1;
%     fprintf('--> df: %.3g (~%.1f[%%])\n', DLdF(tn), 100*DLdF(tn));
       
end


% % gathering all the data from the parfor procedure:
% tst_db.pitch_est    = pitch_est_v;
% tst_db.pitch_opt    = pitch_opt_v;
% tst_db.pitch_salience= pitch_salience_v; 
% tst_db.F02          = F02_v;
% tst_db.harmonics2   = harmonics2_v;
% tst_db.dF2          = dF2_v;
% tst_db.P            = P;
% tst_db.fff_G        = fff_G;
% tst_db.F0_tst       = F0_tst;
% % tst_db.limen_f      = limen_f;
% tst_db.fff_hs       = fff_hs;

% geomean_fun = 'geomean(abs(pitch_est_v(1:tn)-F02)./F02))';
% DLdf = geomean(abs(pitch_est_v-F02)./F02);

return;


%% save results:


% ista_: ista without D:
ista_ = ista;
ista_ = rmfield(ista_, 'D');

% save( [net.data_dir, sprintf('results_F0(%d)_(%ddB)_anf(%d)_ista(%d)_ms(%d)',...
%     F02, net.stim.amp_dBSPL, net.anf.method, ista.method, fix(net.msecs))], ...
%     'ista_', 'tst_db', 'net' ...
% );
% 

% % For IRN:
% save( [net.data_dir, sprintf( 'IRN_results_F0(%d)_sign(%d)_rep(%d)_(%ddB)_anf(%d)_ista(%d)_ms(%d)',...
%     F02, net.stim.IRN.sign, net.stim.IRN.rep, net.stim.amp_dBSPL, net.anf.method, ista.method, fix(net.msecs))], ...
%     'ista_', 'tst_db', 'net', 'fff_dic' ...
% );

% % For JND:
% save( [net.data_dir, sprintf('JND_results_hrm([%s])_SPL(%ddB)_anf(%d)_ista(%d)_ms(%d)',...
%     num2str(find(harmonics2)), net.stim.amp_dBSPL, net.anf.method, ista.method, fix(net.msecs))], ...
%     'ista_', 'tst_db', 'net' ...
% );

if length(F0_tst) > 1
    error('!!! length(F0_tst) > 1 !!!')
end

save( [net.data_dir, sprintf('results_ALT_anf(%d)_f0(%g)_stimtype(%s)_region(%s).mat',...
    net.anf.method, F0_tst, net.stim.Shackelton94.type,  net.stim.Shackelton94.region)], ...
    'ista_', 'tst_db', 'net' ...
);
























