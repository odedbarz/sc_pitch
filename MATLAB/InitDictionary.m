function [ D ] = InitDictionary( dim1, dim2 )

% opt. #1:
% %{
    %p = 0.1;
    %D = full(sprandn(dim1,dim2,p));
    
    D = randn(dim1,dim2);
    
    %D = conv2(D,ones(10)/10,'same');
    
%}

% opt. #2:
%{
    sig = 2*2.0;
    mu = linspace(1,dim1,dim2);
    D = zeros(dim1, dim2);

    xx = [1:dim1];

    for ii = 1:dim2        
        D(:,ii) = exp( -0.5*((xx - mu(ii))/sig).^2);

    end
%}

%  opt. #3:
%{
p = 0.1;
scale = 1.0/sqrt(p*dim2); 
D = sprandn(dim1,dim2,p)*scale;     % (Oded) p is the density of the matrix (non-zero values). 
D = full(D);

hf = ones(20)/20;

D = conv2(D, hf, 'same');
%}



% normalaize the variance of the dictionary
D = D*diag( 1./sqrt(sum(D.*D)) );
    
assert( 0 == nnz(isnan(D)), '\n\n -> Error in InitDictionary(): \n\t0 ~= nnz(isnan(D))\n\n' );

