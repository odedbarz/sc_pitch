%
% run_simulations
% 

clc
clear all

%% build a base:
anf_method = 10;
Build_Base_v02;    


%% #1
net.stim.amp_dBSPL = 2* 45;        '*** setting the amplitude [dB SPL] ***'
main_SC_v04;


%% #2
net.stim.amp_dBSPL = 45;        '*** setting the amplitude [dB SPL] ***'
main_SC_v04;

