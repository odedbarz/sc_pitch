
function y = LoadMtx(par_name, N, M, partype)

if 4 > nargin
    partype = 'double';     % default par type
end

filename = ['..\data\', par_name,'.dat'];
fid = fopen(filename, 'r');

if (fid==-1)
    error(sprintf('-> Error in LoadMtx(): couldn''t open the file %s !!!\n', filename));
end

y = fread( fid, N*M, partype );
fclose(fid);

y = reshape(y, N, M);
% y = reshape(y, M, N);
% y = y';
