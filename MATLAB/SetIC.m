

function SetIC( db_st, x0, z0 )

    if (0 == db_st.IC_flag)
        sprintf('--> !! Warning !! SetIC(): exit without I.C. (IC_flag=0)\n');
        return;
    end
    fid = fopen(db_st.x0_filename, 'w');
    fwrite(fid, x0, db_st.partype);
    fclose(fid);

    fid = fopen(db_st.z0_filename, 'w');
    fwrite(fid, z0, db_st.partype);
    fclose(fid);

end

