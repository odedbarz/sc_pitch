function y = Create_ERB_Freq( N, freqs )
% 
% Inputs:
%   N: (1x1) the length of the vector,
%   freqs: (1x2) [f(start), f(end)] the starting & ending frequencies,
%
% Desc.
%   Creates a vector of frequencies according to the ERB space matrix.
%

if ~exist('freqs', 'var') || isempty(freqs)
    y = sort( ERBSpace( net.dic.F0, net.dic.F1, N ), 'ascend' );
    
else
    y = sort( ERBSpace( freqs(1), freqs(2), N ), 'ascend' );

end

