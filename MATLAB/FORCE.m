% 
% force_external_feedback_loop.m
%
%
% This function generates the sum of 4 sine waves in figure 2D using the architecture of figure 1A with the RLS
% learning rule.
%
% written by David Sussillo

disp('Clearing workspace.');
clear;

% (Oded) Debug mode:
debug.train = 0;
debug.fig1  = 99; 

% restart the debug figure
if 1 == ishandle(debug.fig1)
    figure(debug.fig1)
    cla
end

pause_time = 0.05;      % (Oded) for plotting in real time

% (Oded) parameers for the <plot()> function
linewidth = 3;
fontsize = 14;
fontweight = 'bold';

N = 1024;               % (Oded) # of neurons in the simulation
p = 0.1;                % (Oded) density of the network 
g = 1.5;				% g greater than 1 leads to chaotic networks
alpha = 1.0;
nsecs = 1440;           % [time units] (Oded) total # of seconds in the simulation
dt = 0.1;               % (Oded) time scale for the simulation
tau = 10*dt;
learn_every = 2;

% (Oded) M == J in the article. This is the neurons connection matrix
scale = 1.0/sqrt(p*N);          % (Oded) should be variance of the Gaussian distribution (with zero mean), 
                                %        but by this use (multiplying by the <scale>), it is actually a std!                              
M = sprandn(N,N,p)*g*scale;     % (Oded) p is the density of the matrix (non-zero values). 
M = full(M);

%     % (Oded)
%     fprintf('-> (Oded) Removing the self recurring (diag) entries from M\n')
%     M = M - diag(diag(M));
%     fprintf('\n')

nRec2Out = N;
wo = zeros(nRec2Out,1);
dw = zeros(nRec2Out,1);
wf = 2.0*(rand(N,1)-0.5);       % (Oded) uniform dist. in [-1,1)

disp(['   N: ', num2str(N)]);
disp(['   g: ', num2str(g)]);
disp(['   p: ', num2str(p)]);
disp(['   nRec2Out: ', num2str(nRec2Out)]);
disp(['   alpha: ', num2str(alpha,3)]);
disp(['   nsecs: ', num2str(nsecs)]);
disp(['   learn_every: ', num2str(learn_every)]);

% (Oded) The simulation time vector(s):
simtime = 0:dt:nsecs-dt;
simtime_len = length(simtime);
simtime2 = 1*nsecs:dt:2*nsecs-dt;

% (Oded) The input:
amp = 1.3;
freq = 1/60;
ft = (amp/1.0)*sin(1.0*pi*freq*simtime) + ...
     (amp/2.0)*sin(2.0*pi*freq*simtime) + ...
     (amp/6.0)*sin(3.0*pi*freq*simtime) + ...
     (amp/3.0)*sin(4.0*pi*freq*simtime);
ft = ft/1.5;

%     ft = (amp/1.0)*sin(1.0*pi*freq*simtime) + ...
%          (amp/2.0)*sin(200.0*pi*freq*simtime) + ...
%          (amp/6.0)*sin(300.0*pi*freq*simtime) + ...
%          (amp/3.0)*sin(400.0*pi*freq*simtime);


ft2 = (amp/1.0)*sin(1.0*pi*freq*simtime2) + ...
      (amp/2.0)*sin(2.0*pi*freq*simtime2) + ...
      (amp/6.0)*sin(3.0*pi*freq*simtime2) + ...
      (amp/3.0)*sin(4.0*pi*freq*simtime2);
ft2 = ft2/1.5;


wo_len = zeros(1,simtime_len);    
zt = zeros(1,simtime_len);
    zpt0 = zeros(1,simtime_len);     % (Oded) 
zpt = zeros(1,simtime_len);
x0 = 0.5*randn(N,1);
z0 = 0.5*randn(1,1);

x = x0; 
r = tanh(x);
z = z0; 

figure(10);
ti = 0;
P = (1.0/alpha)*eye(nRec2Out);
for t = simtime
    ti = ti+1;	
    
    if mod(ti, nsecs/2) == 0
        disp(['time: ' num2str(t,3) '.']);
        subplot 211;
        plot(simtime, ft, 'linewidth', linewidth, 'color', 'green');
        hold on;
        plot(simtime, zt, 'linewidth', linewidth, 'color', 'red');
        title('training', 'fontsize', fontsize, 'fontweight', fontweight);
        legend('f', 'z');	
        xlabel('time', 'fontsize', fontsize, 'fontweight', fontweight);
        ylabel('f and z', 'fontsize', fontsize, 'fontweight', fontweight);
        hold off;

        subplot 212;
        plot(simtime, wo_len, 'linewidth', linewidth);
        xlabel('time', 'fontsize', fontsize, 'fontweight', fontweight);
        ylabel('|w|', 'fontsize', fontsize, 'fontweight', fontweight);
        legend('|w|');
        pause( pause_time );	
    end
    
    % sim, so x(t) and r(t) are created.
    % (Oded) using Euler aprox. for dx/dt
    x = (1.0-dt/tau)*x + M*(r*dt/tau) + wf*(z*dt/tau);  
    r = tanh(x);
    z = wo'*r;
    
    if mod(ti, learn_every) == 0
        % update inverse correlation matrix
        k = P*r;
        rPr = r'*k;
        c = 1.0/(1.0 + rPr);
        P = P - k*(k'*c);

        % update the error for the linear readout
        e = z-ft(ti);

        % update the output weights
        dw = -e*k*c;	
        wo = wo + dw;
    end
    
    % (Oded) DEBUG:
    if 1 == debug.train
        figure(99)
        plot([x/max(abs(x)), wo/max(abs(wo))], '.--')
        legend('wo_{norm}', 'x_{norm}')
%         hold on        
%         plot(ti, z, 'x', 'markersize', 12, 'linewidth', 2)    
%         title('z(t)', 'fontsize', fontsize, 'fontweight', fontweight);
        pause( pause_time )
    end
    
    % Store the output of the system.
    zt(ti) = z;
    wo_len(ti) = sqrt(wo'*wo);	
end
error_avg = sum(abs(zt-ft))/simtime_len;
disp(['Training MAE: ' num2str(error_avg,3)]);    
disp(['Now testing... please wait.']);    


%% Now test. 
ti = 0;

X = zeros(N,simtime_len);
% I = zeros(simtime_len,1);
% I(simtime_len-200+(1:200)) = hanning(200);

for t = simtime				% don't want to subtract time in indices
    ti = ti+1;    
    
    % sim, so x(t) and r(t) are created.
    x = (1.0-dt/tau)*x + M*(r*dt/tau) + wf*(z*dt/tau); % + I(ti);
    r = tanh(x);
    z = wo'*r;
    
    zpt(ti) = z;
    %X(:,ti) = r;
    
end

plot(X(1,:));
hold on
plot(I,'r');
hold off
grid on
%%
error_avg = sum(abs(zpt-ft2))/simtime_len;
disp(['Testing MAE: ' num2str(error_avg,3)]);


figure(11);
subplot 211;
plot(simtime, ft, 'linewidth', linewidth, 'color', 'green');
hold on;
plot(simtime, zt, 'linewidth', linewidth, 'color', 'red');
title('training', 'fontsize', fontsize, 'fontweight', fontweight);
xlabel('time', 'fontsize', fontsize, 'fontweight', fontweight);
hold on;
ylabel('f and z', 'fontsize', fontsize, 'fontweight', fontweight);
legend('f', 'z');


subplot 212;
plot(simtime2, ft2, 'linewidth', linewidth, 'color', 'green'); 
hold on;
axis tight;
plot(simtime2, zpt, 'linewidth', linewidth, 'color', 'red');
axis tight;
title('simulation', 'fontsize', fontsize, 'fontweight', fontweight);
xlabel('time', 'fontsize', fontsize, 'fontweight', fontweight);
ylabel('f and z', 'fontsize', fontsize, 'fontweight', fontweight);
legend('f', 'z');
hold off

