
function ft = WeightFt(F1, F2, ft_min, ft_max)

ft = (F1>F2)*ft_min + (F1<=F2)*ft_max;
% ft = (F1<F2)*ft_min + (F1>=F2)*ft_max;