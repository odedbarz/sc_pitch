%
% ForceIt.m
%


if (net.USE_CUDA)
    
    if 1 == net.training_flag
        file2CUDA = net.train_filename;
    else
        file2CUDA = net.test_filename;
    end
    
    % run the model using CUDA:
    fprintf('\n\n');
    try
        status = dos([net.pitchest_dir, 'PitchEst ', file2CUDA]);         
        if 0 ~= status, error(sprintf('\t-> PitchEst.exe FAILED to run!\n')), end;
    catch
        
            pause(0.5);
        
        fprintf('--> ForceIt: try-catch for PitchEst()\n');
        status = dos([net.pitchest_dir, 'PitchEst ', file2CUDA]); 
        if 0 ~= status, error(sprintf('\t-> PitchEst.exe FAILED to run!\n')), end;
    end
    fprintf('\n');
    
    % load the model's result:
    zt = LoadMtx('zt', net.n_samples, net.z_d1);
    

else    % run it on MATLAB    
    
    for ti = 1:net.n_samples    % run ONE epoch
    
        x = (1.0-dt2tau)*x + dt2tau *( M*r + Wf*z + Jin*anf(:,ti) );  
        r = tanh(x);
        z = wo'*r; 
        
        zt(ti,:) = z;

        if net.save_X
            Xn(:,ti) = x;
        end
        
        if (1 == net.training_flag) % && (mod(ti, net.learn_every) == 0)

            for ii = 1:net.z_d1
                % update inverse correlation matrix
                k = P(:,:,ii)*r;
                rPr = r'*k;
                c = 1.0/(1.0 + rPr);
                P(:,:,ii) = P(:,:,ii) - k*(k'*c);

                % update the error for the linear readout
                e = z(ii)-ft(ti,ii);

                % update the output weights
                dw = -e*k*c;	
                wo(:,ii) = wo(:,ii) + dw;
            end

        end
    
    end % end of "run ONE epoch"
    
end




