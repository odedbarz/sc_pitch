function [ masd_v, pitch_est ] = MASD( anf, cochlea_freq )
%
% CF x Time = size(anf)
% 

masd_v = sum(abs(diff(anf,1)),2);
% masd_v = sum(abs(diff(anf(end:-1:1,:),1)),2);

% make the masd_v vector to have the same length (freq.) as the anf matrix:
masd_v = [masd_v(:); 0];


if exist('cochlea_freq', 'var') && (nargout >=2)
    [~,Ix_max_pitch] = max(masd_v);

    % pitch_est = cochlea_freq(end-Ix_max_pitch+1);
    pitch_est = cochlea_freq(Ix_max_pitch);
end

end

