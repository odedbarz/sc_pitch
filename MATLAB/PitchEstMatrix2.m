

function [ Qest, est_win, fff_est ] = PitchEstMatrix2( fff_dic, Ny, sieve_db )


    fprintf('-> starting PitchEstMatrix2.m...\n');

    if length(fff_dic)~=Ny
        error('!!! Error at PitchEstMatrix2.m: length(fff_est)~=Ny\n');
    end

    Nyp = sieve_db.interp1_factor*Ny;
    
    Qest = zeros(Nyp,Nyp);

    if 1 == sieve_db.sparse_flag
        Qest = sparse(Qest);
    end

    % 1-D interpulation:
    %Qest = interp1( 1:Ny, full(Qest)', linspace(1,Ny, sieve_db.interp1_factor*Ny) )';
    fff_est = interp1( 1:Ny, fff_dic(:), linspace(1,Ny, Nyp) )';

        
    %sieve_v = zeros(1,Nyp);   % init. the sieve response (freq. domain)
    
    
    % Give bigger priority for smaller pitches (LPF):
    if ~isempty(sieve_db.gaussLPF) &&  ~isnan(sieve_db.gaussLPF(1)) 
        est_win = gaussmf( fff_est', sieve_db.gaussLPF );     % estimation window
        
    elseif ~isempty( sieve_db.gammafun ) 
        est_win = sieve_db.gammafun( fff_est' );     % estimation window
        
    else
        est_win = ones(1,length(fff_est));
        
    end

    %
    fff = fff_est';
    fff1 = fff(1);
    %sigma_ = 0.4* ERB( 1e-3*fff_est(1) );
    
    
    %
    parfor ii = 1:Nyp
        if ~mod(ii-1,floor(Nyp/5))
            fprintf('--> ii: %d\n', ii);
        end
        
        %sieve_v = 0*sieve_v;   % init. the sieve response (freq. domain)
        sieve_v = zeros(1,Nyp);   % init. the sieve response (freq. domain)

        f_ii = fff_est(ii);
        %sigma_ii = 0.25* ERB( 1e-3*f_ii );
        sigma_ii = min( 0.5*fff1, 0.05*f_ii );
        
        % create the sieve:
        for jj = 1:(Nyp-ii+1)
            f_jj = jj*f_ii;
            % (*)
            %sigma_jj = 0.4* ERB( 1e-3*f_jj );
            %sieve_v = sieve_v + gaussmf( fff, [sigma_jj, f_jj]);   
            % (**)
            sieve_v = sieve_v + gaussmf( fff, [sigma_ii, f_jj]); 
            % (***)
            %sieve_v = sieve_v + gaussmf( fff, [sigma_, f_jj]); 
        end
                
        % normalize the sieve:
        %sieve_v = sieve_v ./sum(sieve_v);
        
        Qest(ii,:) = sieve_v .* est_win;
        %Qest(ii,:) = sieve_v * est_win(ii);

        
    end
   
         
    fprintf('-> end of PitchEstMatrix2.m...\n');
   
    
end

























