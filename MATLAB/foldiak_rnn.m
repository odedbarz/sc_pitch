%
% function res_db = sc_rnn(net, Nepoch, F0_tst, phase2, harmonics2_, flags,   debug_mode)
%
% foldiak_rnn.m
%

% '%%%%%%%%%%%%%%%%%'
% N_itr = 100
% N_init = 100



if 1 == debug_mode.sparse
%     T = zeros(Nepoch,1);
%     X = zeros(Nepoch,1);
    Y_      = nan(Ny,N_itr);
    Ya_     = nan(Ny,N_itr);
    dQ_     = nan(Nepoch,1);
    dW_     = nan(Nepoch,1);
%     estErr_ = nan(Nepoch,1);   % estimation error
    esterrprod = nan(Nepoch,1);   % estimation error
    misses_  = 0;
end
  


%% threshold Initialization... 
%{
fprintf('-> starting threshold initialization...\n');
for ti = 1:N_init
    y = (1.0-dt2tau)*y + dt2tau*phi( Q*x + W*y - theta ); 

    % Threshold modification:
    d_teta = gamma0*(y - pbits);
    theta = theta + d_teta;

end
%}



%%

for tn = 1:Nepoch

%     if ~mod(tn-1,fix(Nepoch/10))
%         fprintf('\n');           
%         fprintf('-> tn = %d (out of %d)\n', tn, Nepoch);
%     end
        
    % Create the stimulus:
    [stim2, F02, harmonics2, res_db.phase2(tn), dF2] = CreateIO(net, F0_tst, harmonics2_, dF2_, phase2);  %, F0, harmonics, dF, phase);       
        
    %
    fprintf('\n');   
    fprintf('-> tn = %d (out of %d)\n', tn, Nepoch);
    fprintf('--> harmonics2: ['); fprintf(' %.3g', harmonics2); fprintf(' ]\n');                        
    fprintf('--> F02: %.3g Hz\n', F02);

    
    
    % stimulus --> ANFs:
    anf = CreateANF( net, stim2, Hd_ear, fcoefs, fff_cf );        
    
    if length(anf(:)) ~= Nx; error('Error, check it out: << length(anf(:)) ~= Nx >> !'); end
    
    
    % input to the RNN:   
    %x = anf(:);
    x = zeros(size(Q,2),1);
    
    %{
    % normalize the input:
    if (1 == flags.anf_normalize)
        x = fun_norm(x);    % set the input:
    end
    %}
    
    
    y = abs(0.01*rand(size(y)));
    ya = 0.0 + 0*y;   % <y>
    
%     %{
    %fprintf('-> starting threshold initialization...\n');
    for tj = 1:N_init
        y = (1.0-dt2tau)*y + dt2tau*phi( 0*Q*x + 0*W*y - theta ); 

        % Threshold modification:
        d_teta = gamma0*(y - pbits);
        theta = theta + d_teta;

    end
    %}
    
    
    %theta = abs(0.01*rand(size(theta)));
    %theta = 0*theta;        '###########################'
    %gamma1 = 0
        
    
    
    % Running the RNN for N_itr iterations...
    for ti = 1:N_itr        
        if ~mod(ti-1,fix(N_itr/10))
            fprintf('---> ti = %d (out of %d)\n', ti, N_itr);
        end
        
        % input with sliding window:
        x = 0*x;
        dummy = anf(:,ti+(1:slide_win.smp)-1);
        Ix_win = net.cochlea.Nfb*(ti-1) + (1:net.cochlea.Nfb*slide_win.smp);
        x(Ix_win) = dummy(:) .* slide_win.win;
        %dummy = anf(:, 10+(1:slide_win.smp) );
        %x(1:slide_win.smp*net.cochlea.Nfb) = dummy(:);
        
        if (1 == flags.anf_normalize)
            x = fun_norm(x);    % set the input:
            x = x/(1e-16+max(x));
        end

        %
        y = (1.0-dt2tau)*y + dt2tau*phi( Q*x + W*y - theta ); 

        ya = (1-delta)*ya + delta*y;
        
        
%         % ---------
%         % training:
%         % ---------        
%         if (1 == net.training_flag) && ~mod(ti, net.learn_every)                        
%             [ W, Q, theta ] = Foldiak_Training( W, Q, x, ya, alpha, beta, gamma1, theta, pbits, flags, Nx );
%         end

        
%         %{
        % ** DEBUG **
        if (1 == debug_mode.sparse)
            Y_(:,ti) = y;               % DEBUG                   
            Ya_(:,ti) = ya;               % DEBUG                   
            %dQ_(tn) = norm(dQ,2);       % DEBUG
            %dW_(tn) = norm(dW,2);       % DEBUG
        end
        %}

        if 0  %(1 == debug_mode.sparse)
            figure(99);
            subplot 211
            imagesc(anf); colorbar;
            hold on
            contour(reshape_mt(x), 3, 'r');
            hold off

            subplot 212
            plot(f_v, [Q*x, theta, y, ya], '.-');
            legend('Q*x', '\theta', 'y', 'y_a');
            hold on
            partials = nonzeros(harmonics2.*[1:net.N_hrm]*F02');
            plot(repmat(partials',2,1), repmat([0,1]',1,length(partials)), '--r');
            hold off
            axis([0, 1.4*max(partials), 0, 1.5]);
            title(sprintf('f_0: %.1f Hz', F02));
            xlabel('freq. [Hz]')
            
            %pause(1e-6);
            drawnow;
        end
        
        
    end

    
    
    % ---------
    % training:
    % ---------        
    if (1 == net.training_flag) && ~mod(ti, net.learn_every)                        
        [ W, Q, theta ] = Foldiak_Training( W, Q, x, ya, alpha, beta, gamma1, theta, pbits, flags, Nx );
    end
    
    
    
    %{
    % **************************************************************  
    % training:
    % **************************************************************
    if (1 == net.training_flag)
        if 1 == flags.y_round
            y = round(y);
        end

        %  --- if sum(abs(y)>1); error('-> !!! sum(abs(y)>1)\n'); end;

        % anti-Hebbian learning:
        dW = -alpha*(y*y'-pbits^2);
        dW( dW > 0 ) = 0;   % make it all-negative only
        dW = dW - diag(diag(dW));   % set wij == 0
        W = W + dW;     % updatethe feedforward matrix

        % Hebbian learning:
        dQ = beta*(y*x' - (y   *ones(1,Nx)) .* Q );
        Q = Q + dQ;

        % Threshold modification:
        d_theta = gamma1*(y - pbits);
        theta = theta + d_theta;

    end
    %}
    
    
    % ********************************************************
        %{
    % ** DEBUG **
    if (1 == debug_mode.sparse)
        %Y_(:,ti) = y;      % DEBUG       

%         figure(99);
%         subplot 211
%         imagesc(anf); colorbar;
%         %imagesc(reshape_mt(x)); colorbar;
%         hold on
%         contour(reshape_mt(x), 3, 'r');
%         hold off
% 
%         subplot 212
%         plot(f_v, [Q*x, theta, y], '.-');
%         legend('Q*x', '\theta', 'y');

        figure(100)
        %subplot 313
        imagesc( linspace(0,net.msecs,net.n_samples), log2(f_v/f_v(1)), Y_);colorbar
        pitch_est = PitchEst( y, base_db.f_v, res_db.sieve_sig, res_db.gaussLPF, debug_mode.pitch_est );
        title(sprintf('Y, f_{est.}: %.1f', pitch_est))
        %set(gca,'YDir', 'normal', 'YScale', 'log');
        hold on
        plot( repmat([0,net.msecs],net.N_hrm,1)', log2(repmat(([1:net.N_hrm]*F02)',1,2)'/f_v(1)), 'k--', 'linewidth', 2)
        %plot( repmat([0,net.msecs],net.N_hrm,1)', log2(repmat((harmonics2.*[1:net.N_hrm]*F02)',1,2)'/f_v(1)), 'k--', 'linewidth', 2)
        hold off

        drawnow;
        %pause(0.001);
    end
        %}
        
%     end

   
    
%     %{
    % -------------------------------------------------------------
    % Estimate pitch:
    
    %}
    
    if 1  %(1 == debug_mode.sparse)
        figure(35);
        imagesc(Ya_);
        title('ya over time');
        
            figure(99);
            subplot 211
            imagesc(anf); colorbar;
            hold on
            contour(reshape_mt(x), 3, 'r');
            hold off

            subplot 212
            plot(f_v, [Q*x, theta, y, ya], '.-');
            legend('Q*x', '\theta', 'y', 'y_a');
            hold on
            partials = nonzeros(harmonics2.*[1:net.N_hrm]*F02');
            plot(repmat(partials',2,1), repmat([0,1]',1,length(partials)), '--r');
            hold off
            axis([0, 1.4*max(partials), 0, 1.5]);
            title(sprintf('f_0: %.1f Hz', F02));
            xlabel('freq. [Hz]')

            drawnow;
    end
    
    
    % **************************************
    y_avg = mean(Ya_(:,ceil(end/2:end)),2);  
    
%             pitch_est = [];
    
%     %{
    [pitch_est, pitch_opt, pitch_salience] = PitchEst( y_avg, base_db.f_v, res_db.sieve_sig, res_db.gaussLPF, debug_mode.pitch_est );
    if isempty(pitch_est)                     
        misses_ = 1+misses_;
        fprintf('--> SKIPING to next tn...\n');
        continue; 
    end;       
    %}
    
    
    % save the results into the database:
    res_db.F02(tn)          = F02;
    res_db.harmonics2(tn,:) = harmonics2;
        
    if ~isempty(dF2)
        res_db.dF2(tn) = dF2;
    end
    res_db.pitch_est(tn) = pitch_est;
    res_db.pitch_opt{tn} = pitch_opt;
    res_db.err_est(tn)   = abs(pitch_est-F02);
    
    
    
    
    
    % plot results on-screen:
    if  1  %(1 == debug_mode.sparse) && ~mod(tn-1,fix(Nepoch/10))        
        %{
        figure(20)
        [~,Iy] = max(y);
        subplot 131
        imagesc( reshape(D(:,Iy), net.cochlea.Nfb, base_db.D_smp) );
        colorbar;
        title( sprintf('D(:,%d)', Iy) );
        subplot 132
        imagesc( reshape(Q(Iy,:)', net.cochlea.Nfb, base_db.D_smp) );
        colorbar;
        title( sprintf('Q(%d,:)', Iy) );
        subplot 133
        imagesc( reshape(abs(Q(Iy,:)'-D(:,Iy)), net.cochlea.Nfb, base_db.D_smp) );
        colorbar;
        title( sprintf('|Q^T-D|_{%d}', Iy) );
        linkaxes; 
        %}
        
        %{
        figure(25)
        plot([Q*x, W*y, theta, phi(Q*x + W*y - theta)]);
        grid on
        legend('Qx','Wy','theta','\phi(*)');
        %}
        
        %
        figure(30)
        plot(res_db.F02, res_db.pitch_est, '.', 'markersize', markersize);
        hold on;
        plot([0, net.F0_end], [0, net.F0_end], '--k', 'linewidth', 2)
        plot([0, net.F0_end], [0, 2*net.F0_end], '--k')
        plot([0, net.F0_end], [0, 4*net.F0_end], '--k')
        plot([0, net.F0_end], [0, 1/2*net.F0_end], '--k')
        plot([0, net.F0_end], [0, 1/3*net.F0_end], ':k')
        plot([0, net.F0_end], [0, 1/4*net.F0_end], '--k')
        hold off;
        grid on;
        xlabel('f_L [Hz]');
        ylabel('Estimations [Hz]');
        title('Pitch Estimations');
        
%         figure(35);
%         imagesc(Y_);
%         title('y over time');
        %xlabel()
        
        %
        fprintf('--> misses_: %d out of tn: %d ==> ratio: %.1f \n', misses_, tn, misses_/tn);
        
        
        drawnow;
        %pause(0.001);

        
        esterrprod(tn) = F02/pitch_est;
        fprintf('--> pitch_est: %.1f Hz\n', pitch_est);
        fprintf('--> est. error: %.1f Hz\n', res_db.err_est(tn));
        fprintf('--> est. prod. error: %.1f \n', esterrprod(tn));  
        fprintf('--> pitch_salience:           1e3* [ ');fprintf('%.1f ', 1e3*pitch_salience); fprintf('] \n');    
        fprintf('--> pitch_opt/F02:                 [ ');fprintf('%.1f ', pitch_opt/F02); fprintf('] \n');
        fprintf('--> pitch_opt/(F02/esterrprod):    [ ');fprintf('%.1f ', pitch_opt/(F02/esterrprod(tn))); fprintf('] \n');
        

    end
    
    
end

 

%
% fprintf('-> misses_: %d out of tn: %d ==> ratio: %.1f \n', misses_, tn, misses_/tn);

