%
% main_SC_v04.m
%
% Notes:
%   * 07/04/2015: I have added the sparse coding with 'sliding windows'
%                 option.
%
%   * 12/08/2015: Perform_SC.m, case #4 (the Rozell's based RNN): Back to
%                 the previous version of PitchEst_v13_04.
%

clc
    
% % mysparse = @(x) sparse(x); 
% mysparse = @(x) x;      fprintf('-> mysparse is inactivated !');

fontsize   = 36;
markersize = 30;

%% create the bases' database:

% tic
% net.stim.amp_dBSPL  = 25;        % [dB SPL] stimulus level
% net.stim.noise_level= 0.0;     % [0,1] Set the amount of added white noise in the stimulus, noise_level*[SNR]

if ~exist('base_db','var')
    % Options for the <anf_method> field:
    % 1. Heinz model;
    % 2. Roy Patterson's Model (Malcom Slaney's toolbox, version 2, 1998);
    % 3. Lyon Passive Ear model (Malcom Slaney's toolbox, version 2, 1998);
    % 4. Ray Meddis� hair cell model (Malcom Slaney's toolbox, version 2, 1998);
    % 5. Modification (#1) of Ray Meddis� hair cell model (Malcom Slaney's toolbox, version 2, 1998);
    % 10. The model of Zilany et. al. (2014);
    % 20. Miriam, Oded et. al., (2012) MATLAB & Time-Domain;
    % 21. Miriam, Oded et. al., (2012) C++ & Time-Domain;
    anf_method = 4;
    
    '* Build_Base ==> Build_Base_atoms_with_different_amplitudes.,'
    Build_Base_atoms_with_different_amplitudes;     
else
    % assuming that there is already a loaded <net> structure in the workspace
	addpath(net.AN_slaney_dir);
    InitCochlea;
    
end

fprintf('-> [main.m]: net.cochlea.Nfb:  %g\n', net.cochlea.Nfb);
fprintf('-> [main.m]: net.dic.dim2:     %g\n', net.dic.dim2);
fprintf('-> [main.m]: ANF threshold:    %g\n', net.anf.thr);


%% Init. Sparse Coding data:
% [1] (my) ISTA(); 
% [2] (MATLAB's) lasso();
% [3] (Foldiak's based) RNN;
% [4] (Rozell's based) RNN;
ista.method = 2;       

% Initialize the SC engine:
InitSC;             

% set the debug mode for the ISTA precoedure (like setting verbose to ON):
ista.debug  = 0;


% For Biological plausible simulations:
if ista.method~=4 && (6 < net.dic.msec)
    warning('-> [main.m]: for this SC method, please change net.msecs to 5 ms!');
end

%% Testing the sparse coding

% testing phase. Currently it's just for legacy...
net.training_flag   = 0;

% pre-defined desired harmonics:
net.tst_hrm_slc = nan;
% net.tst_hrm_slc = 11;   '*** IRN *** '  
%     net.stim.IRN.sign   = 1;
%     net.stim.IRN.rep    = 1; 
%     %ista.gamma_ratio = 0.02;    '*** Raise the gamma for the IRN case ***'
% % net.tst_hrm_slc = 26;  'TT (Oxenham,2004)'

net.N_hrm = 100;

% harmonic_locations = [1 6 10 17 22];
% harmonic_locations = [1:3:22];
harmonic_locations = [1 4 6 10 13 17 19 22];

fprintf('-> [main.m]: Override the CreateIO.m options\n');
% fprintf('-> [main.m]: harmonics2_: ['); fprintf(' %g', harmonics2_); fprintf(']\n');

% # of epoches:
net.N_epoch2 = length(harmonic_locations);

% stim. volume:
net.stim.amp_dBSPL = 60;        % '*** setting the amplitude [dB SPL] ***'
net.stim.noise_level = 0;       % '*** Add Gaussian noise to the input ***'
fprintf('\n  STIMULUS FEATURES:\n');
fprintf('-> net.stim.amp_dBSPL: %.1f [dB SPL]\n', net.stim.amp_dBSPL);
if 0 ~= net.stim.noise_level, fprintf('-> net.stim_noise_level: %g\n', net.stim.noise_level); end;
fprintf('\n');

% inharmonic frequency delay:
dF2_ = [];          % [Hz]
% dF2_ = 200       % [Hz]


if (~isempty(dF2_) && ~isnan(dF2_)); fprintf('-> !! Note: using inharmonic setup, dF2_ = %g Hz\n ', dF2_); end;


% stimulus inharmonics:
% F0_tst = [];
% F0_tst = 433;   % [Hz]
F0_tst = 273;   % [Hz]
% F0_tst = 137;   % [Hz]
% F0_tst = linspace(130, 5000, net.N_epoch2);

% stimulus phase:    
phase2 = 0;


%% Testing database:
tst_db.F02          = nan(net.N_epoch2, 1);
tst_db.harmonics2   = nan(net.N_epoch2, net.N_hrm);
tst_db.dF2          = nan(net.N_epoch2, 1);
tst_db.phase2       = nan(net.N_epoch2, 1);
%tst_db.fff_dic      = fff_dic;
tst_db.H            = zeros(net.dic.dim2, net.N_epoch2);
tst_db.pitch_est    = nan(net.N_epoch2, 1);
tst_db.pitch_opt    = nan(net.N_epoch2, 4);
%tst_db.masker_flag  = 0;
tst_db.sieve_sig    = []; %0.05;              
tst_db.gauss_LPF    = []; %[1e3, 3e3];  '*** gauss_LPF is ON ***'   % [sigma, mu]

debug_mode.plot     = 1;

% total time in this simulation:
fprintf('-> total stimulus time: %g [msec]\n', net.stim.msecs);
fprintf('-> ACTUAL stimulus time: %g [msec]\n',  net.stim.msecs- net.dic.start_msec);

% preparing for the parfor:
sieve_sig   = tst_db.sieve_sig;
N_epoch2    = net.N_epoch2;
gauss_LPF   = tst_db.gauss_LPF;
pitch_est_v = zeros(N_epoch2, 1);
pitch_opt_v = zeros(N_epoch2, 4);
pitch_salience_v= zeros(N_epoch2, 4);
F02_v       = zeros(N_epoch2, 1);
harmonics2_v= zeros(N_epoch2, net.N_hrm);
dF2_v       = zeros(N_epoch2, 1);


%% Create Harmonic Sieve Matrix:
if isempty(net.hs.G)
    net.hs.sigma_type = 1;
    starting_freq_G = 50;   % Hz
    fff_G = linspace( starting_freq_G, net.dic.F1, D_dim2 );
    [net.hs.G, net.hs.freq_hs] = CreateHarmonicSieve( fff_G, gauss_LPF, net.hs.interp_factor, net.hs.sigma_type );
    [net.hs.G, net.hs.freq_hs] = CreateHarmonicSieve( fff_dic, gauss_LPF, net.hs.interp_factor, net.hs.sigma_type );
end

%%

% Save the probability functions:
P = zeros( size(net.hs.G,1), N_epoch2 );

ista.gamma_ratio = 0.05;


%% running the main epoches: 
plot_every = 9;
salience = zeros(1, N_epoch2);


for tn = 1:N_epoch2
    fprintf('\n -> tn = %d / %d\n', tn, N_epoch2);

    % Create the stimulus:
    % --------------------
    if length( F0_tst ) > 1     % for a batch of test frequencies:
        F0_n = F0_tst(tn);
    else
        F0_n = F0_tst;
    end
    
    harmonics2_ = zeros(1,net.N_hrm); 
    harmonics2_( harmonic_locations(tn) + (0:11)) = 1;

    % remove harmonics above 20 k Hz:
    if isnan(net.tst_hrm_slc) || isempty(net.tst_hrm_slc)
        harmonics2 = harmonics2_;
        harmonics2( F0_n*[1:net.N_hrm].*harmonics2_> net.cochlea.highfreq ) = 0;
    else 
        harmonics2 = [];
    end
    [stim2, F02, harmonics2, phase2_, dF2] = CreateIO(net, F0_n, harmonics2, dF2_, phase2);  %, F0, harmonics, dF, phase);       
    
    % stimulus --> ANF:
    % -------------------    
    % stimulus --> ANFs:
    anf = CreateANF( net, stim2, Hd_ear, fcoefs, cochlea_freq );        
    
    % ANF --> SC:
    % -------------------
    % perform the SC - extract the sparse coefficient vector h
    [h, ~] = Perform_SC(net, ista, anf); %, fff_dic, harmonics2, F02);   % >>> for debugging
    if 0==nnz(h); fprintf('-> h is ALL-ZEROS --> continue the for loop\n'); continue; end;    

    
    % SC --> pitch estimation:
    % ------------------------ 
    % Estimate pitch:
    pitch_range_Hz = F0_n * 2.^[-6/12, 6/12];
    %pitch_range_Hz = F0_n * 2.^[-18/12, 18/12];    
    %pitch_range_Hz = [];
    debug_est = 0;  

    [pitch_est_v(tn), pitch_opt_v(tn,:), pitch_salience_v(tn,:), P(:,tn), fff_hs ] = ...
       PitchEst( h, net.hs.freq_hs, net.hs.G, pitch_range_Hz, debug_est );
        
   
    salience(tn) = pitch_salience_v(tn,2)/pitch_salience_v(tn,1);
   
    
    % Save results:
    % -------------------        
    % save current results:
    tst_db.phase2(tn)   = phase2_;
    tst_db.H(:,tn)      = h;
    %tst_db.pitch_masd(tn) = MASD( anf, cochlea_freq );  % pitch estimation using Cedolin & Delgutte's (2010) method
    
    fprintf('--> F02: %.3f Hz\n', F02);
    fprintf('--> Fest: ** %.3f Hz **\n', pitch_est_v(tn));
    fprintf('--> harmonics2: ['); fprintf(' %.3g', harmonics2); fprintf(' ]\n');        
    fprintf('--> salience(%d): %.3g\n', tn, salience(tn));
    
    % save current results:
    F02_v(tn) = F02;
    harmonics2_v(tn,:) = harmonics2;
    if ~isempty(dF2)
        dF2_v(tn) = dF2;
    end
    
    if ~mod(tn-1,plot_every)
        %{
        figure(21)
        plot(F02_v(1:tn), pitch_opt_v(1:tn,1), '.', 'markersize', markersize);
        %plot(F02_v(1:tn), pitch_opt_v(1:tn,:), '.', 'markersize', markersize);
        grid on;
        xlabel('f [Hz]', 'interpreter', 'latex', 'fontsize', fontsize);
        ylabel('\^{f} [Hz]', 'interpreter', 'latex', 'fontsize', fontsize);
        title('Pitch Estimations');
        set(gca, 'fontsize', fontsize);
        %}
        
        %{
        fignum = 32;
        chroma_resolution = 100;    % # of bins
        chroma4P(fff_hs, P(:,tn), F02, chroma_resolution, fignum);
        %}
        
        %{
        figure(25)
        hist(pitch_opt_v(1:tn), 100);
        %}
        
        drawnow;
    end
    
end


% gathering all the data from the parfor procedure:
tst_db.pitch_est    = pitch_est_v;
tst_db.pitch_opt    = pitch_opt_v;
tst_db.pitch_salience= pitch_salience_v; 
tst_db.F02          = F02_v;
tst_db.harmonics2   = harmonics2_v;
tst_db.dF2          = dF2_v;
tst_db.P            = P;
tst_db.F0_tst       = F0_tst;
tst_db.fff_hs       = fff_hs;

tst_db.pitch_range_Hz= pitch_range_Hz;
tst_db.salience      = salience;
tst_db.harmonic_locations = harmonic_locations;
tst_db.fff_hs = fff_hs;

%% PLOT
figure(1)

fontsize = 48;
markersize = 40;
linewidth = 4;

salience = tst_db.salience;
salience_norm = salience(1)./salience;
% salience_norm = salience;
harmonic_locations = tst_db.harmonic_locations;
N = length(salience_norm);

% handles to the graphic objects:
plt1 = zeros(1,N);

plot(harmonic_locations, salience_norm, '--k', 'markersize', markersize, 'linewidth', linewidth);    
hold on
for ii = 1:N
    plt1(ii) = plot(harmonic_locations(ii), salience_norm(ii), 'o',...
        'markersize', markersize);    
    
    % if this harmonic location is included in the original resolved\unresolved 
    % figure of the paper then color it appropriately:
    hrm_in_fig = find(harmonic_locations(ii) == [1 6 10 17 22]);
    if 0 ~= find(harmonic_locations(ii) == [1 6 10 17 22]);
        color_ii = get(plt1(hrm_in_fig), 'Color');
        %set(plt1(ii), 'Color', 'k');
        set(plt1(ii), 'markerfacecolor', color_ii);
    end
        
end
hold off

set(plt1, 'markeredgecolor', 'k');           
set(plt1, 'linewidth', linewidth);

grid on;

set(gca, 'fontsize', fontsize);
xlabel('First Harmonic Number', 'interpreter', 'latex');
ylabel('($2^{nd}$ Peak)/($1^{st}$ Peak)', 'interpreter', 'latex')
title('Prominence (over one octave)', 'interpreter', 'latex')
% axis([0 25 0.25 1.1])



return;

%% save results:


% ista_: ista without D:
ista_ = ista;
ista_ = rmfield(ista_, 'D');

% save( [net.data_dir, sprintf('results_F0(%d)_(%ddB)_anf(%d)_ista(%d)_ms(%d)',...
%     F02, net.stim.amp_dBSPL, net.anf.method, ista.method, fix(net.msecs))], ...
%     'ista_', 'tst_db', 'net' ...
% );
% 

% % For IRN:
% save( [net.data_dir, sprintf( 'IRN_results_F0(%d)_sign(%d)_rep(%d)_(%ddB)_anf(%d)_ista(%d)_ms(%d)',...
%     F02, net.stim.IRN.sign, net.stim.IRN.rep, net.stim.amp_dBSPL, net.anf.method, ista.method, fix(net.msecs))], ...
%     'ista_', 'tst_db', 'net', 'fff_dic' ...
% );

% % For JND:
% save( [net.data_dir, sprintf('JND_results_hrm([%s])_SPL(%ddB)_anf(%d)_ista(%d)_ms(%d)',...
%     num2str(find(harmonics2)), net.stim.amp_dBSPL, net.anf.method, ista.method, fix(net.msecs))], ...
%     'ista_', 'tst_db', 'net' ...
% );


save( [net.data_dir, sprintf('results_salience_anf(%d)_hrm(%d,%d)_atoms(%d)_Nfb(%d)_dicFs(%gkHz)_ms(%d)',...
    net.anf.method,  active_hrm(1), active_hrm(end), D_dim2, net.cochlea.Nfb, 1e-3*net.dic.Fs, fix(net.msecs))], ...
    'ista_', 'tst_db', 'net' ...
);
























