function [G, freq_hs] = CreateHarmonicSieve( freq, gauss_LPF, interp_factor, sigma_type )
%
% Input:
%   * freq: the appropriate frequencies of the sparse vector
%   * sieve_sig: the sieves' sigma
%   * gaussLPF: [mean, sig], the two moments of the Gaussian LPF
%   * debug_mod: debug mode flag
%
% Output:
%   * G: the harmonic sieve's matrix
%
% Desciption:
%   Calculates the harmonic sieve's matrix G.
%
%
    
    % interpulation factor:
    %interp_factor = 10;
    dummy       = length(freq);    
    freq_hs     = interp1( 1:dummy, freq, linspace(1, dummy, dummy*interp_factor) );
    N_f         = length(freq_hs);
    
    % Give bigger priority for smaller pitches (LPF):
    if ~isnan(gauss_LPF)
        est_win = gaussmf( freq_hs, gauss_LPF );     % estimation window
    else
        est_win = ones(1,length(freq_hs));
    end
    
    % energy concentration:
    %Es = cumsum(hi.^2)/sum(hi.^2);

    G = zeros(N_f,N_f);   % init. the sieve response (freq_hs. domain)
    sieve = zeros(1, N_f);
    
    %erb0 = ERB( 1e-3*freq_hs(1) );
    
    
    % print on screen:
    switch sigma_type
        case 0
            fprintf('[CreateHarmonicSieve.m]: sigma_type == 0 ==> min(10*sieve_sig*freq_hs(1), sieve_sig*freq_hs(ii))\n');
        
            sieve_sig = 0.05;
            fprintf('[CreateHarmonicSieve.m]: sieve_sig = 0.05\n');
            
        case 1
            fprintf('[CreateHarmonicSieve.m]: sigma_type == 1 ==> 0.2*ERB( 1e-3*freq_hs(ii) )\n');

        case 2
            fprintf('[CreateHarmonicSieve.m]: sigma_type == 2 ==> log(ERB( 1e-3*freq_hs(ii) ))\n');                

        otherwise
            error('==> ERROR at [CreateHarmonicSieve.m]: sig_ii is not defined properly!\n');
    end
    
    
    
    
    for ii = 1:N_f
        
        %sig_ii = min(10*sieve_sig*freq_hs(1), sieve_sig*freq_hs(ii));
        %sig_ii = sqrt( 24.7*( 4.37*1e-3*freq_hs(ii) + 1 ) );   % use ERB for the Gaussian std
        
        freq4sig = max(100, freq_hs(ii));
        switch sigma_type
            case 0
                sig_ii = min(10*sieve_sig*freq_hs(1), sieve_sig*freq4sig);
                
            case 1
                sig_ii = 0.2*ERB( 1e-3*freq4sig );
                
            case 2
                sig_ii = log(ERB( 1e-3*freq4sig ));
            
            otherwise
                error('--> ERROR: sig_ii is not defined properly!\n');
        end

        
        sieve = 0*sieve;   % init. the sieve response (freq_hs. domain)
        nn = 0;	% init. the harmonic counter
        
        % create the sieve:
        for jj = ii:N_f
            if freq_hs(end) <= nn*freq_hs(ii)  % stop if: the mean of the next first gaussian is bigger than the possible frequency
                break;
            %elseif Es(jj) > 1         % stop if: most of the signal was analyzed
            %    break;
            else 
                nn = nn+1;
            end
            
            sieve = sieve + gaussmf( freq_hs, [sig_ii, nn*freq_hs(ii)]);
        
        end
                
        G(ii,:) = sieve;
        
    end
    
    G = max(0,G);     % remove non-zeros from the estimation
    G = G.*repmat(est_win,N_f,1);   % priority for low pitches
    
end

