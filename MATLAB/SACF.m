



function [pitch_Hz, salience, sacf] = SACF( anf, fs, fig_num ) 
%
% function [pitch_Hz, sacf] = SACF( net, anf, max_lag_ms, fs, fig_num ) 
%
% [pitch_SACF, sacf] = SACF( net, anf, fig_num ) 
% 

    % # of channels, 
    % N_ch = 128;  	% (Meddis & Hewitt, 1991)
    % N_ch =  60;      % (Meddis & O'Mard, 1997)

    % # of autocorrelation lags,
    % N_lags  = 343;	% (Meddis & Hewitt, 1991)
    % N_lags  = 191;	% (Balag, Meddis et al., 2008)


    % time constant

    % % ** (Balag, Meddis et al., 2008)
    % lags_start = 1/1000;    % [sec]
    % lags_end   = 1/30;      % [sec]
    % lags_v     = linspace(lags_start, lags_end, N_lags); 
    % tau_L      = 2*lags_v;

    % % ** (Meddis & Hewitt, 1991)
    % lags_v = linspace(1/20e3, 1/60, N_lags); % (Meddis & Hewitt, 1991)
    % tau_L = 2.5e-3*ones(1, N_lags);

    % % ** (Meddis & O'Mard, 1997)
    % lags_v = linspace(1/20e3, 1/60, N_lags); % (Meddis & Hewitt, 1991)
    % tau_L = 10e-3*ones(1, N_lags);

    debug_mode = 0;
    
    %fs = net.cochlea.Fs;    % [Hz]
    dt = 1/fs;              % [s]

    [N_ch, N_smp] = size(anf);
    tau = 10e-3;    % [ms]
    winsize = 3*ceil(tau/dt);
    N_lags  = N_smp - winsize + 1;	

    acf = zeros(N_lags, N_lags);

    %max_lag = floor(1e-3*max_lag_ms/dt);
    
    % this parameter should be less than N_lags:
    %N_loop = min(N_lags, max_lag);
    N_loop = N_lags;
    
    %RemoveMean = @(X) X - repmat(mean(X,2), 1, size(X,2));
    RemoveMean = @(X) X;    % Disable this function 
    
    
    %
    fprintf('--> [SACF.m]: Starting the loop on the lags...\n');  

    for nn = 1:N_loop 
        if ~mod(nn, 500); fprintf('---> [SACF.m]: nn = %d (%d)\n', nn, N_loop); end

        % window:
        Wn = repmat( exp( -(0:(winsize-1))*dt/tau ), N_ch, 1 );
        pt1 = RemoveMean( anf(:,1:winsize) );
        pt2 = RemoveMean( anf(:,nn + [1:winsize] -1) );
        
        acf_nn = 1/tau * pt1 .* pt2 .* Wn;

        acf(nn,1:winsize) = sum(acf_nn, 1);

        if (1 == debug_mode) && ~mod(nn, 100)
            figure(99)
            imagesc(acf);
            drawnow;

        end

    end

    fprintf('--> [SACF.m]: Finsihed main lop\n');

    sacf = mean(acf, 2);

    % window the self (first) peak of the autocorrelation function:
    sacf4peaks = sacf;
    %sacf4peaks(1:100) = 0;    
    
    if 0 == nnz(sacf4peaks)
        printf('--> [SACF.m]: Found no peaks\n');
        return;
    end
    
    % normalize by the first peak:
    sacf4peaks = sacf4peaks/max(sacf4peaks);    % peak_value < 1
    [pks, locs] = findpeaks(sacf4peaks, 'NPeaks', 5, 'SortStr', 'descend');

    if isempty(pks)
        error('--> [SACF.m] ERROR: no pitch detected (1==isempty(pks)) !!!');
    end
    
    if (1e-3 < abs(sacf4peaks(1)-1.0))
        warning('--> [SACF.m] ERROR: the autocorrelation should have the maximum peak at n = 0 !!!');
        [pks, locs] = setdiff(pks, 1);
    else
        pks = pks(1:4);
        locs = locs(1:4);
    end
    
    % % findpeaks will not selecet the peak at n=0:
    %[pks, locs] = findpeaks(sacf4peaks,'MinPeakProminence',0.01*max(sacf4peaks), 'NPeaks', 4, 'SortStr', 'descend');
    %[peak_value, dummy] = max(pks);
    %peak_lag = locs(dummy);

    % the estimated pitch:
    %pitch_Hz = 1/(dt*peak_lag);   % [s]
    %salience = peak_value;  % peak_value < 1 (because of the normalization)

    pitch_Hz = 1./(dt*locs);   % [s]    
    salience = pks;
    
    %
    if exist('fig_num', 'var')
        fontsize = 30;
        
        figure(fig_num);

        ttt_ms = 1e3 * linspace(0, dt*length(sacf), length(sacf));

        plot( ttt_ms, sacf, 'linewidth', 2.5 );

        set(gca, 'fontsize', fontsize);

        grid on;

        xlabel('lags [ms]', 'interpreter', 'latex');
        ylabel('SACF', 'interpreter', 'latex');
        
        hold on
        plot(ttt_ms(peak_lag), peak_value, 'ro', 'linewidth', 3, 'markersize', 10)
        hold off

        title( sprintf('$\\hat{f}_{0,SACF} = %g Hz$', pitch_Hz), 'interpreter', 'latex' )    

        %axis([0, max_lag_ms, 0.8*min(sacf(1:max_lag)), 1.2*max(sacf(1:max_lag))]);
    end

end

















