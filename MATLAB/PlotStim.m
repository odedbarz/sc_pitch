%
% PlotStim.m
%

figure(21);
cla;

linewidth = 5;

plot(linspace(0, net.stim.Fs,length(stim2))/F02, abs(fft(stim2)),'linewidth',linewidth)
set(gca, 'fontsize', fontsize)

stim_hrm = nonzeros(F02.*[1:net.N_hrm].*harmonics2)';    % current stimulus' harmonics
stim_hrm = stim_hrm/F02;    % [Hz] ==> [kHz]

hold on
plot(repmat(stim_hrm,2,1), repmat([0 5]', 1, 4), '--k', 'linewidth', ceil(0.5*linewidth));
hold off

xlabel(sprintf('Normalized Frequency (by %g Hz)', F02));
title(sprintf('Complex Harmonics (%s)', mat2str(stim_hrm)));

axis([0, 16, 0, 2.5]);