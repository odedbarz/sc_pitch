%
% Perform_SC.m
%
% Build the descriptor vector h.
%
%
% * 12/08/2015: case #4 (RNN): Back to the previous version of PitchEst_v13_04 
%
  
   
function [h, ista] = Perform_SC(net, ista, anf, varargin)

% >>> debugging block:
if length(varargin) >= 1
    fff_dic = varargin{1};
end

if length(varargin) >= 2
    harmonics2 = varargin{2};
end

if length(varargin) >= 3
    F02 = varargin{3};
end



% >>> end of debugging block


D_dim2 = net.dic.dim2;

switch ista.method

    % (my) ISTA:
    case 1
        ok = true;
        while_counter = 0;

        while ok    % make sure that h is not all-zeros
            % Initialize the sparse code:
            h0 = InitSP( D_dim2, 1, ista.seed_level);

            % full scope SC:
            h = ISTA( ista.D , h0, anf(:), ista );      % apply SC

            while_counter = while_counter +1;
            
            %
            if 0==nnz(h) || 4 < while_counter
                ista.gamma_ratio = 0.5* ista.gamma_ratio;
                fprintf('-> ### ista.gamma_ratio: %g ###\n', ista.gamma_ratio);
            else
                ok = false;     % get out of the while loop
            end

        end

    % (MATLAB's) lasso:
    case 2
        % using MATLAB's lasso function:
        ok = true;
        while_counter = 0;

        while ok    % make sure that h is not all-zeros
            h = lasso( ista.D, anf(:),...
                'lambda', ista.gamma_ratio,...
                'Options', ista.UseParallel,...                
                'RelTol', ista.RelTol );     % apply SC:

            while_counter = while_counter +1;
            
            %
            if 0==nnz(h) || 10 < while_counter
                ista.gamma_ratio = 0.5* ista.gamma_ratio;
                fprintf('-> ### ista.gamma_ratio: %g ###\n', ista.gamma_ratio);
            else
                ok = false;     % get out of the while loop
            end

        end


        
        
    % (Foldiak's based) RNN:
    case 3 
        N_itr  = net.dic.time_smp - ista.win.t_smp +1;    % [smp] # of iterations
        
        % Layer 1 - Init.:
        yt      = zeros(D_dim2,1);  %abs( 0.01*rand(D_dim2,1) );  %zeros(D_dim2,1);
        %st      = anf(:);     % stimulus
        
        % Layer 2 - Init.:
        h       = zeros(D_dim2, 1);
        alpha   = ista.alpha;   
        theta   = zeros(D_dim2, 1);
        dt2tau  = ista.dt2tau;
        gamma   = ista.gamma;   % 10e-3;
        pbits   = -0.05;
        
        
        %
        for ti = 1:N_itr
            % Layer 1:
            Iw_lag = ista.I_win + net.cochlea.Nfb*(ti -1);   % the indexes of the location of the temporal window             
            %bt = Dt(:,Iw_lag)*st(Iw_lag);
            xs = anf(Iw_lag)';
            Qs = ista.Dt(:,Iw_lag);
            
            % Layer 1:
            %yt = yt + u_dt2tau*( bt - yt - DDI*at );
            yt = (1.0-dt2tau)*yt + dt2tau*ista.phi( Qs*xs +0*h - ista.W*yt - theta ); 
            
            y_ = medfilt1(yt,10);
            d_theta = gamma*(y_ - pbits);
            theta = theta + d_theta;
            

            % Layer 2:
            h = (1-alpha)*h + alpha*yt;
            
            % (debug) save states:
            %Ut(ti,:) = yt;
            
            % (debug) plot states:
            if (~isunix) && (length(varargin) >=3) && (~mod(ti,10)) %&& (1==ista.debug) && (~mod(ti,10))
                figure(21);
                subplot 211
                plot(fff_dic, [Qs*xs, ista.W*yt, yt, theta]);                           
                hold on;
                plot(fff_dic, h, 'm', 'linewidth', 2);
                plot( ones(2,1)*nonzeros(harmonics2.*(1:net.N_hrm)*F02)', [0, max(yt)], 'k--', 'linewidth', 2 );
                hold off;
                grid on;
                xlabel('Hz');
                legend('Qs*xs', 'W*yt', 'y_t', '\theta', 'h');
                
                subplot 212
                imagesc( (-0.5)* anf); colorbar;
                %colormap(gca, 'default')     % default                                
                hold on
                %contour( ti+(1:ista.win.t_smp)-1, (1:net.cochlea.Nfb)'-1, reshape(st(Iw_lag),net.cochlea.Nfb,ista.win.t_smp), 3, 'r');
                imagesc( ti+(1:ista.win.t_smp)-1, (1:net.cochlea.Nfb)'-1, reshape(xs,net.cochlea.Nfb,ista.win.t_smp));
                %colormap hot     % default
                hold off

                pause(0.01)
                drawnow;
            end
        end
        
        
        
    % (Rozell's based) RNN:
    case 4                       
        % Layer 1: Init. the state variable vector:
        ut = zeros(D_dim2,1);
        at = ista.theta(ut);
        st = anf(:);     % stimulus
        
        % Layer 2: Init. the state variable vector:
        h = zeros(D_dim2,1);
        alpha = ista.alpha;   
        
        %N0 = 3*floor(net.dic.Fs*ista.tau2);	% [smp] extra samples for convergence
        N_itr  = net.dic.time_smp - ista.win.t_smp +1;    % [smp] # of iterations

        Ut = zeros(N_itr,D_dim2);
        
        %
        for ti = 1:N_itr
            % Layer 1:
            Iw_lag = ista.I_win + net.cochlea.Nfb*(ti -1);   % the indexes of the location of the temporal window             
            bt = ista.Dt(:,Iw_lag) * st(Iw_lag);
            
            ut = ut + ista.dt2tau*( bt - ut - ista.DDI*at );
            at = ista.theta(ut);
        
            % Layer 2:
            h = (1-alpha)*h + alpha*ut;
            %h = (1-alpha)*h + alpha*at;
            
            % (debug) save states:
            if (~ista.debug)
                Ut(ti,:) = ut;
            end
            
            % (debug) plot states:
            if (1==ista.debug) && (~mod(ti,10))
                figure(21);
                subplot 211
                plot(fff_dic, [bt, -ista.DDI*at, ut]);                           
                hold on;
                plot(fff_dic, h, 'm', 'linewidth', 2);
                plot( ones(2,1)*nonzeros(harmonics2.*[1:net.N_hrm]*F02)', [0, max(bt)], 'k--', 'linewidth', 2 );
                hold off;
                grid on;
                xlabel('Hz');
                legend('b_t', '-(D^tD-I)*a_t', 'u_t', 'h');
                
                subplot 212
                imagesc( (-0.5)* anf); colorbar;
                %colormap(gca, 'default')     % default                                
                hold on
                %contour( ti+(1:ista.win.t_smp)-1, (1:net.cochlea.Nfb)'-1, reshape(st(Iw_lag),net.cochlea.Nfb,ista.win.t_smp), 3, 'r');
                imagesc( ti+(1:ista.win.t_smp)-1, (1:net.cochlea.Nfb)'-1, reshape(st(Iw_lag),net.cochlea.Nfb,ista.win.t_smp));
                %colormap hot     % default
                hold off

                pause(0.001)
                drawnow;
            end
        end
                       
        
        if (~ista.debug) && (~mod(ti,50)) && (~isunix)
            figure(31)
            imagesc(Ut); colorbar
            drawnow;
        end
        
    otherwise
        error('Error: wrong <ista.method> option !');

end


% make the  sparse coefficients all positive and remove local DC:
h = h-medfilt1(h,5);
h = max(0, h);


%
if 0==nnz(h)
   fprintf('\n--> [Perform_SC.m]: ** note ** h == 0 !!!\n');
end
