function [X_bias] = PlotBias( X, fff, fig_num )
%
% PlotBias( X, xxx )
%

if ~exist('fig_num', 'var')
    fig_num = 95;
end

fontsize = 20;

figure(fig_num);
set(gca, 'fontsize', fontsize)

X_bias = mean(X,2);

% low_thr = 0.8;
% X_bias( X_bias <= low_thr*max(X_bias) ) = 0;
% X_bias( X_bias > low_thr*max(X_bias) ) = 1;

plot(log10(fff), X_bias, '.-b', 'markersize', 20, 'linewidth', 2);
% plot(X_bias, '.-b', 'markersize', 20, 'linewidth', 2);
xlabel('Log(CF) [Hz]')
ylabel('mean()')
grid on


end

