% 
% SaveBin2Mat.m
% 

data_path   = [cd, '\Data\'];
deploy_path = [cd, '\Data\'];

dummy = load( [data_path, 'DB_st'] );
DB_st = dummy.DB_st;
clear dummy

% Construct the model:
obj = CM_new_C( DB_st.Fs );


% Read all bin files available in the directory:
file_list = dir( [data_path, '*.bin'] );
L_files = length( file_list );


for i = 1:L_files
    
    filename_i = file_list(i).name(1:end-4);
    fprintf( '\n%d. File:\t %s\n', i, filename_i )
    
    % Save if the file already exist. If so jump to the next file:
    if ~isempty( dir( [deploy_path, filename_i, '.mat'] ) )
        fprintf( '\t-> <%s> already exist!\n', filename_i )
        continue;
        
    end
    
    try
        % Read the binary file into the workspace:
        [ttt, M] = obj.Read_Data(...
            filename_i,...
            data_path,...
            DB_st.Fs,...                Fs
            DB_st.SECTIONS...
        );
        M = 0.01*M;     % [m/s] Velocity
        
    catch ME
        % Read the binary file into the workspace:
        [ttt, M] = obj.Read_Data(...
            filename_i,...
            data_path,...
            DB_st.Fs,...                Fs
            1 ...
        );
        M = 0.01*M;     % [m/s] Velocity
        
    end
    
    save( [deploy_path, filename_i, '.mat'], 'M', '-mat' )
    clear M
    
end

save( [deploy_path, 'ttt.mat'], 'ttt', '-mat' )

