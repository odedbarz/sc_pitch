% 
% Ploting_Spiral_Cochlea_and_Frequencies.m
% 
% 


figure(9)


L_x = 512;
% x = linspace(0, 3.5, L_x);   % [cm]

t = linspace( 5*pi, 0, L_x )';

a = @(z) z.^1.*cos(z);
b = @(z) -1*z.^1.*sin(z);

plot3( a(t), b(t), t );

% dO = 0.1;
% O = 1+[dO; -dO];
% X = t.*cos(t);
% X = ( O*X' )';
% Y = -1*t.*sin(t);
% Y = ( O*Y' )';
% patch( X, Y, 1 )

set(gca,'CameraPosition', [0 0 180])
axis square




hold on

    % Plot 8k [Hz]:
    Ix_t      = 34;
    t_        = t(Ix_t);
    plot3( a(t_), b(t_), t_, '.', 'MarkerSize', 10 )
    text( a(t_), b(t_), '8k [Hz]' )
    
    % Plot 4k [Hz]:
    Ix_t      = 99;
    t_        = t(Ix_t);
    plot3( a(t_), b(t_), t_, '.', 'MarkerSize', 10 )
    text( a(t_), b(t_), '4k [Hz]' )

    % Plot 1k [Hz]:
    Ix_t      = 199;
    t_        = t(Ix_t);
    plot3( a(t_), b(t_), t_, '.', 'MarkerSize', 10 )
    text( a(t_), b(t_), '1k [Hz]' )
    
    % Plot 0.5k [Hz]:
    Ix_t      = 245;
    t_        = t(Ix_t);
    plot3( a(t_), b(t_), t_, '.', 'MarkerSize', 10 )
    text( a(t_), b(t_), '0.5k [Hz]' )
    
    
hold off




