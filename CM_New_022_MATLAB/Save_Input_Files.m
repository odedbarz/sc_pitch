%
%
% Description:
%   Creates the input file + log file (txt).
%
%

function Save_Input_Files( CM_obj, gamma_, input )

% Save the <gamma> vector
CM_obj.gamma =  gamma_* ones(CM_obj.SECTIONS, 1);

% Write <gamma> to a binary file:
gamma_full_filename = CM_obj.Full_File_Name( CM_obj.gamma_filename, CM_obj.path.gamma );

fid_gamma = fopen( gamma_full_filename, 'wb');
if 0 == fid_gamma
    error('ERROR - Couldn''t open file for writing !!!')
end

% Write gamma to a binary file:
fwrite( fid_gamma, CM_obj.gamma, 'double' );
fclose( fid_gamma );



%% Write Input File:

% Write the input to a binary file:
input_full_filename = CM_obj.Full_File_Name( CM_obj.input_filename, CM_obj.path.input );
fid_input = fopen( input_full_filename, 'wb');
if 0 == fid_input
    error('ERROR - Couldn''t open file for writing !!!')
end
fwrite( fid_input, input, 'double' );
fclose( fid_input );









