% 
% result_plot_WKB_vs_Delta.m
% 
% Description:
%   Plots the resilts of the comparison between the WKB approximation and
%   the time simulation, using the C++ engine.
% 
% 
% Simulation Info - time domain (main_time_02.m):
%   model_type = 'CM_New_004';          % Time domain C++ engine
%   Fs                  = 50e3;         % [Hz] Sample rate
%   gamma               = 0.5;          % (1x1) OHC (constant) enhancement
%   run_time            = 0.03;         % [sec] Total excitation (input) time
%   pressure_input_SPL  = 0;            % (1x1) [dB SPL] Pressure gain
% 
%   The input vector was a simple delta with one nonzero tip at the
%       amplitude of pressure_input_cm> 
%
%   % -> delta:
%   input_st.input_type_str = 'delta';                % (str) The excitation type
%   input_st.delay_sec      = 0.01*run_time;         % (1x1) [sec] where does the delta starts
%   input_st.G_delta        = pressure_input_cm;      % (1x1) The delta's amplitude
% 
% Simulation Info - frequency domain (main_WKB_new_model_02.m)::
%   f_min       = 1;            % [Hz]
%   f_max       = 50e3;         % [Hz]
%   f_SECTIONS  = 1000;         % [Hz]
% 
% 

%% Choose a gamma to run with:
gamma               = 0.5;          % (1x1) OHC (constant) enhancement

% add_wkb_2_fig:
%   add_wkb_2_fig == 1: The WKB curves will be added to the figures.
%   add_wkb_2_fig == 0: The WKB curves will not be added to the figures.
add_wkb_2_fig = 1;

%% Load data:

% The path were to the data:
data_path = [cd, '\Results - WKB vs Delta\'];

% Load the <input> vector:
dummy = load( [data_path, 'input_delta.mat'], '-mat' );
input = dummy.input;
clear dummy

% Load the <M_sp_3> matrix (time domain simulation's result):
full_filename = sprintf( '%sM_sp_gamma(%g)_delta.mat', data_path, gamma );
dummy = load( full_filename, '-mat' );
M_sp_3 = dummy.M_sp_3;
clear dummy


%% Define pars, as was used in the simulations:
SECTIONS            = 512;
f_SECTIONS          = 1000;         % [Hz]

Fs                  = 50e3;         % [Hz] Sample rate
run_time            = 0.03;         % [sec] Total excitation (input) time
pressure_input_SPL  = 0;            % (1x1) [dB SPL] Pressure gain

% Construct the model:
obj = CM_new_C( Fs );

f_min               = 1;            % [Hz]
f_max            	= 50e3;         % [Hz]
f                   = linspace( f_max, f_min, f_SECTIONS );
w                   = 2*pi*f;
% w = obj.w_bm_cf;
% f = w/(2*pi);
% f_SECTIONS = length(f);

len                 = 3.5;                      % [cm]
dx                  = len/SECTIONS;
xxx                 = [ 0:SECTIONS-1 ]' * dx; 


%% Define the CM object & perform a WKB simulation:

% % Construct the model:
% obj = CM_new_C( Fs );

pressure_input_cm = 10* SPL_2_Pa( pressure_input_SPL );    % (1x1) [dB SPL] Pressure gain

% Solve the WKB model:
V_bm_3 = 0.01*obj.Solve_WKB(...
    pressure_input_cm,...
    gamma * ones(SECTIONS,1)...
);      % [m/s] Velocity:


%%
% -----------------------------------------
% Choose the location to "look" at along 
% the cochlea:
%     method == 0: (default) Calc the CF location using the BM
%                resonance frequencies, i.e. w_cf = sqrt(K_bm/M_bm).
%     method == 1: Use WKB to calc the CF along x.
%     method == 2: Use a given (preset & precalculated) data
%                file which contains a table of CF vs x.
%                The function will the interpulates for the
%                desired x.
% 
f_2_chk       = 1e3*[ 0.25, 0.5, 1, 2, 3, 4, 6, 8 ];                            % [Hz]
[Ix_x_d, x_d] = Freq_2_Ix( obj, f_2_chk, 2 );
% -----------------------------------------


%% Plot the results:

% transform into frequency domain: 
fM_sp = fft( M_sp_3', f_SECTIONS ).';

legend_str = cell(length(f_2_chk),1);
for i = 1:length(f_2_chk)
    legend_str{i} = sprintf( '%g [kHz]', 1e-3*f_2_chk(i) );
end

% Remove the bias at the beginning:
dummy = 20*log10(abs( fM_sp ));
bias_dB = max( dummy(Ix_x_d(1),1) );
        
figure(30)
plot( 1e-3*f, 20*log10(abs( fM_sp(Ix_x_d,:).' )) -bias_dB, 'LineWidth', 1.5 )

if 1 == add_wkb_2_fig        
    hold on
    plot( 1e-3*f, 20*log10(abs( V_bm_3(Ix_x_d,:).' )) -bias_dB, '-.', 'LineWidth', 1.5 )
    hold off
    title_str = sprintf( 'WKB vs Time Domain Simulations\nDash-dot lines are WKB (\\gamma = %g)', gamma );
else
    title_str = sprintf( 'Time Domain Simulations (\\gamma = %g)', gamma );
end

title( title_str )
legend( legend_str );
grid on
xlabel('f [kHz]')
ylabel( '|V_{bm}| [dB]' )

axis([ 0, 18, -100, 160 ])


%% 

Ix_f = zeros(length(f_2_chk),1);

% Find the indexes of the desired frequencies on the given <f> vector:
for i = 1:length(f_2_chk)
    Ix_f(i) = find( f_2_chk(i) >= f, 1, 'first' );
    
end

dummy = 20*log10(abs( fM_sp ));
bias_dB = -max(abs(max( dummy(:,Ix_f,1) )));

figure(31)
plot( xxx, 20*log10(abs( fM_sp(:,f_SECTIONS-Ix_f) )) - bias_dB, 'LineWidth', 1.5 )

if 1 == add_wkb_2_fig
    hold on
    plot( xxx, 20*log10(abs( V_bm_3(:,Ix_f) )) - bias_dB, '-.', 'LineWidth', 1.5 )
    hold off
    title_str = sprintf( 'WKB vs Time Domain Simulations\nDash-dot lines are WKB (\\gamma = %g)', gamma );

else
    title_str = sprintf( 'Time Domain Simulations (\\gamma = %g)', gamma );

end
    title( title_str );

legend( legend_str );
grid on
xlabel('x [cm]')
ylabel( '|V_{bm}| [dB]' )

axis([0, 3.5, -150, 80])








