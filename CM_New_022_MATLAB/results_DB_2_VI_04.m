% 
% 
% results_DB_2_VI_01.m
%  
% Description:
%   Reads the database and create a velosity-intensity graphas
% 
% Notes:
%   * In this script I assume that the bin files were already replaced to the
%     mat files.
%   * The data in the *.mat files already contains the normalization
%     [cm]->[m] ( multiplied by *0.01 ).
%
%

clc

%% Desired set to load from the bin files:
gamma       = 3* 0.5;

% Choose the simulated type you which to load, e.g. BM_sp, BM_disp, TM_sp
% etc:
data_type = 'BM_sp';

% The first figure in the sequence: 
fig_num = 20

%
mu_scale = 1e6;

% The fisrt frequency to count from while plotting:
start_freq = 1;     % 5 or 1

% debug mode for the maximum absolute sinus calculation. <debug_mode> is
% also the number of the figure to plot in. To cancel the <debug_mode> just set it to be 
%   <debug_mode> = 0.
debug_mode = 0;    


%%
% Path to the database files. Each gamma should has its own directory:
%data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_model_13\' ]; 
% data_path 	= [cd, '\Data\' ]; 
data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')\' ]; 


% Load the database DB_stucture file:
dummy       = load( [data_path, 'DB_st.mat'], '-mat' );
DB_st       = dummy.DB_st;


%%

% CharacteriDB_stic frequency to look at in the max_disp plot:
DB_st.CF_2_chk     = 4e3;     % [Hz]
% ------------------------------------------------------------

Ix_cf_in_vec = find( DB_st.CF_2_chk == fix(DB_st.f_sin_v), 1 );
if isempty( Ix_cf_in_vec )
    error(sprintf( 'ERROR: DB_st.CF_2_chk ~= DB_st.f_sin_v\n\tPlease Choose a CF frequency from the available CF vector <DB_st.f_sin_v>' ))
end

% ConDB_struct the model:
obj = CM_new_C( DB_st.Fs );

% Find the location (x index) of the frequency along the cochlea:
Ix_loc_on_cochlea = Freq_2_Ix( obj, DB_st.CF_2_chk, 2, 0 );

% Initialize more parameters:
N_spl	= length( DB_st.pressure_input_SPL_v );
N_freq	= length( DB_st.f_sin_v );


%% Create a DB_structure for the loaded data:
DB_st.Ix_f_v = Freq_2_Ix( obj, DB_st.f_sin_v, 2, 0 );
DB_st.VI  	 = zeros(N_spl, N_freq);

for i = 1:N_spl         % Run on the intensities
    
    fprintf( '\n%d. P_in = %g [dB SPL] (%d out of %d)\n', i, DB_st.pressure_input_SPL_v(i), i, N_spl );
    fprintf( '-------------------------------------\n' );
    
    for j = start_freq:N_freq %[6, 10, 13, 17]    % Run on the excitation frequencies

        fprintf( '\t-> Loading j = #%d out of #%d\n', j, N_freq );
                
        % Load BM disp:
        [bm_sp_ij, warn_msg] = Load_DB_File(...
            data_type,...
            gamma,...
            DB_st.pressure_input_SPL_v(i),...
            DB_st.f_sin_v(j),...
            DB_st.run_time,...
            ...DB_st.Fs,...
            data_path...
            );
        if isempty( BM_disp_ij )
            warning( warn_msg );
            continue;
        end
                   
        % Absolute value of the BM_disp at the desired frequency:
        DB_st.VI(i,j) = Calc_Amp_of_Sin(...
            bm_sp_ij,...
            Ix_loc_on_cochlea,...
            DB_st.f_sin_v(j),...
            DB_st.Fs,...
            debug_mode ...
            );  
%         DB_st.VI(i,j) = max(abs(bm_sp_ij(Ix_loc_on_cochlea,:)));

        if 0 ~= debug_mode
            debug_mode = debug_mode +1;
        end
                
        clear bm_sp_ij
        
    end
    
end


%% Text Properties:
font_size   = 12;
font_weight = 'bold';


%% Plot Results:

figure( fig_num )
% loglog(  DB_st.f_sin_v, 1e6*DB_st.VI', '.-' );
plot_1 = semilogx(  DB_st.f_sin_v, 20*log10(mu_scale*DB_st.VI'), '.-' );
% plot_1 = loglog( DB_st.f_sin_v, (mu_scale* DB_st.VI'), '.-' );

% Set the axes font properties:
set( gca,...
    'FontSize',     font_size,...
    'FontWeight',   font_weight ...
);

xlabel( 'f [Hz]',...
    'FontSize',     14,...%font_size, ...
    'FontWeight',   font_weight ...
)
ylabel( 'Velosity [\mum/s]',...
    'FontSize',     14,...%font_size,...
    'FontWeight',   font_weight ...
)

title_str = sprintf(...
    'Cochlea Time Model Simulation\nx = %2.2g [cm] ( f = %g [kHz] ), \\gamma = %g',...
    obj.x(Ix_loc_on_cochlea),...
    1e-3* DB_st.CF_2_chk,...
    gamma...
);
title( title_str,...
    'FontSize',     font_size,...
    'FontWeight',   font_weight ...
)

% set all lines and legend (graphic) properties:
legend_freq_DB_str = cell(1,N_spl);
all_marker_hnd = set( plot_1(1), 'marker' );

for i = 1:N_spl
    % Legend:
    legend_freq_DB_str{i} = sprintf( '%g [dB SPL]',DB_st.pressure_input_SPL_v(i) );
        
    % Lines:
    set( plot_1(i),...
        'Marker',           all_marker_hnd{i},...
        'MarkerSize',       10, ...
        'LineWidth',        2.5, ...
        'MarkerFaceColor',  get( plot_1(i), 'Color' )...
        );
    
end
legend( legend_freq_DB_str,...
    'Location',     'SouthWest',...
    'FontSize',     font_size,...
    'FontWeight',   font_weight ...
)

% axis([ DB_st.f_sin_v(1), DB_st.f_sin_v(end), 1e-5, 1e5 ]);
grid on

%%
font_size   = 14;
font_weight = 'bold';

lineWidth_prime = 2.5;
lineWidth       = 2;

markerSize_prime = 10;
markerSize       = 8;

%%
    
figure( fig_num+1 )
% semilogy(  DB_st.pressure_input_SPL_v, 1e6*DB_st.VI(:, Ix_cf_in_vec), 's-', 'linewidth', 2 );
% hold on
% semilogy(  DB_st.pressure_input_SPL_v, 1e6*DB_st.VI(:, 1:(Ix_cf_in_vec-1)), '.-' );
plot_2_prime = semilogy(  DB_st.pressure_input_SPL_v, mu_scale* DB_st.VI(:, Ix_cf_in_vec) );
hold on
plot_2 = semilogy(  DB_st.pressure_input_SPL_v, mu_scale* DB_st.VI(:, start_freq:(Ix_cf_in_vec-1)), '.-' );

% Add a logarithmic line:
semilogy(...
    [DB_st.pressure_input_SPL_v(1), DB_st.pressure_input_SPL_v(end)],...
    ( [10^0, 10^(N_spl-1)]*10^2 ),...
    'k--',...
    'LineWidth', lineWidth_prime ...
);

hold off

xlabel( 'SPL [dB]',...
    'FontSize',     14,...%font_size, ...
    'FontWeight',   font_weight ...
)
ylabel( 'BM Velosity [\mum/s]',...
    'FontSize',     14,...%font_size,...
    'FontWeight',   font_weight ...
)

title( 'Velocity-Intensity Functions of BM Responses to Tunes ',...
    'FontSize',     font_size,...
    'FontWeight',   font_weight ...
)

% The 4Hz line:
set( plot_2_prime,...
    'Marker',           's',...
    'MarkerSize',       markerSize_prime, ...
    'LineWidth',        lineWidth_prime, ...
    'MarkerFaceColor',  get( plot_2_prime, 'Color' )...
    );

% Set the axes font properties:
set( gca,...
    'FontSize',     font_size,...
    'FontWeight',   font_weight ...
);

legend_freq_DB_str = cell(1,Ix_cf_in_vec-start_freq);
legend_freq_DB_str{1} = sprintf( '%g [kHz]', 1e-3*DB_st.f_sin_v(Ix_cf_in_vec) );
for i = 1:(Ix_cf_in_vec-start_freq)
    
    legend_freq_DB_str{i+1} = sprintf( '%g [kHz]', 1e-3*DB_st.f_sin_v(i+start_freq-1) );
    
    % Lines:
    set( plot_2(i),...
        'Marker',           all_marker_hnd{i},...
        'MarkerSize',       markerSize, ...
        'LineWidth',        lineWidth ...
         );
    
    
end
legend_freq_DB_str{end+1} = 'Linear Growth';
legend( legend_freq_DB_str, 'Location', 'SouthEast' )
grid on


%% ----
figure( fig_num+2 )
plot_3_prime = semilogy(  DB_st.pressure_input_SPL_v, ( mu_scale* DB_st.VI(:, Ix_cf_in_vec) ) );
hold on
plot_3 = semilogy(  DB_st.pressure_input_SPL_v, ( mu_scale* DB_st.VI(:, Ix_cf_in_vec+1:end) ), '.-' );

% Add a logarithmic line:
semilogy(...
    [DB_st.pressure_input_SPL_v(1), DB_st.pressure_input_SPL_v(end)],...
    ( 10.^([0, N_spl-1] + 2) ),... ( [10^0, 10^(N_spl-1)]*10^2 ),...
    'k--',...
    'LineWidth', lineWidth_prime ...
);

hold off

xlabel( 'SPL [dB]',...
    'FontSize',     14,...%font_size, ...
    'FontWeight',   font_weight ...
)
ylabel( 'BM Velosity [\mum/s]',...
    'FontSize',     14,...%font_size,...
    'FontWeight',   font_weight ...
)

title( 'Velocity-Intensity Functions of BM Responses to Tunes ',...
    'FontSize',     font_size,...
    'FontWeight',   font_weight ...
)

% The 4Hz line:
set( plot_3_prime,...
    'Marker',           's',...
    'MarkerSize',       markerSize_prime, ...
    'LineWidth',        lineWidth_prime, ...
    'MarkerFaceColor',  get( plot_3_prime, 'Color' )...
    );

% Set the axes font properties:
set( gca,...
    'FontSize',     font_size,...
    'FontWeight',   font_weight ...
);

legend_freq_DB_str = cell(1,N_freq-Ix_cf_in_vec);
legend_freq_DB_str{1} = sprintf( '%g [kHz]', 1e-3*DB_st.f_sin_v(Ix_cf_in_vec) );
for i = (Ix_cf_in_vec+1):N_freq
    
    legend_freq_DB_str{i-Ix_cf_in_vec+1} = sprintf( '%g [kHz]', 1e-3*DB_st.f_sin_v(i) );
    
    % Lines:
    set( plot_3(i-Ix_cf_in_vec),...
        'Marker',           all_marker_hnd{mod(i-Ix_cf_in_vec,14)+1},...
        'MarkerSize',       markerSize, ...
        'LineWidth',        lineWidth ...
         );
    
end
legend_freq_DB_str{end+1} = 'Linear Growth';
legend( legend_freq_DB_str, 'Location', 'SouthEast' )
grid on












