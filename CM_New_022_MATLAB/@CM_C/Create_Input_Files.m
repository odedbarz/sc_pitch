%
%
% Description:
%   Creates the input file + log file (txt).
%
%

function input = Create_Input_Files( obj, input_st, run_time, gamma )

clear input

N_tot = floor(run_time * obj.Fs);      % [smp] Total samples available.

%% Creates the <gamma.bin> file:
% OHC density - Create the <gamma.bin> file:
if 1 == length( gamma )  % gamma is a given as a scalar
    gamma = gamma * ones(obj.SECTIONS, 1);            	
end

% Write <gamma> to a binary file:
gamma_full_filename = obj.Full_File_Name( obj.gamma_filename, obj.path.gamma );

fid_gamma = fopen( gamma_full_filename, 'wb');
if 0 == fid_gamma
    error('ERROR - Couldn''t open file for writing !!!')
end

% Write gamma to a binary file:
fwrite( fid_gamma, gamma, 'double' );
fclose( fid_gamma );


%%
t = [ 0:N_tot-1 ]/obj.Fs; 	% [sec] Time interval vector.

fprintf( '\n' )
fprintf( 'Create_Log_Input_S.m\n' )
fprintf( '--------------------------------------------------------\n' )

% -----------------------------------
log_str = sprintf( 'Cochlea Model - MATLAB:\n' );

% log_str = sprintf( '%s\nFlags:\n', log_str );
% log_str = sprintf( '%s\t-> psi_flag = %d\n', log_str, obj.psi_flag );           % Nonlinearity damping
% log_str = sprintf( '%s\t-> ow_flag = %d\n', log_str, obj.ow_flag );             % OW
% log_str = sprintf( '%s\t-> DL_flag = %d\n', log_str, obj.DL_flag );             % OW
% log_str = sprintf( '%s\t-> alpha_r = %d\n', log_str, obj.alpha_r );             % Nonlinearity damping

log_str = sprintf( '%s\nParameters:\n', log_str );
log_str = sprintf( '%s\t-> x SECTIONS = %d\n', log_str, obj.SECTIONS );	% Number of Cochlea partititions.
log_str = sprintf( '%s\t-> Fs = %d [kHz]\n', log_str, obj.Fs/1000 );            	% [Hz] Fs.
log_str = sprintf( '%s\t-> run_time = %g [sec]\n', log_str, run_time );	% [sec] total input time.
log_str = sprintf( '%s\t-> N_tot = %d [smp] ( total number of input samples )\n', log_str, N_tot );                % [sec] Sampling rate.
log_str = sprintf( '%s\t-> gamma(1) = %g\n', log_str, gamma(1) );       	% Sinus amplitude.

log_str = sprintf( '%s\nInput Parameters:\n', log_str );                    % Input pars
log_str = sprintf( '%s\t-> type = %s\n', log_str, input_st.input_type_str );       	% Input type        


% Add input data:
switch input_st.input_type_str
    
    case 'sin( 2*pi*f0 )'
        log_str = sprintf( '%s\t-> f0   = %g [kHz]\n', log_str, input_st.f_sin/1e3 );	% Sinus frequency.        
        log_str = sprintf( '%s\t-> Gain = %g [dB SPL] (%g [Pa])\n',...
            log_str,...
            Pa_2_SPL( 0.1*input_st.G_sin ),...    % [gr*cm/s^2/cm^2] -> [Pa]
            0.1*input_st.G_sin...
            ); 
        
        % Create the sinus:
        input = input_st.G_sin * sin( 2*pi*input_st.f_sin*t );
        
        % Enable windowing of the signal (+padding):
        if 1 == input_st.win
            N_pad = 5;      % [smp] number of samples to pad (one side)
            R_win = 0.04;   % tukeywin ratio
            W = zeros( 1, length(input) );
            W(N_pad:end-N_pad-1) = tukeywin( length(input)-2*N_pad, R_win )';
            input = W.*input;
        end
        log_str = sprintf( '%s\t-> win = %d\n', log_str, input_st.win  );

        
    case 'sigma(sin)'
%         log_str = sprintf( '%s\t-> f0   = %g [kHz]\n', log_str, input_st.f_sin/1e3 );	% Sinus frequency.        
        log_str = sprintf( '%s\t-> Gain = %g [dB SPL] (%g [Pa])\n',...
            log_str,...
            Pa_2_SPL( 0.1*input_st.G_sin ),...    % [gr*cm/s^2/cm^2] -> [Pa]
            0.1*input_st.G_sin...
            ); 
        
        % Create the sinus combination:
        input = input_st.G_sin * sin( 2*pi*input_st.f_sin*t );
        input = sum( input );
        
        % Enable windowing of the signal (+padding):
        if 1 == input_st.win
            N_pad = 5;      % [smp] number of samples to pad (one side)
            R_win = 0.04;   % tukeywin ratio
            W = zeros( 1, length(input) );
            W(N_pad:end-N_pad-1) = tukeywin( length(input)-2*N_pad, R_win )';
            input = W.*input;
        end
        log_str = sprintf( '%s\t-> win = %d\n', log_str, input_st.win  );
        

    case 'delta'
        if input_st.delay_sec >= run_time
            error('ERROR - input_st.delay_sec must not exceed the total run time !!!')
        end
        log_str = sprintf( '%s\t-> delay = %g [sec]\n', log_str, input_st.delay_sec );	        
        log_str = sprintf( '%s\t-> Gain = %g [dB SPL] (%g [Pa])\n',...
            log_str,...
            Pa_2_SPL( 0.1*input_st.G_delta ),...    % [gr*cm/s^2/cm^2] -> [Pa]
            0.1*input_st.G_delta...
            ); 
        
        % Create the delta:
        delay_smp = floor(input_st.delay_sec * obj.Fs);         % [smp] The delta's delay.
        input = input_st.G_delta * [1:N_tot == delay_smp];

        
    case 'step'
        if input_st.delay_sec >= run_time
            error('ERROR - input_st.delay_sec must not exceed the total run time !!!')
        end
        log_str = sprintf( '%s\t-> delay = %g [sec]\n', log_str, input_st.delay_sec );	
        log_str = sprintf( '%s\t-> Gain = %g [dB SPL] (%g [Pa])\n',...
            log_str,...
            Pa_2_SPL( 0.1*input_st.G_step ),...    % [gr*cm/s^2/cm^2] -> [Pa]
            0.1*input_st.G_step...
            ); 
        
        % Create the step:
        delay_smp = floor(input_st.delay_sec * obj.Fs);         % [smp] The delta's delay.

        if 0 ~= input_st.tukey_r   
            %input = input_st.G_step * ([1:N_tot <= N_tot-delay_smp+1] .* [1:N_tot >= delay_smp]);
            Ix_tukey = [1:N_tot <= N_tot-delay_smp+1] & [1:N_tot >= delay_smp];
            W = tukeywin( sum(Ix_tukey), input_st.tukey_r );
            input = zeros(1,N_tot);
            input(Ix_tukey) = input_st.G_step * W;
            
        else    % A simple step function
            input = input_st.G_step * ([1:N_tot <= N_tot-delay_smp+1] .* [1:N_tot >= delay_smp]);
            
        end
        
    otherwise
        error( sprintf('ERROR - Create_Input_Files():\n\tThe <input_type_str> is invalid!') )
        
end


% Write the input to a binary file:
input_full_filename = obj.Full_File_Name( obj.input_filename, obj.path.input );
fid_input = fopen( input_full_filename, 'wb');
if 0 == fid_input
    error('ERROR - Couldn''t open file for writing !!!')
end
fwrite( fid_input, input, 'double' );
fclose( fid_input );


 % Show all input data on screan (command line):
disp( log_str )        

fprintf( '--------------------------------------------------------\n' )

% Write all input data to a log file:
log_input_filename = obj.Full_File_Name( obj.log_input_filename, obj.path.input, 'txt' );

fid_log = fopen( log_input_filename, 'w' );
if 0 == fid_log
    error( 'ERROR - Couldn''t open log file !' )
end
fprintf( fid_log, log_str );
fclose( fid_log );

if ~exist( 'input', 'var' )
    error( 'ERROR - You Didn''t create the <input> vector !!!' )
end


obj.debug.input = input;







