% 
% 
% Set_IO_Files_S - Set I/O files script.
% 
% 

%% Input File:

input_full_filename = obj.Full_File_Name( obj.input_filename, obj.path.input );

% Check that the INPUT file exists:
if isempty(  dir( input_full_filename ) )
    error('ERROR - CM_BM_C->CM_BM_C: The given filename <%s> couldn''t be found !!!', input_full_filename )
end

% Open for reading the given binary file:
obj.input_file_hnd = obj.Open_4_Reading( obj.input_filename, obj.path.input );

% Get the total length of the input:
obj.Total_Input_Time;       % -> <ran_time>

% Open Output Files:
obj.Open_Output_Files;

%% Get the gamma:
obj.Read_Gamma_File;



















