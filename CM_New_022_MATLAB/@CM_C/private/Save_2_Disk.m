% 
% 
% function save2disk = Save_2_Disk_Struct
% 
% Input:
%     Flags to save:
%         1. bm_disp_flag
%         2. bm_sp_flag
%     
% Description:
%     Creates a structure which holds flags of "what to save".
% 
% 

function save2disk = Save_2_Disk( bm_disp_flag, bm_sp_flag )

% BM Displacement:
save2disk.bm_disp.flag      = bm_disp_flag;
save2disk.bm_disp.filename	= 'bm_disp_out';  	% (string) The output's file name.
save2disk.bm_disp.hnd    	= [];    

% BM Speed:
save2disk.bm_sp.flag        = bm_sp_flag;
save2disk.bm_sp.filename    = 'bm_sp_out';  	% (string) The output's file name.
save2disk.bm_sp.hnd         = [];    










