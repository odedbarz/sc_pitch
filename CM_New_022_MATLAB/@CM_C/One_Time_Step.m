% 
% 
% function obj = One_Time_Step( obj, step_type )
%
% Description:
%   Implement one step for the current state ( 1 time step ).
%   step_type can be 'euler' or 'trapez'.
%
%

function obj = One_Time_Step( obj, step_type )

if obj.IsNan( obj.prev.bm_disp ) || obj.IsNan( obj.prev.bm_sp ) || obj.IsNan( obj.prev.bm_acc )
    error( 'ERROR - <obj.now.bm_disp> OR <obj.now.bm_sp> OR <obj.now.bm_acc> has NaNs in it !!!\n' )
end
if obj.IsNan( obj.now.bm_disp ) || obj.IsNan( obj.now.bm_sp ) || obj.IsNan( obj.now.bm_acc )
    error( 'ERROR - <obj.now.bm_disp> OR <obj.now.bm_sp> OR <obj.now.bm_acc> has NaNs in it !!!\n' )
end


if strcmpi( obj.ODE_EULER, step_type )

    % BM displacement & speed:
    obj.now.bm_disp     = obj.Euler( obj.prev.bm_disp,  obj.prev.bm_sp );
    obj.now.bm_sp		= obj.Euler( obj.prev.bm_sp,    obj.prev.bm_acc );
    
    % OW - Oval Window:
    if true == obj.ow_flag
        obj.now.ow_disp	= obj.Euler( obj.prev.ow_disp,	obj.prev.ow_sp );
        obj.now.ow_sp 	= obj.Euler( obj.prev.ow_sp, 	obj.prev.ow_acc );

%     else
%         %obj.now.ow_disp	= obj.Calc_OW_Disp;   % -> obj.now.ow_disp       
%         obj.now.ow_disp	= 0;
%         obj.now.ow_sp 	= 0;
    end     
    
    % OHC:
    if isequal( obj.gamma, zeros( obj.SECTIONS, 1) )
        obj.now.p_ohc       = 0*obj.now.p_ohc;   % gamma == 0
        obj.now.psi_ohc     = 0*obj.now.psi_ohc;   % gamma == 0
        
    elseif false == obj.psi_flag
        obj.now.p_ohc       = obj.Euler( obj.prev.p_ohc,    obj.prev.d_p_ohc );
        
    else    % true == obj.psi_flag
        obj.now.psi_ohc     = obj.Euler( obj.prev.psi_ohc,  obj.prev.d_psi_ohc );
        obj.Calc_P_OHC;
        
    end
    
else    % For the Trapezoidal Case strcmpi( obj.ODE_TRAPEZ, ode_type )

    % BM displacement & speed
    obj.now.bm_disp     = obj.Trapez( obj.prev.bm_disp, obj.prev.bm_sp,     obj.now.bm_sp );
    obj.now.bm_sp		= obj.Trapez( obj.prev.bm_sp,   obj.prev.bm_acc,	obj.now.bm_acc );
        
    % OW - Oval Window
    if true == obj.ow_flag    
        obj.now.ow_disp	= obj.Trapez( obj.prev.ow_disp,	obj.prev.ow_sp,     obj.now.ow_sp );
        obj.now.ow_sp 	= obj.Trapez( obj.prev.ow_sp, 	obj.prev.ow_acc,    obj.now.ow_acc );
        
%     else
%         %obj.now.ow_disp	= obj.Calc_OW_Disp;   % -> obj.now.ow_disp      
%         obj.now.ow_disp	= 0;
%         obj.now.ow_sp 	= 0;
% 
    end
    
    % OHC:
    if isequal( obj.gamma, zeros( obj.SECTIONS, 1) )
        obj.now.p_ohc       = 0*obj.now.p_ohc;   % gamma == 0
        obj.now.psi_ohc     = 0*obj.now.psi_ohc;   % gamma == 0

    elseif false == obj.psi_flag
        obj.now.p_ohc       = obj.Trapez( obj.prev.p_ohc,   obj.prev.d_p_ohc,   obj.now.d_p_ohc );
        
    else    % true == obj.psi_flag
        obj.now.psi_ohc     = obj.Trapez( obj.prev.psi_ohc, obj.prev.d_psi_ohc, obj.now.d_psi_ohc );
        obj.Calc_P_OHC;
        
    end
    
end
      
% Calc Y for U*P = Y ( Y(x,t) == G(x,t), for x != 0 ).
obj.Calc_Y;                                 % --> G(x,t)

% Solve for P(x,t)
obj.Solve_TriMat;                           % --> P(x,t) ( U * P = Y )

% BM acceleration:
obj.Calc_BM_Acc;

% OW acceleration:
% if true == obj.ow_flag
    obj.Calc_OW_Acc;
% end


%
if isequal( obj.gamma, zeros( obj.SECTIONS, 1) )
    obj.now.d_p_ohc     = 0*obj.now.d_p_ohc;   % gamma == 0
    obj.now.d_psi_ohc   = 0*obj.now.d_psi_ohc;   % gamma == 0

elseif false == obj.psi_flag
    obj.Calc_d_P_OHC;                       % @P(x, t)/@t
    
else    % false == obj.psi_flag
    obj.Calc_OHC_Psi_Deriv;                 % @P_ohc(x, t)/@t

end

if obj.IsNan( obj.now.bm_disp ) || obj.IsNan( obj.now.bm_sp ) || obj.IsNan( obj.now.bm_acc )
    error( 'ERROR - <obj.now.bm_disp> OR <obj.now.bm_sp> OR <obj.now.bm_acc> has NaNs in it !!!\n' )
end















