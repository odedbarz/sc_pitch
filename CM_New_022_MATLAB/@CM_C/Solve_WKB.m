% 
% 
% [V_bm, P] = Solve_WKB( obj, P_in )
% 
% Description:
%     Approximate solution of the Cochlea model.
%     
% Notes - Output:
%   All matrixs & vectors are of the form:
%       1. MATRIXES: LENGTH (x) for columns, FREQUENCIES (w) for rows.
%       2. VECTORS - FREQUENCY as ROW vectors.
%       3. VECTORS - LENGTH as COLUMN vectors.
%     
%


function [V_bm, P, K, Z_oc] = Solve_WKB( obj, P_in, gamma ) %

obj.Z_oc    = zeros( obj.SECTIONS, obj.f_SECTIONS );      % Impedance of the organ of corti
obj.K       = zeros( obj.SECTIONS, obj.f_SECTIONS );      % Wave number
obj.P       = zeros( obj.SECTIONS, obj.f_SECTIONS );      % Pressure


for Ix_w = 1:obj.f_SECTIONS      % Run over all frequencies    
    
    % Current angular frequency:
    w_i = obj.w(Ix_w);
        
    % Model's impedance:
    F_bm  = F_OSC2( obj.M_bm, obj.R_bm, obj.S_bm, [], w_i );
    F_psi = -1.0*F_PSI( obj.w_ohc, obj.alpha_2, obj.alpha_1, [], w_i );
    F_oc  = F_bm + gamma.*F_psi;
        
    % OC impedance:   
    obj.Z_oc(:, Ix_w) = F_oc ./ (1j*w_i);
    
    % Wave number:
    obj.K(:, Ix_w) = sqrt( -1j*w_i*2*obj.rho*obj.beta ./ ( obj.area.*obj.Z_oc(:, Ix_w) ) );     
    K0 = obj.K(1, Ix_w);

    %
    e_plus = exp( 1j* obj.dx * cumtrapz( obj.K(:, Ix_w) ) );
    %e_plus = exp( 1j* dx * cumsum( K(:, Ix_w) ) );
    e_minus = 1./e_plus;
    C_w = e_minus(end).^2;
    
    % % B.C. (simple)
    %A2 = 2*w_i.^2*obj.rho*P_in ./ ( sqrt(obj.K(1, Ix_w)).*( 1 + C_w ) );
    
    % % B.C. (without the OW)
    %A2 = 2*w_i.^2*obj.rho.*sqrt(obj.K(1, Ix_w)).*P_in ./...
    %    ( -2*w_i.^2*obj.rho.*( 1 - C_w ) + obj.K(1, Ix_w).*( 1 + C_w ).*( obj.sigma_ow*obj.w_ow^2 + obj.G_me.*obj.C_me ) );
    
%     % B.C. - without xi_ow_disp & xi_ow_sp:
%     D_w = -w_i^2;
%     A2 = (obj.G_me*2*w_i^2*obj.rho) * sqrt(K0) * P_in/...
%        ( 1j*obj.sigma_ow*D_w*(1+C_w)*K0 - 2*obj.rho*w_i^2*(1-C_w) );
    
    % B.C. with OW:
    D_w = -w_i^2 + 1j*obj.gamma_ow + obj.a1/obj.sigma_ow;
    A2 = (obj.G_me*2*w_i^2*obj.rho) * sqrt(K0) * P_in/...
        ( 1j*obj.sigma_ow*D_w*(1+C_w)*K0 - 2*obj.rho*w_i^2*(1-C_w) );

    
    %
    obj.P(:, Ix_w) = A2./sqrt(obj.K(:, Ix_w)).*( e_minus - C_w.*e_plus );   % [Pa]
    
end


% Calc the BM speed:
obj.V_bm = obj.P ./ obj.Z_oc;

if 1 <= nargout
    V_bm = obj.V_bm;
    P = obj.P;
    Z_oc = obj.Z_oc;
    K = obj.K;
end


































