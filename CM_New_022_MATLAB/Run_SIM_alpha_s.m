% -------------------------------------------------------------------------
%
% Written by Barzelay Oded - 13/03/2012
% 
% -------------------------------------------------------------------------
%
% Run_SIM_alpha_s.m
%
%   A simple script which runs the (MATLAB) model for different <alpha_s> &
%   frequencies.
% 
% 


% clear classes
clc
% close all


% Model Type:
%   1. 'WKB'               	- The WKB approximation. 
%   2. 'CM_Time'            - The non-linear Cochlea project.
%   3. 'CM_Time_cpp'        - The non-linear Cochlea cpp project.
%   4. 'CM_Yaniv_Linear'  	- The linear Cochlea project, writen by Yaniv
%                             Halmut (June 2004).

SECTIONS = 512;

% model_type = 'WKB';                       % MATLAB
% model_type = 'WKB_old';                   % MATLAB
model_type = 'CM_Time';                     % MATLAB

%
Fs                  = 50e3;        % [Hz] Sample rate
gamma               = 1;          % (1x1) OHC (constant) enhancement
run_time            = 0.005;         % [sec] Total excitation (input) time
pressure_input_SPL  = 80;           % (1x1) [dB SPL] Pressure gain


%% DEBUG (only for the non-linear model):            
if strcmpi( 'CM_Time', model_type )
    debug_mode.enable.all                   = 1;              
    debug_mode.enable.txt_2_cmd             = 0;  
    debug_mode.enable.plot_real_time        = 1;  
    debug_mode.enable.enable_release_txt  	= 1;
else
    debug_mode = [];
end


%% Create the Model's object:

fprintf( '--------------------------------------------------------\n' )
fprintf( 'Model Type = < %s >\n', model_type )
fprintf( '--------------------------------------------------------\n' )

% Construct the model:
obj = CM_new_C( Fs, debug_mode );
if strcmpi( 'CM_Time', model_type )
    log_str = '';
    log_str = sprintf( '%s\nFlags:\n', log_str );
    log_str = sprintf( '%s\t-> ow_flag = %d\n', log_str, obj.ow_flag );             % OW
    log_str = sprintf( '%s\t-> DL_flag = %d\n', log_str, obj.DL_flag );             % OW
    log_str = sprintf( '%s\t-> alpha_r = %d\n', log_str, obj.alpha_r );             % Nonlinearity damping
    fprintf( '%s', log_str )
end


%% Calc Relevant Pars:
pressure_input_Pa = SPL_2_Pa( pressure_input_SPL );    % (1x1) [dB SPL] Pressure gain
pressure_input_cm = 10* pressure_input_Pa;  % 1[Pa] -> 10 [(gr*cm/s^2)/cm^2], 1[Pa] = 1[N/m^2] = 10[(gr*cm/s^2)/cm^2]
gamma_v = gamma.* ones(SECTIONS, 1);                 % OHC density
N_tot = run_time * obj.Fs;                            % [smp] Total samples available.

% The input structure:
%   -> sinus:
input_st.input_type_str = 'sin( 2*pi*f0 )';     % (str) The excitation type
input_st.f_sin          = [];                  % [Hz] Sinus frequency (excitation)
input_st.G_sin          = pressure_input_cm; 	% [Pa] Sinus amplitude (gain)
input_st.win            = 1;                    % Enable\Disable windowing + padding of the sinus
input_st.delay_start_sec = 0.00 %0.0*run_time;      	% [sec] Sinus frequency (excitation)
input_st.delay_end_sec   = 0.00  %0.0*run_time;     	% [sec] Sinus frequency (excitation)
   
    
%% ------------------------------------------------

% Start a timer:
t_Start = tic;  

% The time interval, in indexes out of the <input> vector, in which the
%   signal is assumed to be stationary (transient effects are already off):
time_int_bd = unique(round( N_tot*[0.5, 0.85] ));     % boundaries
time_int_idx = [time_int_bd(1):time_int_bd(2)];

% All frequencies to check:
% freq_v = 1e3* [0.5, 0.8 1:10];
    freq_v = 1e3* 4; %[1:6];
L_freq_v = length(freq_v);

% All alpha_s options:
% alpha_s_v = 10.^-[1:10];
    alpha_s_v = 0.1 %0.1;
L_alpha_s_v = length(alpha_s_v);
    

            %obj.alpha_L = 2;
            %aa = 20 %30;
            obj.alpha_L = 1 % 5e-5; %aa*alpha_s_v;

            
%    
%M_sp = zeros(SECTIONS, N_tot, L_freq_v, L_alpha_s_v );


% Create the database structure:
db_st.input_st  = input_st;
db_st.alpha_s_v = alpha_s_v;
db_st.freq_v    = freq_v;
db_st.gain_mt   = zeros(SECTIONS, L_freq_v, L_alpha_s_v);    % The gain matrix (the results)
db_st.time_int_bd       = time_int_bd;
db_st.gain_max_mt       = zeros(L_freq_v, L_alpha_s_v);
db_st.gain_max_idx_mt   = zeros(L_freq_v, L_alpha_s_v);
db_st.f_bm_cf   = obj.f_bm_cf;      % [Hz] Save also the CF


%
for j = 1:L_freq_v

    % Set the frequency for the jth simulation & create the input signal:
    input_st.f_sin = freq_v(j);     % [Hz] Sinus frequency (excitation)
    input = Create_Input_Files( obj, input_st, run_time, gamma_v );
    fprintf('\n-----------------------------------------\n')
    fprintf('->input_st.f_sin = %g [Hz]\n', input_st.f_sin);
    fprintf('-----------------------------------------\n')

    
    for i = 1:L_alpha_s_v

        % Set the ith <alpha_s> for the ith simulation:
        obj.alpha_s = alpha_s_v(i);    
        fprintf('\n\t-----------------------------------------\n')
        fprintf('\t->obj.alpha_s = %g [Hz]\n', obj.alpha_s);
        fprintf('\t-----------------------------------------\n')

        % Solve the model:
        obj.Solve;                                              

        % Read the binary file into the workspace:
        [ttt, M_sp_1_cm] = obj.Read_Data( obj.save2disk.bm_sp.filename, obj.path.output, Fs, SECTIONS );
        %M_sp(:,:,j,i) = 0.01*M_sp_1_cm;     % [cm/s] -> [m/s] Velocity        
        
        % Save the gain's maximum peak:
        db_st.gain_mt(:,j,i) = sum(abs( 0.01*M_sp_1_cm(:,time_int_idx) ).^2, 2);
        [max_ij, max_idx_ij] = max(db_st.gain_mt(:,j,i));
        db_st.gain_max_mt(j,i)      = max_ij;
        db_st.gain_max_idx_mt(j,i)  = max_idx_ij;
        
        
    end
    
end

% Stop the timer:
t_Elapsed = toc( t_Start );
fprintf( '\n->Total elapsed time = %g [sec]\n', t_Elapsed )


%% Plot

for i = 1:L_alpha_s_v
   legend_str{i} = num2str(alpha_s_v(i)); 
end


% figure(10)
% plot(ttt, input)
% xlabel('Time [sec]')
% title(sprintf('Input - windowed sinus\nf_{sin} = %g, Amp = %g', input_st.f_sin, pressure_input_SPL))
% 
% 
% figure(11)
% semilogy( 1e-3*freq_v, db_st.gain_max_mt(j,1) )      
% title('Excitation Energy') 
% legend(num2str(alpha_s_v(1)))
% xlabel('[kHz]')
% legend(legend_str)

%%
figure(15)
semilogy(  1e-3*obj.f_bm_cf, squeeze(db_st.gain_mt(:,:,1)), 'c.-')
hold on
semilogy(  1e-3*obj.f_bm_cf, squeeze(db_st_ref.gain_mt(:,:,1)/2e-6), '-.')
hold off
xlabel('[kHz]')
title(sprintf('\\alpha_s = %g, \\alpha_L = %g', obj.alpha_s, obj.alpha_L))

%%
        
        
        
        
        
        
        



