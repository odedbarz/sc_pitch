% -------------------------------------------------------------------------
%
% Written by Barzelay Oded - June 2010 (start)
% 
% -------------------------------------------------------------------------


classdef Debug_Status < handle
    
    properties (Constant)
        MAX_VECTOR_SIZE = 5e5; 
    end
    
    properties

        % Enable\Disable debug modes:
        enable_all              
        enable_txt_2_cmd
        enable_plot_real_time  
        enable_release_txt
        
        % Controls the number of times inside a loop
        % which the debug feature will be used ( using <mod()> ).
        base_counter_1          = 20;
        base_counter_release    = 200;
        
        % Show stuff on the command window while solving:
        cmd_status = 1;

        fig_1_debug = 99;

        input = [];     % Excitation.
         
        time    = nan( Debug_Status.MAX_VECTOR_SIZE, 1 );    
        ow_disp = nan( Debug_Status.MAX_VECTOR_SIZE, 1 );
        ow_sp   = nan( Debug_Status.MAX_VECTOR_SIZE, 1 );
        ow_acc  = nan( Debug_Status.MAX_VECTOR_SIZE, 1 );
    end


    % Constructor\ Destructor:
    methods
        
        % Constructor:
        function obj = Debug_Status( debug_mode )
            
            if ishandle( obj.fig_1_debug )
                close( figure( obj.fig_1_debug ) );
            end
            
            if ~exist( 'debug_mode', 'var' ) 
                obj.enable_all              = 0;              
                obj.enable_txt_2_cmd        = 0;  
                obj.enable_plot_real_time   = 0;  
                obj.enable_release_txt      = 0;  

            elseif isempty( debug_mode )
                obj.enable_all              = 0;              
                obj.enable_txt_2_cmd        = 0;  
                obj.enable_plot_real_time   = 0;  
                obj.enable_release_txt      = 0;  
                
            else
                obj.enable_all              = debug_mode.enable.all;              
                obj.enable_txt_2_cmd        = debug_mode.enable.txt_2_cmd;  
                obj.enable_plot_real_time   = debug_mode.enable.plot_real_time;  
                obj.enable_release_txt      = debug_mode.enable.enable_release_txt;  
                
                
            end
            
        end
        
    end
    
    methods

        % Mod condition. if mod( counter, base ) == 0, then the result is
        % true. Otherwise the result is false.
        function yes = Mod_Cond( obj, counter, base )
            
            if false == obj.enable_all
                yes = 0;
                return;
            end
            
            yes = ~mod( counter, base);
        end
        
        % Show relevant data on the command line. <obj_ref> is an object
        % with a relevant information (in this case it's CM_BM_C).
        function Txt_2_cmd( obj, obj_ref, counter_1, counter_2 )
                       
            if false == obj.enable_txt_2_cmd
                return;
            end
            
            if obj.Mod_Cond( counter_1, obj.base_counter_1 ) 
                
                clc
                fprintf( '--------------------------------------------------------------------\n' )
                fprintf( '\n' );
                fprintf( 'counter_1 = %d\n', counter_1 );
                fprintf( '\n' );
                
                fprintf( 'PLEASE NOTE:\n' )
                if true == obj_ref.psi_flag
                    fprintf( '\t->(true == obj_ref.psi_flag): psi is been use\n' )
                else
                    fprintf( '\t->(FALSE == obj_ref.psi_flag): psi ISN''T used! i.e. using a direct p_ohc calculation !!!\n' )
                end
                    
                fprintf( '\n' )
                fprintf( 'DEBUG Mode:\n' )
                fprintf( '\n' )                
                fprintf( '\talpha_s = %g\n', obj_ref.alpha_s );
                fprintf( '\talpha_l = %g\n', obj_ref.alpha_L );
                fprintf( '\n' )
                fprintf( '\tSECTIONS = %d [smp]\n', obj_ref.SECTIONS );
                fprintf( '\tFs = %g [kHz]\n', obj_ref.Fs / 1e3 );
                fprintf( '\n' )
                fprintf( '\tcounter_1 = %d\n', counter_1 );
                fprintf( '\tcounter_2 = %d (while loop repetitions)\n', counter_2 );
                fprintf( '\n' );
                fprintf( '\n' )
                fprintf( 'MATLAB - Completed = %g[%%]\n', 100*obj_ref.time/obj_ref.run_time ); 

                fprintf( 'w_ohc = %s\n', mat2str( obj_ref.w_ohc ) );
                fprintf( 'K_ohc = %s\n', mat2str( obj_ref.K_ohc ) );
                fprintf( 'alpha_L = %s\n', mat2str( obj_ref.alpha_L ) );

                fprintf( 'M_bm = %s\n', mat2str( obj_ref.M_bm ) );
                fprintf( 'R_bm = %s\n', mat2str( obj_ref.R_bm ) );                
                fprintf( 'S_bm = %s\n', mat2str( obj_ref.S_bm ) );
                
                %fprintf( 'C_d_psi_1 = %s\n', mat2str( obj_ref.C_d_psi_1 ) );                
                %fprintf( 'C_d_psi_2 = %s\n', mat2str( obj_ref.C_d_psi_2 ) );
                
            end
        end
        
        
        % Show relevant data on the command line. <obj_ref> is an object
        % with a relevant information (in this case it's CM_BM_C).
        function Txt_2_Release( obj, obj_ref, counter_1 )
                       
            if false == obj.enable_release_txt
                return;
            end
            
            if obj.Mod_Cond( counter_1, obj.base_counter_release ) 
                fprintf( 'MATLAB - Completed = %g[%%]\n', 100*obj_ref.time/obj_ref.run_time ); 
            end
            
        end
        
        
        
        % Plots relevant data on the command line. <obj_ref> is an object
        % with a relevant information (in this case it's CM_BM_C).
        function Plot_Real_Time( obj, obj_ref, counter_1 )  %, ~ )
                        
            if false == obj.enable_plot_real_time
                return;
            end
            
            if ~obj.Mod_Cond( counter_1, obj.base_counter_1 ) 
                return;
            end
            
            figure( obj.fig_1_debug )

            % Subplot pars:
            M = 5;
            N = 3;

            %
            subplot( M, N, 1 )
            plot( 10*obj_ref.xxx, 1e9*1e-2*obj_ref.now.bm_disp )
            hold on
            plot( 10*obj_ref.xxx, 1e9*1e-2*obj_ref.now.tm_disp, 'k' )
            hold off
            title( '\xi^{BM}_{disp} & \xi^{TM}_{disp}' )
            %ylabel( '[cm]' )    
            ylabel( '[nm]' )    
            grid on
            xlabel( 'x [mm]' )

            %
            subplot( M, N, 2 )
            plot( 10*obj_ref.xxx, 1e6*1e-2*obj_ref.now.bm_sp )
            hold on
            plot( 10*obj_ref.xxx, 1e6*1e-2*obj_ref.now.tm_sp, 'k' )
            hold off
            title( '\xi^{BM}_{sp} & \xi^{TM}_{sp}' )
            ylabel( '[\mum/s]' )    
            grid on
            xlabel( 'x [mm]' )

            %
            subplot( M, N, 3 )
            plot( obj_ref.xxx, 1e3*1e-2*obj_ref.now.bm_acc )
            hold on
            plot( obj_ref.xxx, 1e3*1e-2*obj_ref.now.tm_acc, 'k' )
            hold off
            title( '\xi^{BM}_{acc} & \xi^{TM}_{acc}' )
            ylabel( '[mm/s^2]' )   
            grid on
            xlabel( 'x [mm]' )

            %
            subplot( M, N, 4 )
            plot( obj_ref.xxx, obj_ref.now.p_ohc )
            ylabel( '[Pa]' )
            title( sprintf( 'P_{OHC} (\\gamma(1) = %g)', obj_ref.gamma(1) ) )
            grid on
            xlabel( 'x [cm]' )

            %
            subplot( M, N, 5 )
            plot( obj_ref.xxx, obj_ref.p )
            ylabel( '[Pa]' )
            title( 'P(x,t)' )
            grid on
            xlabel( 'x [cm]' )

            %
            subplot( M, N, 6 )
            ttt = linspace( 0, obj_ref.Total_Input_Time, length(obj.input) );
            plot( ttt, obj.input )
            hold on
            plot( [obj_ref.time, obj_ref.time], [min(obj.input), max(obj.input)] , '-+r', 'LineWidth', 2 )
            hold off
            ylabel( '[Pa]' )
            %title( sprintf( 'Input( t = %2.3g [s] )', obj_ref.time ) )
            title( sprintf( 'time step = %g [s] )', obj_ref.time_step ) )
            grid on
            xlabel( 't [s]' )

            %
            Ix_current = counter_1/obj.base_counter_1;
            if counter_1 <= obj.MAX_VECTOR_SIZE
                obj.time( Ix_current )       = obj_ref.time;
                obj.ow_disp( Ix_current )    = obj_ref.now.ow_disp;
                obj.ow_sp( Ix_current )      = obj_ref.now.ow_sp;
                obj.ow_acc( Ix_current )     = obj_ref.now.ow_acc;

            else
                obj.time( end +1 )          = obj_ref.time;
                obj.ow_disp( end +1 )       = obj_ref.now.ow_disp;
                obj.ow_sp( end +1 )         = obj_ref.now.ow_sp;
                obj.ow_acc( end +1 )        = obj_ref.now.ow_acc;

            end

            %
            subplot( M, N, 7 )
            hold on
            plot( obj.time( 1:Ix_current ), obj.ow_disp( 1:Ix_current ), '-r' )
            hold off
            ylabel( '[cm]' )
            title( '\xi^{OW}_{disp}' )
            grid on
            xlabel( 't [s]' )

            %
            subplot( M, N, 8 )
            hold on
            plot( obj.time( 1:Ix_current ), obj.ow_sp( 1:Ix_current ), '-r' )
            hold off
            ylabel( '[cm/s]' )
            title( '\xi^{OW}_{sp}' )
            grid on
            xlabel( 't [s]' )

            %
            subplot( M, N, 9 )
            hold on
            plot( obj.time( 1:Ix_current ), obj.ow_acc( 1:Ix_current ), '-r' )
            hold off
            ylabel( '[cm/s^2]' )
            title( '\xi^{OW}_{acc}' )
            grid on
            xlabel( 't [s]' )

            
            % (psi):            
            subplot( M, N, 10 )
                % Nonlinear ratio:
                DL_nlin = obj_ref.now.D_L_ohc;
                DL_lin  = -obj_ref.alpha_L * obj_ref.now.psi_ohc;
                DL_nonLinRatio = norm( (DL_nlin-DL_lin) ) / norm( DL_nlin );            
            plot( obj_ref.xxx, [obj_ref.now.D_L_ohc, DL_lin] )
            hold on
            plot( obj_ref.xxx, obj_ref.D_L_fun( linspace( -2, 2, obj_ref.SECTIONS ) ), 'k' )
            hold off
            ylabel( '[cm]' )
            title( sprintf('\\DeltaL(\\psi), NLR = %g', DL_nonLinRatio) )
            grid on
            xlabel( 'x [cm]' )


            subplot( M, N, 11 )
            plot( obj_ref.xxx, obj_ref.now.psi_ohc )
            ylabel( '[V]' )
            title( '\psi' )
            grid on
            xlabel( 'x [cm]' )

            subplot( M, N, 12 )
            plot( obj_ref.xxx, obj_ref.now.d_psi_ohc )
            ylabel( '[dV/dt]' )
            title( 'd\psi/dt' )
            grid on
            xlabel( 'x [cm]' )

            %
            subplot( M, N, 13 )
            plot( obj_ref.xxx, obj_ref.gamma )
            ylabel( 'Gain' )
            title( sprintf( '\\gamma(x) = %g', obj_ref.gamma(1) ) )
            grid on
            xlabel( 'x [cm]' )

            
            drawnow;

        end

        
    end

end







