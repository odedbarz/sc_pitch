%
% function x_SPL = Pa_2_SPL( x_Pa, dB_SPL_ref )
%
% Pascal to dB SPL
%

function x_SPL = Pa_2_SPL( x_Pa, dB_SPL_ref )

if ~exist('dB_SPL_ref', 'var')
    dB_SPL_ref = 20e-6;     % 20 �[Pa] RMS
elseif isempty(dB_SPL_ref)
    dB_SPL_ref = 20e-6;     % 20 �[Pa] RMS
end

x_SPL = 20*log10(x_Pa/dB_SPL_ref);


