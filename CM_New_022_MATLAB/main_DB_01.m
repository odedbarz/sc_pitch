% -------------------------------------------------------------------------
%
% Written by Barzelay Oded - 15/03/2011
% 
% -------------------------------------------------------------------------
%
% main_DB_02.m
%
%   This script calc and creates a database of sinus for various
%   frequencies and input pressure. 
%   In order to perform it quickly, I use only the c++ projects. 
% 
%
%

% clear classes
clc
% close all

%% Model Type:
%   1. 'WKB'               	- The WKB approximation. 
%   2. 'CM_Time'            - The non-linear Cochlea project.
%   3. 'CM_Time_cpp'        - The non-linear Cochlea cpp project.
%   4. 'CM_Yaniv_Linear'  	- The linear Cochlea project, writen by Yaniv
%                             Halmut (June 2004).

% model_type = 'CM_Old_Cpp_Ref_05';         % C++
% model_type = 'CM_New_005_Model_13';
% model_type = 'CM_New_005_Model_13_START_TIME{0.03[sec]}';
% model_type = 'CM_New_021_2';        % C++
% model_type = 'CM_New_021_3';        % C++
% model_type = 'CM_New_021_2_alpha_L15';        % C++
% model_type = 'CM_New_022';        % C++
model_type = 'CM_New_022_START_TIME{0.03[sec]}';        % C++


% Location to save the data:
data_path = [cd, '\Data\'];

%
Fs                   = 50e3;                        % [Hz] Sample rate
    model_factor = 3;
gamma                = model_factor* 0.4;                         % (1x1) OHC (constant) enhancement
run_time             = 0.05;                        % [sec] Total excitation (input) time
pressure_input_SPL_v = [0:20:100];                  % (1x1) [dB SPL] Pressure gain
f_sin_v              = 1e3* [0.1:0.2:0.9 1:10];     % [kHz] The frequencies to check with

line_str = '--------------------------------';
fprintf('\n\n%s\n\t-> gamma = %g\n%s\n\n', line_str, gamma, line_str);


% Construct the model:
obj = CM_new_C( Fs );


fprintf( '%s\n', line_str )
fprintf( 'Model Type = < %s >\n', model_type )
fprintf( '%s\n', line_str )


%% Start the loop on all intensities:

% Initialize more parameters:
N_spl	= length( pressure_input_SPL_v );
N_freq	= length( f_sin_v );


for i = 1:N_spl         % Run on the intensities

    % Calc the kth pressure input:
    pressure_input_Pa_i = SPL_2_Pa( pressure_input_SPL_v(i) );    % (1x1) [dB SPL]->[Pa] Pressure gain
    
    % 1[Pa] = 1[N/m^2] = 10[(gr*cm/s^2)/cm^2]:
    pressure_input_gr_cm_sec_i = 10* pressure_input_Pa_i;  % 1[Pa] -> 10[(gr*cm/s^2)/cm^2]
    
    for j = 1:N_freq    % Run on the excitation frequencies

        % The jth frequency:
        f_sin_v_j = f_sin_v(j);

        % Create the sinus input structure:
        %   -> sinus:
        input_st.input_type_str = 'sin( 2*pi*f0 )';             % (str) The excitation type
        input_st.f_sin          = f_sin_v_j;                    % [Hz] Sinus frequency (excitation)
        input_st.G_sin          = pressure_input_gr_cm_sec_i; 	% [gr*cm/s^2] Sinus amplitude (gain)
        input_st.win            = 0;                            % Enable\Disable windowing + padding of the sinus
        input_st.delay_start_sec = 0.00; %0.0*run_time;      	% [sec] Sinus frequency (excitation)
        input_st.delay_end_sec   = 0.00;  %0.0*run_time;     	% [sec] Sinus frequency (excitation)
        input_i                 = obj.Create_Input_Files( input_st, run_time, gamma );

        
        % Solve by the chosen algorithm:
        switch model_type

            case 'CM_Old_Cpp_Ref_05'
                proj_name_cpp           = '_CM_Old_Cpp_Ref_05';
                data_path_cpp           = '..\\Data\\';
                input_filename_cpp      = 'in';
                output_filename_cpp     = sprintf( 'BM_sp_cpp_%gSPL_%gkHz', pressure_input_SPL_v(i), 1e-3*f_sin_v_j );
                oae_filename_cpp        = sprintf( 'OAE_cpp_%gSPL_%gkHz', pressure_input_SPL_v(i), 1e-3*f_sin_v_j );

                input_full_filename_cpp     = sprintf( '%s%s',data_path_cpp, input_filename_cpp );
                output_full_filename_cpp    = sprintf( '%s%s',data_path_cpp, output_filename_cpp );
                oae_full_filename_cpp       = sprintf( '%s%s',data_path_cpp, oae_filename_cpp );

                cmd2run = sprintf( '%s %d %s.bin %s.bin %s.bin  %s.bin',...
                    proj_name_cpp,...        
                    Fs,...
                    input_full_filename_cpp,...
                    output_full_filename_cpp,...
                    oae_full_filename_cpp...
                );

                fprintf( '\n\nStarting The C++ Algorithm:\n\n' )
                fprintf( '-> %s\n', cmd2run );

                % Run the cpp model:
                cd( [cd, '\cpp_model\'] );         
                dos( cmd2run );
                cd ..

                % Read the binary file into the workspace:
                %[ttt, M_sp_3_cm] = obj.Read_Data( output_filename_cpp, obj.path.output, Fs, obj.SECTIONS );
                %M_bm_sp_i = 0.01*M_sp_3_cm;     % [m/s] Velocity
                
                %[ttt, v_OAE]	= obj.Read_Data( oae_filename_cpp, obj.path.output, Fs, run_time, 1 );
                %[ttt, v_OAE]	= obj.Read_Data( oae_filename_cpp, obj.path.output, Fs, 1 );
                        
                
            otherwise
                
                proj_filename_cpp = model_type;
                
                %generic_extention_filename = '_cpp';
                generic_extention_filename = sprintf( '_gamma(%g)_SPL(%g)_f(%g)kHz_runTime(%g)msec',...
                    gamma, pressure_input_SPL_v(i), 1e-3*f_sin_v_j, 1e3* run_time );                
                
                data_path_cpp = '..\\Data\\';

                cmd2run = sprintf( '%s %d %s %s %s %s',...
                    proj_filename_cpp,...        
                    Fs,...
                    generic_extention_filename,...
                    data_path_cpp...
                );

                fprintf( '\n\nStarting The C++ Algorithm:\n\n' )
                fprintf( '-> %s\n', cmd2run );

                % Run the cpp model:
                cd( [cd, '\cpp_model\'] );         
                dos( cmd2run );
                cd ..
                                

        end
        
        
    end
    
    
    
end



%% Save the simulation results:
DB_st.info                  = 'Database of sinus with different frequencies and input pressure';
DB_st.pressure_input_SPL_v  = pressure_input_SPL_v;
DB_st.f_sin_v               = f_sin_v;
DB_st.gamma                 = gamma; 
DB_st.SECTIONS              = obj.SECTIONS;
DB_st.Fs                    = Fs;
DB_st.run_time              = run_time;

save( [data_path, 'DB_st.mat'], 'DB_st', '-mat' )


%% Saves the *.bin data files into a *.mat files:
%   -> !!! Already includes the normalization from [cm]->[m] !!!
%
% (SaveBin2Mat.m)
% 

% data_path   = [cd, '\Data\', model_type, '\'];
% deploy_path = [cd, '\Data\', model_type, '\'];
data_path   = [cd, '\Data\'];
deploy_path = [cd, '\Data\'];

dummy = load( [data_path, 'DB_st'] );
DB_st = dummy.DB_st;
clear dummy

% Construct the model:
obj = CM_new_C( DB_st.Fs );


% Read all bin files available in the directory:
file_list = dir( [data_path, '*.bin'] );
L_files = length( file_list );


for i = 1:L_files
    
    filename_i = file_list(i).name(1:end-4);
    fprintf( '\n%d. File:\t %s\n', i, filename_i )
    
    % Save if the file already exist. If so jump to the next file:
    if ~isempty( dir( [deploy_path, filename_i, '.mat'] ) )
        fprintf( '\t-> <%s> already exist!\n', filename_i )
        continue;
        
    end
    
    try
        % Read the binary file into the workspace:
        [ttt, M] = obj.Read_Data(...
            filename_i,...
            data_path,...
            DB_st.Fs,...                Fs
            DB_st.SECTIONS...
        );
        
    catch ME
        % Read the binary file into the workspace:
        [ttt, M] = obj.Read_Data(...
            filename_i,...
            data_path,...
            DB_st.Fs,...                Fs
            1 ...
        );
        
    end
    
    % NORMALIZATION:
    M = 0.01*M;         % [cm/s] -> [m/s] Velocity
    
    save( [deploy_path, filename_i, '.mat'], 'M', '-mat' )
    clear M
    
end

save( [deploy_path, 'ttt.mat'], 'ttt', '-mat' )









