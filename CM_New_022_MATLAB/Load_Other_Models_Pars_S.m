%
% Load_Other_Models_Pars_S
%   Load other model's pars to the workspace.
%

fprintf('=> Running <Load_Other_Models_Pars_S>\n');

% -> From Lu's paper (Guinepig):
fprintf('\t-> Lu''s Model (Guinepig): Loading S_bm_lu, M_bm_lu & R_bm_lu\n')
Q_tm_lu = 4 *exp( -2.303*x ./ len );                    % (Lu) Quality factor for the TM
S_bm_lu = 1/0.182 * exp( -3.464* x );                       % [N/m] BM stiffnes (spring) per unit area.
S_tm_lu = 1/6*S_bm_lu;                                          % [N/m]
%w_bm_cf_lu = w_gw;
%w_tm_cf_lu = 1/sqrt(2) * w_bm_cf_lu;
%M_tm_lu = S_tm_lu ./( w_tm_cf_lu.^2 );                          % TM mass per unit area.
%R_tm_lu = 1./Q_tm_lu .*sqrt( M_tm_lu .* S_tm_lu );           	% TM losses (resistance\disipation) per unit area.

L_lu    = 1.85;                             % [cm] 
x_lu    = linspace(0,L_lu,length(x))';  	% [cm]

H_ohc_lu = 32.4324*x_lu + 20; 
Gk_lu   = -0.6503*H_ohc_lu + 55.7;          % [pS]
Cm_lu   = 0.3574*H_ohc_lu + 5.8416;         % [pF]

tau_lu  = Cm_lu./Gk_lu;                     % [sec]
w_ohc_lu = 1./tau_lu; 

kv_lu   = 4;                                % [mV/nm]
kf_lu   = 0.5*(exp(-2.7*x)-0.00035);        % [nN/mV]         



% -> From Neely's paper (Human):
fprintf('\t-> Neely & Liu model (Human): Loading S_bm_neely, M_bm_neely & R_bm_neely\n')
T_neely         = 2.4e6* ones(size(x));
M_tm_neely      = Log_Quad_Interp( x, len, 2.8e-8, 5e-7, 2.8e-5 );   	% TM mass per unit area.
R_tm_neely      = Log_Quad_Interp( x, len, 9.4e-4, 9.2e-4, 2.7e-3 );     % TM losses (resistance\disipation) per unit area.            
S_tm_neely      = Log_Quad_Interp( x, len, 200, 11, 0.76 );           	% [N/m]
G_neely         = 1e-9*  Log_Quad_Interp( x, len, 91, 51, 33 ); 	% [S] OHC basolateral conductance, From Lu, 2006.            
C_neely         = 1e-12* Log_Quad_Interp( x, len, 14, 32, 79 );  	% [F] OHC basolateral membrane capacitance.            
alpha_d_neely   = Log_Quad_Interp( x, len, 1.6e-3, 6.2e-4, 2e-4 );    % [A/m] alpha_d: From Liu & Neely, 2010 (Human !) 
alpha_v_neely   = Log_Quad_Interp( x, len, 4.4e-6, 1.8e-6, 6.8e-7 );  % [C/m] alpha_v: From Liu & Neely, 2010 (Human !)
w_ohc_neely     = G_neely./C_neely;

I_max           = 1e-12*Log_Quad_Interp( x, len, 640, 320, 83 );

eta_1_neely   = alpha_d_neely./C_neely;
%alpha_2_neely   = alpha_v_neely./C_neely;
eta_2_neely   = (alpha_v_neely - 1./T_neely)./C_neely;

K_ohc_neely     = Log_Quad_Interp( x, len, 200, 11, 0.76 );

%  Greenwood's function Human: 
f_gw = 165.4*( 10.^( 2.1*(len-x)/len ) -1.0 );	
w_gw = 2*pi*f_gw;

fprintf('=> End of <Load_Other_Models_Pars_S>\n\n');










