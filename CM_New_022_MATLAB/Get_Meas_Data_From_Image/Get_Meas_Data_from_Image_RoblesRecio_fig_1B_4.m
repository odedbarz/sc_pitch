% 
%  
% For TUNING CURVES
% 
% Get_Meas_Data_from_Image_RoblesRecio_fig_1B 3.m
% 
% using:
%   -> [x,y] = ginput( 1 )
%

clc

image_path = [cd, '\Results - Papers Figures\']; 
image2read = 'RuggeroRich1997 - Tuning Curves of chinchilla CF(10[kHz]) (fig 11).jpg ';

% Read the image into the workspace:
[Img, Img_map] = imread( [image_path, image2read] );

% Plot the loaded image on screen:
fig_num = 90;
figure( fig_num )
img_hnd = image( Img );

% % for a 'semilogy' figures:
% set( gca, 'Yscale', 'log' );

% The bottom left image point (image's (0,0) point):
img_bl_x = 86.1971;     %65.1724;    
img_bl_y = 512.8441;    %244.2988;  

% The upper righ image point:
img_ur_x = 731.0015; %445.1522;    
img_ur_y = 19.3900;     %9.3169;      

% x data axis (boundaries):
x_data = [1, 20];
% y data axis (boundaries):
y_data = [0, 100];     % [1, 35e4];

clear x_img_v y_img_v
while_counter = 1;

% Select the point on the image - keep selecting while you press the LEFT
% MOUSE button;
button = 1;

while 1 == button
    
    [x_tmp, y_tmp, button] = ginput( 1 );
        
    if 1 == button
        x_img_v(while_counter,1) = x_tmp;
        y_img_v(while_counter,1) = y_tmp;
        while_counter = while_counter+1;
    end
    

end

% Calc the real x axis of the image (linear):
x_pow_v = (x_img_v - img_bl_x)/(img_ur_x - img_bl_x) * diff( log10( x_data ) );
x_fig_v = x_data(1)* 10.^x_pow_v;

% Calc the real y axis of the image (log):
y_fig_v = (y_img_v - img_bl_y)/(img_ur_y - img_bl_y) * diff(y_data);


%%

if ~exist('DB_st', 'var')
    error('! You better get the DB_st for sensitivity !')
end

cf_hu       = DB_st.CF_2_chk;
cf_chin     = 9.5e3;  % Chinchilla's cochlea
mu_scale    = 1; %2e-7;

% Human:
% f_hu_v  = DB_st.f_sin_v'/cf_hu;
f_hu_v  = DB_st.f_sin_v';

% Chinchilla:
% f_ch_v  = (x_fig_v-(cf_chin-cf_hu))/cf_hu;
f_ch_v  = (1e3 * x_fig_v - (cf_chin-cf_hu));

%%
if ~exist('meas_st', 'var')
    meas_st.M_x = cell(0);
    meas_st.M_y = cell(0);
end
    
% M_y{end+1} = y_fig_v;
% M_x{end+1} = f_ch_v;
    
% Save the data structure:
meas_st.paper = 'RuggeroRecio2001 - Mechanical bases of frequency tuning and neural excitation...';
meas_st.M_x{end+1} = f_ch_v;
meas_st.M_y{end+1} = y_fig_v;
meas_st.spl = [0 20 40 60 80 100];


%% Re-plot:

% figure( fig_num+1 )

% %
% X = DB_st.pressure_input_SPL_v(ones(size(DB_st.VI,2),1),:)';
% Y = DB_st.f_sin_v(ones(size(DB_st.VI,1),1),:);
% [C, h] = contour( Y, X, 20*log10(DB_st.VI/mu_scale), ISO_2_plot_v, '-', 'LineWidth', 2 );

% 
% hnd_colors = get( hnd_sim, 'color' );

hold on
for i = 1:size(meas_st.M_y,2)

    semilogx( meas_st.M_x{i}, meas_st.M_y{i}, 'ok' )
    
%     semilogx( meas_st.M_x{i}, meas_st.M_y{i},...
%             'o',...
%             'linewidth', 2.5,...
%             'color', hnd_colors{i}...
%         );


end


hold off








