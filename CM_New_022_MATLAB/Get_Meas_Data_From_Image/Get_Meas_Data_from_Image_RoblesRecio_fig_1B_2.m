% 
% 
% Get_Meas_Data_from_Image_RoblesRecio_fig_1-B.m
% 
% using:
%   -> [x,y] = ginput( 1 )
%

clc

image_path = [cd, '\Results - Papers Figures\']; 
image2read = 'RuggeroRecio2001- Gain (sensitivity) - Velocity vs stimulus pressure (fig 1B) (2).jpg ';

% Read the image into the workspace:
[Img, Img_map] = imread( [image_path, image2read] );

% Plot the loaded image on screen:
fig_num = 90;
figure( fig_num )
img_hnd = image( Img );

% % for a 'semilogy' figures:
% set( gca, 'Yscale', 'log' );

% The bottom left image point (image's (0,0) point):
img_bl_x = 77.0937;     %65.1724;    
img_bl_y = 670.7135;    %244.2988;  

% The upper righ image point:
img_ur_x = 1.1245e+003; %445.1522;    
img_ur_y = 26.2969;     %9.3169;      

% x data axis (boundaries):
x_data = [0, 18e3];
% y data axis (boundaries):
y_data = [0.1, 3e3];     % [1, 35e4];

clear x_img_v y_img_v
while_counter = 1;

% Select the point on the image - keep selecting while you press the LEFT
% MOUSE button;
button = 1;

while 1 == button
    
    [x_tmp, y_tmp, button] = ginput( 1 );
        
    if 1 == button
        x_img_v(while_counter,1) = x_tmp;
        y_img_v(while_counter,1) = y_tmp;
        while_counter = while_counter+1;
    end
    

end

% Calc the real x axis of the image (linear):
x_fig_v = (x_img_v - img_bl_x)/(img_ur_x - img_bl_x) * diff(x_data);

% Calc the real y axis of the image (log):
y_pow_v = (y_img_v - img_bl_y)/(img_ur_y - img_bl_y) * diff( log10( y_data ) );
y_fig_v = y_data(1)* 10.^y_pow_v;


%% ! Don't forget to save the data into the database ! 

% xxx = [x_data(1):100:x_data(end)]';
% xxx_chin = xxx .* (xxx - xxx(1))/(xxx(end) - xxx(1));


%%

cf_hu       = DB_st.CF_2_chk;
cf_chin     = 9.5e3;  % Chinchilla's cochlea
mu_scale    = 1; %2e-7;

% Human:
fff_norm_hu_v  = DB_st.f_sin_v/cf_hu;
fff_norm_hu_v  = fff_norm_hu_v(:).^0.5;     % normalaized to match to the chinchilla's greenwood function

% Chinchilla:
fff_norm_chin_v = x_fig_v/cf_chin;

%%
if ~exist('M_y', 'var')
    M_y = cell(0);
    M_x = cell(0);
end
    
M_y{end+1} = y_fig_v;
M_x{end+1} = fff_norm_chin_v;
    
%% Save the data structure:
meas_st.paper = 'RuggeroRecio2001 - Mechanical bases of frequency tuning and neural excitation...';
meas_st.M_x = M_x;
meas_st.M_y = M_y;
meas_st.spl = [0 20 40 60 80 100];


%% Re-plot:

figure( fig_num+1 )

%
if exist('DB_st', 'var')
    hnd_sim = loglog(  fff_norm_hu_v, 1/mu_scale * DB_st.sensitivity', '.-', 'linewidth', 2.5 );
else
    error('! You better get the DB_st for sensitivity !')
end

% 
hnd_colors = get( hnd_sim, 'color' );

hold on
for i = 1:size(meas_st.M_y,2)
%     semilogx( M_x{i}, M_y{i},...
%             'o',...
%             'linewidth', 2.5,...
%             'color', hnd_colors{i}...
%         );
    semilogx( meas_st.M_x{i}, meas_st.M_y{i},...
            'o',...
            'linewidth', 2.5,...
            'color', hnd_colors{i}...
        );


end


hold off








