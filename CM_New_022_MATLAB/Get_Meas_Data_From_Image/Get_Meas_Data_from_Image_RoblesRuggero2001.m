% 
% 
% Get_Meas_Data_from_Image.m
% 
% using:
%   -> [x,y] = ginput( 1 )
%

image_path = [cd, '\Results - Papers Figures\']; 
image2read = 'RoblesRuggero2001 isointensity curves (fig 4).jpg';

% Read the image into the workspace:
[Img, Img_map] = imread( [image_path, image2read] );

% Plot the loaded image on screen:
fig_num = 190;
figure( fig_num )
img_hnd = image( Img );

% % for a 'semilogy' figures:
% set( gca, 'Yscale', 'log' );

% The bottom left image point (image's (0,0) point):
img_bl_x = 58.4980;
img_bl_y = 328.1695;    	% linear

% The upper righ image point:
img_ur_x = 507.3972;
img_ur_y = 10.92246;             % linear

% x data axis (boundaries):
x_data = 1e3* [2, 20];
% y data axis (boundaries):
y_data = [2, 20e3];     % [1, 35e4];       

clear x_img_v y_img_v
while_counter = 1;

% Select the point on the image - keep selecting while you press the LEFT
% MOUSE button;
button = 1;

while 1 == button
    
    [x_tmp, y_tmp, button] = ginput( 1 );
    
    if 1 == button
        x_img_v(while_counter,1) = x_tmp;
        y_img_v(while_counter,1) = y_tmp;
        while_counter = while_counter+1;
    end
    

end

% Calc the real x axis of the image (linear):
%x_fig_v = (x_img_v - img_bl_x)/(img_ur_x - img_bl_x) * diff(x_data);
% x_pow_v = (x_img_v - img_bl_x)/(img_ur_x - img_bl_x) * diff( log10( x_data ) );
% x_fig_v = 10.^x_pow_v;
x_pow_v = (x_img_v - img_bl_x)/(img_ur_x - img_bl_x) * diff( log10( x_data ) );
x_fig_v = x_data(1)*10.^x_pow_v;

% Calc the real y axis of the image (log):
y_pow_v = (y_img_v - img_bl_y)/(img_ur_y - img_bl_y) * diff( log10( y_data ) );
y_fig_v = y_data(1)*10.^y_pow_v;
        

%% ! Don't forget to save the data into the database ! 

xxx = [x_data(1):100:x_data(end)]';
% xxx_chin = xxx .* (xxx - xxx(1))/(xxx(end) - xxx(1));
xxx_chin = xxx;


%%

[dummy, unique_indx] = unique(x_fig_v);

if exist('M_y', 'var')
    M_y(:,end+1) = interp1( x_fig_v(unique_indx), y_fig_v(unique_indx), xxx );
else
    M_y = interp1( x_fig_v, y_fig_v, xxx );
end


%% Re-plot:

figure( fig_num+1 )

cf_chin     = 10e3%3.8e3;  % Chinchilla's cochlea
mu_scale    = 2e-7; %2e-7;


loglog( xxx_chin/cf_chin, M_y, 'x', 'linewidth', 2.5 )


hold on

if exist('DB_st', 'var')
    
    cf_hu = DB_st.CF_2_chk;
%     loglog(  DB_st.f_sin_v/cf_hu, mu_scale*DB_st.VI', '.-' );
    loglog(  DB_st.f_sin_v/cf_hu, 1/mu_scale * DB_st.VI', '.-', 'linewidth', 2.5 );
    
end

hold off



% %% Normalaized Re-plot:
% 
% figure( fig_num+2 )
% 
% % x_h_lim  = [100, 12e3]; 
% % x_ch_lim = [0, 18e3];
% 
% xxx_chin = xxx(end)*10.^(1/0.4 * log10(xxx/xxx(end)));
% 
% cf_hu       = DB_st.CF_2_chk;
% % cf_chin     = 19.71/35 * 10e3; % ~5e3;              % Chinchilla's cochlea
% cf_chin     = 10e3; % ~5e3;              % Chinchilla's cochlea
% 
% M_y_norm    = zeros(size(M_y,1), size(M_y,2));
% M_VI_norm   = zeros(size(DB_st.VI,2), size(M_y,2)); 
% 
% jump_by_y = 0.5;
% 
% for i = 1:size(M_y,2)
%     
%     M_y_norm(:,i) = i^jump_by_y + M_y(:,i)/max(M_y(:,i));
%     M_VI_norm(:,i) = i^jump_by_y + ( DB_st.VI(i,:)/max(DB_st.VI(i,:)) ).';
% 
% end
% 
% loglog( xxx_chin/cf_chin, M_y_norm, ':', 'linewidth', 2 );
% hold on
% loglog( DB_st.f_sin_v/cf_hu, M_VI_norm, 'linewidth', 2 );
% hold off
% 









