% 
% 
% Get_Meas_Data_from_Image_RoblesRecio_fig_1-B.m
% 
% using:
%   -> [x,y] = ginput( 1 )
%

clc

image_path = [cd, '\Results - Papers Figures\']; 
image2read = 'RuggeroRich1997 - Gain (sensitivity) - Velocity vs stimulus pressure (fig 1B) (2).jpg';

% Read the image into the workspace:
[Img, Img_map] = imread( [image_path, image2read] );

% Plot the loaded image on screen:
fig_num = 90;
figure( fig_num )
img_hnd = image( Img );

% % for a 'semilogy' figures:
% set( gca, 'Yscale', 'log' );

% The bottom left image point (image's (0,0) point):
img_bl_x = 77.0937;     %65.1724;    
img_bl_y = 670.7135;    %244.2988;  

% The upper righ image point:
img_ur_x = 1.1245e+003; %445.1522;    
img_ur_y = 26.2969;     %9.3169;      

% x data axis (boundaries):
x_data = [0, 18e3];
% y data axis (boundaries):
y_data = [0.1, 3e3];     % [1, 35e4];

clear x_img_v y_img_v
while_counter = 1;

% Select the point on the image - keep selecting while you press the LEFT
% MOUSE button;
button = 1;

while 1 == button
    
    [x_tmp, y_tmp, button] = ginput( 1 );
        
    if 1 == button
        x_img_v(while_counter,1) = x_tmp;
        y_img_v(while_counter,1) = y_tmp;
        while_counter = while_counter+1;
    end
    

end

% Calc the real x axis of the image (linear):
x_fig_v = (x_img_v - img_bl_x)/(img_ur_x - img_bl_x) * diff(x_data);

% Calc the real y axis of the image (log):
y_pow_v = (y_img_v - img_bl_y)/(img_ur_y - img_bl_y) * diff( log10( y_data ) );
y_fig_v = y_data(1)* 10.^y_pow_v;


%% ! Don't forget to save the data into the database ! 

% xxx = [x_data(1):100:x_data(end)]';
% xxx_chin = xxx .* (xxx - xxx(1))/(xxx(end) - xxx(1));


%%

cf_hu       = DB_st.CF_2_chk;
cf_chin     = 10e3;  % Chinchilla's cochlea
mu_scale    = 1; %2e-7;

fff_norm_v  = DB_st.f_sin_v/cf_hu;
fff_norm_v  = fff_norm_v(:).^(0.525);

% fff_norm_chin_v = [fff_norm_v; [13e3:1e3:18e3]'/cf_hu];
fff_norm_chin_v = linspace(0, 18e3, 40)'/cf_chin;

[dummy, unique_indx] = unique(x_fig_v);

%%
% if exist('M_y', 'var')
%     M_y(:,end+1) = interp1( x_fig_v(unique_indx), y_fig_v(unique_indx), xxx );
% else
%     M_y = interp1( x_fig_v, y_fig_v, xxx );
% end
if exist('M_y', 'var')
    M_y(:,end+1) = interp1( x_fig_v(unique_indx)/cf_chin, y_fig_v(unique_indx), fff_norm_chin_v );
else
    M_y = interp1( x_fig_v(unique_indx)/cf_chin, y_fig_v(unique_indx), fff_norm_chin_v );
end



%% Re-plot:

figure( fig_num+1 )

loglog( fff_norm_chin_v, M_y, ':', 'linewidth', 2.5 )
% legend('0', '100')

hold on

if exist('DB_st', 'var')
    loglog(  fff_norm_v, 1/mu_scale * DB_st.sensitivity', '.-', 'linewidth', 2.5 );
    
end

hold off



% %% Normalaized Re-plot:
% 
% figure( fig_num+2 )
% 
% % x_h_lim  = [100, 12e3]; 
% % x_ch_lim = [0, 18e3];
% 
% xxx_chin = xxx(end)*10.^(1/0.4 * log10(xxx/xxx(end)));
% 
% cf_hu       = DB_st.CF_2_chk;
% % cf_chin     = 19.71/35 * 10e3; % ~5e3;              % Chinchilla's cochlea
% cf_chin     = 10e3; % ~5e3;              % Chinchilla's cochlea
% 
% M_y_norm    = zeros(size(M_y,1), size(M_y,2));
% M_VI_norm   = zeros(size(DB_st.sensitivity,2), size(M_y,2)); 
% 
% jump_by_y = 0.5;
% 
% for i = 1:size(M_y,2)
%     
%     M_y_norm(:,i) = i^jump_by_y + M_y(:,i)/max(M_y(:,i));
%     M_VI_norm(:,i) = i^jump_by_y + ( DB_st.sensitivity(i,:)/max(DB_st.sensitivity(i,:)) ).';
% 
% end
% 
% loglog( xxx_chin/cf_chin, M_y_norm, ':', 'linewidth', 2 );
% hold on
% loglog( DB_st.f_sin_v/cf_hu, M_VI_norm, 'linewidth', 2 );
% hold off










