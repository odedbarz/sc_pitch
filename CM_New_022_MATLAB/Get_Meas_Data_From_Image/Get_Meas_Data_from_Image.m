% 
% 
% Get_Meas_Data_from_Image.m
% 
% using:
%   -> [x,y] = ginput( 1 )
%

image_path = [cd, '\figs\']; 
image2read = 'Robles2001_LowCF_TM_various_animals.png';

% Read the image into the workspace:
[Img, Img_map] = imread( [image_path, image2read] );

% Plot the loaded image on screen:
fig_num = 90;
figure( fig_num )
img_hnd = image( Img );

% % for a 'semilogy' figures:
% set( gca, 'Yscale', 'log' );

% The bottom left image point (image's (0,0) point):
img_bl_x =  106.9461;
img_bl_y =  451.7587;    	% linear

% The upper righ image point:
img_ur_x = 716.4814;
img_ur_y = 21.8105;             % linear

% x data axis (boundaries):
x_data = [0.04, 3]*1e3;
% y data axis (boundaries):
y_data = [1, 30e3];     % [1, 35e4];

clear x_img_v y_img_v
while_counter = 1;

% Select the point on the image - keep selecting while you press the LEFT
% MOUSE button;
button = 1;

while 1 == button
    
    [x_tmp, y_tmp, button] = ginput( 1 );
    
    if 1 == button
        x_img_v(while_counter,1) = x_tmp;
        y_img_v(while_counter,1) = y_tmp;
        while_counter = while_counter+1;
    end
    

end

% Calc the real x axis of the image (linear):
% x_fig_v = (x_img_v - img_bl_x)/(img_ur_x - img_bl_x) * diff(x_data);
x_pow_v = (x_img_v - img_bl_x)/(img_ur_x - img_bl_x) * diff( log10( x_data ) );
x_fig_v = x_data(1)* 10.^x_pow_v;

% Calc the real y axis of the image (log):
y_pow_v = (y_img_v - img_bl_y)/(img_ur_y - img_bl_y) * diff( log10( y_data ) );
y_fig_v = y_data(1)* 10.^y_pow_v;

%% ! Don't forget to save the data into the database ! 

chinchilla_cf = 10e3;

figure(fig_num+1)
% loglog( x_fig_v/chinchilla_cf, y_fig_v, '.-' )
loglog( x_fig_v, y_fig_v, '.-' )



