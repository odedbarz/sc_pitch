%
% function state = Create_State
%   
% Description:
%   Just creates a structure which represents a state at time t.
%
% Assumption: all vectors in a given object are of the same length.

classdef State_New_C < handle
    
    properties
        
        SECTIONS
        
        % Current input sample:
        sample_input
        
        % BM:
        bm_disp
        bm_sp
        bm_acc

        % TM:
        tm_disp
        tm_sp
        %tm_acc
        p_tm
        
        % OHC:
        p_ohc
        d_p_ohc
        
        % OHC - psi:
        psi_ohc
        d_psi_ohc
        %p_em
        DeltaL_disp
        DeltaL_sp
        
        % OW:
        ow_disp
        ow_sp 
        ow_acc 
        
        
    end

    % Dependent parameters:
    properties (Dependent)
        ohc_disp
        ohc_sp
        %ohc_acc
        
    end
    
    % ------------------------------------------------------------------------------
    
    methods
        
        %
        function obj = State_New_C( SECTIONS )
            
            if nargin == 1
                obj.SECTIONS = SECTIONS;
            else
                obj.SECTIONS = 0;
            end
           
            obj.Create_States;
            
        end
        
        %
        function obj = Create_States( obj )
            
            % Current input sample:
            obj.sample_input  = [];
            
            % BM:
            obj.bm_disp   = zeros( obj.SECTIONS, 1 );
            obj.bm_sp     = zeros( obj.SECTIONS, 1 );
            obj.bm_acc    = zeros( obj.SECTIONS, 1 );

            % OW:
            obj.ow_disp   = 0;
            obj.ow_sp     = 0;
            obj.ow_acc    = 0;
            
            % TM:
            obj.tm_disp   = zeros( obj.SECTIONS, 1 );
            obj.tm_sp     = zeros( obj.SECTIONS, 1 );
            %obj.tm_acc    = zeros( obj.SECTIONS, 1 );
            obj.p_tm      = zeros( obj.SECTIONS, 1 );
            
            % OHC:
            obj.p_ohc     = zeros( obj.SECTIONS, 1 );
            obj.d_p_ohc   = zeros( obj.SECTIONS, 1 );
               
            % OHC - psi:
            obj.psi_ohc     = zeros( obj.SECTIONS, 1 );
            obj.d_psi_ohc   = zeros( obj.SECTIONS, 1 );
            %obj.p_em        = zeros( obj.SECTIONS, 1 );
            obj.DeltaL_disp	= zeros( obj.SECTIONS, 1 );
            obj.DeltaL_sp 	= zeros( obj.SECTIONS, 1 );
                        
        end
        
        % Clear all fields in the <state_name> structure.
        %   <state_name> can only be 'now' or 'prev'.
        function obj = Reset( obj )
            obj.Create_States;

        end
        
        %
        function now_obj = Next_State( prev_obj, now_obj )

            % BM:
            prev_obj.bm_disp = now_obj.bm_disp;
            prev_obj.bm_sp   = now_obj.bm_sp;
            prev_obj.bm_acc  = now_obj.bm_acc;
            
            % TM:
            prev_obj.tm_disp = now_obj.tm_disp;
            prev_obj.tm_sp   = now_obj.tm_sp;
            %prev_obj.tm_acc = now_obj.tm_acc;
            prev_obj.p_tm    = now_obj.p_tm;
            
            % OW:
            prev_obj.ow_disp = now_obj.ow_disp;
            prev_obj.ow_sp   = now_obj.ow_sp;
            prev_obj.ow_acc  = now_obj.ow_acc;
            
            % OHC:
            prev_obj.p_ohc   = now_obj.p_ohc;
            prev_obj.d_p_ohc = now_obj.d_p_ohc;
            
            % OHC - psi:
            prev_obj.psi_ohc   = now_obj.psi_ohc;
            prev_obj.d_psi_ohc = now_obj.d_psi_ohc;
            %prev_obj.p_em     = now_obj.p_em;
            prev_obj.DeltaL_disp = now_obj.DeltaL_disp;
            prev_obj.DeltaL_sp   = now_obj.DeltaL_sp;
                        
            %
            now_obj.Reset;
            
        end
                        
    end     % END of methods. 

    
    % Set\Get methods:
    methods
        % obj.now.ohc_disp:
        function y = get.ohc_disp(obj)
            y = obj.tm_disp - obj.bm_disp - obj.DeltaL_disp;
                        
        end

        % obj.now.ohc_sp:
        function y = get.ohc_sp(obj)
            y = obj.tm_sp - obj.bm_sp - obj.DeltaL_sp;
        end

    end
    
    
end














