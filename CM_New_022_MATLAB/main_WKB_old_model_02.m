% 
% 
% main_wkb_old_model_02.m
% 
%   05/01/2011
%   Writen by Barzelay Oded
%
%   WKB simulation of the old model which includes the Z_bm & Z_ohc.
%   
%

% clc

%% Run Miriam's Model:

wkb_miriam_path = [cd, '\wkb_miriam\'];

addpath( wkb_miriam_path );
runModelWKB;    % -> VbmAbs
rmpath( wkb_miriam_path );


%% Model Parameters:

% OHC:
gamma = 0.5;


% Pressure in:
P_in_spl    = 0;
P_in        = 10*( 10^(P_in_spl/20)*20e-6 );   % 1[Pa] -> 10[(gr*cm/s^2)/cm^2]

% Number of partititions along the x axis:    
SECTIONS    = 512;

% BM:
area        = 0.5;					% [cm^2] cochlear cross section.
beta        = 0.003;        		% [cm] BM width.        
len         = 3.5;					% [cm] cochlear length.
rho         = 1.0;					% [g/cm^3] liquid density (Density of perilymph).
w_ohc       = 2*pi* 1000.0;         % [rad] OHC angular frequency.
K_ohc       = 0.01;         		% Normalaized constant.

% Middle Ear:
G_me        = 21.4;	% Mechanical gain of ossicles.
C_me        = (2*pi*1340)^2*0.059/(0.49*1.4); % (~6e6) Coupling of oval window displacement to ear canal pressure.       

% OW - Oval window:
C_ow        = 6e-3;        %0.032/0.011;     % (~2.909) Coupling of oval window to basilar membrane.
sigma_ow    = 0.5;      % 1.85     	% [g/cm^2] Oval window aerial density.
gamma_ow    = 20e3;      % 500    	% [1/sec] Middle ear damping constant.
f_ow        = 1500;     % 1500   	% [Hz]
w_ow        = 2*pi*f_ow;% [Hz] ~9424.8 Hz.


%% Simulation Parameters:

dx = len/SECTIONS;

% Longitudinal distance along the cochlea:
x = [0:dx:dx*(SECTIONS-1)]';
% x = linspace(0, len, SECTIONS)';


%% Data From Miriam's paper:
M0      = 1.286e-6;             % [g/cm^2] Const factor of the Basilar Membrane mass density per unit area.
M1      = 1.5;                  % [g/cm^2] Exponent factor of the Basilar Membrane mass density per unit area.
R0      = 0.25;                 % [g/(cm^2*sec)] Const factor of the Basilar Membrane mass resistance per unit area.
R1      = -0.06;                % [g/(cm^2*sec)] Exponent factor of the Basilar Membrane mass resistance per unit area.
S0      = 1.2821e4;             % [g/(cm^2*sec^2)] (elasticity) ExpConstonent factor of the Basilar Membrane mass stiffness per unit area.
S1      = -1.5;                 % [g/(cm^2*sec^2)] (elasticity) Exponent factor of the Basilar Membrane mass stiffness per unit area.

% psi(x, t):
alpha_s = 0.5;              	% D_L_ohc parameters.
alpha_L = 1;                    % D_L_ohc parameters.
 
M_bm = M0 * exp( M1 * x );   	% BM mass per unit area.
R_bm = R0 * exp( R1 * x );   	% BM losses (resistance\disipation) per unit area.
S_bm = S0 * exp( S1 * x );      % BM stiffnes (spring) per unit area.

% CF:
w_cf = sqrt(S_bm./M_bm);

% OHC:
alpha_1 = -1 * R_bm .* S_bm ./ M_bm;        % (SECTIONS x 1 ) for the P_ohc derivative.
alpha_2 = w_ohc * R_bm;                   	% (SECTIONS x 1 ) for the P_ohc derivative.

a1 = sigma_ow * w_ow^2 + G_me*C_me;   % bc_1


%% Angular frequency for the WKB simulation:
% f_min   = 1;            % [Hz]
% f_max   = 16e3;         % [Hz]
% N_f     = 512; %1000;        % [Hz]
% w       = 2*pi*linspace( f_min, f_max, N_f );
w = w_cf;
f = w./(2*pi);

% Number of frequency partititions:
f_SECTIONS = length(w);


%% WKB Solution:

F_bm    = zeros( SECTIONS, f_SECTIONS );      % Impedance of the organ of corti
F_ohc   = zeros( SECTIONS, f_SECTIONS );      % Impedance of the organ of corti
F_oc    = zeros( SECTIONS, f_SECTIONS );      % Impedance of the organ of corti
Z_oc    = zeros( SECTIONS, f_SECTIONS );      % Impedance of the organ of corti
K       = zeros( SECTIONS, f_SECTIONS );      % Wave number
P       = zeros( SECTIONS, f_SECTIONS );      % Pressure
V_bm_cm = zeros( SECTIONS, f_SECTIONS );


for Ix_w = 1:f_SECTIONS      % Run over all frequencies    
    
    % Current angular frequency:
    w_i = w(Ix_w);
        
    % Model's impedance:
    F_bm(:, Ix_w)  = F_OSC2( M_bm, R_bm, S_bm, [], w_i );
    %F_bm(:, Ix_w) = -1.0*w_i.^2*M_bm + 1j*w_i*R_bm + S_bm;
    F_ohc(:, Ix_w) = -1.0*F_PSI( w_ohc, alpha_2, alpha_1, [], w_i );    
    %F_ohc(:, Ix_w) = -1.0*( alpha_1 + 1j*w_i.*alpha_2 )./( 1j*w_i + w_ohc );
    
    F_oc(:, Ix_w)  = F_bm(:, Ix_w) + gamma.*F_ohc(:, Ix_w);
    
    % OC impedance:   
    Z_oc(:, Ix_w) = F_oc(:, Ix_w) ./ (1j*w_i);
    
    % Wave number:
    K(:, Ix_w) = sqrt( -1j*w_i*2*rho*beta ./ ( area.*Z_oc(:, Ix_w) ) ); 
    K0 = K(1, Ix_w);
    
    %
    e_plus = exp( 1j* dx * cumtrapz( K(:, Ix_w) ) );
    %e_plus = exp( 1j* dx * cumsum( K(:, Ix_w) ) );
    e_minus = 1./e_plus;
   
    C_w = e_minus(end).^2;
        
%     % B.C. - simple:
%     A2 = 2*w_i.^2*rho*P_in ./ ( sqrt(K(1, Ix_w)).*( 1 + C_w ) );
    
%     % B.C. - without the OW derivatives:
%     A2 = 2*w_i.^2*rho.*sqrt(K(1, Ix_w)).*P_in ./...
%        ( -2*w_i.^2*rho.*( 1 - C_w ) + K(1, Ix_w).*( 1 + C_w ).*( sigma_ow*w_ow^2 + G_me.*C_me ) );
    
%     % B.C. - without xi_ow_disp & xi_ow_sp:
%     D_w = -w_i^2;
%     A2 = (G_me*2*w_i^2*rho) * sqrt(K0) * P_in/...
%        ( 1j*sigma_ow*D_w*(1+C_w)*K0 - 2*rho*w_i^2*(1-C_w) );

    % B.C. with OW:
    D_w = -w_i^2 + 1j*w_i*gamma_ow + a1/sigma_ow;
    A2 = (G_me*2*w_i^2*rho*C_ow) * sqrt(K0) * P_in/...
        ( 1j*sigma_ow*D_w*(1+C_w)*K0 - 2*rho*w_i^2*(1-C_w) );
    
    P(:, Ix_w) = A2./sqrt(K(:, Ix_w)).*( e_minus - C_w.*e_plus );   % [Pa]
    

%     % --------------------------
%     % B.C. - Miriam's algorithm:
%     A2 = -2*w_i*rho / sqrt(K0);
%     P(:, Ix_w) = A2./sqrt(K(:, Ix_w)).*( e_minus );   % [Pa]


    % Calc the BM velosity:
    V_bm_cm(:, Ix_w) = P(:, Ix_w) ./ Z_oc(:, Ix_w);     % [cm/s]
    
end


% Calc the BM speed:
V_bm    = 0.01*V_bm_cm;             % [cm/s] -> [m/s] Velocity    



return;


%% Plot Results:

if ~exist('VbmAbs', 'var')
    error('To run this script you MUST run prof. Miriam''s <runModelWKB.m> code')
end

sumAbs_V_bm = sum(VbmAbs.^2, 2);
sumAbs_V_bm_ = sum(abs(V_bm_cm).^2, 1)';

figure(10)
plot( f, -10*log10([sumAbs_V_bm, sumAbs_V_bm_]) );
legend('\Sigma(|V_{bm}^{Miriam}|^2)', '\Sigma(|V_{bm}^{Oded}|^2)')
grid on
xlabel('f [Hz]')
ylabel('20*log_{10}( \Sigma(| V_{bm} |) )')
title( sprintf( '\\sigma_{ow} = %g, \\gamma_{ow} = %g', sigma_ow, gamma_ow) )

figure(11)
plot( f(1:end-1), -diff([sumAbs_V_bm/max(abs(sumAbs_V_bm)), sumAbs_V_bm_/max(abs(sumAbs_V_bm_))]) )
title( sprintf( 'Normalaized diff()\n\\sigma_{ow} = %g, \\gamma_{ow} = %g', sigma_ow, gamma_ow) )
grid on
xlabel('f [Hz]')
legend('diff_{normalized}(\Sigma(|V_{bm}^{Miriam}|^2))', 'diff_{normalized}(\Sigma(|V_{bm}^{Oded}|^2))')
ylabel('diff( \Sigma(| V_{bm} |) )/max( \Sigma(| V_{bm} |) )')


%% Plot Results (2):

ME_1 = F_OSC2( 1, 500, a1/1.85, [], w );            % the original (Talmadge) parameters
ME_2 = F_OSC2( 1, gamma_ow, a1/sigma_ow, [], w );

figure(15)
plot(1e-3*f, 20*log10(abs([ME_1, ME_2])))
xlabel('f [kHz]')
ylabel('20*log_{10}(|F_{me}|) [dB]')
title('F_{me} = \xi_{ow}\prime\prime + \gamma_{ow}\xi_{ow}\prime + (\omega_{ow} + G_{me}*C_{me}/\sigma_{ow})\xi_{ow}')
grid on

figure(16)
plot(1e-3*f, angle([ME_1, ME_2]))
xlabel('f [kHz]')
ylabel('\angle{F_{me}|}')
title('F_{me} = \xi_{ow}\prime\prime + \gamma_{ow}\xi_{ow}\prime + (\omega_{ow} + G_{me}*C_{me}/\sigma_{ow})\xi_{ow}')
grid on


%% Results:
figure(21)
semilogy(x, abs( V_bm(:, 10:50:end) ) )

xlabel( 'x [cm]' )
title( 'wkb old model - abs(V_bm)' )

figure(22)
plot(x, unwrap(angle( V_bm(:, 10:50:end)) ))
xlabel( 'x [cm]' )
title( 'wkb old model - \angle(V_bm)' )



%%

% % Model's impedance:
% F_bm_   = @(ww) F_OSC2( M_bm, R_bm, S_bm, [], ww );
% F_ohc_  = @(ww) -1*F_PSI( w_ohc, alpha_2, alpha_1, [], ww );
% F_oc_   = @(ww) F_bm_(ww) + gamma.*F_ohc_(ww);
% 
% F_bm_w   = @(Ixx) F_OSC2( M_bm(Ixx), R_bm(Ixx), S_bm(Ixx), [], w ).';
% F_ohc_w  = @(Ixx) -1*F_PSI( w_ohc, alpha_2(Ixx), alpha_1(Ixx), [], w ).';
% F_oc_w   = @(Ixx) F_bm_w(Ixx) + F_ohc_w(Ixx);
% 
% F_bm_tf  = @(Ixx) tf( [M_bm(Ixx), R_bm(Ixx), S_bm(Ixx)], 1 );
% F_ohc_tf = @(Ixx) tf( [alpha_2(Ixx),  alpha_1(Ixx)], [1,  w_ohc ] );
% 









