% 
% 
% results_DB_2_ISO.m
%  
% Description:
%   Reads the database and create a velosity-intensity graphas
% 
% Notes:
%   In this script I assume that the bin files were already replaced to the
%   mat files.
%
%

clc

%% Desired set to load from the bin files:
gamma       = 3* 0.5;

ISO_2_plot_v = 10.^[0:0.5:4];

% Choose the simulated type you which to load, e.g. BM_sp, BM_disp, TM_sp
% etc:
data_type = 'BM_sp';

% The first figure in the sequence: 
fig_num = 10


mu_scale = 1e6;


%%
% Path to the database files. Each gamma should has its own directory:
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_ME0\' ]; 
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_ME1\' ]; 
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_ME2\' ]; 
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_model_10\' ]; 
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_model_12\' ]; 
% data_path 	= [cd, '\Data\model_13_gamma_(', num2str(gamma), ')\' ]; 
% data_path 	= [cd, '\Data\' ]; 
data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')\' ]; 


% Load the database DB_stucture file:
dummy       = load( [data_path, 'DB_st.mat'], '-mat' );
DB_st       = dummy.DB_st;


%%

% CharacteriDB_stic frequency to look at in the VI plot:
DB_st.CF_2_chk     = 4e3;     % [Hz]

Ix_cf_in_vec = find( DB_st.CF_2_chk == fix(DB_st.f_sin_v), 1 );
if isempty( Ix_cf_in_vec )
    error(sprintf( 'ERROR: DB_st.CF_2_chk ~= DB_st.f_sin_v\n\tPlease Choose a CF frequency from the available CF vector <DB_st.f_sin_v>' ))
end

% ConDB_struct the model:
obj = CM_new_C( DB_st.Fs );

% Find the location (x index) of the frequency along the cochlea:
Ix_loc_in_cochlea = Freq_2_Ix( obj, DB_st.CF_2_chk, 2, 0 );

% Initialize more parameters:
N_spl	= length( DB_st.pressure_input_SPL_v );
N_freq	= length( DB_st.f_sin_v );


%% Create a DB_structure for the loaded data:
DB_st.Ix_f_v = Freq_2_Ix( obj, DB_st.f_sin_v, 2, 0 );
DB_st.VI  	 = zeros(N_spl, N_freq);

for i = 1:N_spl         % Run on the intensities
    
    fprintf( '\n%d. P_in = %g [dB SPL] (%d out of %d)\n', i, DB_st.pressure_input_SPL_v(i), i, N_spl );
    fprintf( '-------------------------------------\n' );
    
    for j = 1:N_freq    % Run on the excitation frequencies

        fprintf( '\t-> Loading j = #%d out of #%d\n', j, N_freq );
        
        % Load BM disp:
        [BM_sp, warn_msg] = Load_DB_File(...
            data_type,...
            gamma,...
            DB_st.pressure_input_SPL_v(i),...
            DB_st.f_sin_v(j),...
            DB_st.run_time,...
            ...DB_st.Fs,...
            data_path...
            );
        if isempty( BM_disp_ij )
            warning( warn_msg );
            continue;
        end
                
                        
        % Absolute value of the BM_disp at the desired frequency:
        BM_sp_abs_max = Calc_Amp_of_Sin(...
            BM_sp,...
            Ix_loc_in_cochlea,...
            DB_st.f_sin_v(j),...
            DB_st.Fs...
            );  
        
        DB_st.VI(i,j)  = max(BM_sp_abs_max);
        
        clear dummy
        
    end
    
end




%% Plot Results:

% ---
figure( fig_num )
% subplot(1,2,1)
% semilogx(  DB_st.f_sin_v, 20*log10(1e6*DB_st.VI'), '.-' );
loglog(  DB_st.f_sin_v, mu_scale* DB_st.VI', '.-' );
xlabel( 'f [Hz]' )
ylabel( 'Velosity [\mum/s] (in dB SPL)' )
title(...
    sprintf(...
        'Cochlea Time Model Simulation\n\\gamma = %g, x = %2.2g [cm] ( f = %g [kHz] )',...
        gamma,...
        obj.x(Ix_cf_in_vec),...
        1e-3* DB_st.CF_2_chk...
    )...
)

legend_freq_DB_str = cell(1,N_spl);
for i = 1:N_spl
    legend_freq_DB_str{i} = sprintf( '%g [dB SPL]',DB_st.pressure_input_SPL_v(i) );
end
legend( legend_freq_DB_str )


% -------------------------------
figure( fig_num+1 )

% subplot(1,2,2)
X = DB_st.pressure_input_SPL_v(ones(size(DB_st.VI,2),1),:)';
Y = DB_st.f_sin_v(ones(size(DB_st.VI,1),1),:);
% [C, h] = contour( Y, X, 20*log10(mu_scale*DB_st.VI), ISO_2_plot_v );
[C, h] = contour( Y, X, mu_scale* DB_st.VI, ISO_2_plot_v );

set(gca, 'XScale', 'log');

text_handle = clabel( C, h );
% text_handle = clabel( C, h, 'manual' );
set(...
    text_handle,...
    'BackgroundColor',[1 1 .6],...
    'Edgecolor',[.7 .7 .7]...
)
% colormap hot

xlabel( 'f [Hz]' )
ylabel( 'Sound Level [dB SPL]' )
title(sprintf( 'ISO-Output Curves\nThe yelow labels indicates BM velocity in [dB]' ))








