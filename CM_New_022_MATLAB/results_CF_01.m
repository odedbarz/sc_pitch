% 
% 
% wkb_new_model_005.m
% 
%   05/01/2011
%   Writen by Barzelay Oded
% 
%   Based on "2011-01-05 Prop 16_05_01.doc"  
%
%

clc

%% Desired set to load from the bin files:
gamma_v       = 3* [0.01, 0.1, 0.25, 0.5]; %[0.01, 0.1:0.1:0.5];% 

% Desired SPL to load (must also be available in the database):
pressure_input_SPL_v = 0;

% Choose the simulated type you which to load, e.g. BM_sp, BM_disp, TM_sp
% etc:
data_type = 'BM_sp';

% The first figure in the sequence: 
fig_num = 10;

% debug mode for the maximum absolute sinus calculation. <debug_mode> is
% also the number of the figure to plot in. To cancel the <debug_mode> just set it to be 
%   <debug_mode> = 0.
debug_mode = 0;    

% Path to the database files. Each gamma should has its own directory:
%data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_model_13\' ]; 
% data_path 	= [cd, '\Data\' ]; 

% CharacteriDB_stic frequency to look at in the max_disp plot:
%CF_2_lookAt     = 4e3;     % [Hz]


%% Model Parameters:

for i = 1:length(gamma_v)
    
    data_path = sprintf( '%s\\Data\\gamma_(%g)\\', cd, gamma_v(i) );
                    
    % Read data-base:
    [DB_st_i, obj_i] = Load_Simulated_Data(...
        data_type,...
        gamma_v(i),...
        data_path,...
        pressure_input_SPL_v...
    );

    Ix_cf2x_v(:,i) = DB_st_i.Ix_f2x_v;

    fprintf('\n')

end




%% Results:
    
figure(65)

    
    gamma_v = 1/3*gamma_v;

f_bm_cf     = 1/(2*pi)*sqrt(obj_i.S_bm./obj_i.M_bm);
semilogx( 1e-3* f_bm_cf, obj_i.x, '--' ); 

%% 
clear legend_str
legend_str{1}  = '\omega_{bm}^{no damping osc.}';

hold on
semilogx( 1e-3*DB_st_i.f_sin_v, obj_i.x( Ix_cf2x_v ), 'x-', 'LineWidth', 3, 'markersize', 14 ) 
hold off
   
%%
for i = 1:length(gamma_v)    
    legend_str{1+i} = sprintf( '\\gamma(x) = %g', gamma_v(i) );
    
end

% for i = 1:4  
%     if i <= 2
%         legend_str{1} = sprintf( '\\gamma^{WKB} = %g', gamma_v(i) );
%     else
%         legend_str{1+i} = sprintf( '\\gamma^{time} = %g', gamma_v(mod(i,2)+1) );
%     end
%     
% end

grid on
title('Characteristic Frequencies (CF) Map (freq. to cochlea location)')
ylabel('x [cm]')
xlabel('f [kHz]')

legend( legend_str );





