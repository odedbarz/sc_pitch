% 
% 
% results_DB_2_Audiogram.m
%  
% Description:
%   Construct Audiograms
% 
%
%

clc

%% Desired set to load from the bin files:
% gamma       = 0.5;

% Choose the simulated type you which to load, e.g. BM_sp, BM_disp, TM_sp
% etc:
data_type = 'BM_sp';

% The first figure in the sequence: 
fig_num = 30

%
mu_scale = 1e-6;


%%
% Path to the database files. Each gamma should has its own directory:
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_ME0\' ]; 
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_ME1\' ]; 
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_ME2\' ]; 
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_model_10\' ]; 
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_model_12\' ]; 
% data_path 	= [cd, '\Data\model_13_gamma_(', num2str(gamma), ')\' ]; 
% data_path 	= [cd, '\Data\Model_13_gamma(', num2str(gamma), ')_Fs(100kHz)\' ]; 
% data_path 	= [cd, '\Data\' ]; 


%%

% gamma vector:
gamma_v = [0.01 0.1:0.1:0.5];

% The normal (reference) gamma index:
gamma_normal_Ix = find(gamma_v == 0.5, 1, 'first');

% Load one of the databases:
data_path 	= [cd, '\Data\Model_13_gamma(', num2str(gamma_v(end)), ')_Fs(100kHz)\' ]; 

% Load the database DB_stucture file:
dummy       = load( [data_path, 'DB_st.mat'], '-mat' );
DB_st       = dummy.DB_st;

f_sin_v     = DB_st.f_sin_v([2, 5, 10, 11, 15, 17]);

% Initialize more parameters:
N_gamma	= length( gamma_v );
N_freq	= length( f_sin_v );


%% Create a DB_structure for the loaded data:

% Audiogram
audiogram_bm_sp_v = zeros(N_gamma, N_freq);

for i = 1:N_gamma % Run only the 0 dB SPL for the Audiogram
    
    fprintf( '\n%d. gamma = %g (%d out of %d)\n', i, gamma_v(i), i, N_gamma );
    fprintf( '-------------------------------\n' );
    
    gamma_i = gamma_v(i);
    
    data_path 	= [cd, '\Data\Model_13_gamma(', num2str(gamma_i), ')_Fs(100kHz)\' ]; 
    
    % Run on the excitation frequencies:
    for j = 1:N_freq    

        fprintf( '\t-> Loading j = #%d out of #%d\n', j, N_freq );
        
        % Load BM disp:
        [BM_sp, warn_msg] = Load_DB_File(...
            data_type,...
            gamma_i,...
            DB_st.pressure_input_SPL_v(i),...   % Run only the 0 dB SPL for the Audiogram
            f_sin_v(j),...
            DB_st.Fs,...
            data_path...
            );
        if isempty( BM_sp )
            warning( warn_msg );
            continue;
        end
         
        % Stimulus duration:
        T = size(BM_sp, 2)/DB_st.Fs;
        
        % 
        audiogram_bm_sp_v(i,j) = 20*log10( 1/T *sum(abs(BM_sp(:)).^2) );
        %audiogram_bm_sp_v(i,j) = 1/T *sum(abs(BM_sp(:)).^2);
        
    end
    
end




%% Plot FTC Results:

figure( fig_num )

for i = 1:N_gamma
    audiogram_bm_sp_ref_v(i,:) = audiogram_bm_sp_v(i,:) - audiogram_bm_sp_v(gamma_normal_Ix,:);
end

plot( f_sin_v, audiogram_bm_sp_ref_v' )
% plot( DB_st.f_sin_v, 20*log10(audiogram_bm_sp_ref_v) )

figure( fig_num+1 )
plot( f_sin_v, audiogram_bm_sp_v' )










