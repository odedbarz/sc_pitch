% 
% 
% results_NONLINEARITY.m
%  
% Description:
%   Reads the database and create a velosity-intensity graphas
% 
% Notes:
%   In this script I assume that the bin files were already replaced to the
%   mat files.
%
%

clc

%% Desired set to load from the bin files:
gamma       = 1.0;

% Choose the simulated type you which to load, e.g. BM_sp, BM_disp, TM_sp
% etc:
% data_type       = 'BM_disp';
% data_type_ref   = 'OW_disp';
data_type       = 'BM_sp';
data_type_ref   = 'OW_sp';


%%

% The first figure in the sequence: 
fig_num = 10

% % Compensate between Velosity and Displacement: 
% %   Velosity        -> plot_scale = 1e6 [mu]
% %   Displacement    -> plot_scale = 1e9 [n]
% if strcmpi('disp', data_type(end-3:end))        % Displacement
%     plot_scale = 1e9;
% 
% else    % Velocity
%     plot_scale = 1e6;
%     
% end

    plot_scale = 1e6;

% debug mode for the maximum absolute sinus calculation. <debug_mode> is
% also the number of the figure to plot in. To cancel the <debug_mode> just set it to be 
%   <debug_mode> = 0.
debug_mode = 0;    


%%
% Path to the database files. Each gamma should has its own directory:
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_ME0\' ]; 
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_ME1\' ]; 
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_ME2\' ]; 
% data_path 	= [cd, '\Data\model_13_gamma_(', num2str(gamma), ')\' ]; 
% data_path 	= [cd, '\Data\Model_13_gamma(', num2str(gamma), ')_Fs(100kHz)\' ]; 
data_path 	= [cd, '\Data\', model_type, '\' ]; 
% data_path 	= [cd, '\Data\' ]; 


% Load the database DB_stucture file:
dummy       = load( [data_path, 'DB_st.mat'], '-mat' );
DB_st       = dummy.DB_st;


%%

% ------------------------------------------------------------
% CharacteriDB_stic frequency to look at in the max_disp plot:
CF_2_chk = 4e3; % 0.6e3;     % [Hz]
% ------------------------------------------------------------

Ix_cf_in_vec = find( CF_2_chk == DB_st.f_sin_v, 1 );
if isempty( Ix_cf_in_vec )
    error(...
        sprintf( 'ERROR: CF_2_chk ~= DB_st.f_sin_v\n\tPlease Choose a CF frequency from the available CF vector <DB_st.f_sin_v>' )...
    )
end

% ConDB_struct the model:
obj = CM_new_C( DB_st.Fs );

% Find the location (x index) of the frequency along the cochlea:
Ix_loc_on_cochlea = Freq_2_Ix( obj, CF_2_chk, 2, 0 );

% Initialize more parameters:
N_spl	= length( DB_st.pressure_input_SPL_v );
Ix_available_freq_v = [ 1 == zeros(1) ];
DB_st.loudness = zeros(N_spl, 1);

%% Create a DB_structure for the loaded data:
DB_st.Ix_f_v = Freq_2_Ix( obj, DB_st.f_sin_v, 1, 0 );
DB_st.max_disp = zeros(N_spl, 1);

for i = 1:N_spl         % Run on the intensities
    
    fprintf( '\n%d. P_in = %g [dB SPL] (%d out of %d)\n', i, DB_st.pressure_input_SPL_v(i), i, N_spl );
    fprintf( '-------------------------------------\n' );
                    
        % Load BM disp:
        [BM_disp_ij, warn_msg] = Load_DB_File(...
            data_type,...
            gamma,...
            DB_st.pressure_input_SPL_v(i),...
            CF_2_chk,...
            DB_st.Fs,...
            data_path...
            );
        if isempty( BM_disp_ij )
            warning( warn_msg );
            continue;
        end
                
        % Load OW disp:        
        [OW_disp_ij, warn_msg] = Load_DB_File(...
            data_type_ref,...
            gamma,...
            DB_st.pressure_input_SPL_v(i),...
            CF_2_chk,...
            obj.Fs,...            
            data_path...
            );
        if isempty( OW_disp_ij )
            warning( warn_msg );
            continue;
        end              
        
        % Absolute value of the BM_disp at the desired frequency:
        BM_disp_abs_max = Calc_Amp_of_Sin(...
            BM_disp_ij,...
            Ix_loc_on_cochlea,...
            CF_2_chk,...
            DB_st.Fs,...
            debug_mode ...
            );  
        
        % CASE 1:        
        %pressure_input_Pa_i  = SPL_2_Pa( DB_st.pressure_input_SPL_v(i) );
        DB_st.loudness(i) = BM_disp_abs_max; %/pressure_input_Pa_i;
        
    
end


%% Plot loudness:

font_size   = 30;
font_weight = 'normal';

% -------------------------------------
figure( fig_num +1 )
% subplot( 2, 2, 3 )
% -------------------------------------

% plot_1 = loglog(  DB_st.f_sin_v, plot_scale* DB_st.loudness', '.-' );
plot_1 = plot( DB_st.pressure_input_SPL_v, 20*log10(plot_scale* DB_st.loudness'), '.-' );

% Set the axes font properties:
set( gca,...
    'FontSize',     font_size,...
    'FontWeight',   font_weight ...
);

xlabel( '[dB SPL]',...
    'FontSize',     font_size,...%font_size, ...
    'FontWeight',   font_weight ...
);

ylabel_str = data_type(1:2);    % BM or TM  
if strcmpi('disp', data_type(end-3:end))    % Displacement
    ylabel_str = [ylabel_str, ' Displacement'];

else                            % Velocity
    ylabel_str = [ylabel_str, ' Velocity'];
    
end
ylabel( ...
    sprintf( '%s Gain [dB re \\mum/(sec*Pa)]',...
        ylabel_str...
    ),...
    'FontSize',     font_size,...  %font_size,...
    'FontWeight',   font_weight ...
);   


grid on

title(sprintf( 'x = %2.3g[cm] & \\gamma(x) = %2.3g', obj.x(Ix_loc_on_cochlea), gamma) )

