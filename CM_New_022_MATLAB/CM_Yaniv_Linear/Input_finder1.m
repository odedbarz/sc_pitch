function Input_now = Input_finder1(now_time)
global Pe F_input M N R S h Row omega alpha1 alpha2 a0 a1 a2 Cme Gme Gamma IM_delta Wohc OUTPUT_RATE   SAMPLE_RATE% set the global variables for the use in functions.

% This function returns the sub step input by linear approximation
%
% deltay = after - before
% deltax = next_full_step_time - full_step_start_time
% slant = deltay/deltax
% delta_time = now_time - full_step_start_time
% output = before + slant*delta_time

%% check the next closest points in the F_input vector
full_step_start_time =  floor(now_time/SAMPLE_RATE);  % this is equal to floor(round(1/SAMPLE_RATE)*now_time)
next_full_step_time = full_step_start_time +1;

if full_step_start_time == 0
    fprintf(' ERROR in Input Finder - now_time entered OUTSIDE low time boundry \n');
    Input_now = 0;
    return;
end;

if full_step_start_time >= length(F_input)
    fprintf(' ERROR in Input Finder - requested a time OUTSIDE high time boundry \n');
    return;
end;

%% calculate the "linear fit" of the F_input between the two nearest point in the vector
Before = F_input(full_step_start_time);
After  = F_input(next_full_step_time);
Delta = (next_full_step_time - full_step_start_time)/(1/SAMPLE_RATE);
time_delta_from_step_start = now_time-full_step_start_time/(1/SAMPLE_RATE);

%% return the variable
Input_now = Before + time_delta_from_step_start*((After - Before)/Delta);