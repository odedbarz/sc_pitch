function [ORKwuv,ORKy,ORKOW_dt_dt] = One_Runge_Kutta_step(ORKu,ORKv,ORKx,ORKOW,ORKOW_dt,ORKInput_value);

global Pe F_input M N R S h Row omega alpha1 alpha2 a0 a1 a2 Cme Gme Gamma IM_delta Wohc OUTPUT_RATE   SAMPLE_RATE Kohc Psy0 alfaL% set the global variables for the use in functions.

% This is ONE RUNGE KUTTA STEP
%
% it uses RKstep as the step size, RKnow_T as the time
% it returns values in RKu, RKv, RKx, RKOW, RKOW_dt
%
% input :
% u is csiBM, v is csiBM_deltaT, wuv = csiBM_deltaT_deltaT
% x is Pohc, y is Pohc_deltaT, OW = csiOW, OW_dt = csiOW_deltaT
% OW_dt_dt = csiOW_deltaT_deltaT
%
% Written by Yaniv Halmut

% parameters for the nonlinear R
par1 = 1;
par2 = 0;  % 0.01;

% Different nonlinearities
%u_nln = (u./ (1-(u.*u).^(0.1)) );     % NOAM equation #1
%u_nln = (0.01*u.*log((utemp))+2*u);   % NOAM equation #2
%utemp = (abs(u) < eps).*eps + (abs(u)>=eps).*abs(u);

u_nln = ORKu;   % linear R
R_nln = R'.*(par1 + par2 .* (abs(ORKv).^2));
G =  - (R_nln.*ORKv + S'.*u_nln) + ORKx;

%Y(1,1) = 0.5*(h^2)*G(1)*omega(1)+(Gme.*-c2*-c3*)*h*c1;
Y(1)        = h*a0*(ORKInput_value-a1*ORKOW-a2*ORKOW_dt);
Y(2:N-1)    = (h^2)*G(2:N-1).*omega(2:N-1)';
Y(N)        = 0;
P = IM_delta * Y';

% Calculate output parameters
ORKwuv = (P + G)./M';
ORKy = Gamma.*(alpha1'.*ORKu + alpha2'.*ORKv) - ORKx.*Wohc;
ORKOW_dt_dt = (P(1)+Gme*ORKInput_value-a2*ORKOW_dt-a1*ORKOW)/1.85; % Sigma_ow=1.85














