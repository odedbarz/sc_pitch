%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The implementation in matlab of the Cochlear Model by Miriam Furst
% Written by Yaniv Halmut - June 2004 (start)
%
% 10/02/2005 turned into a sub-script in the big audiogram maker
%
% 02/03/2005 Changing time steps
% 03/03/2005 TIME STEP in place and working
%
% 04/04/2005 Changed so all parameters are setup in the parameter run...
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
global Pe F_input M N R S h Row omega alpha1 alpha2 a0 a1 a2 Cme Gme Gamma IM_delta Wohc OUTPUT_RATE   SAMPLE_RATE% set the global variables for the use in functions.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initialize conditions  (I start all the parameters as zeros)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

z = zeros(N,1);
csiBM_t = z;
csiBM_deltaT_t = z;
csiBM_deltaT_deltaT_t = z;
Pohc_t = z;
Pohc_deltaT_t = z;
csiOW_t = 0;
csiOW_deltaT_t = 0;

% stop_time = input_length(end);
simulation_length = size(input_length,2)/(OUTPUT_RATE/SAMPLE_RATE);

% DATA collection variables
csiOW_time = zeros(simulation_length,1);
Pe = zeros(simulation_length,1);
data = zeros(N,simulation_length);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% test variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

atemp = zeros(simulation_length,1);
btemp = zeros(simulation_length,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% start the simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('\n_________________________________________________\n');
fprintf('Simulation started - length is %g miliseconds \n',run_time*1000);   % show the simulation length
fprintf('_________________________________________________\n');

now_time = OUTPUT_RATE;
data_save_time = now_time + OUTPUT_RATE;
half_step = OUTPUT_RATE/2;
    print_num_delta = 250;
print_num = 250;
now_T = 1;    % the data matrix index

tic   % start the timer to measure the time the program runs

STEPS_PASSED = 0;

%% start of TIME loop
while now_time < (run_time - 2*half_step)

    %% time stamp Progress printing - every 250 cycles of the while loop
    if now_T>print_num
        print_num = print_num + print_num_delta;
        fprintf('time : %4g [ms]\n',(now_time-OUTPUT_RATE)*1000);    % show where in time we are.
    end
    
    u_before = csiBM_t;
    v_before = csiBM_deltaT_t;
    x_before = Pohc_t;
    OW_before = csiOW_t;
    OW_dt_before = csiOW_deltaT_t;

    time_step_start = now_time;                                  % for first small step
    time_step_middle = now_time + half_step;          % for second small step

    %% two small steps
    [RKu,RKv,RKx,RKOW,RKOW_dt] = Runge_Kutta1_ME(u_before,v_before,x_before,OW_before,OW_dt_before,time_step_start,half_step);
    [RKu2steps,RKv2steps,RKx2steps,csiOW_t,csiOW_deltaT_t] = Runge_Kutta1_ME(RKu,RKv,RKx,RKOW,RKOW_dt,time_step_middle,half_step);

    %% one big step
    [RKu1step,RKv1step,RKx1step,csiOW_t1step,csiOW_deltaT_t1step] = Runge_Kutta1_ME(u_before,v_before,x_before,OW_before,OW_dt_before,time_step_start,half_step*2);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% step compare
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % here we check to see if the algorithm is any good... we take the biggest delta and divide it by the current number
    % if the ratio is smaller then 1e-3 it is ok... else we check if the
    % current number is too small , and if it is not we alert  the user

%                     %% Oded, Debug
% 
%                     figure( 99 )
% 
%                     subplot( 2, 2, 1)
%                     plot( csiBM_t )
%                     legend( '\xi_{BM}' )
%                     grid on
%                     xlabel( 'x [smp]' )
% 
%                     subplot( 2, 2, 2)
%                     plot( csiBM_deltaT_t )
%                     legend( 'd\xi_{BM}\\dt' )
%                     grid on
%                     xlabel( 'x [smp]' )
% 
%                     subplot( 2, 2, 3)
%                     plot( Pohc_t )
%                     legend( 'P_{OHC}' )
%                     grid on
%                     xlabel( 'x [smp]' )
% 
%                     drawnow
%     
%     %%
    
    
    delta_temp = abs(RKv1step-RKv2steps);
    RKv1steps_temp = (RKv1step == 0)+RKv1step; %% replace zeros with ones so we don't have division by zero
    dd_ratio = delta_temp./abs(RKv1steps_temp);

    if max(dd_ratio) > 1e-2 %1/1000000

        if (half_step < 1e-9)
            fprintf('[%d]  STEP SIZE is TOO SMALL !!!!  %d \n',now_T,half_step);

        else
            half_step = half_step / 2;   % we half the stepsize only if it is not too small
        end;

        STEPS_PASSED = 0;
    else


        if sum(isnan(RKv2steps)) >= 1   % stop the run if there are Nan in the vectors            keyboard;
        end;

        if data_save_time <= now_time

            atemp(now_T) = half_step; % record the step sizes during the run

            data_save_time = data_save_time + OUTPUT_RATE;  % advance the "next data saving time"

            % data saving with linear extrapolation. we save the "exact"
            % value that was at the time we should have saved
            data(:,now_T) = csiBM_deltaT_t + ((RKv2steps-csiBM_deltaT_t)./(half_step*2)) .* (data_save_time-(now_time-half_step*2));
            csiOW_at_data_save_time = OW_before + ((csiOW_t-OW_before)./(half_step*2)) .* (data_save_time-(now_time-half_step*2));

            csiOW_time(now_T) = csiOW_at_data_save_time;        % OW displacement time analysis
            % calculate Pe = "Emission Pressure"
            Pe(now_T) = Input_finder1(time_step_start)-Cme*csiOW_at_data_save_time; % the constant is wrong ?!
            % advance the "saved data" index
            now_T = now_T + 1;
            
        end

        % check to see if we need to enlarge the step size UP
        STEPS_PASSED = STEPS_PASSED + 1;

        % advance the time
        now_time = now_time + half_step*2;

        % variable setup for next time step
        csiBM_t = RKu2steps;
        csiBM_deltaT_t = RKv2steps;
        Pohc_t = RKx2steps;

    end;
    
    if STEPS_PASSED > 100
        if half_step<OUTPUT_RATE/2                 % don't increase step size if we are at OUTPUT_RATE/2
            half_step = half_step *2;
        end;
    end;

end;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% the simulation has ended, tell the time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end_time = toc;
fprintf('\n the run took %.2g minutes \n',end_time/60);









