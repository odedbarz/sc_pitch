%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The implementation in matlab of the Cochlear Model by Miriam Furst
% THIS file is the one that loads the Gamma setup and the INPUT vector and
% runs the model simulation
% Written by Yaniv Halmut - June 2004 (start)
%
% 24/03/2005 mass running script for the pure sinus audiogram
%
% rewritten on 08/11/2005 for the comparison with the frequency model.
% added a masker to minimize low frequency contamination...
% changed filter calculations so they should include 200 time bins...
% 09/11/2005 - frequency model calculated csiBM and not csiBM_deltaT - when changed
% gives a good correlation with the time domain model results
%
% 11/11/2005 - updated to auditory model (with middle ear)
% 16/11/05 - corrected the csiOW_dt_dt error.
% 17/11/05 - changed into a click runing script
%
% 11/06/2010 - Modified by Oded in order to embed (and compare) the model into the CM
%              project.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% close all
%clear all;
global Pe F_input M N R S h Row omega alpha1 alpha2 a0 a1 a2 Cme Gme Gamma IM_delta Wohc OUTPUT_RATE SAMPLE_RATE % set the global variables for the use in functions.

%% General Model Parameters

% Cochlear_Length = 3.5;    	% [cm] cochlear length
% Row = 1;                    % [g/cm^3] Density of perilymph
% Beta = 0.003;               % [cm]  width of the basilar membrane
% A = 0.5;                    % [cm^2] cross-sectional area of the cochlea scalae
% Wohc = 2*pi*1000;           % [Hz] OHC cutoff frequency 1000 is NORMAL
% Fs = 50e3;                  % [Hz] Sample rate
% OUTPUT_RATE = 1/Fs;         % [sec] THE SAMPLE_RATE of the OUTPUT we want to recieve 
% N = 512;                    % [smp] number of sections along the cochlea
% 
% G_gamma     = 0.5;          % Gamma's gain
% f_sin       = 1000;         % [Hz] Sinus frequency
% G_sin       = 1e-10;        % Sinus amplitude
% stop_time 	= 0.02;         % [sec] input duration
% 
%     run_time = stop_time;

Cochlear_Length = obj.len;          % [cm] cochlear length
Row             = obj.rho;          % [g/cm^3] Density of perilymph
Beta            = obj.beta;      	% [cm]  width of the basilar membrane
A               = obj.area;        	% [cm^2] cross-sectional area of the cochlea scalae
Wohc            = obj.w_ohc;       	% [Hz] OHC cutoff frequency 1000 is NORMAL
OUTPUT_RATE   	= 1/Fs;         	% [sec] THE SAMPLE_RATE of the OUTPUT we want to recieve 
N               = obj.SECTIONS;   	% [smp] number of sections along the cochlea



%% Create Gamma file

% Gamma = G_gamma * ones(N, 1);
% fprintf( '\t->Gamma(1) = %g\n', Gamma(1) )
Gamma = gamma;


%% Create the INPUT vector

SAMPLE_RATE = OUTPUT_RATE;    	% SAMPLE RATE in seconds
input_length = [ SAMPLE_RATE : SAMPLE_RATE : run_time ];

% % For a SINUS DPs
% fprintf( '\t->The input was set to be a simple sinus\n' )
% F_input = G_sin * sin( 2*pi * f_sin * input_length );
F_input = input;

%% Constant calculations

h = Cochlear_Length/N;            	% in cm (h = 0.1 cm)
Len = 0 : h : Cochlear_Length-h; 	% a vector for the x's along the cochlea (also in cm)

% M = 1.286*10^-6*exp(1.5*Len);       % in g/cm^2
% S = 1.282*10^4*exp(-1.5*Len);       % in g/cm^2*s^2
% R = 0.25*exp(-0.06*Len);            % in g/cm^2*s
M = obj.M_bm';
S = obj.S_bm';
R = obj.R_bm';

% A constant vector: 
% omega  = (2*Row*Beta/A)./M;
omega = obj.Q';                      % 2*rho*beta/(area*_M)			

% % OHC - d_Pohc ODE coefficients: (Oded):
% alpha1 = -(R.*S)./M;
% alpha2 = R.*Wohc;
alpha1 = obj.alpha_1';      % (SECTIONS x 1 ) for the P_ohc derivative.
alpha2 = obj.alpha_2';      % (SECTIONS x 1 ) for the P_ohc derivative.


% Middle ear model Constants
% --------------------------
% c1 = (0.0055*1100)/1.85;       	% this is in [1 / cm]
% c2 = 500*1.85;                 	% this is in [g / cm^2 * sec]
% c3 = 1.85*((2*pi*1500)^2);      	% this is in [g*hz/cm^2]

% Gme = 21.4;
% Cme = (2*pi*1340)^2*0.059/(0.49*1.4);
Gme = obj.G_me;                     %21.4;
Cme = obj.C_me;                     %(2*pi*1340)^2*0.059/(0.49*1.4);

% a0 = 2*Row*0.032/(1.85*0.011);   	% a0 = 2*rho*Cow/Sigma_ow
% a1 = 1.85*(2*pi*1500)^2+Gme*Cme;   	% a1=Sigma_ow*Omega_ow^2+Gme*Cme % a1 = Sigma_ow*Omega_ow^2 + Gme*Cme
% a2 = 1.85*500;                     	% a2 = a0*Sigma_ow*Gamma_ow
a0 = obj.a0;                        %2*Row*0.032/(1.85*0.011);      % a0 = 2*rho*Cow/Sigma_ow
a1 = obj.a1;                        %1.85*(2*pi*1500)^2+Gme*Cme;   	% a1=Sigma_ow*Omega_ow^2+Gme*Cme % a1 = Sigma_ow*Omega_ow^2 + Gme*Cme
a2 = obj.a2;                        %1.85*500;                     	% a2 = a0*Sigma_ow*Gamma_ow

%% Delta Matrix ( calculated only once )

init = -(1 + h*a0);                     % place 1 if omega is equal to X0

H0  = [init -(2+omega(2:N-1)*h^2)  1];
H1  = ones(1,N-1);
H_1 = [ones(1,N-2) 0];
H2  = diag(H0) + diag(H1,1) + diag(H_1,-1);

% calculate the inverse matrix:
IM_delta = inv(H2);
clear H2;                               % remove the big matrix from memory for better speed


% IM_delta = obj.triMat_inv;


%% RUN the simulation
    
cochlea_model_time_step1_ME;	% --> data

%Show Results 
% showRAWdata(data,Len)
%  PlotPe

% % QA - Oded
% plot( input_length, data(80, :) )
% grid on
