function [RKu,RKv,RKx,RKOW,RKOW_dt] = Runge_Kutta1_ME(u,v,x,OW,OW_dt,RKnow_T,RKstep)
global Pe F_input M N R S h Row omega alpha1 alpha2 a0 a1 a2 Cme Gme Gamma IM_delta Wohc OUTPUT_RATE   SAMPLE_RATE Kohc Psy0 alfaL% set the global variables for the use in functions.

% This is the implementation of the Fourth Order Runge Kutta Method :
% Four steps are used.
%
% Rewritten by Yaniv Halmut 17/11/2005

% calculate the different input values needed for all the four steps
Input_value_at_step_start     = Input_finder1(RKnow_T);
Input_value_at_step_middle    = Input_finder1(RKnow_T+RKstep/2);
Input_value_at_step_end       = Input_finder1(RKnow_T+RKstep);

[wuv,y,OW_dt_dt] = One_Runge_Kutta_step(u,v,x,OW,OW_dt,Input_value_at_step_start);

% extrapolate variables for second RK step
u1 = u + 0.5*RKstep*v;
v1 = v + 0.5*RKstep*wuv;
x1 = x + 0.5*RKstep*y;
OW1 = OW + 0.5*RKstep*OW_dt;
OW_dt1 = OW_dt + 0.5*RKstep*OW_dt_dt;

[wuv1,y1,OW_dt_dt1] = One_Runge_Kutta_step(u1,v1,x1,OW1,OW_dt1,Input_value_at_step_middle);

% extrapolate variables for third RK step
u2 = u + 0.5*RKstep*v1;
v2 = v + 0.5*RKstep*wuv1;
x2 = x + 0.5*RKstep*y1;
OW2 = OW + 0.5*RKstep*OW_dt1;
OW_dt2 = OW_dt + 0.5*RKstep*OW_dt_dt1;

[wuv2,y2,OW_dt_dt2] = One_Runge_Kutta_step(u2,v2,x2,OW2,OW_dt2,Input_value_at_step_middle);

% extrapolate variables for fourth RK step
u3 = u + RKstep*v2;
v3 = v + RKstep*wuv2;
x3 = x + RKstep*y2;
OW3 = OW + 0.5*RKstep*OW_dt2;
OW_dt3 = OW_dt + 0.5*RKstep*OW_dt_dt2;

[wuv3,y3,OW_dt_dt3] = One_Runge_Kutta_step(u3,v3,x3,OW3,OW_dt3,Input_value_at_step_end);

%% Calculate and return Fourth Order Runge Kutta variables
RKu = u + (RKstep/6)*(v+2*v1+2*v2+v3);
RKv = v + (RKstep/6)*(wuv+2*wuv1+2*wuv2+wuv3);
RKx = x + (RKstep/6)*(y+2*y1+2*y2+y3);
RKOW = OW + (RKstep/6)*(OW_dt+2*OW_dt1+2*OW_dt2+OW_dt3);
RKOW_dt = OW_dt + (RKstep/6)*(OW_dt_dt+2*OW_dt_dt1+2*OW_dt_dt2+OW_dt_dt3);