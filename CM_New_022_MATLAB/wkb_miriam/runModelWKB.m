%runModelWKB


Amp=0.5;
SetParametersA; 
etaX=0;

% fn=find(CF/(2*pi)/(Fs/2)<1);
% Wf=[2*pi*(Fs/2) CF(fn) 0 ];
Wf = sqrt(w2CF);

for i= 1:length(Wf)-1
    w=Wf(i);
    s=sqrt(-1)*w;
    %ohc=Amp.*((s-w1)./(s+w0));
    %Z=m*s*s+r*s+k-psy.*ohc;
    ReZbm=r;
    ReZohc=r0-Amp.*(alfa2.*w0-alfa1)./(w*w+w0*w0);
    ReZ=ReZbm+ReZohc-r0;
    ImZbm=m*w-k./w;
    ImZohc=(Amp./w).*(alfa1.*w0+alfa2.*w.*w)./(w*w+w0*w0); %OHC only
    ImZ=ImZbm+ImZohc;    
    Z=complex(ReZ,ImZ);
    %Zbm=complex(ReZbm,ImZbm);                          % masked by oded
    %Zohc(i,:)=complex(ReZohc,ImZohc);                  % masked by oded
    %Z=complex(ReZbm,ImZbm);
    %Z=complex(ReZohc,ImZohc);

    %Gec=1/(w_ow*gama_ow)

    %Dme=1./(sigma_ow.*(-w*w+w_ow*w_ow-s*gama_ow+F));   % masked by oded
    %Rme=2*ro*w*w.*Dme;                                 % masked by oded
    eta=-(2*s*ro*B)./(A*Z);
    etaQ=sqrt(eta);
    etaX=cumsum(etaQ)*h;
    P=-2*w*ro./(sqrt(etaQ(1)).*sqrt(etaQ)).*exp(-s*etaX/w);
    V=P./Z;
    
    %PP(i,:) = P;
    
    %Pbm(i,:)=Zbm.*V;
    %Pohc(i,:)=Zohc(i,:).*V;

    VbmAbs(i,:)=abs(V);            
    
    % Added by Oded:
    V_bm_ref(i,:) = V;
    
end


VbmAbs(length(Wf),:)=0;

V0=max(VbmAbs); %max of each filter
Vmax=max(max(VbmAbs));
Alpha=1./(V0);
for n=1:N
    VbmAbs0(:,n)= VbmAbs(:,n)*Alpha(n);
end

Va=VbmAbs0';

    % Added by Oded:
    V_bm_ref( length(Wf), : ) = 0;


   
   

    