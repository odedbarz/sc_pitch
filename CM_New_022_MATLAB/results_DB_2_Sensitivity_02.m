% 
% 
% results_DB_2_Sensitivity.m
%  
% Description:
%   Reads the database and create a velosity-intensity graphas
% 
% Notes:
%   In this script I assume that the bin files were already replaced to the
%   mat files.
%
%

clc

%% Desired set to load from the bin files:
gamma       = 3*0.5;

% Choose the simulated type you which to load, e.g. BM_sp, BM_disp, TM_sp
% etc:
% data_type       = 'BM_disp';
% data_type_ref   = 'OW_disp';
data_type       = 'TM_disp';
data_type_ref   = 'OW_disp';
% data_type       = 'TM_sp';
% data_type_ref   = 'OW_sp';
% data_type       = 'BM_sp';
% data_type_ref   = 'OW_sp';

%%

% The first figure in the sequence: 
fig_num = 20

% % Compensate between Velosity and Displacement: 
% %   Velosity        -> plot_scale = 1e6 [mu]
% %   Displacement    -> plot_scale = 1e9 [n]
% if strcmpi('disp', data_type(end-3:end))        % Displacement
%     plot_scale = 1e9;
% 
% else    % Velocity
%     plot_scale = 1e6;
%     
% end

    plot_scale = 1e9;

% debug mode for the maximum absolute sinus calculation. <debug_mode> is
% also the number of the figure to plot in. To cancel the <debug_mode> just set it to be 
%   <debug_mode> = 0.
debug_mode = 0;    


%%
% Path to the database files. Each gamma should has its own directory:
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_ME0\' ]; 
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_ME1\' ]; 
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_ME2\' ]; 
% data_path 	= [cd, '\Data\model_13_gamma_(', num2str(gamma), ')\' ]; 
% data_path 	= [cd, '\Data\Model_13_gamma(', num2str(gamma), ')_Fs(100kHz)\' ]; 
% data_path 	= [cd, '\Data\Model_14_gamma_(', num2str(gamma), ')\' ]; 
% data_path 	= [cd, '\Data\' ]; 
data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')\' ]; 


% Load the database DB_stucture file:
dummy       = load( [data_path, 'DB_st.mat'], '-mat' );
DB_st       = dummy.DB_st;


%%

% ------------------------------------------------------------
% CharacteriDB_stic frequency to look at in the max_disp plot:
DB_st.CF_2_chk = 1e3; % 0.6e3;     % [Hz]
% ------------------------------------------------------------

Ix_cf_in_vec = find( DB_st.CF_2_chk == DB_st.f_sin_v, 1 );
if isempty( Ix_cf_in_vec )
    error(...
        sprintf( 'ERROR: DB_st.CF_2_chk ~= DB_st.f_sin_v\n\tPlease Choose a CF frequency from the available CF vector <DB_st.f_sin_v>' )...
    )
end

% ConDB_struct the model:
obj = CM_new_C( DB_st.Fs );

% Find the location (x index) of the frequency along the cochlea:
Ix_loc_on_cochlea = Freq_2_Ix( obj, DB_st.CF_2_chk, 2, 0 );

% Initialize more parameters:
N_spl	= length( DB_st.pressure_input_SPL_v );
N_freq	= length( DB_st.f_sin_v );
Ix_available_freq_v = [ 1 == zeros(1, N_freq) ];
DB_st.sensitivity = zeros(N_spl, N_freq);

%% Create a DB_structure for the loaded data:
DB_st.Ix_f_v = Freq_2_Ix( obj, DB_st.f_sin_v, 2, 0 );
DB_st.max_disp  	 = zeros(N_spl, N_freq);

for i = 1:N_spl         % Run on the intensities
    
    fprintf( '\n%d. P_in = %g [dB SPL] (%d out of %d)\n', i, DB_st.pressure_input_SPL_v(i), i, N_spl );
    fprintf( '-------------------------------------\n' );
    
    for j = 1:N_freq    % Run on the excitation frequencies

        fprintf( '\t-> Loading j = #%d out of #%d\n', j, N_freq );
                
        % Load BM disp:
        [BM_disp_ij, warn_msg] = Load_DB_File(...
            data_type,...
            gamma,...
            DB_st.pressure_input_SPL_v(i),...
            DB_st.f_sin_v(j),...
            DB_st.run_time,...
            ...DB_st.Fs,...
            data_path...
            );
        if isempty( BM_disp_ij )
            warning( warn_msg );
            continue;
        end
                
        % Load OW disp:        
        [OW_disp_ij, warn_msg] = Load_DB_File(...
            data_type_ref,...
            gamma,...
            DB_st.pressure_input_SPL_v(i),...
            DB_st.f_sin_v(j),...
            DB_st.run_time,...
            ...DB_st.Fs,...
            data_path...
            );
        if isempty( OW_disp_ij )
            warning( warn_msg );
            continue;
        end              
        
        % Absolute value of the BM_disp at the desired frequency:
        BM_disp_abs_max = Calc_Amp_of_Sin(...
            BM_disp_ij,...
            Ix_loc_on_cochlea,...
            DB_st.f_sin_v(j),...
            DB_st.Fs,...
            debug_mode ...
            );  
        
        % CASE 1:        
        pressure_input_Pa_i    = SPL_2_Pa( DB_st.pressure_input_SPL_v(i) );
        DB_st.sensitivity(i,j) = BM_disp_abs_max/pressure_input_Pa_i;
        
          %         % CASE 2:
%         % Absolute value of the OW_disp at the desired frequency:
%         OW_disp_abs_max = Calc_Amp_of_Sin(...
%             OW_disp_ij.',...
%             1,...
%             DB_st.f_sin_v(j),...
%             DB_st.Fs,...
%             0 ...
%             );  
%         DB_st.sensitivity(i,j) = BM_disp_abs_max/OW_disp_abs_max;
        
%         % Ow to stimulus level TF:
%         OW_disp_abs_max = Calc_Amp_of_Sin(...
%             OW_disp_ij.',...
%             1,...
%             DB_st.f_sin_v(j),...
%             DB_st.Fs,...
%             0 ...
%             );  
%         OW2Pa(i,j) = OW_disp_abs_max/pressure_input_Pa_i;

    end
    
end


%% Plot Sensitivity:

font_size   = 30;
font_weight = 'normal';

% -------------------------------------
figure( fig_num +1 )
% subplot( 2, 2, 3 )
% -------------------------------------

% plot_1 = loglog(  DB_st.f_sin_v, plot_scale* DB_st.sensitivity', '.-' );
plot_1 = semilogx(  DB_st.f_sin_v, 20*log10(plot_scale* DB_st.sensitivity'), '.-' );

% Set the axes font properties:
set( gca,...
    'FontSize',     font_size,...
    'FontWeight',   font_weight ...
);

xlabel( 'Frequency [kHz]',...
    'FontSize',     font_size,...%font_size, ...
    'FontWeight',   font_weight ...
);

ylabel_str = data_type(1:2);    % BM or TM  
if strcmpi('disp', data_type(end-3:end))    % Displacement
    ylabel_str = [ylabel_str, ' Displacement'];

else                            % Velocity
    ylabel_str = [ylabel_str, ' Velocity'];
    
end
ylabel( ...
    sprintf( '%s Gain [dB re \\mum/(sec*Pa)]',...
        ylabel_str...
    ),...
    'FontSize',     font_size,...  %font_size,...
    'FontWeight',   font_weight ...
);   

% set all lines and legend (graphic) properties:
legend_freq_DB_str  = cell(1, N_spl);
all_marker_hnd      = set( plot_1(1), 'marker' );
all_marker_hnd = all_marker_hnd([1:3, 5:end]);    % Remove the point


for i = 1:N_spl
    % Legend:
    legend_freq_DB_str{i} = sprintf( '%g [dB SPL]',DB_st.pressure_input_SPL_v(i) );
        
    % Lines:
    set( plot_1(i),...
        'Marker',           all_marker_hnd{i},...
        'MarkerSize',       10, ...
        'LineWidth',        2.5, ...
        'MarkerFaceColor',  get( plot_1(i), 'Color' )...
        );
    
end
legend( legend_freq_DB_str,...
    'Location',     'NorthWest',...
    'FontSize',     font_size,...
    'FontWeight',   font_weight ...
)

% axis([ DB_st.f_sin_v(1), DB_st.f_sin_v(end), 1e-2, 3e4 ]);
% axis([ DB_st.f_sin_v(1), DB_st.f_sin_v(end), -50, 150 ]);
grid on

title(sprintf( 'x = %2.3g[cm] & \\gamma(x) = %2.3g', obj.x(Ix_loc_on_cochlea), gamma) )


%% Plot for Comparison with Ruggero et al. 

font_size   = 30;
font_weight = 'normal';

% -------------------------------------
figure( fig_num +2 )
% subplot( 2, 2, 3 )
% -------------------------------------

f_norm      = 4e3;      % Normalize the current model by this frequency
g_compare   = 3e-5;     % Normalaize by this constant 

% plot_1 = loglog(  DB_st.f_sin_v, plot_scale* DB_st.sensitivity', '.-' );
plot_2 = loglog( DB_st.f_sin_v/f_norm, g_compare*plot_scale* DB_st.sensitivity', 'x-' );


% Set the axes font properties:
set( gca,...
    'FontSize',     font_size,...
    'FontWeight',   font_weight ...
);

xlabel( 'Normalaized Frequency [kHz]',...
    'FontSize',     font_size,...%font_size, ...
    'FontWeight',   font_weight ...
);

ylabel_str = data_type(1:2);    % BM or TM  
if strcmpi('disp', data_type(end-3:end))    % Displacement
    ylabel_str = [ylabel_str, ' Displacement'];

else                            % Velocity
    ylabel_str = [ylabel_str, ' Velocity'];
    
end
ylabel( ...
    sprintf( '%s Gain\n[dB re \\mum/(sec\\cdotPa)]',...
        ylabel_str...
    ),...
    'FontSize',     font_size,...  %font_size,...
    'FontWeight',   font_weight ...
);   

% % set all lines and legend (graphic) properties:
% legend_freq_DB_str  = cell(1, N_spl);
% all_marker_hnd      = set( plot_2(1), 'marker' );

for i = 1:N_spl
    % Legend:
    legend_freq_DB_str{i} = sprintf( '%g dB SPL',DB_st.pressure_input_SPL_v(i) );
        
    % Lines:
    set( plot_2(i),...
        'Marker',           all_marker_hnd{i},...
        'MarkerSize',       10, ...
        'LineWidth',        2.5, ...
        'MarkerFaceColor',  get( plot_2(i), 'Color' )...
        );
    
end
legend( legend_freq_DB_str,...
    'Location',     'NorthWest',...
    'FontSize',     font_size,...
    'FontWeight',   font_weight ...
)

% axis([ DB_st.f_sin_v(1), DB_st.f_sin_v(end), 1e-2, 3e4 ]);
% axis([ DB_st.f_sin_v(1), DB_st.f_sin_v(end), -50, 150 ]);
grid on

title(sprintf( 'x = %2.3g cm & \\gamma(x) = %2.3g', obj.x(Ix_loc_on_cochlea), gamma) )

% axis([0.01, 3, 0.1, 10^4])


























