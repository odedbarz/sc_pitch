

function y = F_OSC2(M, R, S, Ix, w)

if isempty(Ix)
    y = -1.0*w.^2*M + 1j*w*R + S;
    
elseif 1 <= size(Ix,2) && 1 <= size(w,2)
    N_f = size(w,2);
    y = -1.0*w.^2.*M(:,ones(N_f,1)) + 1j*w.*R(:,ones(N_f,1)) + S(:,ones(N_f,1));
    
else
    y = -1.0*w.^2*M(Ix) + 1j*w*R(Ix) + S(Ix);
    
end

