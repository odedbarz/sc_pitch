

function sin_abs_max = Calc_Amp_of_Sin(...
    M,...
    Ix_cf_cochlea,...
    f_sin_2_chk,...
    Fs,...
    debug_mode...
    )  

% Boundaries (in indexes) to look for peaks around the calculated frequency
% index:
bnd_freq_v = [-10:10];

if ~exist('debug_mode', 'var' )
    debug_mode = 0;     % default    
end

V_ij            = M(Ix_cf_cochlea,:).';
N_t             = size(V_ij,1);
fff             = [0:(N_t-1)]'/N_t*Fs;    % ASSUMING a known time dimension length
Ix_frq_fft      = find( fff >= f_sin_2_chk, 1, 'first' );
Ix_fft_bnd      = bnd_freq_v + Ix_frq_fft;    % fft boundaries
Ix_fft_bnd      = unique(min(Ix_fft_bnd, N_t));
Ix_fft_bnd      = unique(max(Ix_fft_bnd, 1));
fV_abs_norm_ij	= abs( 2/N_t* fft(V_ij).' );

% Find the absolute frequency factor:
warning('off')
[sin_abs_max, Ix_abs_max] = findpeaks( fV_abs_norm_ij(Ix_fft_bnd) );
warning('on')

% in case of multiple peaks, get the largest:
if length( sin_abs_max ) > 1
    [dummy, Ix_biggest_peak] = max(sin_abs_max);
    sin_abs_max             = sin_abs_max(Ix_biggest_peak);
    Ix_abs_max              = Ix_abs_max(Ix_biggest_peak);
end


% In case there is no peak at all in this location along the cochlea:
if isempty( Ix_abs_max )
    Ix_abs_max              = Ix_frq_fft;
    sin_abs_max             = fV_abs_norm_ij(Ix_abs_max);    
    abs_max_indx_location   = Ix_abs_max;
    
else
    abs_max_indx_location   = min(Ix_fft_bnd)+Ix_abs_max-1;
    
end


%% Debug Mode - Plot the results on screen:

if 0 ~= debug_mode
    
    font_size   = 12;
    font_weight = 'bold';
    
    figure( 10 )
    %subplot(2,2,debug_mode)    
        
    abs_fft_V_ij = Pa_2_SPL(1e6* fV_abs_norm_ij);
    
    plot( 1e-3* fff(1:fix(end/2)), abs_fft_V_ij(1:fix(end/2)) )
    hold on
    plot( 1e-3* fff(Ix_fft_bnd), abs_fft_V_ij(Ix_fft_bnd), 'r' )
    plot( 1e-3* fff(abs_max_indx_location), Pa_2_SPL(1e6* sin_abs_max), 'xk', 'MarkerSize', 12, 'LineWidth', 2 )
    hold off
        
    % Set the axes font properties:
    set( gca,...
        'FontSize',     font_size,...
        'FontWeight',   font_weight ...
    );

    xlabel( 'f [Hz]',...
        'FontSize',     14,...%font_size, ...
        'FontWeight',   font_weight ...
    );

    ylabel( 'Velosity [\mum/s]',...
        'FontSize',     14,...%font_size,...
        'FontWeight',   font_weight ...
    );
        
    %title(sprintf( 'max(abs(2/N*fft( sin(2*\\pi*f_0*t) ))) = %g, f_0 = %g [kHz]', sin_abs_max, 1e-3* f_sin_2_chk ))
    title(sprintf( 'f_{sin} = %g [kHz]', 1e-3* f_sin_2_chk ))
    axis( [0, 16, -20+min(abs_fft_V_ij), 20+max(abs_fft_V_ij) ] )
    
end







