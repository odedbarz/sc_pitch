% 
% 
% results_DB_2_JND.m
%  
% Description:
%   Reads the database and create a velosity-intensity graphas
% 
% Notes:
%   In this script I assume that the bin files were already replaced to the
%   mat files.
%
%

clc

%% Add Tal Klap's (Thesis) Code to the path:
current_path = path;
addpath([cd,'\Tal_Thesis_Code_03-04-2011\']);


%% Desired set to load from the bin files:
gamma = 0.5;

% Characteristic frequency to look at:
CF_2_chk = 4e3;         % [Hz]

% Choose the simulated type you which to load, e.g. mtData, BM_disp, TM_sp
% etc:
data_type = 'BM_sp';

% IHC filter type to apply:
IHC_filter_type = 'hamming';

% The first figure in the sequence: 
fig_num = 30


%%
% Path to the database files. Each gamma should has its own directory:
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_ME0\' ]; 
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_ME1\' ]; 
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_ME2\' ]; 
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_model_10\' ]; 
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_model_12\' ]; 
data_path 	= [cd, '\Data\Model_13_gamma(', num2str(gamma), ')_Fs(100kHz)\' ]; 
% data_path 	= [cd, '\Data\' ]; 


% Load the database DB_stucture file:
dummy       = load( [data_path, 'DB_st.mat'], '-mat' );
DB_st       = dummy.DB_st;


%%

% CharacteriDB_stic frequency to look at in the JND plot:
DB_st.CF_2_chk = CF_2_chk;     % [Hz]

Ix_cf_in_vec = find( DB_st.CF_2_chk == fix(DB_st.f_sin_v), 1 );
if isempty( Ix_cf_in_vec )
    error(sprintf( 'ERROR: DB_st.CF_2_chk ~= DB_st.f_sin_v\n\tPlease Choose a CF frequency from the available CF vector <DB_st.f_sin_v>' ))
end

% ConDB_struct the model:
obj = CM_new_C( DB_st.Fs );

% Find the location (x index) of the frequency along the cochlea:
Ix_loc_in_cochlea = Freq_2_Ix( obj, DB_st.CF_2_chk, 2, 0 );

% Initialize more parameters:
N_spl	= length( DB_st.pressure_input_SPL_v );
N_freq	= length( DB_st.f_sin_v );


%% Create a DB_structure for the loaded data:
DB_st.Ix_f_v = Freq_2_Ix( obj, DB_st.f_sin_v, 2, 0 );
DB_st.JND  	 = zeros(N_spl, N_freq);

for i = 1:N_spl         % Run on the intensities
    
    fprintf( '\n%d. P_in = %g [dB SPL] (%d out of %d)\n', i, DB_st.pressure_input_SPL_v(i), i, N_spl );
    fprintf( '-------------------------------------\n' );
    
    for j = 1:N_freq    % Run on the excitation frequencies

        fprintf( '\t-> Loading j = #%d out of #%d (f = %g [kHz])\n', j, N_freq, DB_st.f_sin_v(j) );
        
        % Load BM disp:
        [mtData, warn_msg] = Load_DB_File(...
            data_type,...
            gamma,...
            DB_st.pressure_input_SPL_v(i),...
            DB_st.f_sin_v(j),...
            DB_st.Fs,...                        
            data_path...
            );        
        if isempty( mtData )
            warning( warn_msg );
            continue;
        end
        
        % Half-rectifier:
        data_v_ij       = mtData(Ix_loc_in_cochlea,:)'; 
        fData_v_ij      = abs(fft(data_v_ij));
        rate_v_ij       = synapseModel( fData_v_ij, 'med', DB_st.Fs );
        
        % Apply IHC LP filter:
        diff_rate_v_ij = [diff(rate_v_ij); 0];
        
        % Using CRLB (optimal estimator) to set the JND:
        DB_st.JND(i,j) = sum( 1./rate_v_ij .* diff_rate_v_ij.^2 )^(-0.5);
               
    end
    
end


%% Remove Tal Klap's (Thesis) Code from the path:
path(current_path);


%% Apply IHC LP filter:

semilogy( DB_st.JND )










