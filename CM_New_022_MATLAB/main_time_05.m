% -------------------------------------------------------------------------
%
% Written by Barzelay Oded - 14/03/2011
% 
% -------------------------------------------------------------------------
%
% main_time_04.m
%
%   A simple script which runs the non-linear Cochlea model (CM_Time) OR the WKB
%   approximation to the model.
% 
%   * This model omits the OW derivations (BC).
%
%
% Example of loading the data from the HD:      
%     % Read the binary file into the workspace:
%     [ttt, M_sp_3_cm] = obj.Read_Data(...
%         ['TM_sp', generic_extention_filename],...
%         obj.path.output,...
%         Fs,...
%         ...run_time,...
%         SECTIONS...
%     );
%     M_sp_3 = 0.01*M_sp_3_cm;     % [m/s] Velocity

% clear classes
clc
% close all

fig_num = 0;

%% Model Type:
%   1. 'WKB'               	- The WKB approximation. 
%   2. 'CM_Time'            - The non-linear Cochlea project.
%   3. 'CM_Time_cpp'        - The non-linear Cochlea cpp project.
%   4. 'CM_Yaniv_Linear'  	- The linear Cochlea project, writen by Yaniv
%                             Halmut (June 2004).

SECTIONS = 512;

% model_type = 'WKB';                       % MATLAB
% model_type = 'WKB_old';                   % MATLAB
model_type = 'CM_Time';                     % MATLAB
% model_type = 'CM_New_021_2';        % C++
% model_type = 'CM_New_021_2_StartRec-30msec';        % C++


%
Fs                  = 2*50e3;        % [Hz] Sample rate
gamma               = 1;          % (1x1) OHC (constant) enhancement
run_time            = 0.005;         % [sec] Total excitation (input) time
pressure_input_SPL  = 0;           % (1x1) [dB SPL] Pressure gain


%% DEBUG (only for the non-linear model):            
if strcmpi( 'CM_Time', model_type )
    debug_mode.enable.all                   = 1;              
    debug_mode.enable.txt_2_cmd             = 0;  
    debug_mode.enable.plot_real_time        = 1;  
    debug_mode.enable.enable_release_txt  	= 1;
else
    debug_mode = [];
end

%% Create the Model's object:

fprintf( '--------------------------------------------------------\n' )
fprintf( 'Model Type = < %s >\n', model_type )
fprintf( '--------------------------------------------------------\n' )


% Construct the model:
obj = CM_new_C( Fs, debug_mode );

if strcmpi( 'CM_Time', model_type )
    log_str = '';
    log_str = sprintf( '%s\nFlags:\n', log_str );
    log_str = sprintf( '%s\t-> ow_flag = %d\n', log_str, obj.ow_flag );             % OW
    log_str = sprintf( '%s\t-> DL_flag = %d\n', log_str, obj.DL_flag );             % OW
    log_str = sprintf( '%s\t-> alpha_r = %d\n', log_str, obj.alpha_r );             % Nonlinearity damping
    fprintf( '%s', log_str )
end

%% Calc Relevant Pars:

pressure_input_Pa = SPL_2_Pa( pressure_input_SPL );    % (1x1) [dB SPL] Pressure gain

% 1[Pa] = 1[N/m^2] = 10[(gr*cm/s^2)/cm^2]:
pressure_input_cm = 10* pressure_input_Pa;  % 1[Pa] -> 10 [(gr*cm/s^2)/cm^2]

gamma_v = gamma.* ones(SECTIONS, 1);                 % OHC density
N_tot = run_time * obj.Fs;                            % [smp] Total samples available.


%% The input structure:
%   -> sinus:
input_st.input_type_str = 'sin( 2*pi*f0 )';     % (str) The excitation type
input_st.f_sin          = 1000 %4e3;                  % [Hz] Sinus frequency (excitation)
input_st.G_sin          = pressure_input_cm; 	% [Pa] Sinus amplitude (gain)
input_st.win            = 0;                    % Enable\Disable windowing + padding of the sinus
input_st.delay_start_sec = 0.00 %0.0*run_time;      	% [sec] Sinus frequency (excitation)
input_st.delay_end_sec   = 0.00  %0.0*run_time;     	% [sec] Sinus frequency (excitation)

% %   -> sinus combimations:
% input_st.input_type_str = 'sin comb';       % (str) The excitation type
% input_st.f1             = 4e3;
% input_st.f_sin          = [1, 2]*input_st.f1;     	% [Hz] Sinus frequency (excitation)
% input_st.G_sin          = pressure_input_cm*[0.2, 2]; 	% [Pa] Sinus amplitude (gain)
% input_st.win            = 1;                  % Enable\Disable windowing + padding of the sinus
% input_st.delay_start_sec = 0.0005 %0.02*run_time;      	% [sec] Sinus frequency (excitation)
% input_st.delay_end_sec   = 0.003  %0.1*run_time;     	% [sec] Sinus frequency (excitation)

% %   -> delta:
% input_st.input_type_str = 'delta';           	% (str) The excitation type
% input_st.G_delta        = pressure_input_cm;    % (1x1) The delta's amplitude
% input_st.delay_start_sec = 0.01*run_time;      	% [sec] Sinus frequency (excitation)
% input_st.delay_end_sec   = 0.0*run_time;     	% [sec] Sinus frequency (excitation)
% input_st.delta_length_smp = 1;                  % [sec]

% %   -> step:
% input_st.input_type_str     = 'step';                   % (str) The excitation type
% input_st.delay_start_sec    = 0.05*run_time;            % [sec] Sinus frequency (excitation)
% input_st.delay_end_sec      = 0.6*run_time;             % [sec] Sinus frequency (excitation)
% input_st.G_step             = pressure_input_cm;        % Sinus amplitude (gain)
% input_st.tukey_r            = 0.0;
% input_st.delay_start_sec    = 0.02*run_time;      	% [sec] Sinus frequency (excitation)
% input_st.delay_end_sec      = 0.05*run_time;     	% [sec] Sinus frequency (excitation)

% %   -> sinus sweep:
% input_st.input_type_str = 'chirp';              % (str) The excitation type
% input_st.f0             = 1;                    % 
% input_st.t1             = run_time;
% input_st.f1             = 16e3;
% input_st.method         = 'linear' %;'logarithmic';       % 
% input_st.phi            = 0;                  % 
% input_st.start_at_dc    = 0;
% input_st.delay_start_sec    = 0.0*run_time;      	% [sec] Sinus frequency (excitation)
% input_st.delay_end_sec      = 0.0*run_time;     	% [sec] Sinus frequency (excitation)

% 
input = Create_Input_Files( obj, input_st, run_time, gamma_v );
   

    
%% ------------------------------------------------

% Start a timer:
t_Start = tic;  

%         '!!! DEBUG !!!'
%         input = 0*input + 0.2e-7;

% Solve (by the chosen case):
switch model_type
    
    case 'WKB'      % MATLAB
        [V_bm_cm, P, K, Z_oc] = Solve_WKB( obj, pressure_input_cm, gamma_v ); 	% Solve the WKB model
        V_bm = 0.01*V_bm_cm;     % [m/s] Velocity

    case 'WKB_old'      % MATLAB
        % Construct the model:
        old_obj = CM_C( Fs, debug_mode );

        [V_bm_old_cm, P, K, Z_oc] = old_obj.Solve_WKB( pressure_input_cm, gamma_v ); 	% Solve the WKB model
        V_bm_old = 0.01*V_bm_old_cm;     % [m/s] Velocity
        
    case 'CM_Time'       % MATLAB
        % Solve the non-linear model:
        obj.Solve;                                              
                
        % Read the binary file into the workspace:
        [ttt, M_sp_1_cm] = obj.Read_Data( obj.save2disk.bm_sp.filename, obj.path.output, Fs, SECTIONS );
        M_sp_1 = 0.01*M_sp_1_cm;     % [cm/s] -> [m/s] Velocity        
        
    case 'CM_Old_Cpp_Ref_05'
        proj_filename_cpp       = '_CM_Old_Cpp_Ref_05';
        data_path_cpp           = '..\\Data\\';
        input_filename_cpp      = 'in';
        %output_filename_cpp     = sprintf( 'BM_sp_cpp_%gSPL_%gkHz', pressure_input_SPL, 1e-3*input_st.f_sin );
        %output_filename_cpp     = sprintf( 'BM_sp_cpp_%gSPL_%gkHz', pressure_input_SPL, 1e-3*input_st.f_sin );        
        oae_filename_cpp        = sprintf( 'OAE_cpp_%gSPL_%s', pressure_input_SPL, 'delta' );
        output_filename_cpp     = sprintf( 'BM_sp_cpp_%gSPL_%s', pressure_input_SPL, 'delta' );    

        input_full_filename_cpp     = sprintf( '%s%s',data_path_cpp, input_filename_cpp );
        output_full_filename_cpp    = sprintf( '%s%s',data_path_cpp, output_filename_cpp );
        oae_full_filename_cpp       = sprintf( '%s%s',data_path_cpp, oae_filename_cpp );

        cmd2run = sprintf( '%s %d %s.bin %s.bin %s.bin  %s.bin',...
            proj_filename_cpp,...        
            Fs,...
            input_full_filename_cpp,...
            output_full_filename_cpp,...
            oae_full_filename_cpp...
        );

        fprintf( '\n\nStarting The C++ Algorithm:\n' )
        fprintf( '\t-> %s\n\n', cmd2run );

        % Run the cpp model:
        cd( [cd, '\cpp_model\'] );         
        dos( cmd2run );
        cd ..

        % Read the binary file into the workspace:
        %[ttt, M_sp_2_cm] = obj.Read_Data( output_filename_cpp, obj.path.output, Fs, run_time, SECTIONS );
        [ttt, M_sp_2_cm] = obj.Read_Data( output_filename_cpp, obj.path.output, Fs, SECTIONS );
        M_sp_2 = 0.01*M_sp_2_cm;     % [m/s] Velocity

        %[ttt, v_OAE] = obj.Read_Data( oae_filename_cpp, obj.path.output, Fs, run_time, 1 );
        [ttt, v_OAE] = obj.Read_Data( oae_filename_cpp, obj.path.output, Fs, 1 );
        
    
    case 'CM_Yaniv_Linear'
        
        addpath( [cd, '\CM_Yaniv_Linear'] )
        
            % Run Yaniv's code. The output is <data> matrix:
            RunLinearClick;         % --> data

        rmpath( [cd, '\CM_Yaniv_Linear'] )
      
        
    otherwise
        proj_filename_cpp = model_type;
        generic_extention_filename = '_cpp';
        data_path_cpp = '..\\Data\\';

        cmd2run = sprintf( '%s %d %s %s %s %s',...
            proj_filename_cpp,...        
            Fs,...
            generic_extention_filename,...
            data_path_cpp...
        );

        fprintf( '\n\nStarting The C++ Algorithm:\n\n' )
        fprintf( '-> %s\n\n', cmd2run );

        % Run the cpp model:
        cd( [cd, '\cpp_model\'] );         
        dos( cmd2run );
        cd ..

        % Read the binary file into the workspace:
        [ttt, M_sp_3_cm] = obj.Read_Data(...
            ['BM_sp', generic_extention_filename],...
            obj.path.output,...
            Fs,...
            ...run_time,...
            SECTIONS...
        );
        M_sp_3 = 0.01*M_sp_3_cm;     % [m/s] Velocity
                      
        
end

% Stop the timer:
t_Elapsed = toc( t_Start );
fprintf( '\n->Total elapsed time = %g [sec]\n', t_Elapsed )

%%
        % Read the binary file into the workspace:
        [ttt, bm_sp_3_cm] = obj.Read_Data(...
            ['BM_sp', generic_extention_filename],...
            obj.path.output,...
            Fs,...
            ...run_time,...
            SECTIONS...
        );
        bm_sp_3 = 0.01*bm_sp_3_cm;     % [m/s] Velocity


%         % Read the binary file into the workspace:
%         [ttt, tm_sp_3_cm] = obj.Read_Data(...
%             ['TM_sp', generic_extention_filename],...
%             obj.path.output,...
%             Fs,...
%             ...run_time,...
%             SECTIONS...
%         );
%         tm_sp_3 = 0.01*tm_sp_3_cm;     % [m/s] Velocity
% 
%         [ttt, ow_disp_3_cm] = obj.Read_Data(...
%             ['OW_disp', generic_extention_filename],...
%             obj.path.output,...
%             Fs,...
%             ...run_time,...
%             1 ...
%         );
%         ow_disp_3 = 0.01*ow_disp_3_cm;     % [m/s] Velocity



%Results_Plot_TimeDomain_v01;

% %%
% figure(1+fig_num)
% imagesc( 1e3*ttt, obj.x, 20.*log10(abs(M_sp_3(1:end-1,:))) )
% xlabel('t [msec]')
% ylabel('Distance from Stapes [cm]')
% %title('Sinus of 1k [Hz]')
% colorbar

% figure(2+fig_num)
% plot( obj.x, 1e6*[M_sp_3(:,1), tm_sp_3(:,1)] )
% xlabel('Distance from Stapes [cm]')
% ylabel('Velocity [\mum/sec]')
% %title('Sinus of 1k [Hz]')
% legend('BM', 'TM')
        


% Plotting Results:

    figure( 30 )

    font_size   = 14;
    font_weight = 'bold';

    line_width   = 2;
    marker_size  = 8;

    % +1e-32 to avoid the log10(0)
    sp_mt = M_sp_3 +1e-40;    
%     sp_mt = M_sp_3;
%     [dummy_min, min_indx] = max(abs( 1/M_sp_3(:,1)) );
%     [I, J] = ind2sub( size(M_sp_3), min_indx);
%     sp_mt(sp_mt==0) = 1e-2* min(M_sp_3(I,J));   
    
    
    % Read the binary file into the workspace:
    [ttt, OW_sp_3_cm] = obj.Read_Data(...
        ['OW_sp', generic_extention_filename],...
        obj.path.output,...
        Fs,...
        ...run_time,...
        1 ...
    );
    OW_sp_3 = 0.01*OW_sp_3_cm;     % [m/s] Velocity
    
    ttt = ttt';
    ow_v = 1e6*OW_sp_3;    % [m] -> micro [m]   
    
    % ---------------------------------------
    h1 = subplot(2,1,1);
    % ---------------------------------------    
    plot( 1e3*ttt, ow_v )
    axis(h1, [1e3*ttt(1), 1e3*ttt(end), 1.3* min(ow_v), 1.3* max(ow_v)])
    
    ylabel( h1, sprintf('OW Velocity\n[\\mum/sec]'),...
        'FontSize',     font_size,...
        'FontWeight',   font_weight ...
    )
    xlabel( h1, 't [msec]',...
        'FontSize',     font_size,...
        'FontWeight',   font_weight ...
    )
    
    
    % ---------------------------------------
    h2 = subplot(2,1,2);
    % ---------------------------------------
    linkaxes([h1, h2], 'x');
%     imagesc( 1e3* ttt, obj.x, sign(sp_mt).*log10(abs(sp_mt)) ) 
    imagesc( 1e3* ttt, obj.x, log10(abs(sp_mt)) )  
    hold on
    X = obj.x(:,ones(size(ttt,1),1));
    Y = 1e3* ttt(:,ones(size(obj.x,1),1))';
    
    colorbar
    
%     contour( Y, X, sp_mt, 'k' )
%     contour( Y, X, sp_mt, [0:0.5:5], 'k' )
        
    hold off
    set( h1, 'Position', [0.1300 0.8173 0.7208 0.0777] )
    set( h2, 'Position', [0.1300 0.0857 0.7208 0.6592] )

    ylabel( h2, 'Distance From Stapes [cm]',...
        'FontSize',     font_size,...
        'FontWeight',   font_weight ...
    )
    xlabel( h2, 't [msec]',...
        'FontSize',     font_size,...
        'FontWeight',   font_weight ...
    )
    
    set( [h1, h2],...
        'FontSize',     font_size,...
        'FontWeight',   font_weight ...
    );
    
      
        
        
        
        
        
        
        
        
        
        



