% 
% function [Ix_f2x_v, fff] = CF_2_X( BM_mt, f_desired, x, Fs )
% 
% Input:
%     BM_mt       - (NxM) BM matrix of displacement or velocity
%     f_desired   - (1x1) The desired frequency. If empty then all 
%     x           - (Nx1) The x locations
%     Fs          - (1x1) The frequency sampling rate
% 


function [Ix_f2x_v, fff] = CF_2_X( BM_mt, f_desired, x, Fs )

% Get the size of the BM matrix:
[L_x, L_time] = size(BM_mt);

fff = [0:L_time-1]'/L_time*Fs;

if isempty( f_desired )
    f_desired   = fff;
    L_f         = L_time;
else
    L_f         = length(f_desired);
end

% The indexes of the frequencies with respect to x:
Ix_f2x_v = zeros(L_f, 1);

% Transform fourier for each of the rows (the time axis):
fBM_abs_mt = abs(fft(BM_mt'))';

for i = 1:L_f
    % Find the desired frequency index:
    Ix_f_desired = round( f_desired*L_time/Fs +1 );
      
    % Find the maximum amplitude:
    [dummy, Ix_f2x_v(i)] = max( fBM_abs_mt(:,Ix_f_desired) );
        
end





