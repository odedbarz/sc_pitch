%
% Results_Plot_TimeDomain_v01.m
% 
% Plotting Results - Time domain
%
%
%


figure( 10 )

font_size   = 14;
font_weight = 'bold';

line_width   = 2;
marker_size  = 8;

% +1e-32 to avoid the log10(0)
% sp_mt = M_sp_3 +1e-40;    

% Read the binary file into the workspace:
[ttt, OW_sp_3_cm] = obj.Read_Data(...
    ['OW_sp', generic_extention_filename],...
    obj.path.output,...
    Fs,...
    ...run_time,...
    1 ...
);
OW_sp_3 = 0.01*OW_sp_3_cm;     % [m/s] Velocity

ttt = ttt';
ow_v = 1e6*OW_sp_3;    % [m] -> micro [m]   

% Read the binary file into the workspace:
[ttt, bm_sp_3_cm] = obj.Read_Data(...
    ['BM_sp', generic_extention_filename],...
    obj.path.output,...
    Fs,...
    ...run_time,...
    obj.SECTIONS...
);
bm_sp_3 = 0.01*bm_sp_3_cm;     % [m/s] Velocity

% Read the binary file into the workspace:
[ttt, tm_sp_3_cm] = obj.Read_Data(...
    ['TM_sp', generic_extention_filename],...
    obj.path.output,...
    Fs,...
    ...run_time,...
    obj.SECTIONS...
);
tm_sp_3 = 0.01*tm_sp_3_cm;     % [m/s] Velocity       


% ---------------------------------------    
h1 = subplot(3,1,1);
% ---------------------------------------    

plot( 1e3*ttt, ow_v )
axis(h1, [1e3*ttt(1), 1e3*ttt(end), 1.3* min(ow_v), 1.3* max(ow_v)])

ylabel( h1, sprintf('OW Velocity\n[\\mum/sec]'),...
    'FontSize',     font_size,...
    'FontWeight',   font_weight ...
)
xlabel( h1, 't [msec]',...
    'FontSize',     font_size,...
    'FontWeight',   font_weight ...
)


% ---------------------------------------
h2 = subplot(3,1,2);
% ---------------------------------------
imagesc( 1e3* ttt, obj.x, log10(abs(bm_sp_3)) )  
hold on
X = obj.x(:,ones(size(ttt,1),1));
Y = 1e3* ttt(:,ones(size(obj.x,1),1))';


% ---------------------------------------
h3 = subplot(3,1,3);
% ---------------------------------------

linkaxes( [h1, h2, h3], 'x' );

imagesc( 1e3* ttt, obj.x, log10(abs(tm_sp_3)) )  
hold on
X = obj.x(:,ones(size(ttt,1),1));
Y = 1e3* ttt(:,ones(size(obj.x,1),1))';


colorbar_h = colorbar;
set( colorbar_h,'position', [0.8758, 0.0872, 0.0208, 0.6502] )


    contour( Y, X, sp_mt, 'k' )
    contour( Y, X, sp_mt, [0:0.5:5], 'k' )


hold off
set( h1, 'Position', [0.1300 0.8173 0.7208 0.0777] )
set( h2, 'Position', [0.1300 0.4452 0.7208 0.2953] )
set( h3, 'Position', [0.1300 0.0857 0.7208 0.2777] )

%
ylabel( h2, 'Distance From Stapes [cm]',...
    'FontSize',     font_size,...
    'FontWeight',   font_weight ...
)
xlabel( h2, 't [msec]',...
    'FontSize',     font_size,...
    'FontWeight',   font_weight ...
)

%
ylabel( h3, 'Distance From Stapes [cm]',...
    'FontSize',     font_size,...
    'FontWeight',   font_weight ...
)
xlabel( h3, 't [msec]',...
    'FontSize',     font_size,...
    'FontWeight',   font_weight ...
)


set( [h1, h2, h3],...
    'FontSize',     font_size,...
    'FontWeight',   font_weight ...
);



