% 
% dB SPL to Pascal
%

function x_Pa = SPL_2_Pa( x_dB_SPL, dB_SPL_ref )

if ~exist('dB_SPL_ref', 'var')
    dB_SPL_ref = 20e-6;     % 20 �[Pa] RMS
elseif isempty(dB_SPL_ref)
    dB_SPL_ref = 20e-6;     % 20 �[Pa] RMS
end

x_Pa = 10.^(x_dB_SPL./20) .* dB_SPL_ref;


