%
% From Liu & Neely, 2010 (Human !)
%

function q_x = Log_Quad_Interp( x, len, q0, q1, q2 )

    a = 2*log(q1^2/(q0*q2))/len^2;
    b = log(q2/q0)/len;
    q_x = q0.*exp( a*x.*(len-x) + b*x );

end
