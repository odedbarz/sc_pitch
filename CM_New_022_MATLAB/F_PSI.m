% 
%     w_psi = obj_ref.alpha_1./obj_ref.alpha_2;
%     w_psi = -1.0* w0_ref.^2./obj_ref.w_ohc;
% 

function [ y, G_psi, w_psi ] = F_PSI( w_ohc, a2, a1, Ix, w )

G_psi = [];
w_psi = [];

if isempty(Ix)
    y = ( a1 + 1j*w.*a2 ) ./ ( 1j*w + w_ohc );
        
else
    y = ( 1j*w.*a2(Ix) + a1(Ix) ) ./ ( 1j*w + w_ohc ); 
    
end



