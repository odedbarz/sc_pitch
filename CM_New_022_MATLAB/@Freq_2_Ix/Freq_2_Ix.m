% 
%
% function [Ix_x_d, x_d] = Freq_2_Ix( obj, f_d, method, debug_mode )
%
% Input:
%   obj     - (1x1) CM object
%   f_d     - (Nx1) Desired frequency
%   method  - (1x1) Choose the method by which to calc the CF as a 
%                   function of the given location x:
%                   method == 0: (default) Calc the CF location using the BM
%                                resonance frequencies, i.e. w_cf = sqrt(K_bm/M_bm).
%                   method == 1: Use WKB to calc the CF along x.
%                   method == 2: Use a given (preset & precalculated) data
%                                file which contains a table of CF vs x.
%                                The function will interpulates for the
%                                desired x.
%   debug   - (1x1) Debug mode
%
%
% Output:
%   Ix_x_d  - (Nx1) Index of the desired frequency location along x
%   x_d     - (Nx1) location in [cm] of the desired frequency location along x
%
% Example:
%   Freq_2_Ix( w0, [8, 3, 4, 3, 2, 1, 0.2, 0.25]*1e3, 0, 0 )
%
%
%

function [Ix_x_d, x_d] = Freq_2_Ix( obj, f_d, method, debug_mode )

if ~exist('method', 'var')
    method = 0;     % default
end

if ~exist('debug_mode', 'var')
    debug_mode = 0;     % default
end

N_f_d = length(f_d);
Ix_x_d = zeros(1, N_f_d);

switch method
    
    case 0  % (default)
        Freq_2_Ix_by_w_bm;
        
    case 1
        Freq_2_Ix_by_WKB;
        
    case 2
        Freq_2_Ix_by_table;
        
    otherwise
        error( 'Freq_2_Ix().m - <method> = <%d> is UNDEFINED!!!', method )
        
end


if 1 == debug_mode
    figure(98)
    plot( f_cf_x, '.-' )
    hold on
    for i = 1:N_f_d
        plot( [1, length(f_cf_x)], f_d(i)*[1, 1], ':k' )
        text( length(f_cf_x), f_d(i), sprintf('f = %g [Hz]',f_d(i)) )
    end
    plot( Ix_x_d, f_cf_x(Ix_x_d), 'or', 'Markersize', 8, 'MarkerFaceColor', 'r' )
    hold off
    xlabel( 'x [smp]' )
    ylabel( 'f [Hz]' )
    legend( '\omega_{CF}' )
    
    x_d_str = sprintf( '%2.2g ', x_d );
    Ix_x_d_str = sprintf( '%g ', Ix_x_d );
    title( sprintf('x = [ %s]_{[cm]}, Ix_x = [ %s]_{[smp]}', x_d_str, Ix_x_d_str ))
    
end
















