% 
% Freq_2_Ix_by_table.m
% 

%CF_data_filename = 'CF_st_f(0.001-0.009 0.1-0.9 1-13)kHz';
CF_data_filename = 'CF_st_wkb';

dummy = load(sprintf( '%s.mat', CF_data_filename ), '-mat');    % -> <dummy.CF_st>
CF_st = dummy.CF_st;

%f_cf_x = CF_st.Ix_CF_map_intrp;
x_inv = CF_st.x(end:-1:1);
[Ix_CF_map_, II] = unique( CF_st.Ix_CF_map );
f_cf_x = interp1( x_inv(Ix_CF_map_), CF_st.f_sin_v(II), x_inv, 'cubic' );

% Find the desired index using the loaded table:
Ix_x_d = interp1( CF_st.f_sin_v, CF_st.Ix_CF_map, f_d, 'cubic' );
Ix_x_d = round( Ix_x_d );
x_d    = obj.x( Ix_x_d );   % [cm]


warning( '!!! Currently the calculation is valid only for gamma = 0.5 !!!' )






































