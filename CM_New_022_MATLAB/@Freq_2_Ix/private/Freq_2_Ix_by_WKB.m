% 
% Freq_2_Ix_by_WKB.m
% 

pressure_input_cm = 0;  % arbitrary input

'please note:'
'   Currently <gamma> was set to 0.5 (there isn''t any other option available)'
obj_dummy = obj;
obj_dummy.gamma = 0.5;


[dummy, dummy, K_wkb] = obj_dummy.Solve_WKB( pressure_input_cm ); 	% Solve the WKB model
[dummy, Ix_cf] = max( real(K_wkb).' ); 

f_cf_x = obj_dummy.f( Ix_cf )';

for i = 1:N_f_d
    
    % Upper bound index (lower frequency):
    Ix_ub = find( f_cf_x <= f_d(i), 1, 'first' );
    
    % Lower bound index (upper frequency):
    Ix_lb = find( f_cf_x >= f_d(i), 1, 'last' );

    % Distance upper bound:
    Df_ub = abs( f_d(i) - f_cf_x(Ix_ub) );

    % Distance lower bound:
    Df_lb = abs( f_cf_x(Ix_ub) - f_d(i) );
    
    if Df_ub > Df_lb 
        Ix_x_d(i) = Ix_lb;
    else
        Ix_x_d(i) = Ix_ub;
    end
    
    
end

% The desired locations along the cochlea:
x_d = obj_dummy.x(Ix_x_d);    % [cm]















