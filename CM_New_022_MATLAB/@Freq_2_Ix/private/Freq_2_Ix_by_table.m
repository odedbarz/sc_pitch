% 
% Freq_2_Ix_by_table.m
% 

%CF_data_filename = 'CF_st_f(0.001-0.009 0.1-0.9 1-13)kHz';
CF_data_filename = 'CF_st_wkb';

dummy = load( sprintf( '%s.mat', CF_data_filename ), '-mat');    % -> <dummy.CF_st_wkb>
CF_st_wkb = dummy.CF_st_wkb;

Ix_gamma_05 = find(CF_st_wkb.gamma == 0.5);

% Find the desired index using the loaded table:
Ix_x_d = interp1( CF_st_wkb.f_cf_v, CF_st_wkb.M_indx_cf(:,Ix_gamma_05), f_d, 'cubic' );
Ix_x_d = round( Ix_x_d );
x_d    = obj.x( Ix_x_d );   % [cm]

% 
if ~isempty(obj.gamma) && 0.5 ~= obj.gamma(1)
    warning(sprintf( 'Freq_2_Ix.m - Using the pre-calc table method:\n\t-> !!! Currently the calculation is valid only for gamma = 0.5 !!!\n' ))
end






































