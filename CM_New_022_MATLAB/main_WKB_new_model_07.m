% 
% 
% main_WKB_new_model_07.m
% 
%   05/01/2011
%   Writen by Barzelay Oded
% 
%   Based on "2011-01-05 Prop 16_05_01.doc"  
%
%

% clc

%% Run Miriam's Model (for comparision):

wkb_miriam_path = [cd, '\wkb_miriam\'];

addpath( wkb_miriam_path );
runModelWKB;    % -> VbmAbs
rmpath( wkb_miriam_path );


%% Model Parameters:

% Number of partititions along the x axis:    
SECTIONS = 512;

% OHC Gain:
if ~exist('gamma', 'var')
    gamma       = 0.5;      % passive\active gain
else
    high_light_str = '*';
    fprintf('\n\n%s\n-> main_WKB_new_model_04:\n\t PLEASE NOTE - USING ALREADY DEFINED <gamma> = %g\n%s\n\n',...
        high_light_str(:,ones(60,1)),...        
        gamma,...
        high_light_str(:,ones(60,1))...
    )
end


% Pressure in:
P_in_spl    = 0;
%P_in        = 10 * ( 10^(P_in_spl/20)*20e-6 );   % 1[Pa] -> [dB SPL] -> 10[(gr*cm/s^2)/cm^2]
P_in        = 10 * SPL_2_Pa( P_in_spl );    % 1[Pa] -> [dB SPL] -> 10[(gr*cm/s^2)/cm^2]

% BM:
area        = 0.5;					% [cm^2] cochlear cross section.
beta        = 0.003;        		% [cm] BM width.        
len         = 3.5;					% [cm] cochlear length.
rho         = 1.0;					% [g/cm^3] liquid density (Density of perilymph).
w_ohc       = 2*pi* 1000.0;         % [rad] OHC angular frequency.
w_ohc_2     = w_ohc^2;

% Middle Ear:
G_me        = 21.4;                             % Mechanical gain of ossicles.
C_me        = (2*pi*1340)^2*0.059/(0.49*1.4);   % (~6e6) Coupling of oval window displacement to ear canal pressure.       

% OW - Oval window:
C_ow        = 6e-3;     %0.032/0.011;      % (~2.909) Coupling of oval window to basilar membrane.
sigma_ow    = 0.5;      %1.85;    	% [g/cm^2] Oval window aerial density.
gamma_ow    = 20e3;     %500;    	% [1/sec] Middle ear damping constant.
f_ow        = 1500;             % [Hz]
w_ow        = 2*pi*f_ow;        % [Hz] ~9424.8 Hz.


%% Simulation Parameters:

%
dx = len/SECTIONS;

% Longitudinal distance along the cochlea:
%x = linspace(0.01, len, SECTIONS)';
%x = linspace(0, len, SECTIONS)';
x = [ 0:SECTIONS-1 ]' * dx; 

M0 = 1.268e-6;             % [g/cm^2] Const factor of the Basilar Membrane mass density per unit area.
M1 = 1.5;                  % [g/cm^2] Exponent factor of the Basilar Membrane mass density per unit area.
R0 = 0.25;                 % [g/(cm^2*sec)] Const factor of the Basilar Membrane mass resistance per unit area.
R1 = -0.06;                % [g/(cm^2*sec)] Exponent factor of the Basilar Membrane mass resistance per unit area.
S0 = 1.2821e4;%(2*pi* 25e3)^2*M0 %1.2821e4;             % [g/(cm^2*sec^2)] (elasticity) ExpConstonent factor of the Basilar Membrane mass stiffness per unit area.
S1 = -1.5;                 % [g/(cm^2*sec^2)] (elasticity) Exponent factor of the Basilar Membrane mass stiffness per unit area.

% psi(x, t):
alpha_s = 1e-6; %1e-4;              	% D_L_ohc parameters.
alpha_L = 5e-5; %2e-6; %1e-2;               	% D_L_ohc parameters.

% BM:
M_bm = M0 * exp( M1 * x );  	% BM mass per unit area.
R_bm = R0 * exp( R1 * x );    	% BM losses (resistance\disipation) per unit area.
S_bm = S0 * exp( S1 * x );      % BM stiffnes (spring) per unit area.

% CF:
w_cf   = sqrt(S_bm./M_bm);
w_cf_2 = S_bm./M_bm;    
    
% TM:
delta_R = 1;
M_tm = 0* M_bm;                 % BM mass per unit area.
R_tm = delta_R* R_bm;           % BM losses (resistance\disipation) per unit area.
S_tm = R_bm.*w_cf_2./w_ohc;  	% BM stiffnes (spring) per unit area.

% OHC:
delta_K = 1e-3;             % 2*(w_ohc./w_cf).^2
M_ohc = 0* M_tm;            % BM mass per unit area.
R_ohc = 0.* R_tm;           % BM losses (resistance\disipation) per unit area.
S_ohc = delta_K.* S_tm;  	% BM stiffnes (spring) per unit area.

% eta_1 = 1/alpha_L* w_ohc;
% eta_2 = 1/alpha_L* w_ohc./w_cf_2;
eta_1 = 1/alpha_L* w_ohc;
eta_2 = 0; 

a1 = sigma_ow * w_ow^2 + G_me*C_me;   % bc_1

% % Angular frequency for the WKB simulation:
% f_min         = 1;       	% [Hz]
% f_max         = 100e3;      % [Hz]
% f_SECTIONS    = 1000;        % [Hz]
% f             = linspace( f_max, f_min, f_SECTIONS );
% w             = 2*pi*f;
w = w_cf;
f = w./(2*pi);

f_SECTIONS = length(f);


%% WKB Solution:

Z_oc_3    = zeros( SECTIONS, f_SECTIONS );      % Impedance of the organ of corti
K_3       = zeros( SECTIONS, f_SECTIONS );      % Wave number
A2_3       = zeros( SECTIONS, f_SECTIONS );      % Pressure
P_3       = zeros( SECTIONS, f_SECTIONS );      % Pressure
F_bm_3    = zeros( SECTIONS, f_SECTIONS );      % Impedance of the organ of corti
F_tm_3    = zeros( SECTIONS, f_SECTIONS );      % Impedance of the organ of corti
V_bm_cm_3 = zeros( SECTIONS, f_SECTIONS );
%V_tm_cm_3 = zeros( SECTIONS, f_SECTIONS );
H_tm      =  zeros( SECTIONS, f_SECTIONS );    
F_oc_3    = zeros( SECTIONS, f_SECTIONS );      % Impedance of the organ of corti

for Ix_w = 1:f_SECTIONS      % Run over all frequencies    
    
    % Current angular frequency:
    w_i = w(Ix_w);
        
    % Model's impedance:
    F_bm_i  = F_OSC2( M_bm, R_bm, S_bm, [], w_i );
    F_tm_i  = F_OSC2( M_tm, R_tm, S_tm, [], w_i );
    
    F_ohc_lumped_i  = gamma* F_OSC2( M_ohc, R_ohc, S_ohc, [], w_i );
    F_psi_i         = gamma* alpha_L*F_PSI( w_ohc, eta_2, eta_1, [], w_i ).*ones(SECTIONS,1);                 
    F_ohc_tot_i     = F_ohc_lumped_i + F_tm_i.*F_psi_i;
    H_tm(:, Ix_w)   = F_ohc_tot_i./(F_tm_i + F_ohc_tot_i);
    F_cl_i          = F_tm_i.*H_tm(:, Ix_w);    
            
    F_oc_3(:, Ix_w) = F_bm_i + F_cl_i;
                    
    % For recording purpose ONLY:
    F_bm_3(:, Ix_w) = F_bm_i;
    F_tm_3(:, Ix_w) = F_tm_i;
        
    % OC impedance:   
    Z_oc_3(:, Ix_w) = F_oc_3(:, Ix_w) ./ (1j*w_i);
               
    % Wave number:
    K_3(:, Ix_w) = sqrt( -1j*w_i*2*rho*beta ./ ( area.*Z_oc_3(:, Ix_w) ) );     
    K0 = K_3(1, Ix_w);
        
    %
    e_plus = exp( 1j* dx * cumtrapz( K_3(:, Ix_w) ) );
    %e_plus = exp( 1j* dx * cumsum( K_3(:, Ix_w) ) );
    e_minus = 1./e_plus;
   
    C_w = e_minus(end).^2;
        
    % B.C. - simple:
    %A2 = 2*w_i.^2*rho*P_in ./ ( sqrt(K_3(1, Ix_w)).*( 1 + C_w ) );
    % B.C. - without the OW derivatives:
    %A2 = 2*w_i.^2*rho.*sqrt(K_3(1, Ix_w)).*P_in ./...
    %    ( -2*w_i.^2*rho.*( 1 - C_w ) + K_3(1, Ix_w).*( 1 + C_w ).*( sigma_ow*w_ow^2 + G_me.*C_me ) );
    
    % B.C. - without xi_ow_disp & xi_ow_sp:
    %D_w = -w_i^2;
    %A2 = (G_me*2*w_i^2*rho) * sqrt(K0) * P_in/...
    %    ( 1j*sigma_ow*D_w*(1+C_w)*K0 - 2*rho*w_i^2*(1-C_w) );
        
    % B.C. - without xi_ow_disp & xi_ow_sp:
    %D_w = -w_i^2;
    %A2 = (G_me*2*w_i^2*rho) * sqrt(K0) * P_in/...
    %    ( 1j*sigma_ow*D_w*(1+C_w)*K0 - 2*rho*w_i^2*(1-C_w) );
    
    % B.C. with OW:
    D_w = -w_i^2 + 1j*w_i*gamma_ow + a1/sigma_ow;
    A2_3(:, Ix_w) = (G_me*2*w_i^2*rho*C_ow) * sqrt(K0) * P_in/...
       ( 1j*sigma_ow*D_w*(1+C_w)*K0 - 2*rho*w_i^2*(1-C_w) );
    
    %
    P_3(:, Ix_w) = A2_3(:, Ix_w)./sqrt(K_3(:, Ix_w)).*( e_minus - C_w.*e_plus );   % [Pa]

    % Calc the BM velosity:
    V_bm_cm_3(:, Ix_w) = P_3(:, Ix_w) ./ Z_oc_3(:, Ix_w);     % [cm/s]
    
%     % Clac the TM velocity:
%     V_tm_cm_3(:, Ix_w) = H_tm_i .* V_bm_cm_3(:, Ix_w);     % [cm/s]
    
    
end

% Clac the TM velocity:
V_tm_cm_3 = H_tm .* V_bm_cm_3;     % [cm/s]

% Calc the BM speed:
V_bm_3    = 0.01*V_bm_cm_3;             % [cm/s] -> [m/s] Velocity            (?)
V_tm_3    = 0.01*V_tm_cm_3;             % [cm/s] -> [m/s] Velocity            (?)


return;

%% Results:

% figure number:
% fig_num = 200* gamma;
fig_num = 10;


    main_WKB_old_model_02;  % -> V_bm    

    % Avector of frequencies to plot:
    f_2_plot_v      = 1e3* [4] % 1e3* [0.25, 0.5, 1:4, 6, 8 ];    % [Hz]
    Ix_f_2_plot_v   = zeros(1, length(f_2_plot_v));
    legend_str      = cell(1, length(f_2_plot_v));

    % Find the nearest locations (indexes) of the <f_2_plot_v> along the cochlea axis:
    for i = 1:length( f_2_plot_v )
        Ix_f_2_plot_v(i) = find( f_2_plot_v(i) >= f, 1, 'first' );

        legend_str{i} = sprintf( '%g [kHz]', 1e-3* f_2_plot_v(i) );

    end

    figure(85)
    plot( x, 20*log10(abs( V_bm_3(:, Ix_f_2_plot_v) )) )
    hold on
    plot( x, 20*log10(abs( V_bm(:, Ix_f_2_plot_v) )), '--' )
    hold off

    title( sprintf('WKB Simulation (\\gamma(x) = %g)\nDashed lines are the old WKB', gamma(1)) )
    xlabel( 'x [cm]' )
    ylabel( 'Velosity [\mum/s]' )
    legend( legend_str )
    grid on

    figure(86)
    plot( x, unwrap(angle( V_bm_3(:, Ix_f_2_plot_v) )))
    hold on
    plot( x, unwrap(angle( V_bm(:, Ix_f_2_plot_v) )), '--')
    hold off
    title( sprintf('WKB Simulation (\\gamma(x) = %g)\nDashed lines are the old WKB', gamma(1)) )

    xlabel( 'x [cm]' )
    ylabel( '\angle{V_{bm}}' )
    legend( legend_str )
    grid on



% %%
% 
% erb_fun = @(ff) 0.108*ff + 24.7;
% % erb_fun = @(ff) 9.26*ff./(ff + 230);
% 
% erb_v = zeros(SECTIONS, 1);
% 
% for Ix_x = 1:SECTIONS
% 
% 
% %     abs_v_bm = abs( V_bm_3(Ix_x,:) );
%     abs_v_bm = abs( 1./(j*w).*V_bm_3(Ix_x,:).' );
%     
%     f_erb_v(Ix_x) = 1/max(abs_v_bm)*trapz( f(end:-1:1), abs_v_bm(end:-1:1) );
% %     f_erb_v(Ix_x) = 1/max(abs_v_bm)*trapz( abs_v_bm );
% 
% 
% end
% 
% f_cf = w_cf/(2*pi);
% 
% % semilogy( f, [erb_v, erb_fun(f)] )
% loglog( f_cf, [ f_erb_v',  erb_fun(f_cf)] )
% % loglog( f_cf, [ f_cf./f_erb_v',  f_cf./erb_fun(f_cf)] )
% 
% hold on
% loglog( f_cf, max(100,f_cf/5), '--k' )
% hold off
% 





