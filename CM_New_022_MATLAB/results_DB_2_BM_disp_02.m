% 
% 
% results_DB_2_BM_disp_01.m.m
%  
% Description:
%   Reads the database and create a velosity-intensity graphas
% 
% Notes:
%   In this script I assume that the bin files were already replaced to the
%   mat files.
%
%

clc

%% Desired set to load from the bin files:
gamma       = 0.5;

% Choose the simulated type you which to load, e.g. BM_sp, BM_disp, TM_sp
% etc:
data_type = 'BM_disp';

% The first figure in the sequence: 
fig_num = 20


%%
% Path to the database files. Each gamma should has its own directory:
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_ME0\' ]; 
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_ME1\' ]; 
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_ME2\' ]; 
% data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_model_10\' ]; 
data_path 	= [cd, '\Data\gamma_(', num2str(gamma), ')_model_13\' ]; 
% data_path 	= [cd, '\Data\' ]; 


% Load the database DB_stucture file:
dummy       = load( [data_path, 'DB_st.mat'], '-mat' );
DB_st       = dummy.DB_st;


%%

% CharacteriDB_stic frequency to look at in the VI plot:
DB_st.CF_2_chk     = 4e3;     % [Hz]

Ix_cf_in_vec = find( DB_st.CF_2_chk == fix(DB_st.f_sin_v), 1 );
if isempty( Ix_cf_in_vec )
    error(sprintf( 'ERROR: DB_st.CF_2_chk ~= DB_st.f_sin_v\n\tPlease Choose a CF frequency from the available CF vector <DB_st.f_sin_v>' ))
end

% ConDB_struct the model:
obj = CM_new_C( DB_st.Fs );

% Find the location (x index) of the frequency along the cochlea:
Ix_cf_cochlea = Freq_2_Ix( obj, DB_st.CF_2_chk, 2, 0 );

% Initialize more parameters:
N_spl	= length( DB_st.pressure_input_SPL_v );
N_freq	= length( DB_st.f_sin_v );


%% Create a DB_structure for the loaded data:
DB_st.Ix_f_v = Freq_2_Ix( obj, DB_st.f_sin_v, 2, 0 );
DB_st.VI  	 = zeros(N_spl, N_freq);

for i = 1:N_spl         % Run on the intensities
    
    fprintf( '\n%d. P_in = %g [dB SPL] (%d out of %d)\n', i, DB_st.pressure_input_SPL_v(i), i, N_spl );
    fprintf( '-------------------------------------\n' );
    
    for j = 1:N_freq    % Run on the excitation frequencies

        fprintf( '\t-> Loading j = #%d out of #%d\n', j, N_freq );
        
        % Search for the desired file to load:
        filename_ij = sprintf( '%s_gamma(%g)_SPL(%g)_f(%g)kHz',...
            data_type,...
            gamma,...
            DB_st.pressure_input_SPL_v(i),...
            1e-3* DB_st.f_sin_v(j)...         % [kHz]
            );
        
        full_filename_ij = [data_path, filename_ij, '.mat'];
        
        % Is the file is available?
        if isempty(dir( full_filename_ij ))
            warn_DB_str = sprintf( '\n\nCouldn''t not read the file:\n\tFilename: %s.bin\n\tPath: %s\n',...
                                filename_ij,...
                                data_path...
                                );
            warning( warn_DB_str )
            continue;
            
        end
                
        %
        dummy = load( full_filename_ij );   % -> M [in units of m,sec]
        
        % Add the data into the database DB_structure:
        V_ij        = dummy.M(Ix_cf_cochlea,:).';
        N_t         = size(V_ij,1);
        fff = [0:(N_t-1)]'/N_t*DB_st.Fs;    % ASSUMING a known time dimension length
        Ix_frq_fft = find( fff >= DB_st.f_sin_v(j), 1, 'first' );
        Ix_fft_bnd = [-50:50] + Ix_frq_fft;    % fft boundaries
        Ix_fft_bnd = unique(min(Ix_fft_bnd, N_t));
        Ix_fft_bnd = unique(max(Ix_fft_bnd, 1));
        fV_norm_ij  = 2/N_t*fft(V_ij).';
        abs_max_v   = max(abs(fV_norm_ij(Ix_fft_bnd)));
%         abs_max_v = max(abs(dummy.M(Ix_cf_cochlea,:)));
        
        DB_st.VI(i,j)  = max(abs_max_v);
        
        clear dummy
        
    end
    
end


%% Plot Results:

figure( fig_num )
loglog(  DB_st.f_sin_v, 1e9*DB_st.VI', '.-' );
xlabel( 'f [Hz]' )
ylabel( 'BM Displacement [nm/s]' )
title(sprintf( 'Cochlea Time Model Simulation at x = %2.2g [cm] ( f = %g [kHz] )', obj.x(Ix_cf_in_vec), 1e-3* DB_st.CF_2_chk ))

legend_freq_DB_str = cell(1,N_spl);
for i = 1:N_spl
    legend_freq_DB_str{i} = sprintf( '%g [dB SPL]',DB_st.pressure_input_SPL_v(i) );
end
legend( legend_freq_DB_str )



%%
    
% LOWER FREQUENCIES FIGURE:
figure( fig_num+1 )
semilogy(  DB_st.pressure_input_SPL_v, 1e9*DB_st.VI(:, Ix_cf_in_vec), 's-', 'linewidth', 2 );
hold on
semilogy(  DB_st.pressure_input_SPL_v, 1e9*DB_st.VI(:, 1:(Ix_cf_in_vec-1)), '.-' );

% Add a logarithmic line:
semilogy(...
    [DB_st.pressure_input_SPL_v(1), DB_st.pressure_input_SPL_v(end)],...
    [10^0, 10^(N_spl-1)]*10^-2,...
    'k--',...
    'LineWidth', 2 ...
);

hold off


xlabel( '[dB SPL]' )
ylabel( 'BM Displacement [nm/s]' )
title(sprintf( 'Cochlea Time Model Simulation at x = %2.2g [cm] ( f = %g [kHz] )', obj.x(Ix_cf_in_vec), 1e-3* DB_st.CF_2_chk ))

legend_freq_DB_str = cell(1,Ix_cf_in_vec);
legend_freq_DB_str{1} = sprintf( '%g [kHz]', 1e-3*DB_st.f_sin_v(Ix_cf_in_vec) );
for i = 1:(Ix_cf_in_vec-1)
    legend_freq_DB_str{i+1} = sprintf( '%g [kHz]', 1e-3*DB_st.f_sin_v(i) );
end
legend_freq_DB_str{end+1} = 'Linear Growth';
legend( legend_freq_DB_str )


% UPPER FREQUENCIES FIGURE:
figure( fig_num+2 )
semilogy(  DB_st.pressure_input_SPL_v, 1e9*DB_st.VI(:, Ix_cf_in_vec), 's-', 'linewidth', 2 );
hold on
semilogy(  DB_st.pressure_input_SPL_v, 1e9*DB_st.VI(:, (Ix_cf_in_vec+1):end), '.-' );

% Add a logarithmic line:
semilogy(...
    [DB_st.pressure_input_SPL_v(1), DB_st.pressure_input_SPL_v(end)],...
    [10^0, 10^(N_spl-1)]*10^-7,...
    'k--',...
    'LineWidth', 2 ...
);

hold off

xlabel( '[dB SPL]' )
ylabel( 'BM Displacement [nm/s]' )
title(sprintf( 'Cochlea Time Model Simulation at x = %2.2g [cm] ( f = %g [kHz] )', obj.x(Ix_cf_in_vec), 1e-3* DB_st.CF_2_chk ))

legend_freq_DB_str = cell(1,N_freq-Ix_cf_in_vec);
legend_freq_DB_str{1} = sprintf( '%g [kHz]', 1e-3*DB_st.f_sin_v(Ix_cf_in_vec) );
for i = (Ix_cf_in_vec+1):N_freq
    legend_freq_DB_str{i-Ix_cf_in_vec+1} = sprintf( '%g [kHz]', 1e-3*DB_st.f_sin_v(i) );
end
legend_freq_DB_str{end+1} = 'Linear Growth';
legend( legend_freq_DB_str )












