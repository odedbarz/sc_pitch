% 
% 
% function [v_t_desired, M] = Read_Data( obj, filename, data_path, Fs, L_data )
%
%   Input:
%       data_path   - (string) The path to the data file to read.
%       filename    - (string) The file name to read.
%       L_data      - (1x1) The length of each interval to load from the
%                       binary data file. 
%
%   Output:
%       v_t_desired - (1x1) The desired time vectror.
%       M_bm_disp   - (MxN) Matrix which holds the BM displacement output
%                       of the model.
%
%   Description:
%       This function reads the output files of the model and manipulates
%       it into a matrix of Cochlea length (row) Vs time (column).
% 
%
%   PLEASE NOTE THAT...
%       You sould have the <CM_BM_C> object in your working path! 
%
%   18/01/2011:
%       The <L_data> input parameter was add. Each binary file include a
%       sequence of data of length <L_data> AND in addition a time vector
%
%

function [v_t_desired, M] = Read_Data( obj, filename, data_path, Fs, L_data )

if ~exist( 'data_path', 'var' )
    full_filename = obj.Full_File_Name( filename );
else    
    full_filename = obj.Full_File_Name( filename, data_path );
end

if ~exist( 'L_data', 'var' )
    L_data = obj.SECTIONS;
end


%fprintf( '\n---------------------------\nReading filename = %s\n---------------------------\n', filename )
fprintf( '\nReading file:\n\t->filename = %s.bin\n\t->path = %s\n', filename, data_path )
fid = fopen( full_filename, 'rb');
if -1 == fid
    error('ERROR - Couldn''t open file for reading !!!')
end

V_out = fread( fid, 'double' );         % Reads out one big vector
fclose( fid );
% ----------------------------------------------------------------------------------


dim_1 = length(V_out) / (L_data +1);          % The <+1> is for the <time> vector
if fix(dim_1) ~= dim_1 
    error('Read_Output.m - fix(dim_1) ~= dim_1: Check the parameters of the results bin file' )
end

try
    % Extract the time vector:
    time_vector = V_out( [ 1 : L_data+1 : end ] );

    M = reshape( V_out, L_data+1, dim_1 );   % The <+1> is for the <time> vector
    M = M(2:end, :);    % Remove the time vector from the data.
    
    tot_run_time = time_vector(end) - time_vector(1);
    v_t_desired = linspace( time_vector(1), time_vector(end), floor(tot_run_time*Fs) );
    
    M = interp1( time_vector, M.', v_t_desired ).';
    
catch ME1
    error( sprintf('Read_Output.m:\n\t-> %s\n', ME1.message) )
end











