% ------------------------------------------------------------------------
%
% Written by Barzelay Oded - June 2010 (start)
% 
% -------------------------------------------------------------------------
% 
% Description:
% 
% The current class implement only the macro-mechanical physics of the Cochlea.
% 


classdef CM_new_C < handle
    
    % Constants:
    properties (Constant)
        
        SECTIONS  			= 512;      % Cochlea spatial resolution (sections)
    
        % ODE time steps:
        % ---------------
        INIT_TIME_STEP		= 1e-18;
        MIN_TIME_STEP		= 1e-20;	
        MAX_TIME_STEP		= 1e-6;    	
        MAX_M1_SP_ERROR		= 1e-15;
        MAX_TOLERANCE		= 1e-10;
        
        % ODE type for the time step:
        ODE_EULER           = 'euler';
        ODE_TRAPEZ          = 'trapez';
        
        %SPL                 = 200e-6; 	% [g/(cm*sec^2)] SPL ( from p_ref = 20 �Pa (rms) ).
        dB_SPL_ref           = 20e-6; 	% 20 �[Pa] RMS
        
    end
    
    % Model's flags:
    properties (Constant)
        
        ow_flag             = true;
        DL_flag             = true;         % DL_flag = false -> linear mode
        nonsymm_psi_flag    = false;
        psi_bias_flag       = false;
        
    end
        
    
    % Model's Constatnts Parameters:    
    properties (Constant)
                        
        % Middle Ear:
        G_me	= 21.4;	% Mechanical gain of ossicles.
        C_me 	= (2*pi*1340)^2*0.059/(0.49*1.4); % (~6e6) Coupling of oval window displacement to ear canal pressure.       
        
        % OW - Oval window:
        C_ow     = 6e-3; %2.909; %6e-3;                % (~2.909) Coupling of oval window to basilar membrane.        
        sigma_ow = 0.5; %1.85; %0.5;                 % [g/cm^2] (1.85) Oval window aerial density.
        gamma_ow = 20e3; %500 %;20e3;                % [1/sec] (500) Middle ear damping constant.
        f_ow     = 1500;                % [Hz]
        w_ow     = 2*pi*CM_new_C.f_ow;  % [Hz] ~9424.8 Hz.
        
        % BM:
        area	= 0.5;					% [cm^2] cochlear cross section.
        beta  	= 0.003;        		% [cm] BM width.        
        len		= 3.5;					% [cm] cochlear length.
        rho		= 1.0;					% [g/cm^3] liquid density (Density of perilymph).
		f_ohc 	= 1000.0;				% [Hz] OHC frequency (Hz).
        w_ohc	= 2*pi* CM_new_C.f_ohc; % [rad] OHC angular frequency.

        % BM - Data From Miriam's paper:
        M0      = 1.286e-6;             % [g/cm^2] Const factor of the Basilar Membrane mass density per unit area.
        M1      = 1.5;                  % [g/cm^2] Exponent factor of the Basilar Membrane mass density per unit area.
        R0      = 0.25;                 % [g/(cm^2*sec)] Const factor of the Basilar Membrane mass resistance per unit area.
        R1      = -0.06;                % [g/(cm^2*sec)] Exponent factor of the Basilar Membrane mass resistance per unit area.
        S0      = 1.282e4;              % [g/(cm^2*sec^2)] (elasticity) ExpConstonent factor of the Basilar Membrane mass stiffness per unit area.
        S1      = -1.5;                 % [g/(cm^2*sec^2)] (elasticity) Exponent factor of the Basilar Membrane mass stiffness per unit area.
        
        M_bm = CM_new_C.M0 * exp( CM_new_C.M1 * CM_new_C.x );        % BM mass per unit area.
        R_bm = CM_new_C.R0 * exp( CM_new_C.R1 * CM_new_C.x );        % BM losses (resistance\disipation) per unit area.
        S_bm = CM_new_C.S0 * exp( CM_new_C.S1 * CM_new_C.x );        % BM stiffnes (spring) per unit area.
        
        % BM characteristic frequencies:
        w_bm_cf_pow2 = CM_new_C.S_bm ./ CM_new_C.M_bm;   	% [Hz] Angular CF frequency
        w_bm_cf = sqrt( CM_new_C.w_bm_cf_pow2 );            % [Hz] Angular CF frequency
        f_bm_cf = CM_new_C.w_bm_cf/(2*pi);                	% [Hz] CF frequency

        % ----------------------------------------------            
        % BM CF - Greenwood's function - Human: 
        f_bm_cf_gw = 165.4*( 10.^( 2.1*(CM_new_C.len-CM_new_C.x)/CM_new_C.len ) - 1 );	
        w_bm_cf_gw = 2*pi * CM_new_C.f_bm_cf;
        % ----------------------------------------------
        
        % TM:
        M_tm = 0* CM_new_C.M_bm;                                 	% TM mass per unit area.
%         R_tm = CM_new_C.delta_R* CM_new_C.R_bm;                         % TM losses (resistance\disipation) per unit area.
%         delta_R = 1;
        R_tm = CM_new_C.R_bm;
        
        
%         S_tm = (CM_new_C.w_bm_cf_pow2 .* CM_new_C.R_bm - 2*CM_new_C.w_ohc*CM_new_C.S_ohc)./(CM_new_C.S_ohc./CM_new_C.R_bm + 3*CM_new_C.w_ohc);
        S_tm = CM_new_C.R_bm.*CM_new_C.w_bm_cf_pow2./( CM_new_C.w_ohc );	% TM stiffnes (spring) per unit area.

        % OHC:
        %delta_K = 1e-3; 
        M_ohc = 0 * CM_new_C.M_tm;    	% OHC mass per unit area.
        R_ohc = 0 * CM_new_C.R_tm;      % OHC losses (resistance\disipation) per unit area.
         
        % S_ohc:   
        S_ohc = 0.02/CM_new_C.area .* ones( CM_new_C.SECTIONS, 1 );   % [N/m^2] from Wen & Neely, 2009 (they took it from Hallworth, 2007).
        eta_1 = 0.5/CM_new_C.alpha_L * CM_new_C.w_ohc;
        eta_2 = 0
        
        % OHC Nonlinearity
        alpha_r	= 0.0;      % _alpha_r - Linear factor, in the BM equation: P_BM = ... + (1 + alpha_r*csi_BM^2)*csi_BM + ...                
        
% alpha_s & alpha_L:     
        alpha_s = 5e-7 %1e-6      	% DeltaL_ohc parameters.
        alpha_L = 10*CM_new_C.alpha_s; %5e-5          % DeltaL_ohc parameters.
                
        %
        dx      = CM_new_C.len / CM_new_C.SECTIONS;     % [cm] cochlear length partitition distance.
        dx_pow2 = CM_new_C.dx^2;                        % [cm^2] dx * dx.
        x       = ( 0:CM_new_C.SECTIONS-1 )' * CM_new_C.dx; 

    end
        
    % State parameters and vectors:
    properties
        time;			% [sec] (1x1) Current simulation state's time.
        time_step;      % [sec] (1x1) Current simulation state's step size.
        run_time;       % [sec] (1x1) Total run time, i.e. the total time of the input signal given Fs.
                
        Q;              % A constant vector: 2*rho*beta/(area*_M).
        
        Fs;             % [Hz] Sampling rate.
        Ts;				% [Hz] Time rate (inverse Sampling rate).
        
        % OHC
        gamma       % OHC density.                
        
        % BM
        now         = State_New_C( CM_new_C.SECTIONS );     
        prev        = State_New_C( CM_new_C.SECTIONS ); 
        
        p;			% [Pa ???] ( SECTIONS x 1 ) net pressure on membrane along x. 
        Y_0;		% [Pa ???] (1x1) boundary condition (at x=0), thus produced by the oval window.
        Y_end;      % [Pa ???] (1x1) boundary condition (at x=L=3.5[cm]).
                
        % Tri-Diagonal:
        triMat      = zeros( CM_new_C.SECTIONS );            % Tridiangular matrix.
        triMat_inv  = zeros( CM_new_C.SECTIONS );            % ??? DEBUG ??? Tridiangular matrix.
        G           = zeros( CM_new_C.SECTIONS, 1 );         % G(x,t) from the pressure equation @^2P(x,t)/@t^2 - Q(x)*P(x,t) = G(x,t)
        Y           = zeros( CM_new_C.SECTIONS, 1 );         % Y(x,t) from U*P = Y
           
        % Auxiliary constants:
        a0       	% a0 = 2*rho*Cow/Sigma_ow
        a1        	% a1 = Sigma_ow*Omega_ow^2 + Gme*Cme
        a2       	% a2 = a0*Sigma_ow*Gamma_ow
                
        %Q_bm;
        
        % Tester
        chk_another;            % (bool) Recalc another trapezoidal step flag.
        pass_2_next_time_step;  % (bool) Update state flag. 
                
    end
       
    % Handling file parameters, paths and the debug's flags:
    properties
        
        file_format         = 'bin';            % (string) File format (extension).
        input_filename      = 'in';             % (string) The input's file name.
        input_file_hnd;                         % [hnd] (1x1) Access to the input file (input excitation).
        input_length;                           % [smp] (1x1) File length, i.e. number of bits (which is also number of samples).
        log_input_filename  = 'input'
        
        gamma_filename      = 'gamma';          % (string) The OHC density along the Co.
        
        % Flags to save:
        %  1. bm_disp_flag
        %  2. bm_sp_flag
        save2disk = Save_2_Disk( 1, 1 );     	% Holds all flags of what to save into the hard-disk (non-linear only).
        
        sizeof_double;          % Size, in bytes, of a <double>.
        
        path;                   % A structure for all available paths.       
         
        save_rate = 1;          % Save's data each time counter == <save_rate>.
        
        % Mainly for the figures:
        xxx = (1:CM_new_C.SECTIONS)'/CM_new_C.SECTIONS * CM_new_C.len;	% [cm] Longitudinal axis along the Cochlea.
        ttt                     % [sec] Time axis of al the input.
        
        debug                   % Debug structure.
        
    end
    
    
    % ------------------------------------------------------------------------------     
    % WKB Simulation (Approximation)
    properties 
        f_SECTIONS       
        f           % (f_SECTIONS x 1) working frequencies for the WKB model and for plotting
        w           % (f_SECTIONS x 1) angular frequency
        
        Z_oc        % Cochlea impedance
        K           % Wave propogation (complex) vector
        P           % [Pa] Pressure, frequency domain
        V_bm        % [m/s] BM velocity, frequency domain
                
    end

    
    % -------------------------- Non-Linear Model - METHODS ------------------------
    % Constructor\ Destructor:
    methods
                    
        % Constructor:
        function obj = CM_new_C( Fs, data_dir, debug_mode )
                
            % Define Paths:
            obj.path.current        = [cd, '\'];
            obj.path.log			= obj.path.current;
            obj.path.debug_bin		= obj.path.current;
            obj.path.data           = data_dir; %'..\data\'; %[cd, '\Data\'];
            obj.path.gamma          = obj.path.data;
            obj.path.input          = obj.path.data;
            obj.path.output         = obj.path.data;
            
            obj.Fs = Fs;                        % [Hz] Sampling rate.
            
            % Boundary conditions - Auxiliary constants:
            obj.a0 = 2 * obj.rho * obj.C_ow / obj.sigma_ow;             % bc_0
            obj.a1 = obj.sigma_ow * obj.w_ow^2 + obj.G_me * obj.C_me;   % bc_1
            obj.a2 = obj.sigma_ow * obj.gamma_ow;                       % bc_2

            % A constant vector: 2*rho*beta/(area*_M)
            obj.Q = 2 * obj.rho * obj.beta ./ ( obj.area * obj.M_bm );			
            
            % Init the tridiagonal matrix:
            obj.Init_TriMat;
            
            % Implementing sizeof():
            tmp = double( 1 );
            tmp = whos( 'tmp' );
            obj.sizeof_double = tmp.bytes;
            clear tmp;
                        
            % -----------------------------
            % WKB:
            obj.f_SECTIONS = 1000;
            obj.f = linspace( obj.Fs, 1, obj.f_SECTIONS);  % (w_SECTIONS x 1) Working frequencies for the WKB model and for plotting.
            obj.w = 2*pi * obj.f;
            
            
            % -----------------------------
            % DEBUG:
            if ~exist( 'debug_mode', 'var' )
                debug_mode.enable.all                = 0;              
                debug_mode.enable.txt_2_cmd          = 0;  
                debug_mode.enable.plot_real_time     = 0; 
                debug_mode.enable.enable_release_txt = 0;
            end
            
            obj.debug = Debug_New_Status( debug_mode );
            
                        
        end
    
    end
    
    % Solving the Model:
    methods
                
        % isnan functional - gives a scalar if ANY value in the input
        % vector is nan:
        function yes = IsNan( obj, V )
            yes = 0 ~= sum( isnan( V ) );
        end
        
        % isinf functional
        function yes = IsInf( obj, V )
            yes = 0 ~= sum( isinf( V ) );
        end
        
        % Reads the gamma vector from the hard-disk:
        obj = Read_Gamma_File( obj );
        
        % Initialize the tridiagonal matrix:
        Init_TriMat( obj );        
                
        % Solve the tridiagonal equation U*P = Y:
        function p = Solve_TriMat( obj )
                       
            obj.p = obj.triMat_inv * obj.Y;       
            
            if obj.IsNan( obj.p )
                error( 'ERROR - <obj.p> has NaNs in it !!!\n' )                
            end
            
            if nargout >= 1
               p = obj.p; 
            end
                        
        end
        
        % ODE Related - Time step size should be between the allowed
        %               values: [MIN_TIME_STEP, MAX_TIME_STEP].
        function time_step_bound = Bound_Time_Step( obj, time_step )

            if ( time_step < obj.MIN_TIME_STEP )
                error( 'CM_new_C->Bound_Time_Step - Numerical Stability Error - Too small time step !!!');
            
            elseif ( time_step > obj.MAX_TIME_STEP ) 
                time_step_bound = obj.MAX_TIME_STEP;
            
            else
                time_step_bound = time_step;
            
            end
                        
        end
                
        % Count the number of positions in which elements in v1
        % are greater than their corresponding v2 elements.
        function c = Count_Greater( obj, v1, v2 )
            c = sum( v1 > v2 );
        end
        
        % Calc G(x,t) from the pressure equation @^2P(x,t)/@t^2 - Q(x)*P(x,t) = G(x,t), 
        % (without the boundary conditions).
        function G = Calc_G( obj )
                     
            %obj.G = obj.now.p_ohc - ( obj.R_bm_nl.*obj.now.bm_sp + obj.S_bm.*obj.now.bm_disp );
            obj.G = -1.0*( obj.now.p_tm + obj.R_bm_nl.*obj.now.bm_sp + obj.S_bm.*obj.now.bm_disp );
            
            if obj.IsNan( obj.G )
                error( 'ERROR - <obj.G> has NaNs in it !!!\n' )                
            end
            
            if nargout >= 1
                G = obj.G;
            end
            
        end

        %
        function [Y_0, Y_end] = Calc_Boundary_Condition( obj )
                     
            % Boundary condition at the cochlea's basal part:
            if true == obj.ow_flag
                obj.Y_0 = obj.dx*obj.a0 * ( obj.G_me*obj.get_sample - obj.a1*obj.now.ow_disp - obj.a2*obj.now.ow_sp ); 
            
            else                
                % a1 & a2 are not used in this case:
                obj.Y_0 = obj.dx * obj.a0*obj.G_me*obj.get_sample;
                
            end
            
            % Boundary condition at the cochlea's apex part:            
            obj.Y_end   = 0;
            
            % Output if needed:
            if nargout == 1
                Y_0 = obj.Y_0;
                
            elseif nargout == 2
                Y_0 = obj.Y_0;
                Y_end = obj.Y_end;
                
            end
    
        end
        
        % Calc Y(x,t) from U*P = Y
        function Y = Calc_Y( obj )
            
            obj.Calc_G;                     % G(x,t)
            obj.Calc_Boundary_Condition;	% Boundary conditions

            obj.Y		= obj.dx_pow2*obj.Q.*obj.G;	
            obj.Y(1)	= obj.Y_0;                      % BC
            obj.Y(end)	= obj.Y_end;                    % BC
                
            if nargout >= 1
                Y = obj.Y;
            end
                
        end

        % (BM) Calc BM acceleration
        function bm_acc = Calc_BM_Acc( obj )

            obj.now.bm_acc = ( obj.p + obj.G ) ./ obj.M_bm;    
                           
            if nargout >= 1
               bm_acc = obj.now.bm_acc; 
            end
        
        end
                        
        % (OW) Oval window acceleration
        function ow_acc = Calc_OW_Acc( obj )
            
            if false == obj.ow_flag
                return;
            end
            
            % From Azi's code:
            obj.now.ow_acc = 1/obj.sigma_ow*( obj.p(1) + obj.G_me*obj.get_sample...
              - obj.a2*obj.now.ow_sp - obj.a1*obj.now.ow_disp );  
              
            
            if nargout >= 1
               ow_acc = obj.now.ow_acc; 
            end
            
        end
        
        % Implement one step for the current state ( 1 time step ).
        % step_type can be 'euler' or 'trapez'.
        obj = One_Time_Step( obj, step_type );

        % (BM) Clac non-linear Rnl(x) = R(x)*( 1 + alpha_r * s_BM ) 
        function r_nl = R_bm_nl( obj )
            
            if 0 == obj.alpha_r     % Without non-linear factor in the P(x,t) equation.
                r_nl = obj.R_bm;
            else
                r_nl = obj.R_bm .* ( 1.0 + obj.alpha_r*( obj.now.bm_sp ).^2 );
            end
            
            if obj.IsNan( r_nl )
                error( 'ERROR -  R_bm_nl( obj ) - <r_nl> has NaNs in it !!!\n' )  
            elseif obj.IsInf( r_nl ) 
                error( 'ERROR -  R_bm_nl( obj ) - <r_nl> has Infs in it !!!\n' )  
            end
            
        end
                
        % (OHC) 
        function p_ohc = Calc_P_OHC( obj )
                
%             obj.now.p_ohc = obj.gamma.*( obj.S_ohc.*obj.now.ohc_disp );
%             obj.now.p_ohc = obj.gamma.*( obj.S_ohc.*obj.now.ohc_disp ...
%                 -1.0*( obj.S_tm.*obj.now.ohc_sp + obj.R_tm.*obj.now.DeltaL_sp ));
            obj.now.p_ohc = obj.gamma.*( obj.S_ohc.*obj.now.ohc_disp ...
                -1.0*( obj.S_tm.*obj.now.DeltaL_disp + obj.R_tm.*obj.now.DeltaL_sp ) );            
            
            if 1 <= nargout
                p_ohc = obj.now.p_ohc;
            end
            
        end
        
        % (OHC)
        function d_psi_ohc = Calc_OHC_Psi_Deriv( obj )
                    
            % Save calculation time:
            if isequal( obj.gamma, zeros( obj.SECTIONS, 1) )     % gamma == 0
                obj.now.d_psi_ohc = 0;
                d_psi_ohc = 0;
                return;
            end

            %obj.now.d_psi_ohc = obj.eta_2.*obj.now.ohc_sp + obj.eta_1*obj.now.ohc_disp - obj.w_ohc*obj.now.psi_ohc;
            obj.now.d_psi_ohc = obj.eta_1*obj.now.ohc_disp - obj.w_ohc*obj.now.psi_ohc;
            
            % DEBUG - PSI_0 ~= 0:
            if true == obj.psi_bias_flag
                obj.now.d_psi_ohc = obj.now.d_psi_ohc + obj.psi_bias;
            end
            
            if 1 <= nargout
                d_psi_ohc = obj.now.d_psi_ohc;
            end

            if obj.IsNan( obj.now.d_psi_ohc )
                error( 'ERROR - <obj.now.d_psi_ohc> has NaNs in it !!!\n' )                
            end
            
            
        end
                
        function y = Tanh_fun( obj, x )
            y = -1.0*obj.alpha_s*tanh( obj.alpha_L/obj.alpha_s.* x );
                            
        end
        
        % Delta_L (OHC)
        function Calc_DeltaL_disp( obj )

            if true == obj.DL_flag
                obj.now.DeltaL_disp = obj.Tanh_fun( obj.now.psi_ohc );
                
            else    % The linear approximation case:
                obj.now.DeltaL_disp = -1.0* obj.alpha_L.*obj.now.psi_ohc;
            
            end            
            
        end

        % Delta_L_sp (OHC)
        function Calc_DeltaL_sp( obj )

            if true == obj.DL_flag
                T = -1.0*obj.alpha_L.*( 1.0 - tanh( obj.alpha_L/obj.alpha_s.* obj.now.psi_ohc ).^2 );
                                
            else    % The linear approximation case:
                T = -1.0*obj.alpha_L.*( 1.0 - ( obj.alpha_L/obj.alpha_s.* obj.now.psi_ohc ).^2 );
            
            end            
            
            obj.now.DeltaL_sp = T .* obj.now.d_psi_ohc;
            
        end              
        
        
        % (TM)
        function Calc_TM_Sp( obj )
                         
            obj.now.tm_sp = -1.0 ./ obj.R_tm .* ( obj.now.p_ohc + obj.S_tm.*obj.now.tm_disp );                        
            
        end
        
        
        % 
        function p_tm = Calc_Ptm( obj )
                        
            %obj.now.p_tm = obj.gamma.*( obj.R_tm.*obj.now.tm_sp + obj.S_tm.*obj.now.tm_disp );
            obj.now.p_tm = obj.R_tm.*obj.now.tm_sp + obj.S_tm.*obj.now.tm_disp;
            %obj.now.p_tm = -1.0 * obj.now.p_ohc;
            
            if 1 <= nargout
                p_tm = obj.now.p_tm;
            end
        end
        
        % Solves the model:
        Solve( obj );
        
    end

    % ODE Methods:
    methods
        
        % Solve one step - Euler:
        function y = Euler( obj, past_V, d_past_V )
            y = past_V + obj.time_step * d_past_V;       % ==> y(t+dt)
        end
                
        % Solve one step - Trapezoidal (modified Euler):
        function y = Trapez( obj, past_V, d_past_V, d_now_V )
            y =  past_V + 0.5 * obj.time_step * ( d_past_V + d_now_V );		% ==> y(t+dt)
        end
                
        % ODE Step size tester:
        Tester( obj, sp_ref, acc_ref );
        
    end
    
    % Handeling Binary Files:
    methods
        
        % Create & save the input files and vectors:
        input = Create_Input_Files( obj, input_st, run_time, gamma );
        
        % Add file's format extension:
        function full_filename = Full_File_Name( obj, filename, file_path, file_format )
            
            if ~exist('file_format', 'var')
                file_format = obj.file_format;
            end
            
            if ~exist( 'file_path', 'var' )
                file_path = obj.path.current;
            end
            full_filename = [file_path, filename, '.', file_format];
            
        end
        
        % Open binary file for reading:
        function fid = Open_4_Reading( obj, filename, file_path )
            
            full_filename = obj.Full_File_Name( filename, file_path );
            
            fid = fopen( full_filename, 'rb');
            if 0 == fid
                error('ERROR - CM_new_C->Open_4_Reading: Couldn''t open file for reading !!!')
            end
            
            frewind( fid );
            obj.input_length = length( fread( fid, 'double' ) );
            frewind( fid );
            
        end
        
        % Open binary file for writing:
        function fid = Open_4_Writing( obj, filename, file_path )
            
            full_filename = obj.Full_File_Name( filename, file_path );
            
            fid = fopen( full_filename, 'wb');
            if 0 == fid
                error('ERROR - CM_new_C->Open_4_Reading: Couldn''t open file for writing !!!')
            end

        end
        
        % Read the position given by <pos> from (open) binary file given by
        % <fid>:
        function y = Read_Bin( obj, fid, pos )
                        
            fseek(fid, pos * obj.sizeof_double, 'bof');
            y = fread( fid, 1, 'double' );
            
            % Any problem reading the binary file?
            [ message, errnum ] = ferror( fid );
            if -4 == errnum
                % EOF - Output an empty <y>
                y = [];
            
            elseif 0 ~= errnum
                % For eny other error:
                error( sprintf('\nERROR - CM_new_C->Read_Bin - Reading operation failed !!!\n\t-> ferror->message = %s\n', message) );
                
            end
            
        end
        
        % Open binary files for the output (saving) data:
        function Open_Output_Files( obj )
            
            % Open Output Files:
            if 1 == obj.save2disk.bm_disp.flag
                obj.save2disk.bm_disp.hnd = obj.Open_4_Writing( obj.save2disk.bm_disp.filename, obj.path.output );
            end

            if 1 == obj.save2disk.bm_sp.flag
                obj.save2disk.bm_sp.hnd = obj.Open_4_Writing( obj.save2disk.bm_sp.filename, obj.path.output );
            end
            
            
        end
        
        % Save all results to a binary files. The files
        function Save_Results( obj, counter )
            % Save data on a predefined rate (to save space and make the algo. faster):
            if 0 ~= mod( counter, obj.save_rate )
                return;
            end
            
            % Save the BM displacement vector:
            if 1 == obj.save2disk.bm_disp.flag
                obj.Save2File( obj.save2disk.bm_disp.hnd, [obj.time; obj.now.bm_disp] );
            end

            if 1 == obj.save2disk.bm_sp.flag
                obj.Save2File( obj.save2disk.bm_sp.hnd, [obj.time; obj.now.bm_sp] );
            end

        end
        
        % Save (append) data to a binary file:
        function Save2File( obj, fid, data2save )
            count = fwrite( fid, data2save, 'double' );     
            if 0 == count
                error('ERROR - CM_new_C->Save_Results:Could''t write to the given file handle <fid> !!!')
            end
            
        end

        function run_time = Total_Input_Time( obj )
            
            % Total input (recording) time:
            %obj.run_time = ( obj.input_length-1 ) / obj.Fs;	% [sec]
            obj.run_time = obj.input_length / obj.Fs;	% [sec]
            
            if nargout >= 1
                run_time = obj.run_time;
            end
            
        end           
        
        % Get input's sample at time t (pin(t))
        function p_in_t = get_sample( obj )
            
            % If the sample for the current time step isn't available, calc
            %   it from the input file + interpulation
            if isempty( obj.now.sample_input )
                obj.now.sample_input = obj.read_sample_from_file;
                
            end
            
            p_in_t = obj.now.sample_input;
                        
        end
        
        % Get input's sample at time t (pin(t))
        function p_in_t = read_sample_from_file( obj )
                        
            % Nearest position in the time line: 
            nearest = fix( obj.time * obj.Fs );

            % relative position of x in the interval
            delta = (obj.time - nearest/obj.Fs ) * obj.Fs; 

            % Read the signal's input from the relevant location in the input file: 
            p_t1 = obj.Read_Bin( obj.input_file_hnd, nearest );
            p_t2 = obj.Read_Bin( obj.input_file_hnd, nearest+1 );

            if ~isempty(p_t2)
                p_in_t = p_t2 * delta + p_t1 * (1-delta);
            else
                p_in_t = p_t1 * (1-delta);
            end
            
        end
            
        function Close_File( obj, fid )
                        
            status = fclose( fid );
            if -1 == status
                error('ERROR - CM_new_C->Close_File: Couldn''t open file for writing !!!')
            end

        end
        
        % Read data from hard-disk into the workspace:
        [ttt, M_bm_disp] = Read_Data( obj, filename, data_path, Fs, L_data );
        
    end
    
    % Miscellaneous Functions:
    methods
                
        % Returns a cell of the values which appears in V (e.g. for
        %   legends):
        out_C = Vector_2_Cell( obj, V, pre_str, post_str );
        
    end
    
    
    % ------------------------------------------------------ 
    %  WKB Approximation - METHODS
    methods
        
        % Calc the impedance of the CLASSIC MODEL (Miriam's\Azi's model):
        Z = Z_Model_1( obj )

        % WKB Approximation solution of the Cochlea model:
        [V_bm, P, K, Z_oc] = Solve_WKB( obj, P_in, gamma );
        
    end
        
    
end

















