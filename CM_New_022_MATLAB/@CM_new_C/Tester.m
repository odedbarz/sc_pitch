% 
% ODE Step size tester:
%

function Tester( obj, sp_ref, acc_ref )


% Init output values:
obj.chk_another             = nan;   	% (bool) decides if to run another loop (time step refinement).
obj.pass_2_next_time_step   = nan;      % (bool) decides if ok to move to the next time step.

sp_err = abs( sp_ref - obj.now.bm_sp );	% find the speed error (compared to the past value).

% Prepare a speed error limit vector. The value is the local maximum between the constant tolerance
% and the (local speed) * step size.
sp_err_limit = max( obj.MAX_TOLERANCE, abs(obj.now.bm_sp * obj.time_step) );

% 1st moment of speed error vector
m1_sp_err = sum( sp_err );

% counts the number of points in which the error is greater than the limit:
fault_counter = obj.Count_Greater( sp_err, sp_err_limit );

% calculate lipschitz number
if ( m1_sp_err > obj.MAX_M1_SP_ERROR )      % (m1_sp_err > max_m1_sp_err)
    Lipschitz = max( abs( acc_ref - obj.now.bm_acc ) ) / m1_sp_err;
else
    Lipschitz = 0;
end


% Use the calculated values to decide
if ( Lipschitz * obj.time_step > 2.0 )

    % Iteration didn't pass, decrease step size:
    obj.time_step               = 0.5 * obj.time_step;
    obj.chk_another             = false;            % another loop == false
    obj.pass_2_next_time_step   = false;

else

    if ( fault_counter > 0)

        % another iteration is needed for this step
        %obj.time_step *= 1.0;
        obj.chk_another             = true;         % another == true
        obj.pass_2_next_time_step   = false;
    else

        if ( Lipschitz * obj.time_step < 0.25 )
            % iteration passed, step size increased
            obj.time_step               = 2 * obj.time_step;
            obj.chk_another             = false;	% another == false. Move on to next time step.
            obj.pass_2_next_time_step   = true;
        else

            % iteration passed, same step size
            %step *= 1.0;
            obj.chk_another             = false;	% another == false. Move on to next time step.
            obj.pass_2_next_time_step   = true;

        end

    end

end



end















