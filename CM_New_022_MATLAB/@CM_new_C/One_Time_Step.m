% 
% 
% function obj = One_Time_Step( obj, step_type )
%
% Description:
%   Implement one step for the current state ( 1 time step ).
%   step_type can be 'euler' or 'trapez'.
%
%

function obj = One_Time_Step( obj, step_type )

if obj.IsNan( obj.prev.bm_disp ) || obj.IsNan( obj.prev.bm_sp ) || obj.IsNan( obj.prev.bm_acc )
    error( 'ERROR - <obj.now.bm_disp> OR <obj.now.bm_sp> OR <obj.now.bm_acc> has NaNs in it !!!\n' )
end
if obj.IsNan( obj.now.bm_disp ) || obj.IsNan( obj.now.bm_sp ) || obj.IsNan( obj.now.bm_acc )
    error( 'ERROR - <obj.now.bm_disp> OR <obj.now.bm_sp> OR <obj.now.bm_acc> has NaNs in it !!!\n' )
end


if strcmpi( obj.ODE_EULER, step_type )

    % BM displacement & speed:
    obj.now.bm_disp     = obj.Euler( obj.prev.bm_disp,  obj.prev.bm_sp );
    obj.now.bm_sp		= obj.Euler( obj.prev.bm_sp,    obj.prev.bm_acc );
    
    % OW - Oval Window:
    if true == obj.ow_flag
        obj.now.ow_disp	= obj.Euler( obj.prev.ow_disp,	obj.prev.ow_sp );
        obj.now.ow_sp 	= obj.Euler( obj.prev.ow_sp, 	obj.prev.ow_acc );
    end     
    
    % OHC:
    obj.now.psi_ohc     = obj.Euler( obj.prev.psi_ohc,  obj.prev.d_psi_ohc );
    
    % TM:
    obj.now.tm_disp     = obj.Euler( obj.prev.tm_disp,  obj.prev.tm_sp );
    
    
else    % For the Trapezoidal Case strcmpi( obj.ODE_TRAPEZ, ode_type )

    % BM displacement & speed
    obj.now.bm_disp     = obj.Trapez( obj.prev.bm_disp, obj.prev.bm_sp,     obj.now.bm_sp );
    obj.now.bm_sp		= obj.Trapez( obj.prev.bm_sp,   obj.prev.bm_acc,	obj.now.bm_acc );
        
    % OW - Oval Window
    if true == obj.ow_flag    
        obj.now.ow_disp	= obj.Trapez( obj.prev.ow_disp,	obj.prev.ow_sp,     obj.now.ow_sp );
        obj.now.ow_sp 	= obj.Trapez( obj.prev.ow_sp, 	obj.prev.ow_acc,    obj.now.ow_acc );
    end
    
    % OHC:
    obj.now.psi_ohc     = obj.Trapez( obj.prev.psi_ohc, obj.prev.d_psi_ohc, obj.now.d_psi_ohc );
    
    % TM:
    obj.now.tm_disp     = obj.Trapez( obj.prev.tm_disp, obj.prev.tm_sp,     obj.now.tm_sp );
    
end

% OHC - psi:
obj.Calc_OHC_Psi_Deriv;    % @P_ohc(x, t)/@t, eta_2 == 0

% OHC - Delta_L displacement:
obj.Calc_DeltaL_disp;
    
% OHC - Delta_L speed:
obj.Calc_DeltaL_sp;

% OHC pressure:
obj.Calc_P_OHC;

% TM speed:
obj.Calc_TM_Sp;                
    
% TM pressure:
obj.Calc_Ptm;

% U*P = Y:
obj.Calc_Y;                 % --> G(x,t)

% Solve for P(x,t)
obj.Solve_TriMat;           % --> P(x,t) ( U * P = Y )

% BM acceleration:
obj.Calc_BM_Acc;
    
% OW acceleration:
obj.Calc_OW_Acc;
   
% % OHC - psi:
% obj.Calc_OHC_Psi_Deriv;    % @P_ohc(x, t)/@t

%
if obj.IsNan( obj.now.bm_disp ) || obj.IsNan( obj.now.bm_sp ) || obj.IsNan( obj.now.bm_acc )
    error( 'ERROR - <obj.now.bm_disp> OR <obj.now.bm_sp> OR <obj.now.bm_acc> has NaNs in it !!!\n' )
end















