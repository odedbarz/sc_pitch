%
%
% function Z = Z_Model_1( obj, x, w )
%
% Description:
%   Calc the impedance of the CLASSIC MODEL (Miriam's\Azi's model).
%
%

function Z = Z_Model_1( obj )

x_SECTIONS = obj.SECTIONS;      % [cm]

w = obj.w;                      % [rad/sec]
f_SECTIONS = obj.f_SECTIONS;    % [sec]

Z = zeros( x_SECTIONS, f_SECTIONS );    % impedance

% I = ones(f_SECTIONS, 1);

% %
% f_osc2 = @(M, R, S, x_i) -1.0*w.^2*M(x_i) + 1j*w*R(x_i) + I*S(x_i);
% f_psi = @(x_i) ( 1j*w.*obj.alpha_2(x_i) + obj.alpha_1(x_i) ) ./ ( 1j*w + obj.w_ohc );
% f_psi = @(x_i) ( 1j*w.*obj.alpha_2(x_i) + obj.alpha_1(x_i) ) ./ ( 1j*w + obj.w_ohc );
f_psi = @(Ix) F_PSI(obj, obj.alpha_2, obj.alpha_1, Ix, w );

for x_i = 1:x_SECTIONS      % Run over the x axis
    %f_bm = f_osc2( obj.M_bm, obj.R_bm, obj.S_bm, x_i );    
    f_bm = F_OSC2(obj.M_bm, obj.R_bm, obj.S_bm, x_i, w );            
    F_xw = f_bm - obj.gamma(x_i).*f_psi(x_i);    
    Z(x_i, :) = F_xw ./ (1j*w);
    
end















