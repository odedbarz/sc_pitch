%
% function Solve( obj )
%
%   Solves the model using modified euler.
%

function Solve( obj )

% Reset All Vectors:
obj.now.Reset;
obj.prev.Reset;
obj.G = zeros( obj.SECTIONS, 1 );  	% G(x,t) from the pressure equation @^2P(x,t)/@t^2 - Q(x)*P(x,t) = G(x,t)
obj.Y = zeros( obj.SECTIONS, 1 ); 	% Y(x,t) from U*P = Y
obj.p = zeros( obj.SECTIONS, 1 );

% Sets the I/O files:
Set_IO_Files_S;
   
obj.chk_another             = true;  	% Recalculates the current time step.
obj.pass_2_next_time_step	= false;	% Clac time step with Euler's method?

if obj.Fs < obj.INIT_TIME_STEP
    err_msg = sprintf( '\nCM_V->Solve:\n\t-> The time steps can be too big for the sampling frequency (obj.Fs < MIN_TIME_STEP) !' );
    error(err_msg);
end
obj.time_step = obj.INIT_TIME_STEP; 	% [sec] Time step 
obj.time      = obj.INIT_TIME_STEP; 	% [sec] Current time of algorithm running

% Counters for the while loops:
step_counter = 0;

% Mainly for the figures:
obj.xxx = obj.dx .* [ 0:obj.SECTIONS-1 ]';          % [cm] Longitudinal axis along the Cochlea.


% ------------------------------------ MAIN WHILE ------------------------------------
% As long as the time is less than required run time keep on with the
% while loop:
while ( obj.time <= obj.run_time )

    step_counter = step_counter +1;               % Advance the while loop's counter.
    do_while_counter = 0;

    obj.One_Time_Step( obj.ODE_EULER );
    
    
    % ------------------------------------ DO-WHILE LOOP------------------------------------
    while ( obj.chk_another )

        do_while_counter = do_while_counter +1;           % Advance the while loop's counter.

        % Get reference to check Vs Trapezoidal:
        bm_now_sp_ref  = obj.now.bm_sp;
        bm_now_acc_ref = obj.now.bm_acc;
        
%         % Trapezoidal step (modified euler):
        obj.One_Time_Step( obj.ODE_TRAPEZ );

        % Test the time step:
        obj.Tester( bm_now_sp_ref, bm_now_acc_ref );

        % Check that the time_step is in its right limits:
        obj.time_step = obj.Bound_Time_Step( obj.time_step );
        
        % DEBUG
        % ------        
        obj.debug.Txt_2_cmd( obj, step_counter, do_while_counter );       % DEBUG
        
    end     % (like the "Trapezoidal" loop)
    % ------------------------------------ DO-WHILE LOOP ENDS ------------------------------------

    % Perform at least one loop in the next iteration:
    obj.chk_another = true;	% Recalculates the current time step.
    
    
    % DEBUG
    % ------
    obj.debug.Plot_Real_Time( obj, step_counter )	
    obj.debug.Txt_2_Release( obj, step_counter );       % RELEASE        
        
    % ---------------------------------    
    % Update States & Save the results:
    % ---------------------------------
    if ( obj.pass_2_next_time_step )
                       
        % Save Results:
        obj.Save_Results( step_counter );
        
        % Advance the total time:
        obj.time = obj.time_step + obj.time;

        % Move to the next state:
        %   1. Copy the current state to the previous state.
        %   2. Clear the now state.
        obj.now = obj.prev.Next_State( obj.now );
                        
    end
    % ---------------------------------    
    
end    
% ------------------------------------ MAIN WHILE ENDS ------------------------------------


% Save Results of the last simulation:
obj.Save_Results( step_counter );

        
% Close Input (Binary) File:
obj.Close_File( obj.input_file_hnd );

% Close Output (Binary) File:
if 1 == obj.save2disk.bm_disp.flag
    obj.Close_File( obj.save2disk.bm_disp.hnd );

end

if 1 == obj.save2disk.bm_sp.flag
    obj.Close_File( obj.save2disk.bm_sp.hnd );
    
end



% 
fprintf( '\n\n-----------------------------------------------------\n' );
fprintf( 'Simulation was finished successfully !\n' );
fprintf( '-----------------------------------------------------\n' );























