% 
% function cell_out = Vector_2_Cell( obj, V, pre_str, post_str )
% 
% Input:
%     pre_str\post_str - (string) Optional input. A string to add before\after each entry
%               of the vector <V>, respectively.
%
% Description:
%     Returns a cell of the values which appears in V (e.g. for legends).
% 
% 


function out_C = Vector_2_Cell( obj, V, pre_str, post_str )

if ~exist( 'pre_str', 'var' )
    pre_str = '';   % Default - Empty string.
else
    pre_str = sprintf( '%s = ', pre_str );  
end

if ~exist( 'post_str', 'var' )
    post_str = '';   % Default - Empty string.
else
    post_str = sprintf( ' %s', post_str );   
end


N_v = length( V );
out_C = cell( N_v, 1);

for i = 1:N_v
    
        out_C{i} = sprintf( '%s%g%s', pre_str, V(i), post_str );
    
end

















