% 
% 
% Read_Gamma_File - Reads the <gamma.bin> file into the object.
% 
% 
% Description:
%   Set the GAMMA filename (OHC).
%
%

function obj = Read_Gamma_File( obj )
gamma_full_filename = obj.Full_File_Name( obj.gamma_filename, obj.path.gamma );

% Check that the INPUT file exists:
if isempty(  dir( obj.Full_File_Name( obj.gamma_filename, obj.path.gamma ) ) )
    error('ERROR - CM_C->CM_C: The given gamma filename <%s> couldn''t be found !!!', gamma_full_filename )
end

gamma_fid = fopen( gamma_full_filename, 'rb');
if 0 == gamma_fid
    error( 'ERROR - CM_C-> Couldn''t open the file <%s> for reading !!!', gamma_full_filename )
end

frewind( gamma_fid );
obj.gamma = fread( gamma_fid, 'double' );

obj.Close_File( gamma_fid );






