%
% function Init_TriMat( obj )
%
% Clac the tridiagonal and assign the result into triMat.
%

function triMat = Init_TriMat( obj )

upper_diag  = ones( obj.SECTIONS-1, 1 );        	% Upper Diagonal.
mid_diag    = -1.0 * ( 2.0 + obj.dx_pow2 * obj.Q );	% Middle Diagonal.
lower_diag  = ones( obj.SECTIONS-1, 1 );         	% Lower Diagonal.

% Boundaries:
mid_diag( 1 )	= -1.0 * ( 1.0 + obj.dx * obj.a0 );  % Starting    
mid_diag( end )	= 1.0;          % Ending

lower_diag( end ) = 0.0;      % Ending


% Build the triagonal matrix:
obj.triMat = diag( lower_diag, -1 );
obj.triMat = obj.triMat + diag( upper_diag, 1 );
obj.triMat = obj.triMat + diag( mid_diag, 0 );

obj.triMat = sparse( obj.triMat );

% Save the inverse matrix:
obj.triMat_inv = inv( obj.triMat );

if nargout >= 1
    triMat = obj.triMat;
end
















