% 
% 
% function [loaded_data, warn_msg] = Load_DB_File(...
%     data_type,...
%     gamma_2_load,...
%     spl_2_load,...
%     freq_2_load,...
%     Fs,...
%     data_path...
%     )
% 

function [loaded_data, warn_msg] = Load_DB_File(...
    data_type,...
    gamma_2_load,...
    spl_2_load,...
    freq_2_load,...
    run_time,...
    ...Fs,...
    data_path...
    )

warn_msg = '';

% Search for the desired file to load:
filename_2_load = sprintf( '%s_gamma(%g)_SPL(%g)_f(%g)kHz_runTime(%g)msec',...
    data_type,...
    gamma_2_load,...
    spl_2_load,...
    1e-3* freq_2_load,...         % [kHz]
    1e3*run_time ...
    ... 1e-3* Fs...
    );

full_filename_2_load = [data_path, filename_2_load, '.mat'];

% Is the file is available?
if isempty(dir( full_filename_2_load ))
    warn_msg = sprintf( '\n\nERROR - Load_DB_File.m:\n\tCouldn''t not read the file:\n\tFilename: %s.mat\n\tPath: %s\n',...
                        filename_2_load,...
                        data_path...
                        );
    loaded_data = [];
    return;
end

%
dummy = load( full_filename_2_load );   % -> M [in units of m,sec] 
loaded_data = dummy.M;






